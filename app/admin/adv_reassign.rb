ActiveAdmin.register_page 'adv_reassign' do
  controller do

    def index
      @ads = reassignable_ads.page(params[:page]).per(params[:limit])
      @results = @ads.total_count
      @ads = @ads.to_json(
        include: { advertiser: { methods: [:full_name] }, url: { methods: [:publisher_email] }, campaign: {} }
      )

      respond_to do |format|
        format.html { render 'admin/layouts/active_admin' }
        format.json { render json: { ads: @ads, results: @results } }
      end
    end

    private

    def reassignable_ads
      Ad.reassignable
    end
  end
end
