ActiveAdmin.register_page 'action_items' do
  controller do

    def index
      @ads = Ad.declined_or_refunded.page(params[:p]).limit(params[:limit])
      @results = @ads.total_count
      @ads = @ads.to_json(include: [:comments, :link, url: { methods: [:publisher_email] }])

      respond_to do |format|
        format.html { render 'admin/layouts/active_admin' }
        format.json { render json: { ads: @ads, results: @results } }
      end
    end
  end
end
