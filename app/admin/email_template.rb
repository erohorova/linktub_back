ActiveAdmin.register EmailTemplate do
  controller do
    helper_method :email_template
    before_action :check_email, only: [:index]

    def index
      @emails = EmailTemplate.where(email_id: params[:email_id]).all
      @emails = @emails.to_json
      respond_to do |format|
        format.html { render 'admin/layouts/active_admin' }
        format.json do
          render json: { email_templates: @emails }
        end
      end
    end

    def create
      @email = EmailTemplate.new(email_params)
      if @email.save
        render json: { email_templates: @email.to_json }
      else
        render json: { errors: @email.errors }, status: :unprocessable_entity
      end
    end

    def show
      render json: { email_template: email_template.to_json }
    end

    def update
      if email_template.update(email_params)
        render json: { email_template: email_template.to_json }
      else
        render json: { errors: email_template.errors }, status: :unprocessable_entity
      end
    end

    def destroy
      email_template.email_actions.destroy_all
      email_template.destroy
      render json: { message: 'Successfuly removed email template' }, status: :ok
    end

    private

    def email_template
      @email_template ||= EmailTemplate.find(params[:id])
    end

    def email_params
      params.require(:email_template).permit(:email_id, :subject, :template, :days_delayed, :active, :collate)
    end

    def check_email
      render json: { error: 'Invalid' }, status: :unprocessable_entity unless params[:email_id].present?
    end
  end
end
