ActiveAdmin.register Article do
  controller do
    before_action :check_authorization, only: %i[update destroy]
    helper_method :email, :phone, :first_name, :last_name

    def index
      @articles = filtered_content.order(created_at: :desc).page(params[:page]).per(params[:limit])
      @results = @articles.total_count
      @authors = Author.confirmed_active.to_json(methods: %i[full_name pending_articles_count])
      @states = Article.states.to_json
      @articles = @articles.to_json(
        include: [{ link: { include: [:campaign] } },
                  :url, :ad, :comments,
                  author: { methods: %i[full_name pending_articles_count] }],
        methods: [:blog_post_doc]
      )

      respond_to do |format|
        format.html { render 'admin/layouts/active_admin' }
        format.json { render json: { articles: @articles, results: @results } }
      end
    end

    def update
      old_author_id = article.author&.id

      article.update article_params

      Email.unschedule('author_article_remind_eta', article.author_id) if article_params[:eta].present?
      create_auto_notes(:link_attributes) if article_params[:link_attributes].present?

      send_assign_email if changed_assigned_author?(old_author_id)

      if article_params[:ad_attributes].present?
        create_auto_notes(:ad_attributes)
        article.complete if article_params[:ad_attributes][:blog_post_doc].present?
      end

      if article.save!
        render json: { article: article.to_json(include: [:ad, :link, :comments], methods: [:blog_post_doc]) }
      else
        render json: { error: 'Error updating the article' }, status: :bad_request
      end

    rescue ActiveRecord::RecordInvalid => error
      render json: { errors: error.message }, status: :unprocessable_entity
    end

    def comment
      comment = article.comments.build comment_params.merge(user: current_admin_user, role: current_admin_user.role)
      return render json: { comment: comment.to_json } if comment.save

      render json: { error: 'Error creating the comment' }, status: :bad_request
    end

    def state
      if Article.state_machine.events.map(&:name).include?(state_sym) && article.try(state_sym)
        article.update!(last_action_applied: state_sym)
        render json: { new_state: article.state }
      else
        render json: { error: 'The new state is not valid' }, status: :unprocessable_entity
      end
    end

    def counter
      render json: { draft_articles_count: Article.draft.count }
    end

    def check_authorization
      return true if article.author_id == current_admin_user.id || current_admin_user.admin?

      render json: { error: 'You can\'t update this article' }, status: :unauthorized
    end

    def destroy
      article.advertiser.user.articles_notifications.last&.destroy!
      article.ad&.order&.cancel
      article.destroy!
      head :no_content
    end

    def bulk_create
      content = Content.find(params[:quote_id])
      adv = content.advertiser
      params[:urls].each do |url|
        ActiveRecord::Base.transaction do
          new_article = adv.articles.create!({
            url_id: url[:id],
            ad_attributes: {
              blog_post_title: '',
              link_text: 'Anchor placeholder',
              url_id: url[:id],
              campaign_id: content.campaign_id
            }
          })
          new_article.link = adv.campaigns.find(content.campaign_id).links.find_or_create_by!(href: 'http://www.placeholderurl.com')
          new_article.ad.link = new_article.link
          new_article.ad.advertiser = adv
          new_article.save!
        end
      end
      head :ok
    rescue => error
      return render json: { errors: error.message }, status: :unprocessable_entity
    end

    private

    def article
      @article ||= Article.find(params[:id])
    end

    def article_params
      params.require(:article).permit(
        :eta, :author_id, ad_attributes: %i[id blog_post_doc blog_post_title full_pub_url link_text], link_attributes: %i[id href]
      )
    end

    def comment_params
      params.require(:article).require(:comment).permit(:comment)
    end

    def changed_assigned_author?(old_author_id)
      old_author_id &&
        article_params[:author_id].present? &&
        (article_params[:author_id] != old_author_id) &&
        (Article.states[article.state] == Article.states[:assigned])
    end

    def filtered_content
      return author_articles unless current_admin_user.admin?

      admin_articles.joins(:ad).where(ads: { ordered_date: nil })
    end

    def admin_articles
      Article.try(scope).containing_text(params[:q])
    end

    def author_articles
      current_admin_user.pending_articles.containing_text(params[:q])
    end

    def scope
      params[:scope].present? && (available_scope? || added_scopes?) ? params[:scope] : 'all_but_rejected'
    end

    def added_scopes?
      Article.respond_to?(params[:scope].to_sym)
    end

    def state_sym
      params[:state]&.to_sym
    end

    def send_assign_email
      email_vars = article.attributes.symbolize_keys
      email_vars[:advertiser_first_name] = article.advertiser.try(:first_name)
      email_vars[:advertiser_last_name] = article.advertiser.try(:last_name)
      email_vars[:author_first_name] = article.author.try(:first_name)
      email_vars[:author_last_name] = article.author.try(:last_name)
      last_message = article.comments&.last
      email_vars[:notes] = "#{last_message&.role}: #{last_message&.comment}"
      Email.schedule('author_article_assigned', article.author.try(:id), email_vars)
    end

    def available_scope?
      Article.state_machine.states.map(&:name).include?(params[:scope].to_sym)
    end

    def create_auto_notes(key)
      mapped_attributes = {
        link_text: 'anchor',
        blog_post_title: 'title',
        blog_post_doc: 'doc',
        full_pub_url: 'full publishers url',
        author_id: 'author',
        href: 'destination url'
      }
      article_params[key].each do |att, _|
        next unless att.to_sym != :id

        comment = att.to_sym == :blog_post_doc ? 'has uploaded doc' : "has changed the #{mapped_attributes[att.to_sym]}"
        article.comments.create!(
          comment: comment,
          user: current_admin_user,
          role: current_admin_user.role
        )
      end
    end
  end
end
