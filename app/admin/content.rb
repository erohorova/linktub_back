ActiveAdmin.register Content do
  controller do
    helper_method :content

    def index
      @quotes = filtered_content.ordered_by_date.page(params[:page]).per(params[:limit])
      @assigned_quotes = Content.received.ordered_by_date.page(params[:page]).per(params[:limit])
      @results = @quotes.total_count
      @assigned_results = @assigned_quotes.total_count
      @quotes = @quotes.to_json(
        include: [{ advertiser: { include: [:user] } },
                  :campaign], methods: [:first_approve_link]
      )
      @assigned_quotes = @assigned_quotes.to_json(
        include: [{ advertiser: { include: [:user] } },
                  :campaign], methods: [:first_approve_link]
      )
      respond_to do |format|
        format.html { render 'admin/layouts/active_admin' }
        format.json do
          render json: { quotes: @quotes, results: @results, assigned_quotes: @assigned_quotes,
                         assigned_results: @assigned_results }
        end
      end
    end

    def new
      render json: { advertisers: Advertiser.active_with_active_campaigns.to_json(methods: [:full_name]),
                     campaigns: Campaign.active, categories: Category.all }
    end

    def create
      @quote = Content.new(content_params.merge(created_by: Content::ADMIN))
      return render json: @quote.errors, status: :unprocessable_entity unless @quote.save
      render json: { content: @quote.to_json(include: [{ advertiser: { include: [:user] } }, :campaign],
                                             methods: [:first_approve_link]) }
    end

    def update
      content.update!(agency_content: params[:agency_content])
      head :no_content
    end

    def destroy
      if content.destroy
        head :no_content
      else
        render json: { error: 'Content not destroyed!' }, status: :unprocessable_entity
      end
    end

    def show
      @categories = Category.all
      @publishers = Publisher.active
      @campaigns = content.advertiser.campaigns.active
      @urls = Url.active
      @content = content.to_json(
        include: [
          :categories,
          :campaign,
          { advertiser: { methods: [:full_name] } },
          { urls: { methods: [:publisher_email] } },
          :content_urls
        ], methods: [:first_approve_link]
      )
      render 'admin/layouts/active_admin'
    end

    def filter
      excluded_urls_ids = Content.active
                                 .where(campaign: content.campaign)
                                 .where.not(id: content.id)
                                 .joins(:urls)
                                 .pluck('urls.id')
      filtered_urls_ids = Url.search(params[:words], params[:keywords], filters_params).pluck(:id)
      @urls = Url.where(id: (filtered_urls_ids - excluded_urls_ids)).limit(Url::MAX_SEARCH)
      if params[:publisher_id].present?
        @urls = @urls.joins(:profile)
                     .joins('INNER JOIN publishers ON publishers.id = profiles.publisher_id')
                     .where("profiles.publisher_id = #{params[:publisher_id]}")
      end
      @urls = @urls.where(id: params[:url_id]) if params[:url_id].present?
      results = []
      @urls.each do |url|
        current_url = url.attributes
        current_url['state'] = url.state
        current_url['publisher_email'] = url.publisher_email
        current_url['already_bought'] = Url.already_bought_for_campaign(url[:id], content.campaign.id)
        current_url['already_quoted'] = Url.already_quoted_for_campaign(url[:id], content.campaign.id)
        results.push(current_url)
      end
      render json: { urls: results.to_json }
    end

    def assign
      params[:urls].find_all do |url|
        url[:campaigns].find_all do |campaign|
          new_content = Content.active.find_or_duplicate(content, campaign, url)
          content_url = ContentUrl.find_by(content_id: content[:id], url_id: url[:id])
          content_url.update!(shown_price: url[:shown_price])
          new_content.receive unless content.received?
        end
      end
      head :no_content
    end

    def order
      params[:urls].find_all do |url|
        article = content.advertiser.articles.create!(url_id: url[:id])
        article.ad = Ad.create!(url_id: url[:id], campaign_id: content.campaign_id, advertiser_id: content.advertiser_id)
        content.advertiser.orders.create!(ad: article.ad, url_id: url[:id], created_from: 'quote')
        content.content_urls.find_or_create_by!(url_id: url[:id], shown_price: url[:shown_price])
        content.done
        article.done
      end
      head :no_content
    end

    def counter
      render json: { new_quotes_count: Content.active.requested.count }
    end

    def report
      render json: { urls_spreadsheet_doc: content.urls_spreadsheet_doc } if content.update_urls_spreadsheet
    end

    def upload
      result = content.bulk_upload_from_document(params[:spreadsheet])
      if result[:status]
        head :ok
      else
        render json: { error: result[:error] }, status: :unprocessable_entity
      end
    end

    private

    def filtered_content
      Content.active.try(scope).containing_text(params[:q])
    end

    def scope
      params[:scope].present? && available_scope? ? params[:scope] : 'requested'
    end

    def available_scope?
      Content.state_machine.states.map(&:name).include?(params[:scope].to_sym)
    end

    def content
      @content ||= Content.find(params[:id])
    end

    def content_params
      params.require(:content).permit(:advertiser_id, :campaign_id, :domain_authority_min,
                                      :domain_authority_max, :ahrefs_dr_min, :ahrefs_dr_max,
                                      :trust_flow_min, :trust_flow_max, :agency_content,
                                      category_ids: [])
    end

    def filters_params
      filters ||= {}
      %w(domain_authority trust_flow ahrefs_dr).each do |search_param|
        filters[search_param] = [params["#{search_param}_min".to_sym].to_i,
                                 params["#{search_param}_max".to_sym].to_i]
      end
      filters
    end
  end
end
