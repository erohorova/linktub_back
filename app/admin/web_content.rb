ActiveAdmin.register WebContent do
  controller do
    helper_method :web_content

    def index
      @web_content = filtered_content.order(state: :asc).page(params[:page]).per(params[:limit])
      @results = @web_content.total_count
      @web_content = @web_content.to_json(include: [{ profile: { include: [{ publisher: { include: [:user] } }] } }])

      respond_to do |format|
        format.html { render 'admin/layouts/active_admin' }
        format.json { render json: { web_content: @web_content, results: @results } }
      end
    end

    def update
      web_content.update web_content_params
      return render json: { web_content: web_content.to_json } if web_content.save!
      render json: { error: 'Error updating the social handle' }, status: :bad_request
    end

    def state
      if WebContent.state_machine.events.map(&:name).include?(state_sym) && web_content.try(state_sym)
        render json: { new_state: web_content.state }
      else
        render json: { error: 'The new state is not valid' }, status: :unprocessable_entity
      end
    end

    def counter
      render json: { new_web_contents_count: WebContent.pending.count }
    end

    private

    def web_content
      @web_content ||= WebContent.find(params[:id])
    end

    def filtered_content
      WebContent.try(scope).containing_text(params[:q])
    end

    def web_content_params
      params.require(:web_content).permit(:price)
    end

    def state_sym
      params[:state]&.to_sym
    end

    def scope
      params[:scope].present? ? params[:scope] : 'all'
    end
  end
end
