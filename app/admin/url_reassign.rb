ActiveAdmin.register_page 'url_reassign' do
  controller do
    helper_method :url

    def index
      @urls = filtered_content.page(params[:page]).per(params[:limit])
      @results = @urls.total_count
      @urls = @urls.to_json(
        include: [{ profile: { include: [:publisher] } }]
      )

      respond_to do |format|
        format.html { render 'admin/layouts/active_admin' }
        format.json { render json: { urls: @urls, results: @results } }
      end
    end

    private

    def url
      @url ||= Url.find(params[:id])
    end

    def filtered_content
      Url.try(scope).containing_text(params[:q])
    end

    def scope
      params[:scope].present? && available_scope? ? params[:scope] : 'pending_and_price_changed'
    end

    def available_scope?
      Url.state_machine.states.map(&:name).include?(params[:scope].to_sym)
    end
  end
end
