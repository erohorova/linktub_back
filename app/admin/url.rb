ActiveAdmin.register Url do
  controller do
    helper_method :url

    def index
      @urls = filtered_content.order(state: :asc).page(params[:page]).per(params[:limit])
      @results = @urls.total_count
      # TODO: Improve this code to avoid querying the DB to get all the results counts
      @pending_results = Url.pending.count
      @changed_price_results = Url.under_review.count
      @paused_results = Url.paused.count
      @urls = @urls.to_json(
        include: [{ profile: { include: [:publisher] } }]
      )

      respond_to do |format|
        format.html { render 'admin/layouts/active_admin' }
        format.json {
          render json: {
            urls: @urls,
            results: @results,
            pending_results: @pending_results,
            changed_price_results: @changed_price_results,
            paused_results: @paused_results
          }
        }
      end
    end

    def update
      url.update url_params
      return render json: { url: url.to_json } if url.save!
      render json: { error: 'Error updating the url' }, status: :bad_request
    end

    def state
      if Url.state_machine.events.map(&:name).include?(state_sym) && url.try(state_sym)
        AhrefService.new.get_dr_lrd([url])
        render json: { new_state: url.state }
      else
        render json: { error: 'The new state is not valid' }, status: :unprocessable_entity
      end
    end

    def comment
      comment = url.comments.build comment_params.merge(user: current_admin_user, role: current_admin_user.role)
      return render json: { comment: comment.to_json } if comment.save
      render json: { error: 'Error creating the comment' }, status: :bad_request
    end

    def counter
      render json: { new_urls_count: Url.pending.count }
    end

    def update_pub
      if Order.on_hold_or_completed_for_url(url.id).any?
        return render json: { errors: 'Can\'t update the URL\'s publisher. It has open orders' }, status: :unprocessable_entity
      else
        Order.cancelled_or_bought_for_url(url.id).destroy_all
        if params[:new_pub_id] && params[:new_profile_id]
          url.publisher = Publisher.find(params[:new_pub_id])
          url.profile = Profile.find(params[:new_profile_id])
        end

        url.price = params[:price]
        url.shown_price = params[:shown_price]
        return head :no_content if url.save!
        render json: { errors: 'There was an error updating the URL' }, status: :unprocessable_entity
      end
    rescue => error
      render json: { errors: error.message }, status: :unprocessable_entity
    end

    private

    def url
      @url ||= Url.find(params[:id])
    end

    def url_params
      params.require(:url).permit(:eta, :shown_price, :description, :follow_link, :allow_blog_post, :allow_home_page)
    end

    def comment_params
      params.require(:url).require(:comment).permit(:comment)
    end

    def filtered_content
      Url.try(scope).containing_text(params[:q])
    end

    def scope
      if params[:scope].present? && (available_scope? || params[:scope] === 'all')
        params[:scope]
      else
        'pending'
      end
    end

    def available_scope?
      Url.state_machine.states.map(&:name).include?(params[:scope].to_sym)
    end

    def state_sym
      params[:state]&.to_sym
    end
  end
end
