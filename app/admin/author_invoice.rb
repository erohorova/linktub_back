ActiveAdmin.register AuthorInvoice do
  controller do
    helper_method :author_invoice

    def index
      @author_invoices = filtered_content.ordered_by_date.page(params[:page]).per(params[:limit])
      @results = @author_invoices.total_count
      @author_invoices = @author_invoices.to_json(include: [:author], methods: [:attached_report])

      respond_to do |format|
        format.html { render 'admin/layouts/active_admin' }
        format.json { render json: { author_invoices: @author_invoices, results: @results } }
      end
    end

    def create
      @invoice = current_admin_user.generate_invoice
      render json: { error: 'Error creating the invoice' },
             status: :unprocessable_entity unless @invoice.present? && @invoice.save
    end

    private

    def author_invoice
      @author_invoice ||= AuthorInvoice.find(params[:id])
    end

    def state_sym
      params[:state]&.to_sym
    end

    def filtered_content
      current_admin_user.admin? ? AuthorInvoice.all : current_admin_user.author_invoices
    end
  end
end
