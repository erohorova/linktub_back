ActiveAdmin.register Author do
  controller do
    helper_method :author

    def index
      @authors = filtered_content.page(params[:page]).per(params[:limit])
      @results = @authors.total_count
      @authors = @authors.to_json(methods: %i[articles_count confirmed])
      respond_to do |format|
        format.html { render 'admin/layouts/active_admin' }
        format.json { render json: { authors: @authors } }
      end
    end

    def update
      author.categories = Category.find(params[:author][:categories].map { |c| c[:id] })
      author.update author_params
      return render json: { author: author.to_json } if author.save!
      render json: { error: 'Error updating the author' }, status: :bad_request
    end

    def state
      if Author.state_machine.events.map(&:name).include?(state_sym) && author.try(state_sym)
        render json: { new_state: author.state }
      else
        render json: { error: 'The new state is not valid' }, status: :unprocessable_entity
      end
    end

    def confirm
      author.confirm
      head :ok
    end

    def create
      @author = AdminUser.new(author_params.merge(type: AdminUser::AUTHOR))
      render json: @author.errors, status: :unprocessable_entity unless @author.save
    end

    def show
      @author = author.to_json(include: [:categories])
      @categories = Category.all.to_json
      render 'admin/layouts/active_admin'
    end

    private

    def author
      @author ||= Author.find(params[:id])
    end

    def filtered_content
      Author.containing_text(params[:q])
    end

    def author_params
      params.require(:author).permit(:email, :paypal_email, :skrill_email, :first_name, :last_name, :phone, :password,
        :password_confirmation, categories: [])
    end

    def state_sym
      params[:state]&.to_sym
    end
  end

  member_action :login_as, method: :post do
    user = Author.find(params[:id])
    sign_in user
    head :ok
  end
end
