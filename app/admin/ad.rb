ActiveAdmin.register Ad do
  controller do
    helper_method :ad

    def index
      @ads = filtered_content.page(params[:page]).per(params[:limit])
      @results = @ads.total_count
      @ads = @ads.to_json(include: [url: { methods: [:publisher_email] }])

      respond_to do |format|
        format.html { render 'admin/layouts/active_admin' }
        format.json { render json: { ads: @ads, results: @results } }
      end
    end

    def state
      case state_sym
      when :approved
        ad.try("#{state_sym}!")
        return render json: { new_state: ad.state }
      when :rejected
        ad.comments.create!(comment_params)
        ad.try("#{state_sym}!")
        return render json: { new_state: ad.state }
      when :pause
        url = ad.url
        (url.active? && url.can_pause?) ? url.pause : url.active
        return render json: { url_state: url.state }
      else
        return render json: { error: 'The new state is not valid' }, status: :unprocessable_entity
      end
    end

    def search
      @ads = search_results.page(params[:page]).per(params[:limit])
      @results = @ads.total_count
      @ads = @ads.to_json(
        include: { advertiser: { methods: [:full_name] }, url: { methods: [:publisher_email] }, campaign: {} }
      )
      render json: { ads: @ads, results: @results }
    end

    def update
      ActiveRecord::Base.transaction do
        ad.advertiser = Advertiser.find(params[:adv_id])
        ad.campaign = Campaign.find(params[:campaign_id])

        if ad.order
          ad.order.advertiser = ad.advertiser
          ad.order.save!
        end

        ad.save!
      end
      head :ok
    rescue => error
      render json: { errors: error.message }, status: :unprocessable_entity
    end

    def seen
      ad.update!(seen: true)
      head :ok
    rescue => error
      render json: { errors: error.message }, status: :unprocessable_entity
    end

    def update_action_items_counter
      render json: { new_action_items_count: Ad.declined_or_refunded.not_seen.count }
    end

    def update_unresponsive_ads_counter
      render json: { new_unresponsive_ads_count: filtered_content.count }
    end

    def remind
      email_template = Email.find_by(slug: 'publisher_live_url_reminder').email_templates.first
      doc_url = ad.article&.blog_post_doc&.dig(:url)
      body_params = {
        first_name: ad.publisher.try(:first_name),
        full_pub_url: ad.full_pub_url,
        notes: ad.notes
      }

      email_params = {
        body: Email.render_template(email_template.template, body_params),
        email: ad.publisher.try(:email),
        subject: Email.render_template(email_template.subject, { first_name: ad.publisher.try(:first_name) }),
        doc_url: doc_url
      }

      QuickMailer.email(email_params).deliver_later
      head :ok
    rescue => error
      render json: { errors: error.message }, status: :unprocessable_entity
    end

    private

    def ad
      @ad ||= Ad.find(params[:id])
    end

    def comment_params
      { comment: params[:notes], user: current_admin_user, role: AdminUser::ADMIN }
    end

    def state_sym
      params[:state]&.to_sym
    end

    def filtered_content
      Ad.pending.unresponsive(Ad::DELAY_WINDOW.ago)
    end

    def search_results
      Ad.reassignable.containing_text(params[:q])
    end
  end
end
