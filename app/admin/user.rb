ActiveAdmin.register User do
  controller do
    helper_method :user

    def update
      user.update! user_params
      head :no_content
    end

    def check_email
      return render json: { email_exists: User.where(email: params[:email]).where.not(id: params[:id]).exists? }
    end

    private

    def user_params
      params.require(:user).permit(
        :first_name, :last_name, :email, :phone, :company, :state,
        :password, :country, :city, :address_line_1, :address_line_2,
        :zip_code, :paypal_email
      )
    end

    def email_param
      params[:user][:email]
    end

    def email_change?
      email_param.present? && user.email != email_param
    end

    def user
      @user ||= User.find(params[:id])
    end
  end
end
