ActiveAdmin.register_page 'adv_orders' do
  controller do

    def index
      @transactions = Transaction.completed.page(1).per(50)
      @results = @transactions.total_count
      @transactions = @transactions.to_json(
        include: {
          payer: { methods: [:full_name] },
          orders: {
            include: {
              advertiser: { methods: [:full_name] },
              url: { include: { publisher: { methods: [:full_name] } } },
              ad: { include: [:campaign, :link], methods: [:publisher_email] }
            }
          }
        }
      )

      respond_to do |format|
        format.html { render 'admin/layouts/active_admin' }
        format.json { render json: { transactions: @transactions, results: @results } }
      end
    end
  end
end
