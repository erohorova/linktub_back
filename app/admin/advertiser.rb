ActiveAdmin.register Advertiser do
  controller do
    helper_method :advertiser

    def index
      @advertisers = filtered_content.page(params[:page]).per(params[:limit])
      @results = @advertisers.total_count
      @advertisers = @advertisers.to_json(include: [:user, :campaigns, :agent], methods: %i[full_name confirmed])

      respond_to do |format|
        format.html { render 'admin/layouts/active_admin' }
        format.json { render json: { advertisers: @advertisers, results: @results } }
      end
    end

    def show
      @messages = advertiser.messages
      @categories = Category.all
      @redirect_url = ENV['BASE_SIGN_IN_URL']
      @active_content = advertiser.active_content.to_json(include: [url: { methods: %i[publisher_email] }])
      @agents = Agent.where.not(id: advertiser.agent_id).active
      @advertiser = advertiser.to_json(
        include: [:user, :agent, campaigns: { include: :categories, methods: %i[first_created_link campaign_report_spreadsheet_doc] }],
        methods: %i[full_name active_content_spreadsheet_doc]
      )
      unschedule_emails
      render 'admin/layouts/active_admin'
    end

    def update
      if Advertiser.safe_transition?(params[:transition]) && advertiser.try(params[:transition])
        if params[:notes].present?
          advertiser.comments.create!(
            comment: params[:notes],
            user: current_admin_user,
            role: 'Admin'
          )
        end
        head :no_content
      else
        render json: { error: 'The new state is not valid' }, status: :unprocessable_entity
      end
    end

    def reassign_agent
      new_agent_id = params[:agent_id].present? ? params[:agent_id] : nil
      advertiser.update(agent_id: new_agent_id)
      head :no_content
    end

    def create_campaign
      @campaign = advertiser.campaigns.build(campaign_params)
      @campaign.toggle_categories(categories_param) if categories_param.present?

      if @campaign.save!
        render json: { campaign: @campaign.to_json(
          include: :categories,
          methods: %i[first_created_link campaign_report_spreadsheet_doc]
        ) }
      else
        render json: { errors: @campaign.errors }, status: :bad_request
      end
    end

    def confirm
      advertiser.confirm
      head :ok
    end

    def filter_and_download
      results = advertisers_without_campaigns
      adv_ids = results&.map(&:id)
      start_date = params[:start_date]&.to_date
      end_date = params[:end_date].to_date + 1.days if params[:end_date].present?
      query = params[:q]
      url = {}
      if start_date.present? && end_date.present?
        results = Advertiser.where(id: adv_ids, created_at: start_date..end_date)
        adv_ids = results&.pluck(:id)
      end

      if params[:download]&.to_bool && results.present?
        advertiser = results.first
        advertiser.update_advertisers_report_spreadsheet(results)
        url = advertiser.advertisers_report_spreadsheet_doc
      end

      if query.present?
        results = Advertiser.joins(:user).where(id: adv_ids)
                            .where('users.first_name LIKE ? OR users.last_name LIKE ? OR advertisers.email LIKE ?',
                            "%#{query}%", "%#{query}%", "%#{query}%")
      end
      count = results.count
      results = results.page(params[:page]).per(params[:limit])
      render json: { chosen_advertisers: results&.to_json(include: %i[user agent]), total_count: count, urls_spreadsheet_doc: url }
    end

    def choose_without_campaigns
      agents = Agent.active
      results = advertisers_without_campaigns
      count = results.count
      results = results.page(params[:page]).per(params[:limit])
      render json: { chosen_advertisers: results&.to_json(include: %i[user agent]), agents: agents, total_count: count }
    end

    def aging_adv_accounts
      results = aging_advertisers
      count = results.count
      results = results.page(params[:page]).per(params[:limit])
      render json: { chosen_advertisers: results&.to_json(include: [:user], methods: [:days_last_order]),
                     total_count: count }
    end

    def new_leads
      start_date = Config.find_by(category: 'start_new_lead_report').value&.to_datetime
      agents = Agent.active
      new_advertisers = Advertiser.where('created_at > ? AND agent_id IS ?', start_date, nil)
                                  .order(created_at: :desc)
      advertisers = new_advertisers.page(params[:page]).per(params[:limit])
      name = `Date of the last viewing of the report New Lead by #{current_admin_user.full_name}`
      config = last_seen_config
      if config.present?
        config.update(value: Time.now.to_s)
      else
        Config.create(category: 'new_lead_reports',
                      name: name,
                      data_type: 'datetime',
                      value: Time.now.to_s,
                      configable_type: 'AdminUser',
                      configable_id: current_admin_user.id)
      end
      results = new_advertisers.count
      render json: { new_accounts: advertisers&.to_json(include: %i[user agent]),
                     agents: agents, results: results }
    end

    def count
      start_report_config = Config.find_by(category: 'start_new_lead_report')
      if start_report_config.present?
        start_date = start_report_config.value&.to_datetime
      else
        name = 'Start date for the report New Lead'
        start_date = Time.now
        Config.create(category: 'start_new_lead_report',
                      name: name,
                      data_type: 'datetime',
                      value: start_date.to_s,
                      configable_type: 'AdminUser',
                      configable_id: current_admin_user.id)
      end
      config = last_seen_config
      search_date = config.present? ? config.value&.to_datetime : start_date
      count = Advertiser.where('created_at > ? AND agent_id IS ?', search_date, nil).count
      render json: { count: count }
    end

    private

    def advertiser
      @advertiser ||= Advertiser.find(params[:id])
    end

    def filtered_content
      advertisers = Advertiser.containing_text(params[:q])
      advertisers = advertisers.by_campaign_id(params[:campaign_id]) if params[:campaign_id].present?
      advertisers
    end

    def advertisers_without_campaigns
      with_not_active_campaign = Advertiser.joins(:campaigns).where('campaigns.state > ?', 1).group(:id).having('COUNT(campaigns.id) = ?', 0).distinct
      without_campaign = Advertiser.joins('LEFT OUTER JOIN campaigns ON campaigns.advertiser_id = advertisers.id').where('campaigns.id IS NULL').where('advertisers.id IS NOT NULL').distinct
      with_not_active_campaign.union(without_campaign)
    end

    def aging_advertisers
      date = Time.now - 60.days
      Advertiser.joins(:invoices).where('advertisers.created_at < ? AND invoices.orders_accepted_count > ? AND invoices.created_at < ?', date, 0, date).where.not('invoices.created_at > ?', date).distinct
    end

    def last_seen_config
      Config.where('category = ? AND configable_type = ? AND configable_id = ?',
                   'new_lead_reports', 'AdminUser', current_admin_user.id).first
    end

    def campaign_params
      params.require(:campaign).permit(:name, :categories_ids, links_attributes: %i(id href))
    end

    def categories_param
      params.dig(:campaign, :categories_ids)
    end

    def unschedule_emails
      Email.unschedule('advertiser_admin_message', JSON.parse(advertiser)['user_id'])
    end
  end
end
