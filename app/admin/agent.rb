ActiveAdmin.register Agent do
  controller do
    helper_method :agent

    def index
      @agents = filtered_content.page(params[:page]).per(params[:limit])
      @results = @agents.total_count
      @advertisers_count = 0
      @agents = @agents.to_json(methods: %i[advertisers_count status_confirmed])
      respond_to do |format|
        format.html { render 'admin/layouts/active_admin' }
        format.json { render json: { agents: @agents, results: @results } }
      end
    end

    def update
      agent.update agent_params
      return render json: { agent: agent.to_json } if agent.save!

      render json: { error: 'Error updating the agent' }, status: :bad_request
    end

    def state
      if Agent.state_machine.events.map(&:name).include?(state_sym) && agent.try(state_sym)
        render json: { new_state: agent.state }
      else
        render json: { error: 'The new state is not valid' }, status: :unprocessable_entity
      end
    end

    def confirm
      agent.confirm
      head :ok
    end

    def create
      @agent = AdminUser.new(agent_params.merge(type: AdminUser::AGENT))
      render json: @agent.errors, status: :unprocessable_entity unless @agent.save
    end

    def show
      @agent = agent.to_json
      render 'admin/layouts/active_admin'
    end

    private

    def agent
      @agent ||= Agent.find(params[:id])
    end

    def filtered_content
      Agent.containing_text(params[:q])
    end

    def agent_params
      params.require(:agent).permit(:email, :paypal_email, :skrill_email, :first_name, :last_name, :phone, :password,
                                     :password_confirmation)
    end

    def state_sym
      params[:state]&.to_sym
    end
  end

  member_action :login_as, method: :post do
    user = Agent.find(params[:id])
    sign_in user
    head :ok
  end
end
