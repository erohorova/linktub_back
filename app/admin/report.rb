ActiveAdmin.register_page 'Reports' do
  controller do
    helper_method :ad, :payout, :payout_total

    # This code is needed to allow actions on register_page
    def authorized?(action, _subject = nil)
      action == :read || PayoutsPolicy.new(current_admin_user, action).try("#{action}?")
    end

    def index
      @ads = filtered_content.page(params[:page]).per(params[:limit])
      @results = @ads.total_count
      @payout_total = payout_total
      @ads = @ads.to_json(include: [:advertiser, :url, :link, :campaign, publisher: {
        methods: [:paypal_email, :skrill_email, :payment_method]
      }])
      respond_to do |format|
        format.html { render 'admin/layouts/active_admin' }
        format.json { render json: { ads: @ads, results: @results, payout_total: @payout_total } }
      end
    end

    def cancel
      if ad.cancel
        render json: { message: 'Successfuly cancelled', payout_total: payout_total }, status: :ok
      else
        render json: { error: 'There was an error in cancelling the payout.' }, status: :unprocessable_entity
      end
    end

    def pay
      response = payout.pay!
      render json: { message: response, payout_total: payout_total }, status: :ok
    rescue StandardError => e
      render json: { error: e.message }, status: :unprocessable_entity
    end

    private
    def ad
      @ad ||= Ad.find(params[:id])
    end

    def filtered_content
      Ad.approved
    end

    def payment_method
      ad.publisher.user&.payment_method
    end

    def payout
      @payment_service ||= payment_method == 'skrill' ? ScrillService.new(ad) : PaypalService.new(ad)
    end

    def payout_total
      @payout_total ||= Ad.approved.inject(0) { |sum, a| sum + a.url.price }
    end

  end
end
