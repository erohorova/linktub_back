ActiveAdmin.register Transaction do
  controller do

    def index
      @transactions = filtered_content.page(params[:page]).per(params[:limit])
      @results = @transactions.total_count
      @transactions = @transactions.to_json(
        include: {
          payer: { methods: [:full_name] },
          orders: {
            include: {
              advertiser: { methods: [:full_name] },
              url: { include: { publisher: { methods: [:full_name] } } },
              ad: { include: [:campaign, :link], methods: [:publisher_email] }
            }
          }
        }
      )

      render json: { transactions: @transactions, results: @results }
    end

    private

    def filtered_content
      if params[:from].present? && params[:to].present?
        Transaction.completed.between_dates(params[:from], params[:to]).containing_text(params[:q])
      else
        Transaction.completed.containing_text(params[:q])
      end
    end
  end
end
