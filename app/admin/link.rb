ActiveAdmin.register Link do
  controller do
    helper_method :link

    def index
      @links = filtered_content.page(params[:page]).per(params[:limit])
      @results = @links.total_count
      @links = @links.to_json(include: [{ campaign: { include: [{ advertiser: { include: [:user] } }] } }])
      respond_to do |format|
        format.html { render 'admin/layouts/active_admin' }
        format.json { render json: { links: @links, results: @results } }
      end
    end

    def state
      if Link.state_machine.events.map(&:name).include?(state_sym) && link.try(state_sym)
        render json: { new_state: link.state }
      else
        render json: { error: 'The new state is not valid' }, status: :unprocessable_entity
      end
    end

    private

    def link
      @link ||= Link.find(params[:id])
    end

    def filtered_content
      Link.new_link.containing_text(params[:q])
    end

    def state_sym
      params[:state]&.to_sym
    end
  end
end
