ActiveAdmin.register AdminUser do
  controller do
    def index
      @admin_users = filtered_content.page(params[:page]).per(params[:limit])
      @results = @admin_users.total_count
      @admin_users = @admin_users.to_json(methods: %i(confirmed))

      respond_to do |format|
        format.html { render 'admin/layouts/active_admin' }
        format.json { render json: { admin_users: @admin_users, results: @results } }
      end
    end

    def create
      @admin_user = AdminUser.new(admin_user_params)
      render json: @admin_user.errors, status: :unprocessable_entity unless @admin_user.save
    end

    def check_email
      return render json: { email_exists: AdminUser.where(email: params[:email]).where.not(id: params[:id]).exists? }
    end

    private

    def admin_user_params
      params.require(:admin_user).permit(:email, :first_name, :phone, :last_name, :password)
    end

    def filtered_content
      AdminUser.just_admins.containing_text(params[:q])
    end
  end
end
