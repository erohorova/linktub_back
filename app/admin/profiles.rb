ActiveAdmin.register Profile do
  controller do
    helper_method :profile

    def update
      profile.categories = Category.find(params[:profile][:categories].map { |c| c[:id] })
      profile.save!
      head :ok
    end

    private

    def profile
      @profile ||= Profile.find(params[:id])
    end
  end
end
