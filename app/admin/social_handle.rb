ActiveAdmin.register SocialHandle do
  controller do
    helper_method :social_handle

    def index
      @social_handles = filtered_content.order(state: :asc).page(params[:page]).per(params[:limit])
      @results = @social_handles.total_count
      @social_handles = @social_handles.to_json(include: [{ profile: { include: [{ publisher: { include: [:user] } }] } }])

      respond_to do |format|
        format.html { render 'admin/layouts/active_admin' }
        format.json { render json: { social_handles: @social_handles, results: @results } }
      end
    end

    def update
      social_handle.update social_handle_params
      return render json: { social_handle: social_handle.to_json } if social_handle.save!
      render json: { error: 'Error updating the social handle' }, status: :bad_request
    end

    def state
      if SocialHandle.state_machine.events.map(&:name).include?(state_sym) && social_handle.try(state_sym)
        render json: { new_state: social_handle.state }
      else
        render json: { error: 'The new state is not valid' }, status: :unprocessable_entity
      end
    end

    def counter
      render json: { new_social_handles_count: SocialHandle.pending.count }
    end

    private

    def social_handle
      @social_handle ||= SocialHandle.find(params[:id])
    end

    def filtered_content
      SocialHandle.try(scope).containing_text(params[:q])
    end

    def social_handle_params
      params.require(:social_handle).permit(:price)
    end

    def state_sym
      params[:state]&.to_sym
    end

    def scope
      params[:scope].present? ? params[:scope] : 'all'
    end
  end
end
