ActiveAdmin.register Publisher do
  controller do
    helper_method :publisher

    def index
      @publishers = filtered_content.page(params[:page]).per(params[:limit])
      @results = @publishers.total_count
      @publishers = @publishers.to_json(include: [:user, :profiles], methods: %i[full_name confirmed])
      respond_to do |format|
        format.html { render 'admin/layouts/active_admin' }
        format.json { render json: { publishers: @publishers, results: @results } }
      end
    end

    def show
      @messages = publisher.messages
      @categories = Category.all
      @redirect_url = ENV['BASE_SIGN_IN_URL']
      @publisher = publisher.to_json(
        include: [:user, profiles: { include: :categories, methods: %i[total_ads] }],
        methods: [:active_content_spreadsheet_doc]
      )
      unschedule_emails
      render 'admin/layouts/active_admin'
    end

    def update
      if Publisher.safe_transition?(params[:transition]) && publisher.try(params[:transition])
        if params[:notes].present?
          publisher.comments.create!(
            comment: params[:notes],
            user: current_admin_user,
            role: 'Admin'
          )
        end
        head :no_content
      else
        render json: { error: 'The new state is not valid' }, status: :unprocessable_entity
      end
    end

    def email_publisher
      publisher.send_quick_email(email_params)
      head(:ok)
    end

    def confirm
      publisher.confirm
      head :ok
    end

    def filter_and_download
      results = publishers_without_profiles
      start_date = params[:start_date]&.to_date
      end_date = params[:end_date].to_date + 1.days if params[:end_date].present?
      query = params[:q]
      pub_ids = results&.map(&:id)
      url = {}
      if start_date.present? && end_date.present?
        results = Publisher.where(id: pub_ids, created_at: start_date..end_date)
        pub_ids = results&.pluck(:id)
      end

      if params[:download]&.to_bool && results.present?
        publisher = results.first
        publisher.update_publishers_report_spreadsheet(results)
        url = publisher.publishers_report_spreadsheet_doc
      end
      if query.present?
        results = Publisher.joins(:user).where(id: pub_ids)
          .where('users.first_name LIKE ? OR users.last_name LIKE ? OR publishers.email LIKE ?',
                 "%#{query}%", "%#{query}%", "%#{query}%")
      end
      count = results.count
      results = results.page(params[:page]).per(params[:limit])
      render json: { chosen_publishers: results&.to_json(include: [:user]), total_count: count, urls_spreadsheet_doc: url }
    end

    def choose_without_profiles
      results = publishers_without_profiles
      count = results.count
      results = results.page(params[:page]).per(params[:limit])
      render json: { chosen_publishers: results&.to_json(include: [:user]), total_count: count }
    end

    private

    def publisher
      @publisher ||= Publisher.find(params[:id])
    end

    def publishers_without_profiles
      without_profile = Publisher.joins('LEFT OUTER JOIN profiles ON profiles.publisher_id = publishers.id
                                  LEFT OUTER JOIN urls ON urls.profile_id = profiles.id')
                                  .where('urls.id IS NULL').where('publishers.id IS NOT NULL').distinct
      with_not_active_profile = Publisher.joins(:urls).where('urls.state > ?', 1)
                                          .group(:id).having('COUNT(urls.*) = ?', 0).distinct
      with_not_active_profile.union(without_profile)
    end

    def email_params
      params.require(:email).permit(:subject, :body, :doc_url)
    end

    def filtered_content
      Publisher.containing_text(params[:q])
    end

    def unschedule_emails
      Email.unschedule('publisher_admin_message', JSON.parse(publisher)['user_id'])
    end
  end
end
