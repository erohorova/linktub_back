ActiveAdmin.register Email do
  controller do
    helper_method :email

    def index
      @emails = filtered_content.page(params[:page]).per(params[:limit])
      @results = @emails.total_count
      @emails = @emails.to_json(except: :notes)
      respond_to do |format|
        format.html { render 'admin/layouts/active_admin' }
        format.json do
          render json: { emails: @emails, results: @results }
        end
      end
    end

    def create
      @email = Email.new(email_params)
      if @email.save
        render json: { email: @email.to_json }
      else
        render json: { errors: @email.errors }, status: :unprocessable_entity
      end
    end

    def show
      @email_templates = email.email_templates.to_json
      @email_vars = Email::EMAIL_TYPES.key?(@email['slug']) ? Email::EMAIL_TYPES[@email['slug']].to_json : nil
      @email = email.to_json
      render 'admin/layouts/active_admin'
    end

    def update
      if email.update(email_params)
        render json: { email: @email.to_json }
      else
        render json: { errors: @email.errors }, status: :unprocessable_entity
      end
    end

    def destroy
      email.destroy
      render json: { message: 'Successfuly removed email' }, status: :ok
    end

    private

    def email
      @email ||= Email.find(params[:id])
    end

    def email_params
      params.require(:email).permit(:slug, :name, :receiver_type, :response_required, :redirect_url, :active, :notes)
    end

    def filtered_content
      Email.containing_text(params[:q]).order(order_param)
    end

    def order_param
      return { id: :asc } if params[:order].blank?
      sort = params[:order].include?('-') ? :desc : :asc
      { "#{params[:order].gsub(/[^0-9a-z_]/i, '')}": sort }
    end
  end
end
