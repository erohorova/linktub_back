ActiveAdmin.register Campaign do
  controller do
    helper_method :campaign

    def index
      @campaigns = filtered_content.page(params[:page]).per(params[:limit])
      @results = @campaigns.total_count
      @categories = Category.all
      @campaigns = @campaigns.to_json(
        include: [{ advertiser: { include: [:user] } }, :categories],
        methods: [:first_created_link]
      )

      respond_to do |format|
        format.html { render 'admin/layouts/active_admin' }
        format.json { render json: { campaigns: @campaigns, results: @results } }
      end
    end

    def update
      campaign.categories = Category.find(params[:campaign][:categories].map { |c| c[:id] })
      campaign.save!
      head :ok
    end

    def state
      if Campaign.state_machine.events.map(&:name).include?(params[:state]&.to_sym) && campaign.try(params[:state]&.to_sym)
        schedule_emails_for_first_campaign
        render json: { new_state: campaign.state }
      else
        render json: { error: 'The new state is not valid' }, status: :unprocessable_entity
      end
    end

    def filter
      @campaigns = Campaign.active
      @campaigns = @campaigns.where(advertiser_id: params[:advertiser_id]) if params[:advertiser_id].present?
      @campaigns = @campaigns.containing_text(params[:q])                  if params[:q].present?
      @campaigns = @campaigns.page(params[:page]).per(params[:limit])

      @campaigns = @campaigns.to_json(
        include: [{ advertiser: { include: [:user] } }, :categories],
        methods: [:first_created_link, :approved_links]
      )
      render json: { campaigns: @campaigns }
    end

    def counter
      render json: { new_campaigns_count: Campaign.pending.count }
    end

    private

    def campaign
      @campaign ||= Campaign.find(params[:id])
    end

    def filtered_content
      Campaign.pending.containing_text(params[:q])
    end

    def schedule_emails_for_first_campaign
      return unless campaign.advertiser.active_campaigns.count == 1
      return unless campaign.state == 'active'
      email_vars = {
        first_name: campaign.advertiser.first_name,
        last_name: campaign.advertiser.last_name,
        email: campaign.advertiser.email
      }
      Email.schedule('advertiser_first_campaign_approved', campaign.advertiser.id, email_vars)
    end
  end
end
