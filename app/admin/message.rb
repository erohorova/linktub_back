ActiveAdmin.register Message do
  controller do
    helper_method :message

    def update
      message.update! message_params
      message.reply
      notify_user
      head :no_content
    end

    private

    def message_params
      params.require(:message).permit(:reply_message)
    end

    def message
      @message ||= Message.find(params[:id])
    end

    def notify_user
      slug = message.user.advertiser? ? 'admin_advertiser_message' : 'admin_publisher_message'
      Email.schedule(slug, message.user_id, { name: message.user.first_name })
    end
  end
end
