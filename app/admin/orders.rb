ActiveAdmin.register Order do
  controller do
    helper_method :order

    def index
      @orders = filtered_content.page(params[:page]).per(params[:limit])
      @results = @orders.total_count
      @orders = @orders.to_json(include: [:advertiser, :url, ad: { include: [:link, :comments] }], methods: [:publisher])
      respond_to do |format|
        format.html { render 'admin/layouts/active_admin' }
        format.json { render json: { orders: @orders, results: @results } }
      end
    end

    def state
      if state_sym == :complete
        order.try(state_sym) if Order.states[order.state] != Order.states[:completed]
        order.update!(completed_by: 'admin')
        render json: { new_state: order.state }
      else
        if Order.state_machine.events.map(&:name).include?(state_sym) && order.try(state_sym)
          render json: { new_state: order.state }
        elsif order.respond_to?(state_sym) && state_sym == :decline
          order.decline(params[:message])
          render json: { new_state: order.state }
        elsif state_sym == :refund
          status = payment_service.transaction_status(order.transaction_order.braintree_transaction_id)
          if status === 'settled'
            order.refund(params[:notes])
            render json: { new_state: order.state }
          else
            render json: { error: 'Can\'t refund a non settled transaction. Please try again later.' }, status: :unprocessable_entity
          end
        else
          render json: { error: 'The new state is not valid' }, status: :unprocessable_entity
        end
      end
    end

    def outdated_orders
      outdated_orders = filtered_old_content&.page(params[:page]).per(params[:limit])
      results = outdated_orders.present? ? outdated_orders.total_count : 0
      outdated_orders = outdated_orders&.to_json(include: [:advertiser, :url, ad: { include: [:link, :comments] }], methods: [:publisher])
      render json: { orders: outdated_orders, results: results }
    end

    def accepted_orders
      results = Invoice.where('orders_accepted_count > ? AND created_at >= ? AND created_at <= ?',
                                                0, params[:start_date], params[:end_date])
      amount = results.sum(:total_charged)
      url = {}
      if params[:download]&.to_bool && results.present?
        order = results.first
        order.update_accepted_orders_report_spreadsheet(results)
        url = order.accepted_orders_report_spreadsheet_doc
      end
      total_count = results.count
      results = results&.page(params[:page]).per(params[:limit])
      render json: { orders: results.to_json(include: [{ advertiser: { include: [:agent] } }]), total_amount: amount,
                     total_count: total_count, urls_spreadsheet_doc: url }
    end

    private

    def order
      @order ||= Order.find(params[:id])
    end

    def payment_service
      PaymentService.new(AdminUser.default_admin_user)
    end

    def filtered_content
      Order.all_but_pending.containing_text(params[:q])
    end

    def filtered_old_content
      delay_date = Time.now - 15.days
      Order.joins(:ad).where('ads.approved_date < ?', delay_date)
        .all_but_pending.containing_text(params[:q])
    end

    def state_sym
      params[:state]&.to_sym
    end
  end
end
