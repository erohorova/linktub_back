# encoding: utf-8

class SocialUrlValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, _value)
    record.errors.add(
      attribute,
      "This appears to be an Invalid #{options[:handler].gsub('Handle', '').capitalize.gsub(/tube/) { $&.capitalize }} Handle. "\
      'Please check the handle closely.'
    ) unless SocialHandle::SOCIALS.include?(record.network.to_sym)
  end
end
