json.admin do
  json.email      @admin_user.email
  json.first_name @admin_user.first_name
  json.last_name  @admin_user.last_name
  json.phone      @admin_user.phone
end
