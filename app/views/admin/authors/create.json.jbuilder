json.author do
  json.created_at @author.created_at
  json.email      @author.email
  json.first_name @author.first_name
  json.last_name  @author.last_name
end
