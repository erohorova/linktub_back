json.agent do
  json.created_at @agent.created_at
  json.email      @agent.email
  json.first_name @agent.first_name
  json.last_name  @agent.last_name
end
