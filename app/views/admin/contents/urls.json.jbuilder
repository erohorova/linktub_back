# Ads
json.urls do
  json.array! @urls do |url|
    json.id               url.id
    json.href             url.href
    json.price            url.price
    json.domain_authority url.domain_authority
    json.trust_flow       url.trust_flow
    json.publisher        url.profile.publisher.email
    json.profile_type     url.profile.profile_type
    json.image            url.profile.image.url
    json.activated_date   url.activated_date
    json.suggested_price  '500'
    json.shown_price      url.shown_price
  end
end
