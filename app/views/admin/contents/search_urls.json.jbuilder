# Urls
json.urls do
  json.array! @urls do |url|
    json.id               url.id
    json.href             url.href
    json.created_at       url.created_at
    json.price            url.price
    json.domain_authority url.domain_authority
    json.trust_flow       url.trust_flow
    json.publisher        url.profile.publisher.email
  end
end
