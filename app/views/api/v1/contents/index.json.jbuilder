# Contents
json.contents do
  json.array! @contents, partial: 'api/v1/shared/content', as: :content
end
