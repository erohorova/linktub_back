json.id                article.id
json.eta               article.eta
json.publisher         article&.url.href
json.price             article.url.shown_price
json.destination_url   article.link&.href
json.state             article.state
json.campaign_name     article&.link&.campaign&.name
if article.ad.present?
  json.ad_id           article.ad.id
  json.title           article.ad.blog_post_title
  json.anchor          article.ad.link_text
  json.blog_post_doc do
    article.ad.blog_post_doc.file ? (json.partial! 'api/v1/shared/media', media: article.ad.blog_post_doc) : nil
  end
  json.campaign_id     article.ad.campaign_id
else
  json.title           nil
  json.anchor          nil
  json.blog_post_doc   nil
end
json.notes do
  json.array! article.comments, partial: 'api/v1/shared/comment', as: :comment
end
