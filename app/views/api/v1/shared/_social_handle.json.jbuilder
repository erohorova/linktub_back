json.id              handle.id
json.url             handle.url
json.price           handle.price
json.state           handle.state
json.rejected_reason handle.comments.last.comment if handle.comments.any? && handle.rejected?
json._destroy 0
