json.id             campaign.id
json.name           campaign.name
json.image          campaign.image.url
json.total_links    campaign.total_links
json.total_urls     campaign.total_urls
json.total_expended campaign.total_expended
json.state          campaign.state
json.categories do
  json.array! campaign.categories, partial: 'api/v1/shared/category', as: :category
end
json.links do
  json.array! campaign.links_desc_updated_at, partial: 'api/v1/shared/link', as: :link
end
