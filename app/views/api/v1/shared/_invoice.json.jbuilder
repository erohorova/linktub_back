json.id             invoice.id
json.total_orders   invoice.orders_count
json.date_purchased invoice.created_at
json.total_accepted invoice.orders_accepted_count
json.total_charged  invoice.total_charged
json.orders do
  json.array! invoice.orders, partial: 'api/v1/shared/order', as: :order
end
