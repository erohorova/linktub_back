json.id              payout.id
json.amount          payout.amount
json.currency        payout.currency
json.paypal_email    payout.paypal_email
json.paypal_batch_id payout.paypal_batch_id
json.created_at      payout.created_at
json.updated_at      payout.updated_at
