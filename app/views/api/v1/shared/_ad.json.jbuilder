json.id               ad.id
json.link_text        ad.link_text
json.blog_post_title  ad.blog_post_title
json.blog_post_body   ad.blog_post_body
json.editable         ad.editable
json.full_pub_url     ad.full_pub_url
json.requested_date   ad.requested_date
json.approved_date    ad.approved_date
json.rejected_reason  ad.rejected_reason
json.state            ad.state
json.rejected_counter ad.rejected_counter
json.word do
  ad.blog_post_doc.file ? (json.partial! 'api/v1/shared/media',
                                         media: ad.blog_post_doc) : json.nil!
end
json.image_1 do
  ad.blog_post_image_1.file ? (json.partial! 'api/v1/shared/media',
                                             media: ad.blog_post_image_1) : json.nil!
end
json.image_2 do
  ad.blog_post_image_2.file ? (json.partial! 'api/v1/shared/media',
                                             media: ad.blog_post_image_2) : json.nil!
end
json.campaign do
  ad.campaign ? (json.partial! 'api/v1/shared/campaign', campaign: ad.campaign) : json.nil!
end
json.link do
  ad.link ? (json.partial! 'api/v1/shared/link', link: ad.link) : json.nil!
end
json.url do
  json.partial! 'api/v1/shared/url', url: ad.url
end
json.notes do
  json.array! ad.comments, partial: 'api/v1/shared/comment', as: :comment
end
json.payout do
  ad.payout ? (json.partial! 'api/v1/shared/payout', payout: ad.payout) : json.nil!
end
