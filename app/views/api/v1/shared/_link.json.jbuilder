json.extract! link, :id, :href, :page_rank, :alexa_rank, :back_links, :domain_age, :state
json.urls do
  json.array! link.urls, partial: 'api/v1/shared/url', as: :url
end
