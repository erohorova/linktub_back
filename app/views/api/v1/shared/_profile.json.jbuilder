json.id           profile.id
json.name         profile.name
json.image        profile.image.url
json.profile_type profile.profile_type
json.total_earned profile.total_earned
json.total_orders profile.total_orders
json.total_sites  profile.total_sites
json.products     profile.products
json.web_content  profile.web_content

json.web_content do
  if profile.web_content
    json.partial! 'api/v1/shared/web_content', web_content: profile.web_content
  else
    json.nil!
  end
end

SocialHandle::SOCIALS.each do |type|
  json.set! "#{type}_handles" do
    json.array! profile.send("#{type}_handles"), partial: 'api/v1/shared/social_handle', as: :handle
  end
end

json.categories do
  json.array! profile.categories, partial: 'api/v1/shared/category', as: :category
end

json.urls do
  json.array! profile.urls, partial: 'api/v1/shared/url', as: :url
end
