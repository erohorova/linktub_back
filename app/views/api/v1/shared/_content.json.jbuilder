json.id                   content.id
json.urls_spreadsheet_doc content.urls_spreadsheet_doc
json.trust_flow_min       content.trust_flow_min
json.trust_flow_max       content.trust_flow_max
json.domain_authority_min content.domain_authority_min
json.domain_authority_max content.domain_authority_max
json.date                 content.created_at
json.agency_content       content.agency_content
json.ahrefs_dr_min        content.ahrefs_dr_min
json.ahrefs_dr_max        content.ahrefs_dr_max
json.campaign do
  json.partial! 'api/v1/shared/campaign', campaign: content.campaign
end
json.categories do
  json.array! content.categories, partial: 'api/v1/shared/category', as: :category
end
json.urls do
  json.array! content.content_urls, partial: 'api/v1/shared/content_url', as: :content_url
end
