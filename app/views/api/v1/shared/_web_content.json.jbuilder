json.id                 web_content.id
json.href               web_content.href
json.price              web_content.price
json.state              web_content.state
json.ga_account_id      web_content.ga_account_id
json.ga_profile_id      web_content.ga_profile_id
json.website_url        web_content.website_url
json.muv        web_content.muv
json.rejected_reason    web_content.comments.last.comment if web_content.comments.any? && web_content.rejected?
json._destroy 0
