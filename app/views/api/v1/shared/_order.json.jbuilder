json.id             order.id
json.state          order.state
json.completed_date order.completed_date
json.profile_name   order.profile_name
json.publisher      order.publisher
json.created_from   order.created_from
json.searched_campaign_id order.searched_campaign_id
json.ad do
  order.ad ? (json.partial! 'api/v1/shared/ad', ad: order.ad) : json.nil!
end
json.url do
  json.partial! 'api/v1/shared/url', url: order.url
end
json.categories do
  json.array! order.profile_categories, partial: 'api/v1/shared/category', as: :category
end
