# Publisher's user info
json.partial! 'api/v1/shared/user', user: publisher.user

# Publisher's profiles
json.profiles do
  json.array! @profiles,
              partial: 'api/v1/shared/profile', as: :profile
end
