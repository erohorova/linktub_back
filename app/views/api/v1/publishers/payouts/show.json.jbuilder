# Payouts
json.total_earnings     @total_earnings
json.projected_earnings @projected_earnings
json.last_month_payouts @last_month_payouts

json.items do
  json.array! @items, partial: 'api/v1/shared/item', as: :item
end
