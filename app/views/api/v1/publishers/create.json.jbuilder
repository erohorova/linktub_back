# Publisher's user info
json.user do
  json.partial! 'api/v1/shared/user', user: @publisher.user
end
