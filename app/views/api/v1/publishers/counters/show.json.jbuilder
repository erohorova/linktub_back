# Publisher main info
json.active_ads                publisher.general_approved_ads.count
json.new_orders                publisher.pending_ads.count
json.pricing_and_pausing       publisher.pricing_and_pausing_urls.count
json.resubmitted_content       publisher.resubmitted_ads.count

json.active_ads_badge          publisher.active_ads_notifications_count
json.new_orders_badge          publisher.new_orders_notifications_count
json.pricing_and_pausing_badge publisher.pricing_and_pausing_urls_notifications_count
json.resubmitted_content_badge publisher.resubmitted_content_notifications_count
