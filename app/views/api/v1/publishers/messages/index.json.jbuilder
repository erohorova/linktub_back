json.messages do
  json.array! @messages, partial: 'api/v1/shared/message', as: :message
end
json.unread_messages_count @unread_count
json.pages                 @pages
json.results               @results
