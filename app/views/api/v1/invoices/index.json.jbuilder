# Invoices
json.invoices do
  json.array! @invoices, partial: 'api/v1/shared/invoice', as: :invoice
end
json.results @total_count
