# Campaigns
json.campaigns do
  json.array! @campaigns, partial: 'api/v1/shared/campaign', as: :campaign
end
