# Notifications
json.notifications do
  json.array! @notifications, partial: 'api/v1/shared/notification', as: :notification
end
