# Credit cards
json.credit_cards do
  json.array! @credit_cards, partial: 'api/v1/shared/credit_card', as: :credit_card
end
