if @result.credit_card_verification
  json.errors [{ message: @result.credit_card_verification.status.humanize,
                 code: @result.credit_card_verification.processor_response_code,
                 details: @result.credit_card_verification.processor_response_text }]
else
  json.errors @result.errors
end
