# Advertiser main info
json.active_content       advertiser.active_content_count
json.awaiting_pubs        advertiser.awaiting_pubs_count
json.urls_approved        advertiser.approved_links_count
json.reuse_content        advertiser.rejected_or_cancelled_content_count
json.new_quotes           advertiser.new_quotes_notifications_count
json.new_articles         advertiser.new_articles_notifications_count
json.new_message          advertiser.new_message_count

json.active_content_badge advertiser.active_content_notifications_count
json.awaiting_pubs_badge  advertiser.awaiting_pubs_notifications_count
json.urls_approved_badge  advertiser.approved_links_notifications_count
json.reuse_content_badge  advertiser.rejected_content_notifications_count
