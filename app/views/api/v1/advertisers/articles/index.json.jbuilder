# Articles
json.articles do
  json.array! @articles, partial: 'api/v1/shared/article', as: :article
end
json.zip @zip
