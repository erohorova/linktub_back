# Advertiser's user info
json.partial! 'api/v1/shared/user', user: advertiser.user

# Advertiser main info
json.active_content advertiser.active_content_count
json.awaiting_pubs  advertiser.awaiting_pubs_count
json.urls_approved  advertiser.approved_links.count
json.reuse_content  advertiser.rejected_content_count

# Advertiser's campaigns
json.campaigns do
  json.array! advertiser.active_campaigns_desc_updated_at.limit(3),
              partial: 'api/v1/shared/campaign', as: :campaign
end
