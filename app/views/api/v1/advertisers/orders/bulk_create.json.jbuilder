# Orders
json.orders do
  json.array! @orders, partial: 'api/v1/shared/order', as: :order
end
