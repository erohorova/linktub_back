# Ads
json.spreadsheet_file @url
json.ads do
  json.array! @ads, partial: 'api/v1/shared/ad', as: :ad
end
