json.user do
  json.partial! 'api/v1/shared/user', user: resource
end

json.token resource.authentication_token
json.roles resource.roles
