# Categories
json.categories do
  json.array! @categories, partial: 'api/v1/shared/category', as: :category
end
