# Urls
json.urls do
  json.array! @urls, partial: 'api/v1/shared/url', as: :url
end
json.pages @pages
json.results @results
