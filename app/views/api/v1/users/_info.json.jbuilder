json.id         user.id
json.name       user.full_name
json.first_name user.first_name
json.last_name  user.last_name
json.email      user.email
json.phone      user.phone
json.company    user.company
