# Links
json.links do
  json.array! @links, partial: 'api/v1/shared/link', as: :link
end
