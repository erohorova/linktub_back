json.orders_built(@orders_built) do |order|
  json.href             order.url.href
  json.anchor           order.link_text
  json.domain_authority order.url.domain_authority
  json.trust_flow       order.url.trust_flow
  json.full_pub_url     order.full_pub_url
  json.shown_price      order.url.shown_price
end
