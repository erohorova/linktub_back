# Profiles
json.profiles do
  json.array! @profiles, partial: 'api/v1/shared/profile', as: :profile
end
