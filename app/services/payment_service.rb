class PaymentService
  REFUNDABLE_STATUSES = ['settled', 'settling'].freeze
  VOIDABLE_STATUSES = ['authorized', 'submitted_for_settlement', 'settlement_pending'].freeze

  def initialize(user)
    @user = user
  end

  def generate_token
    create_customer
    Braintree::ClientToken.generate(customer_id: @user.braintree_customer_id)
  rescue Braintree::NotFoundError
    nil
  end

  def charge_payment_method(token, orders)
    total = orders.map(&:amount).inject(0, &:+)
    result = create_transaction(orders, token, total)

    orders.map do |order|
      order.ad.update!(ordered_date: Time.zone.now)
      email = Email.find_by(slug: 'publisher_order_bought')
      return result unless result.success?
      next unless email

      email_vars = build_email_vars(order, email)
      Email.schedule('publisher_order_bought', order.publisher.try(:user_id), email_vars)
      send_email_with_attachment(order, email, email_vars)
    end
    @user.create_invoice(orders)
  end

  def charge_new_payment_method(nonce, last_four_digits, store, orders)
    payment_method = find_payment_method_by_nonce(nonce, last_four_digits)
    if !payment_method.nil? && payment_method.success?
      charge_payment_method(payment_method.payment_method.token, orders)
      update_payment_method(payment_method.payment_method) if store
    elsif payment_method.nil?
      result = create_payment_method(nonce, store)
      result.success? ? charge_payment_method(result.payment_method.token, orders) : result
    else
      payment_method
    end
  end

  def refund_order(order)
    transaction_id = order.transaction_order.braintree_transaction_id
    status = transaction_status(transaction_id)
    if REFUNDABLE_STATUSES.include? status
      void_or_refund_transaction(true, order, transaction_id)
    elsif VOIDABLE_STATUSES.include? status
      void_or_refund_transaction(false, order, transaction_id)
    else
      true
    end
  end

  def transaction_status(transaction_id)
    find_transaction(transaction_id)&.status
  end

  private

  def build_email_vars(order, email)
    email_vars = order.attributes.symbolize_keys
    email_vars[:advertiser_first_name] = order.advertiser.try(:first_name)
    email_vars[:advertiser_last_name] = order.advertiser.try(:last_name)
    email_vars[:publisher_first_name] = order.publisher.try(:first_name)
    email_vars[:publisher_last_name] = order.publisher.try(:last_name)
    email_vars[:href] = order.ad.url&.href
    email_vars[:redirect_url] = email.try(:redirect_url)
    email_vars
  end

  def create_transaction(orders, token, total)
    result = Braintree::Transaction.sale(amount: total, payment_method_token: token,
                                         options: { submit_for_settlement: false })
    create_internal_transaction(result.transaction.id, orders, total, false) if result.success?
    result
  rescue StandardError => error
    error
  end

  def void_or_refund_transaction(refund, order, transaction_id)
    if refund
      result = Braintree::Transaction.refund(transaction_id, order.amount)
      create_internal_transaction(result.transaction.id, [order], order.amount, true)
    else
      result = Braintree::Transaction.void(transaction_id)
      Transaction.find(transaction_id).update!(voided_at: Time.zone.now)
    end
    result
  rescue StandardError => error
    error
  end

  def find_payment_method_by_nonce(nonce, last_four_digits)
    result = valid_nonce(nonce)
    return if result.nil?

    find_payment_method_by_details(
      create_customer.payment_methods, nonce,
      last_four_digits, result.payment_method_nonce.details[:card_type]
    )
  end

  def find_payment_method_by_details(payment_methods, nonce, last_four, card_type)
    payment_method = payment_methods.find do |payment|
      payment.card_type == card_type &&
        payment.last_4 == last_four
    end
    return if payment_method.nil?

    Braintree::PaymentMethod.update(
      payment_method.token,
      payment_method_nonce: nonce,
      options: {
        verify_card: true
      }
    )
  rescue StandardError
    nil
  end

  def valid_nonce(nonce)
    Braintree::PaymentMethodNonce.find(nonce)
  rescue Braintree::NotFoundError
    nil
  end

  # Should return true or errors
  def create_customer
    result = Braintree::Customer.create(
      first_name: @user.first_name, last_name: @user.last_name,
      email: @user.email, company: @user.company, phone: @user.phone
    )
    if result.success?
      @user.update!(braintree_customer_id: result.customer.id)
      result.customer
    else
      result.errors
    end
  end

  def create_payment_method(payment_nonce, should_store)
    result = Braintree::PaymentMethod.create(
      customer_id: @user.braintree_customer_id,
      payment_method_nonce: payment_nonce,
      options: {
        verify_card: true,
        make_default: true,
        fail_on_duplicate_payment_method: false
      }
    )
    create_internal_credit_card(result.payment_method) if result.success? && should_store
    result
  end

  def update_payment_method(payment_method)
    internal_payment_method = CreditCard.find_by(braintree_token: payment_method.token)
    return if internal_payment_method.nil?

    internal_payment_method.update!(
      user: @user, masked_number: "ending in #{payment_method.last_4}",
      braintree_token: payment_method.token, cardholder_name: payment_method.cardholder_name,
      card_type: payment_method.card_type, image_url: payment_method.image_url,
      expiration_month: payment_method.expiration_month.to_i,
      expiration_year: payment_method.expiration_year.to_i,
      street_address: payment_method.billing_address.street_address,
      extended_address: payment_method.billing_address.extended_address,
      country_name: payment_method.billing_address.country_name,
      zip_code: payment_method.billing_address.postal_code,
      city: payment_method.billing_address.locality,
      state: payment_method.billing_address.region
    )
  end

  def create_internal_credit_card(payment_method)
    CreditCard.create!(
      user: @user, masked_number: "ending in #{payment_method.last_4}",
      braintree_token: payment_method.token, cardholder_name: payment_method.cardholder_name,
      card_type: payment_method.card_type, image_url: payment_method.image_url,
      expiration_month: payment_method.expiration_month.to_i,
      expiration_year: payment_method.expiration_year.to_i,
      street_address: payment_method.billing_address.street_address,
      extended_address: payment_method.billing_address.extended_address,
      country_name: payment_method.billing_address.country_name,
      zip_code: payment_method.billing_address.postal_code,
      city: payment_method.billing_address.locality,
      state: payment_method.billing_address.region
    )
  end

  def create_internal_transaction(id, orders, total, refund)
    new_transaction = Transaction.create!(
      braintree_transaction_id: id, orders: orders,
      state: 'authorized', payer: @user, receiver: nil,
      amount: total, description: nil, is_refund: refund
    )
    orders.map do |order|
      order.update!(transaction_id: new_transaction.id)
    end
    # order.bought
    SubmitForSettlementJob.set(wait: 150.hours).perform_later(id)
    new_transaction
  end

  def find_transaction(transaction_id)
    Braintree::Transaction.find(transaction_id)
  rescue Braintree::NotFoundError
    nil
  end

  def send_email_with_attachment(order, email, email_vars)
    email_template = email.email_templates.first
    doc_url = order.ad.blog_post_doc&.file&.url
    email_params = {
      body: Email.render_template(email_template.template, email_vars),
      email: order.publisher.try(:email),
      subject: Email.render_template(email_template.subject, email_vars),
      doc_url: doc_url
    }

    QuickMailer.email(email_params).deliver_later
  end
end
