class ScrillService
  def initialize(ad)
    @publisher = ad.publisher
    @ad = ad
  end

  def pay!
    response = send_payment

    if response.successful?
      transaction_status = response.status_message
      transation_id = response.transation_id
      payout = Payout.new(
        publisher: @publisher,
        ad: @ad,
        amount: @ad.price.to_s,
        paypal_email: @publisher.user.skrill_email,
        paypal_batch_id: ''
      )
    else
      raise StandardError.new response.error_message unless response.error_message.nil?
    end

    if payout.save!
      @ad.pay
      `Successfuly paid. Transaction status: #{transaction_status}. Transaction ID: #{transation_id}. `
    else
      raise 'Payout was successfuly paid.' \
            'However, there was an error saving the details. ' + payout.errors.to_sentence
    end
  end

  private
  def send_payment
    payment = Skrill::Payment.new do |payee|
      payee.email  = @publisher.user.skrill_email
      payee.amount = @ad.price
    end
    payment.deliver
  end
end
