class SuggestedPriceService
  def suggest_price(href)
    stringified_price = suggest_stringified_price(href)
    stringified_price.slice!(0)
    stringified_price.to_i
  end

  def suggest_stringified_price(href)
    dom_info = SocialsService.new.domain_information(href)
    price(dom_info[:domain_authority], dom_info[:trust_flow])
  end

  def price(dom_auth, trust_flow)
    rows = 9
    tf = round_down(trust_flow)
    da = round_down(dom_auth)

    posX = matrix[0].index(tf.to_s)
    posY = matrix.index { |row| row[0] == da.to_s }

    if da > 90 && tf > 90
      return matrix[rows - 1][rows]
    elsif da > 90
      return matrix[rows - 1][posX]
    elsif tf > 90
      return matrix[posY][rows]
    elsif da < 20 || tf < 10
      return '$0'
    end

    matrix[posX][posY]
  end

  def round_down(value)
    (value / 10) * 10
  end

  def matrix
    [
      ['  ', '10', '20', '30', '40', '50', '60', '70', '80', '90'],
      ['20', '$30', '$50', '$70', '$90', '$110', '$125', '$150', '$175', '$200'],
      ['30', '$50', '$70', '$90', '$110', '$125', '$150', '$175', '$200', '$250'],
      ['40', '$70', '$90', '$110', '$125', '$150', '$175', '$200', '$250', '$300'],
      ['50', '$90', '$110', '$125', '$150', '$175', '$200', '$250', '$300', '$350'],
      ['60', '$110', '$125', '$150', '$175', '$200', '$250', '$300', '$350', '$400'],
      ['70', '$125', '$150', '$175', '$200', '$250', '$300', '$350', '$400', '$450'],
      ['80', '$150', '$175', '$200', '$250', '$300', '$350', '$400', '$450', '$500'],
      ['90', '$175', '$200', '$250', '$300', '$350', '$400', '$450', '$500', '$600']
    ]
  end
end
