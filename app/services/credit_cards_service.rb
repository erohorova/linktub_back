class CreditCardsService
  def initialize(user)
    @user = user
  end

  def create_payment_method(payment_nonce)
    result = Braintree::PaymentMethod.create(
      customer_id: create_customer_id,
      payment_method_nonce: payment_nonce,
      options: {
        verify_card: true,
        make_default: true,
        fail_on_duplicate_payment_method: false
      }
    )
    create_internal_credit_card(result.payment_method) if result.success?
    result
  end

  def update_payment_method(payment_nonce, token)
    result = Braintree::PaymentMethod.update(
      token,
      payment_method_nonce: payment_nonce,
      options: {
        verify_card: true
      }
    )
    update_internal_credit_card(result.payment_method) if result.success?
    result
  end

  private

  def create_internal_credit_card(payment_method)
    billing_address = payment_method.billing_address
    CreditCard.create!(
      user: @user, masked_number: "ending in #{payment_method.last_4}",
      braintree_token: payment_method.token, cardholder_name: payment_method.cardholder_name,
      card_type: payment_method.card_type, image_url: payment_method.image_url,
      expiration_month: payment_method.expiration_month.to_i,
      expiration_year: payment_method.expiration_year.to_i,
      street_address: billing_address.try(:street_address),
      extended_address: billing_address.try(:extended_address),
      country_name: billing_address.try(:country_name),
      zip_code: billing_address.try(:postal_code),
      city: billing_address.try(:locality),
      state: billing_address.try(:region)
    )
  end

  def update_internal_credit_card(payment_method)
    billing_address = payment_method.billing_address
    CreditCard.find_by(braintree_token: payment_method.token).update!(
      user: @user, masked_number: "ending in #{payment_method.last_4}",
      braintree_token: payment_method.token, cardholder_name: payment_method.cardholder_name,
      card_type: payment_method.card_type, image_url: payment_method.image_url,
      expiration_month: payment_method.expiration_month.to_i,
      expiration_year: payment_method.expiration_year.to_i,
      street_address: billing_address.try(:street_address),
      extended_address: billing_address.try(:extended_address),
      country_name: billing_address.try(:country_name),
      zip_code: billing_address.try(:postal_code),
      city: billing_address.try(:locality),
      state: billing_address.try(:region)
    )
  end

  def create_customer_id
    result = Braintree::Customer.create(
      first_name: @user.first_name,
      last_name: @user.last_name,
      email: @user.email,
      company: @user.company,
      phone: @user.phone
    )
    @user.update!(braintree_customer_id: result.customer.id) if result.success?
    @user.braintree_customer_id
  end
end
