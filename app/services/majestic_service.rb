require 'uri/http'
require 'net/http'
require 'net/https'
require 'json'

class MajesticService
  def get_data(href)
    href = "http://#{href}" unless href.start_with?('http://', 'https://')
    url = "#{ENV['MAJESTIC_ENDPOINT']}?app_api_key=#{ENV['MAJESTIC_API_KEY']}&cmd=GetIndexItemInfo&items=1&item0=#{url_domain(href)}"
    uri = URI.parse(url)

    https = Net::HTTP.new(uri.host, uri.port)
    https.use_ssl = true

    req = Net::HTTP::Get.new(uri.to_s)
    res = https.request(req)
    parsed_res = JSON.parse(res.body)

    if parsed_res['Code'] == 'OK'
      info = parsed_res['DataTables']['Results']['Data'][0]
      { trust_flow: info['TrustFlow'], citation_flow: info['CitationFlow'] }
    else
      notify_failure(parsed_res, false)
    end
  rescue Timeout::Error, Errno::EINVAL, Errno::ECONNRESET, EOFError,
       Net::HTTPBadResponse, Net::HTTPHeaderSyntaxError, Net::ProtocolError => e
    notify_failure(e, true)
    { trust_flow: -1, citation_flow: -1 }
  end

  def url_domain(url)
    uri = URI.parse(url)
    domain = PublicSuffix.parse(uri.host)
    domain.domain
  end

  def notify_failure(error, rescued)
    email_vars = { service: 'Majestic' }
    if rescued
      email_vars[:error_code] = error.class.to_s
      email_vars[:error_message] = error.to_s
    else
      email_vars[:error_code] = error['Code']
      email_vars[:error_message] = error['ErrorMessage']
    end

    Email.schedule('admin_api_fail_message', AdminUser.first.try(:id), email_vars)
  end
end
