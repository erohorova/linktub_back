class SpreadsheetService
  MAX_LENGTH = 31

  def self.create(file_name)
    file_name = file_name.first(MAX_LENGTH).strip if file_name.length >= MAX_LENGTH
    package = Axlsx::Package.new
    workbook = package.workbook
    workbook.add_worksheet name: file_name do |sheet|
      yield sheet
    end
    StringIO.new(package.to_stream.read)
  end
end
