class AhrefService
  require 'open-uri'
  require 'json'

  def initialize
    @key = Rails.application.secrets.ahrefs_token
  end

  def get_dr_lrd(urls)
    urls.each do |url|
      href = url.href

      refdomains = "https://apiv2.ahrefs.com/?token=#{@key}&target=#{href}&limit=1&output=json&from=refdomains&mode=domain"
      domain_rat = "https://apiv2.ahrefs.com?token=#{@key}&target=#{href}&limit=100&output=json&from=domain_rating&mode=domain"

      refdomains_data = open(refdomains).read
      refdomains_info = JSON.parse(refdomains_data)
      lrd = refdomains_info['stats']['refdomains'] if refdomains_info.present?

      domain_rating_data = open(domain_rat).read
      rating = JSON.parse(domain_rating_data)
      dr = rating['domain']['domain_rating'] if rating.present?

      url.update(ahrefs_dr: dr) if rating['error'].blank? && url.ahrefs_dr != dr
      url.update(ahrefs_lrd: lrd) if refdomains_info['error'].blank? && url.ahrefs_lrd != lrd
    end
  end
end
