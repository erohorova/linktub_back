class PaypalService
  API_PAYOUT_ENDPOINT = "#{ENV['PAYPAL_API_URL']}/v1/payments/payouts".freeze
  API_TOKEN_ENDPOINT  = "#{ENV['PAYPAL_API_URL']}/v1/oauth2/token".freeze
  FAILED = 'failed'.freeze
  SUCCESS = 'success'.freeze

  def initialize(ad)
    @publisher = ad.publisher
    @ad = ad
  end

  def pay!
    response = send_payment

    if response[:status] == FAILED
      refresh_access_token
      response = send_payment

      raise StandardError.new response[:error] if response[:status] == FAILED
    end

    payout = Payout.new(
      publisher: @publisher,
      ad: @ad,
      amount: @ad.price.to_s,
      paypal_email: @publisher.paypal_email,
      paypal_batch_id: response.dig(:body, :batch_header, :payout_batch_id)
    )

    if payout.save!
      @ad.pay
      "Successfuly paid. Payout batch id: #{payout.paypal_batch_id}"
    else
      raise "Payout was successfuly paid. Payout batch id: #{payout.paypal_batch_id}. " \
            'However, there was an error saving the details. ' + payout.errors.to_sentence
    end
  end

  private

  def payload
    {
      sender_batch_header: {
        sender_batch_id: SecureRandom.hex(8),
        email_subject: 'You have a Payout!'
      },
      items: [
        {
          recipient_type: 'EMAIL',
          amount: {
            value: @ad.price.to_s,
            currency: 'USD'
          },
          note: 'Thanks for using Scalefluence!',
          sender_item_id: @ad.id,
          receiver: @publisher.paypal_email
        }
      ]
    }.to_json
  end

  def send_payment
    begin
      response = RestClient::Request.execute(
        method: :post,
        url: API_PAYOUT_ENDPOINT,
        payload: payload,
        headers: {
          'Authorization' => "Bearer #{access_token}",
          'Content-Type' => 'application/json'
        }
      )
    rescue RestClient::Unauthorized, RestClient::Forbidden => e
      result = { status: FAILED, body: JSON.parse(e.response.body) }
    rescue RestClient::UnprocessableEntity => e
      result = { status: FAILED, error: JSON.parse(e.response.body)["message"] }
    else
      result = { status: SUCCESS, body: JSON.parse(response.body) }
    end
    result.with_indifferent_access
  end

  def access_token
    @access_token ||= get_access_token[:access_token]
  end

  def refresh_access_token
    @access_token = get_access_token[:access_token]
  end

  def get_access_token
    begin
      response = RestClient::Request.execute(
        method: :post,
        url: API_TOKEN_ENDPOINT,
        user: ENV['PAYPAL_CLIENT_ID'],
        password: ENV['PAYPAL_CLIENT_SECRET'],
        timeout: 10,
        headers: { Accept: 'application/json', params: { grant_type: 'client_credentials' } }
      )
    rescue RestClient::Unauthorized, RestClient::Forbidden => e
      result = { status: 'failed', message: e.response, access_token: nil }
    else
      json = JSON.parse(response.body)
      result = { status: 'success', access_token: json['access_token'] }
    end
    result.with_indifferent_access
  end
end
