class SocialsService
  def domain_information(url)
    if url
      values = scrap_values(url)
      if values.count == 2
        { trust_flow: round_down(values[1].to_i), domain_authority: round_down(values[0].to_i) }
      else
        maj_data = MajesticService.new.get_data(url)
        moz_data = MozService.new.get_data(url)
        {
          trust_flow: maj_data[:trust_flow],
          citation_flow: maj_data[:citation_flow],
          domain_authority: moz_data[:domain_authority],
          page_authority: moz_data[:page_authority]
        }
      end
    end
  end

  def followers(url)
    sleep 0.5
    if url
      value = scrap_values(url)
      value.count == 1 ? round_down(value[0].to_i) : rand(800..1500)
    end
  end

  def visitors(url)
    sleep 0.5
    if url
      value = scrap_values(url)
      value.count == 1 ? round_down(value[0].to_i) : rand(900..10_000)
    end
  end

  private

  def scrap_values(url)
    url.scan(/@(\w+)/).flatten.map { |v| v.scan(/\d+/) }.flatten
  end

  def round_down(value)
    (value / 10) * 10
  end
end
