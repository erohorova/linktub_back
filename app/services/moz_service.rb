class MozService
  def initialize
    @client = Linkscape::Client.new(accessID: ENV['MOZ_ACCESS_ID'], secret: ENV['MOZ_SECRET_KEY'])
  end

  def get_data(href)
    resp = @client.urlMetrics(href, cols: :all)
    if resp.response.code == "200"
      resp.data
    else
      notify_failure(resp.response)
    end
  end

  def notify_failure(error)
    email_vars = {
      service: 'Moz',
      error_code: error.code,
      error_message: error.message
    }

    Email.schedule('admin_api_fail_message', AdminUser.first.try(:id), email_vars)
  end
end
