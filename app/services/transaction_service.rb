class TransactionService
  def initialize(user)
    @user = user
  end

  def token
    payment_service.generate_token
  end

  def payment_token(token, orders_ids)
    orders = Order.where(id: orders_ids)
    payment_service.charge_payment_method(token, orders)
  end

  def payment_nonce(nonce, last_four, store, orders_ids)
    orders = Order.where(id: orders_ids)
    payment_service.charge_new_payment_method(nonce, last_four, store, orders)
  end

  def refund(order)
    payment_service.refund_order(order)
  end

  private

  def payment_service
    PaymentService.new(@user)
  end
end
