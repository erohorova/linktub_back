class AdvsNoPurchaseJob < ActiveJob::Base
  queue_as :default

  def perform(adv_id)
    adv = Advertiser.find(adv_id)
    if adv.transactions.completed.between_dates(Advertiser::NO_PURCHASE_TIME.ago.to_s, Time.zone.now.to_s).count == 0
      email_vars = adv.attributes.symbolize_keys
      email_vars[:first_name] = adv.try(:first_name)
      Email.schedule('advertiser_no_purchase', adv.user_id, email_vars)
    end
  end
end
