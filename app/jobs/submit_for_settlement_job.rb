class SubmitForSettlementJob < ActiveJob::Base
  queue_as :default

  def perform(braintree_transaction_id)
    transaction = Transaction.find_by(braintree_transaction_id: braintree_transaction_id)
    if transaction.ready? && transaction.amount.to_i.positive?
      result = Braintree::Transaction.submit_for_settlement(braintree_transaction_id, transaction.amount_at_the_moment)
      transaction.submitted if result.success?
      result
    end
  rescue StandardError => error
    nil
  end
end
