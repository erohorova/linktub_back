(function () {
  'use strict';

  angular
    .module('linktub')
    .component('appRemainingTime', appRemainingTime());

  /* @ngInject */
  function appRemainingTime() {
    var component = {
      templateUrl: 'remaining.html',
      controller: RemainingController,
      controllerAs: 'ctrl',
      bindings: {
        startedAt: '<',
        endingIn: '<?'
      }
    };

    return component;
  }

  RemainingController.$inject = ['$interval'];

  /* @ngInject */
  function RemainingController($interval) {
    var vm = this;

    vm.promise;
    vm.time = {};
    vm.finish = false;

    vm.remainingMinutes = remainingMinutes;
    vm.remainingHours = remainingHours;
    vm.isFinishing = isFinishing;
    vm.endingDate = endingDate;

    vm.$onInit = function () {
      vm.endingIn = vm.endingIn || 120;
      updateClock();
    }

    function updateClock() {
      getTimeRemaining(moment(vm.startedAt).add(vm.endingIn, 'hours'));
      stopClock();
      vm.promise = $interval(function () {
        getTimeRemaining(moment(vm.startedAt).add(vm.endingIn, 'hours'))
      }, 1000);
    }

    function stopClock() {
      if (vm.promise) {
        $interval.cancel(vm.promise);
      }
    }

    function remainingMinutes() {
      return (vm.endingIn * 60) - moment(moment()).diff(moment(vm.startedAt), 'minutes');
    }

    function remainingHours() {
      return vm.endingIn - moment(moment()).diff(moment(vm.startedAt), 'hours');
    }

    function isFinishing() {
      return remainingMinutes() < 60;
    }

    function endingDate() {
      return moment(vm.startedAt).add(vm.endingIn, 'hours').calendar()
    }

    function getTimeRemaining(endtime) {
      var t = Date.parse(endtime) - Date.parse(new Date());
      if (t <= 0) {
        stopClock();
        vm.finish = true;
      } else {
        vm.time.seconds = Math.floor((t / 1000) % 60);
        vm.time.minutes = Math.floor((t / 1000 / 60) % 60);
      }
    }
  }
})();
