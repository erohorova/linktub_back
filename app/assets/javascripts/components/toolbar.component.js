(function () {
  'use strict';

  angular
    .module('linktub')
    .component('appToolbar', appToolbar());

  /* @ngInject */
  function appToolbar() {
    var component = {
      templateUrl: 'toolbar.html',
      controller: ToolbarController,
      controllerAs: 'ctrl',
      bindings: {
        userId: '<',
        name: '@'
      }
    };

    return component;
  }

  ToolbarController.$inject = ['$mdSidenav', '$window', '$mdMedia', 'SessionService'];

  /* @ngInject */
  function ToolbarController($mdSidenav, $window, $mdMedia, SessionService) {
    var vm = this;

    // Accessible functions
    vm.toggleMenu = toggleMenu;
    vm.smallScreen = smallScreen;
    vm.bigScreen = bigScreen;
    vm.openMenu = openMenu;
    vm.signOut = signOut;

    // Functions
    function toggleMenu() {
      $mdSidenav('left').toggle();
    }

    function smallScreen() {
      return !$mdMedia('gt-md');
    }

    function bigScreen() {
      return $mdMedia('gt-md');
    }

    function openMenu($mdOpenMenu, ev) {
      $mdOpenMenu(ev);
    }

    function signOut() {
      SessionService.signOut(function () {
        $window.location.href = '/admin/login';
      });
    }
  }
})();