(function () {
  'use strict';

  angular
    .module('linktub')
    .component('appSidenav', appSidenav());

  /* @ngInject */
  function appSidenav() {
    var component = {
      templateUrl: 'sidenav.html',
      controller: SidenavController,
      controllerAs: 'ctrl',
      bindings: {
        adminRole: '@',
        userId: '@'
      }
    };

    return component;
  }

  SidenavController.$inject = ['ContentService', 'UrlsService', 'WebContentService', 'SocialHandlesService', 'ArticlesService', 'AdsService', 'CampaignService'];

  /* @ngInject */
  function SidenavController(ContentService, UrlsService, WebContentService, SocialHandlesService, ArticlesService, AdsService, CampaignService) {
    var vm = this;

    // Attributes
    vm.counter = 0;
    vm.seoCounter = 0;
    vm.articlesCounter = 0;
    vm.webContentCounter = 0;
    vm.socialHandleCounter = 0;
    vm.actionItemsCounter = 0;
    vm.unresponsiveAdsCounter = 0;
    vm.newCampaignsCounter = 0;

    // Accessible functions
    vm.isAdmin = isAdmin;
    vm.isAuthor = isAuthor;

    // Functions
    vm.$onInit = function () {
      ContentService.updateCounter(function (response) {
        vm.counter = response.data.new_quotes_count;
      });
      ArticlesService.updateCounter(function (response) {
        vm.articlesCounter = response.data.draft_articles_count;
      });
      UrlsService.updateCounter(function (response) {
        vm.seoCounter = response.data.new_urls_count;
      });
      WebContentService.updateCounter(function (response) {
        vm.webContentCounter = response.data.new_web_contents_count;
      });
      SocialHandlesService.updateCounter(function (response) {
        vm.socialHandleCounter = response.data.new_social_handles_count;
      });
      AdsService.updateActionItemsCounter(function (response) {
        vm.actionItemsCounter = response.data.new_action_items_count;
      });
      AdsService.updateUnresponsiveCounter(function (response) {
        vm.unresponsiveAdsCounter = response.data.new_unresponsive_ads_count;
      });
      CampaignService.updateCounter(function(response) {
        vm.newCampaignsCounter = response.data.new_campaigns_count;
      });
    }

    function isAuthor() {
      return vm.adminRole === 'author';
    }

    function isAdmin() {
      return vm.adminRole === 'admin';
    }
  }
})();
