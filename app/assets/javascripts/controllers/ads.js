(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('AdsController', AdsController);

  AdsController.$inject = ['$log', '$mdDialog', '$window', 'toastr', 'AdsService', 'ADS_PAYLOADS', 'AD_STATES'];

  /* @ngInject */
  function AdsController($log, $mdDialog, $window, toastr, AdsService, ADS_PAYLOADS, AD_STATES) {
    var vm = this;

    vm.ads = [];
    vm.results = 0;
    vm.query = {
      filter: '',
      order: 'ordered_date',
      limit: 10,
      page: 1,
      search: false
    };

    vm.remainingTime = remainingTime;
    vm.sendEmail = sendEmail;
    vm.handleActionSelection = handleActionSelection;
    vm.togglePause = togglePause;
    vm.getAds = getAds;
    vm.search = search;
    vm.remindPublisher = remindPublisher;

    activate();

    function activate() {
      instantiateAds();
    }

    function instantiateAds() {
      if (angular.isDefined(window.ads)) {
        vm.ads = angular.fromJson(window.ads);
        setupActions();
      }

      if (angular.isDefined(window.results)) {
        vm.results = parseInt(window.results);
      }
    }

    function setupActions() {
      // TODO: Improve this code
      _.forEach(vm.ads, function (ad) {
        ad.actions = [{
          label: 'Approve',
          payload: ADS_PAYLOADS.APPROVE
        }, {
          label: 'Decline',
          payload: ADS_PAYLOADS.DECLINE
        }, {
          label: 'Download',
          payload: ADS_PAYLOADS.DOWNLOAD
        }];
      });
    }

    function getAds(page, limit) {
      vm.query.page = page;
      vm.query.limit = limit;
      vm.search();
    }

    function search() {
      AdsService.indexAds(vm.query, function (response) {
        var data = response.data;
        vm.ads = angular.fromJson(data.ads);
        vm.results = data.results;
        setupActions();
      });
    }

    function handleActionSelection(ev, ad, payload) {
      switch (payload) {
      case ADS_PAYLOADS.APPROVE:
        AdsService.approveAd(ad.id, function (response) {
          search();
        }, function (err) {
          $log.error(err);
        });
        break;
      case ADS_PAYLOADS.DECLINE:
        var confirm = $mdDialog.prompt()
          .title('Are you sure?')
          .placeholder('Please give the reason why')
          .targetEvent(ev)
          .ariaLabel('decline')
          .ok('Decline')
          .cancel('Cancel');

        $mdDialog.show(confirm).then(function (note) {
          AdsService.declineAd(ad.id, note, function (response) {
            search();
          }, function (err) {
            $log.error(err);
          });
        }, function () {
          // Cancelled
        });
        break;
      case ADS_PAYLOADS.DOWNLOAD:
        $window.open(ad.blog_post_doc.url, '_blank');
        break;
      case ADS_PAYLOADS.EMAIL_PUB:
        toastr.warning('Not implemented yet!');
        // TODO: This code should be included in next PR
        // $mdDialog.show({
        //   controller: 'EmailController',
        //   controllerAs: 'ctrl',
        //   templateUrl: 'emailDialog.html',
        //   targetEvent: ev,
        //   clickOutsideToClose: false
        // });
        break;
      case ADS_PAYLOADS.REFUND:
        showConfirmDialog(ev, ad, {
          title: 'Refund Order?',
          content: 'Are you sure you want to decline this ad?. This will refund the advertiser and remove payout to publisher. ' +
            '<br> The content will download automatically. Be sure to save the content.',
          aria: 'refunding'
        }, function (result) {
          OrdersService.refundOrder(ad.id, function (response) {
            $window.location.href = '/admin/ads';
          }, function (err) {
            $log.error(err);
          });
        });
        break;
      }
      ad.action = null;
    }

    function remainingTime(date) {
      return 120 - moment().diff(moment(date), 'hours')
    }

    function sendEmail(ev) {
      toastr.warning('Not implemented yet!');
      // TODO: This code should be included in next PR
      // $mdDialog.show({
      //   controller: 'EmailController',
      //   controllerAs: 'ctrl',
      //   templateUrl: 'emailDialog.html',
      //   targetEvent: ev,
      //   clickOutsideToClose: false
      // });
    }

    function togglePause(ev, ad) {
      var newState = ad.url.state;

      var confirm = $mdDialog.confirm()
        .title('Are you sure?')
        .targetEvent(ev)
        .ariaLabel('Pause URL')
        .ok('OK')
        .cancel('Cancel');

      if (newState === AD_STATES.ACTIVE) {
        confirm = confirm
          .textContent('You are going to unpause this URL and the Publisher would be able to monetize it!')
      } else {
        confirm = confirm
          .textContent('You are going to pause this URL and the Publisher would not be able to monetize it!')
      }

      $mdDialog.show(confirm).then(function () {
        AdsService.pauseAd(ad.id, function (response) {
          toastr.success('Updated successfuly');
        }, function (err) {
          $log.error(err);
        });
      }, function () {
        ad.url.state = newState === AD_STATES.ACTIVE ? AD_STATES.PAUSED : AD_STATES.ACTIVE;
      });

    }

    function remindPublisher(ad) {
      AdsService.remindPublisher(ad.id, function (response) {
        toastr.success('The publisher was reminded.');
      }, function () {
        toastr.error('An error ocurred, try again later.')
      });
    }
  }
})();
