(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('ArticlesController', ArticlesController);

  ArticlesController.$inject = ['$log', '$scope', '$filter', '$mdEditDialog', '$mdDialog', '$window', 'toastr', 'ArticlesService', 'ArticleActionsService', 'ARTICLE_STATES', 'ARTICLE_PAYLOADS', 'ARTICLES_COPY_ATTRIBUTES'];

  /* @ngInject */
  function ArticlesController($log, $scope, $filter, $mdEditDialog, $mdDialog, $window, toastr, ArticlesService, ArticleActionsService, ARTICLE_STATES, ARTICLE_PAYLOADS, ARTICLES_COPY_ATTRIBUTES) {
    var vm = this;

    vm.query = {
      filter: '',
      order: 'order',
      limit: 10,
      page: 1,
      search: false,
      scope: 'all'
    };
    vm.minEtaDate = new Date();
    vm.showProgressBar = false;
    vm.selectedRows = [];

    vm.canChangeAuthor = canChangeAuthor;
    vm.editDestinationURL = editDestinationURL;
    vm.editTitle = editTitle;
    vm.editETA = editETA;
    vm.showNotes = showNotes;
    vm.uploadFile = uploadFile;
    vm.editAnchor = editAnchor;
    vm.noFileUploaded = noFileUploaded;
    vm.downloadDocument = downloadDocument;
    vm.showOnlineDocument = showOnlineDocument;
    vm.querySearchAuthors = querySearchAuthors;
    vm.selectedAuthorLabel = selectedAuthorLabel;
    vm.getArticles = getArticles;
    vm.optionSelected = optionSelected;
    vm.search = search;
    vm.modifyScope = modifyScope;
    vm.getStateLabel = getStateLabel;
    vm.authorSelected = authorSelected;
    vm.getBlogPostDocName = getBlogPostDocName;
    vm.getStateClass = getStateClass;
    vm.deleteContent = deleteContent;
    vm.showArticle = showArticle;
    vm.placeholderStyle = placeholderStyle;
    vm.selectRow = selectRow;
    vm.getRowClass = getRowClass;
    vm.copyAuthor = copyAuthor;
    vm.copyAction = copyAction;
    vm.copyAttribute = copyAttribute;

    activate();

    function activate() {
      instantiateArticles();
      instantiateAuthors();
      instantiateStates();
      listenClick();
    }

    function listenClick() {
      $(window).click(function (ev) {
        var nodeName = $(ev.toElement).prop('nodeName').toLowerCase();
        if(nodeName !== 'td' && nodeName !== 'button') {
          vm.selectedRows = [];
          $scope.$apply();
        }
      });
    }

    function setupActions() {
      _.forEach(vm.articles, function (article) {
        article.eta = article.eta ? article.eta + ' 12:00:00 PM' : null;
        article.actions = getActions(article);
        article.order = ArticleActionsService.getOrderLetter(article.state);
        article.actionPlaceholder = articleActionsPlaceholder(article.last_action_applied);
      });
    }

    function daysFromDate(date) {
      var now = moment();
      var target = moment(date).set({
                    'hour': now.get('hour'),
                    'minute': now.get('minute'),
                    'second': now.get('second') + 1
                   });
      return now.diff(target, 'days');
    }

    function getStateLabel(article) {
      switch (article.state) {
      case ARTICLE_STATES.COMPLETED:
        if (article.rewritten_counter > 0) {
          return 'Rewrite ' + article.rewritten_counter;
        }
        break;
      case ARTICLE_STATES.ASSIGNED:
        if (article.eta && daysFromDate(article.eta) >= 0) {
          return 'Past ETA ';
        }
        break;
      case ARTICLE_STATES.DONE:
        return 'In Cart';
      }
      return $filter('capitalize')($filter('humanize')(article.state));
    }

    function getStateClass(article) {
      switch (article.state) {
      case ARTICLE_STATES.COMPLETED:
        if (article.rewritten_counter > 0) {
          return 'rewritten';
        }
        break;
      case ARTICLE_STATES.ASSIGNED:
        if (daysFromDate(article.eta) >= 0) {
          return 'past-eta';
        }
        break;
      }
      return article.state;
    }

    function getBlogPostDocName(article) {
      return article && article.blog_post_doc && article.blog_post_doc.name || 'Not set yet'
    }

    function instantiateArticles() {
      if (angular.isDefined(window.articles)) {
        vm.articles = angular.fromJson(window.articles);
        setupActions();
      }

      if (angular.isDefined(window.results)) {
        vm.results = parseInt(window.results);
      }
    }

    function instantiateStates() {
      vm.states = [{
          label: 'All',
          payload: ARTICLE_STATES.ALL
        },
        {
          label: 'Draft',
          payload: ARTICLE_STATES.DRAFT
        },
        {
          label: 'Removed from Cart',
          payload: 'removed_from_cart'
        },
        {
          label: 'Past ETA',
          payload: ARTICLE_STATES.PAST_ETA
        }
      ]
    }

    function modifyScope() {
      vm.query.page = 1;
      vm.search();
    }

    function getActions(article) {
      if (window.isAdmin) {
        return ArticleActionsService.defineActionsForAdmins(article.state);
      } else {
        return ArticleActionsService.defineActionsForAuthors(article.state);
      }
    }

    function optionSelected(ev, article, form) {
      // Clean the erors
      form.$setPristine();
      form.action.$setValidity('file', true);
      form.action.$setValidity('unable', true);
      form.action.$setValidity('eta', true);

      executeAction(form, article, ev);
    }

    function actionIsExecutableForArticle(article, action) {
      return action.payload !== ARTICLE_PAYLOADS.REJECT_BY_ADMIN && article.actions && article.actions.find(function (a) {
        return a.payload === action.payload;
      });
    }

    function getOrdinal(n) {
      var suffix = ['th', 'st', 'nd', 'rd'],
          value = n % 100;
      return suffix[(value - 20) % 10] || suffix[value] || suffix[0];
    }

    function executeAction(form, article, ev) {
      // Cast the action
      article.action = angular.fromJson(article.action);
      var canContinue = false;

      // Filter actions
      switch (article.action.payload) {
      case ARTICLE_PAYLOADS.ASSIGN:
        canContinue = true;
        if (!article.author) {
          canContinue = false;
          if(form) {
            form.action.$setValidity('unable', false);
          }
        }
        break;
      case ARTICLE_PAYLOADS.APPROVE:
        canContinue = true;
        if (vm.noFileUploaded(article)) {
          canContinue = false;
          if(form) {
            form.action.$setValidity('file', false);
          }
        }
        break;
      case ARTICLE_PAYLOADS.COMPLETE:
        if (!vm.noFileUploaded(article)) {
          canContinue = true;
        } else if (form) {
          form.action.$setValidity('file', !vm.noFileUploaded(article));
        }
        break;
      case ARTICLE_PAYLOADS.REJECT_BY_ADMIN:
        $mdDialog.show({
          controller: 'NotesController',
          controllerAs: 'ctrl',
          templateUrl: 'notesDialog.html',
          targetEvent: ev,
          clickOutsideToClose: false,
          locals: {
            article: article
          }
        }).then(function (response) {
          performAction(article);
        }, function () {
          $log.log('Dialog dismissed');
        });
        break;
      case ARTICLE_PAYLOADS.REJECT_BY_AUTHOR:
      case ARTICLE_PAYLOADS.CANCEL:
        canContinue = true;
        break;
      default:
        canContinue = false;
      }

      if (canContinue) {
        performAction(article);
      }
    }

    function performAction(article) {
      ArticleActionsService.performSelectedAction(article, function () {
        if (article.action.payload === ARTICLE_PAYLOADS.COMPLETE) {
          vm.showProgressBar = true;
          setTimeout(function () {
            vm.showProgressBar = false;
            vm.search();
          }, 3000);
        } else {
          vm.search();
        }
      });
    }

    function instantiateAuthors() {
      if (angular.isDefined(window.authors)) {
        vm.authors = angular.fromJson(window.authors);
      }
    }

    function getArticles(page, limit) {
      vm.query.page = page;
      vm.query.limit = limit;
      vm.search();
    }

    function search() {
      ArticlesService.indexArticle(vm.query, function (response) {
        var data = response.data;
        vm.articles = angular.fromJson(data.articles);
        vm.results = data.results;
        setupActions();
      });
    }

    function editField(ev, article, config, saveCallback, promiseCallback) {
      $mdEditDialog.large({
        modelValue: config.model,
        placeholder: config.placeholder,
        save: saveCallback,
        targetEvent: ev,
        title: config.title,
        validators: config.validators,
        messages: config.messages
      }).then(promiseCallback);
    }

    function editTitle(ev, article) {
      editField(ev, article, {
        model: article.ad.blog_post_title,
        placeholder: 'Blog post title',
        title: 'Blog Post Title',
        validators: {
          'aria-label': 'Edit title',
          'ng-required': 'true'
        },
        messages: {
          required: 'You must supply a title.'
        }
      }, function (input) {
        updateAd(article, article.ad.link_text, input.$modelValue, 0);
      });
    }

    function editAnchor(ev, article) {
      editField(ev, article, {
        model: article.ad.link_text,
        placeholder: 'Anchor text',
        title: 'Anchor Text',
        validators: {
          'aria-label': 'Edit anchor',
          'ng-required': 'true'
        },
        messages: {
          required: 'You must supply an anchor.'
        }
      }, function (input) {
        updateAd(article, input.$modelValue, article.ad.blog_post_title, 0);
      });
    }

    function editDestinationURL(ev, article) {
      editField(ev, article, {
        model: article.ad.full_pub_url,
        placeholder: 'http://destination.com',
        title: 'Destination URL',
        validators: {
          'aria-label': 'Edit url',
          'ng-required': 'true'
        },
        messages: {
          required: 'You must supply a destination URL.',
          url: 'This is not a valid URL.'
        }
      }, function (input) {
        updateUrl(article, input.$modelValue.replace(/ /g, ''), 0);
      }, function (ctrl) {
        var input = ctrl.getInput();
        input.$viewChangeListeners.push(function () {
          var regexTest = new RegExp('(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?')
            .test(input.$modelValue);
          input.$setValidity('url', regexTest);
        });
      });
    }

    function editETA(article) {
      ArticlesService.updateArticle(article.id, {
        eta: article.eta
      });
    }

    function authorSelected(article) {
      if (article.author) {
        ArticlesService.updateAuthor(article.id, {
          author_id: article.author.id
        });
      }
    }

    function showNotes(ev, article) {
      $mdDialog.show({
        controller: 'NotesController',
        controllerAs: 'ctrl',
        templateUrl: 'notesDialog.html',
        targetEvent: ev,
        clickOutsideToClose: false,
        locals: {
          article: article
        }
      }).then(function (response) {
        product.canEdit = response.canEdit;
      }, function () {
        // Cancelled, added this callback to prevent warning
        // TODO: Improve this callback
      });
    }

    function noFileUploaded(article) {
      return article.ad.blog_post_doc === null || article.blog_post_doc === null;
    }

    function showConfirmCancel(ev) {
      var confirm = $mdDialog.confirm()
        .title('Are you sure you want to cancel this article?')
        .textContent('The article is going to be cancelled.')
        .ariaLabel('Cancel article')
        .targetEvent(ev)
        .ok('Yes')
        .cancel('No');
      return $mdDialog.show(confirm);
    }

    function showConfirmSend(ev) {
      var confirm = $mdDialog.confirm()
        .title('Are you sure you want to send this article?')
        .textContent('The article is going to be sent.')
        .ariaLabel('Sending article')
        .targetEvent(ev)
        .ok('Yes')
        .cancel('No');
      return $mdDialog.show(confirm);
    }

    function uploadFile(ev, article) {
      $mdDialog.show({
        controller: 'UploadController',
        controllerAs: 'ctrl',
        templateUrl: 'uploadDialog.html',
        targetEvent: ev,
        clickOutsideToClose: false
      }).then(function (response) {
        ArticlesService.upadateFile(article.id, {
          id: article.ad.id,
          file: response
        }, function (response) {
          $window.location.reload();
        }, function (err) {
          $log.error(err);
          toastr.error(err.data.error + '. Please refresh the screen.', 'Can\'t update article');
        });
      }, function () {
        // Cancelled, added this callback to prevent warning
        // TODO: Improve this callback
      });
    }

    function showOnlineDocument(article) {
      $window.open("https://docs.google.com/viewer?url=" + article.blog_post_doc.url, '_blank');
    }

    function downloadDocument(article) {
      $window.open(article.blog_post_doc.url, '_blank');
    }

    function querySearchAuthors(query) {
      var results = query ? vm.authors.filter(createFilterForAuthor(query)) : vm.authors;
      return results;
    }

    function createFilterForAuthor(query) {
      var lowercaseQuery = angular.lowercase(query);
      return function filterFn(author) {
        return (angular.lowercase(author.full_name).indexOf(lowercaseQuery) >= 0);
      };
    }

    function selectedAuthorLabel(author) {
      return author.full_name + ' (' + author.pending_articles_count + ')';
    }

    function deleteContent(ev, article) {
      var confirm = $mdDialog.confirm()
        .title('Are you sure you want to delete this article?')
        .textContent('The article is going to be permanentely deleted.')
        .ariaLabel('Deleting article')
        .targetEvent(ev)
        .ok('Yes')
        .cancel('No');
      $mdDialog.show(confirm).then(function () {
        if (article.blog_post_doc) {
          $window.open(article.blog_post_doc.url, '_blank');
        }

        ArticlesService.deleteArticle(article.id, function (response) {
          // Temporal state to hide row from table until the page is refreshed.
          // 'deleted' is not an actual state in the model.
          article.state = 'deleted';
          toastr.success('Successfuly deleted article');
        }, function(error) {
          $log.error(error)
          toastr.error('There was an error trying to delete the article. ' +
          ' Please try again later.', 'Can\'t delete article');
        });
      }, function () {
        // Dialog dismissed.
      });
    }

    function showArticle(state) {
      // Hide articles after they were deleted and the page hasn't been refreshed.
      return state !== 'deleted';
    }

    function placeholderStyle(value) {
      return value == 'http://www.placeholderurl.com' || value == 'Anchor placeholder' ? 'placeholder' : '';
    }

    function selectRow(ev, index, article) {
      if (ev.button === 0) {
        if (ev.shiftKey) {
          var min = vm.selectedRows.length > 0 ? lowestRow().row : 0;
          var visibleArticles = getVisibleArticles();
          vm.selectedRows = [];
          for (var i = min; i <= index; i++) {
            vm.selectedRows.push(newRowObject(i, visibleArticles[i]));
          }
        } else if (ev.ctrlKey || ev.metaKey) {
          if (rowSelected(index)) {
            vm.selectedRows.splice(indexOfRow(index), 1);
          } else {
            vm.selectedRows.push(newRowObject(index, article));
          }
        }

        if (!ev.ctrlKey && !ev.metaKey && !ev.shiftKey) {
          vm.selectedRows = rowSelected(index) ? [] : [newRowObject(index, article)];
        }
      }
    }

    // Returns new object containing the index of the selected row
    // and the article that corresponds to it.
    function newRowObject(index, article) {
      return { row: index, article: article };
    }

    // Returns the element with lowest index row.
    function lowestRow() {
      return vm.selectedRows.reduce(function (min, art) {
        return art.row < min.row ? art : min
      }, vm.selectedRows[0]);
    }

    // Returns true if the value of the parameter exists in the list.
    function rowSelected(index) {
      return vm.selectedRows.filter(function (row) {
        return row.row === index;
      }).length > 0;
    }

    // Returns the position of the element with a given index in the
    // selectedRows list.
    function indexOfRow(index) {
      return vm.selectedRows.findIndex(function (row) {
        return row.row === index;
      });
    }

    // Returns class depending if it's selected or not.
    function getRowClass(index, state) {
      return rowSelected(index) ? 'selected' : state;
    }

    // Sets the author of the first selected row to every other selected row.
    function copyAuthor() {
      if (vm.selectedRows.length > 1) {
        var firstRow = lowestRow();
        if (firstRow.article.author) {
          var author = firstRow.article.author;
          var index = firstRow.row;
          _.forEach(vm.selectedRows, function (row) {
            if (row.row !== index) {
              ArticlesService.updateAuthor(row.article.id, {
                author_id: author.id
              });
              row.article.author = author;
            }
          });
        } else {
          toastr.error('', 'No author assigned in the upmost row.');
        }
      } else {
        toastr.error('', 'Select at least two rows.');
      }
    }

    // Returns only the visible articles.
    function getVisibleArticles() {
      return vm.articles.filter(function (article) {
        return showArticle(article.state);
      });
    }

    // Returns true only when author can be changed
    function canChangeAuthor(article) {
      return (article.state !== ARTICLE_STATES.DONE) && (article.state !== ARTICLE_STATES.APPROVED)
    }

    function articleActionsPlaceholder(last_action_applied) {
      return last_action_applied ? ArticleActionsService.getLabelForPayload(last_action_applied) : 'Please select';
    }

    function copyAction() {
      if (vm.selectedRows.length > 1) {
        var firstRow = lowestRow(),
            action   = { payload: firstRow.article.last_action_applied };

        if (action.payload) {
          _.forEach(vm.selectedRows, function (row, index) {
            if (firstRow.article.id !== row.article.id) {
              if (actionIsExecutableForArticle(row.article, action)) {
                row.article.action = angular.toJson(action);
                executeAction(null, row.article, null);
              } else {
                toastr.error('', 'Can\'t execute action for the ' + (index +1).toString() + getOrdinal(index + 1) + ' article');
              }
            }
          });
        } else {
          toastr.error('', 'No action assigned in the upmost row.');
        }
      } else {
        toastr.error('', 'Select at least two rows.');
      }
    }

    function copyAnchor() {
      if (vm.selectedRows.length > 1) {
        var firstRow = lowestRow(),
            baseArticle  = firstRow.article;

        if (baseArticle.ad.link_text) {
          _.forEach(vm.selectedRows, function (row, index) {
            var article = row.article;
            if (article.id !== baseArticle.id) {
              ArticlesService.updateArticleAd(article.id, {
                id: article.ad.id,
                anchor: baseArticle.ad.link_text
              }, function (response) {
                var data = angular.fromJson(response.data.article);
                article.ad.link_text = data.ad.link_text;
                article.comments = data.comments;
              }, function (error) {
                toastr.error('', 'Error while updating anchor for ' + (index +1).toString() + getOrdinal(index + 1) + ' article');
              });
            }
          });
        } else {
          toastr.error('', 'No anchor assigned in the upmost row.');
        }
      } else {
        toastr.error('', 'Select at least two rows.');
      }
    }

    function copyAttribute(attribute) {
      if (vm.selectedRows.length < 2) {
        return toastr.error('', 'Select at least two rows.');
      }

      var firstRow = lowestRow(),
          baseArticle  = firstRow.article,
          attrValue;

      switch (attribute) {
        case ARTICLES_COPY_ATTRIBUTES.ANCHOR:
          attrValue = baseArticle.ad.link_text;
          break;
        case ARTICLES_COPY_ATTRIBUTES.URL:
          attrValue = baseArticle.link.href;
          break;
        case ARTICLES_COPY_ATTRIBUTES.TITLE:
          attrValue = baseArticle.ad.blog_post_title;
          break;
      }

      if (!attrValue) {
        return toastr.error('', 'Can\'t copy empty value.');
      }

      _.forEach(vm.selectedRows, function (row, index) {
        var article = row.article;
        if (article.id !== baseArticle.id) {
          switch (attribute) {
            case ARTICLES_COPY_ATTRIBUTES.ANCHOR:
              updateAd(article, attrValue, article.ad.blog_post_title, index + 1);
              break;
            case ARTICLES_COPY_ATTRIBUTES.URL:
              updateUrl(article, attrValue, index + 1);
              break;
            case ARTICLES_COPY_ATTRIBUTES.TITLE:
              updateAd(article, article.ad.link_text, attrValue, index + 1);
              break;
          }
        }
      });
    }

    function updateAd(article, newAnchor, newTitle, position) {
      ArticlesService.updateArticleAd(article.id, {
        id: article.ad.id,
        anchor: newAnchor,
        title: newTitle
      }, function (response) {
        var data = angular.fromJson(response.data.article);
        article.ad.link_text = data.ad.link_text;
        article.comments = data.comments;
        article.ad.blog_post_title = data.ad.blog_post_title;
      }, function (error) {
        if (position > 0) {
          toastr.error('', 'Error while updating ' + position.toString() + getOrdinal(position) + ' article');
        } else {
          $log.error(error);
        }
      });
    }

    function updateUrl(article, newUrl, position) {
      article.ad.full_pub_url = newUrl;
      ArticlesService.updateArticleLink(article.id, {
        id: article.link.id,
        target: newUrl
      }, function (response) {
        var data = angular.fromJson(response.data.article);
        article.link.href = data.link.href;
        article.comments = data.comments;
      }, function (error) {
        if(position > 0) {
          toastr.error('', 'Error while updating ' + position.toString() + getOrdinal(position) + ' article');
        } else {
          toastr.error('', 'Error while updating article. ' + error.data.errors);
          $log.error(error);
        }
      });
    }
  }
})();
