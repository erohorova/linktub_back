(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('NotesController', NotesController);

  NotesController.$inject = ['$log', '$mdDialog', 'ArticlesService', 'article'];

  /* @ngInject */
  function NotesController($log, $mdDialog, ArticlesService, article) {
    var vm = this;

    activate();

    function activate() {
      vm.notes = article.comments;
    }

    vm.hide = function () {
      $mdDialog.hide();
    }

    vm.cancel = function () {
      $mdDialog.cancel();
    }

    vm.submit = function (message, form) {
      ArticlesService.createNote(article.id, {
        message: vm.newMessage
      }, function (response) {
        vm.notes.push(angular.fromJson(response.data.comment));
        vm.newMessage = '';
        form.$setPristine();
        form.$setUntouched();
        $mdDialog.hide();
      }, function (error) {
        $log.error(error);
      });
    }
  }
})();
