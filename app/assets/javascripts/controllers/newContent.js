(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('NewContentController', NewContentController);

  NewContentController.$inject = ['$log', '$q', '$mdDialog', '$scope', '$timeout', 'orderByFilter', 'toastr', 'ContentService', 'AdvertisersService', 'CampaignService'];

  /* @ngInject */
  function NewContentController($log, $q, $mdDialog, $scope, $timeout, orderBy, toastr, ContentService, AdvertisersService, CampaignService) {
    var vm = this;

    vm.quote = {
      domain_authority_min: 10,
      domain_authority_max: 100,
      trust_flow_min: 10,
      trust_flow_max: 100,
      ahrefs_dr_min: 0,
      ahrefs_dr_max: 100,
      categories: []
    };
    vm.advertisers = [];
    vm.campaigns = [];
    vm.domainAuthority = {
      options: {
        floor: 10,
        ceil: 100,
        step: 10,
        minRange: 10,
        noSwitching: true,
        hideLimitLabels: true,
        hidePointerLabels: true,
        showTicksValues: true
      }
    };
    vm.trustFlow = {
      options: {
        floor: 10,
        ceil: 100,
        step: 10,
        minRange: 10,
        noSwitching: true,
        hideLimitLabels: true,
        hidePointerLabels: true,
        showTicksValues: true
      }
    };
    vm.ahrefsDr = {
      options: {
        floor: 0,
        ceil: 100,
        step: 10,
        minRange: 10,
        noSwitching: true,
        hideLimitLabels: true,
        hidePointerLabels: true,
        showTicksValues: true
      }
    };

    vm.querySearchAdvertisers = querySearchAdvertisers;
    vm.querySearchCampaign = querySearchCampaign;
    vm.filterByAdvertiser = filterByAdvertiser;
    vm.queryCategories = queryCategories;
    vm.campaignSelected = campaignSelected;

    activate();

    function activate() {
      instantiateAdvertisers();
      refreshSlider();
    }

    // Functions
    vm.cancel = function () {
      $mdDialog.cancel();
    }

    vm.submit = function () {
      var reqBody = {
        advertiser_id: vm.advertiser.id,
        campaign_id: vm.campaign.id,
        category_ids: _.map(vm.campaign.categories, 'id'),
        domain_authority_min: vm.quote.domain_authority_min,
        domain_authority_max: vm.quote.domain_authority_max,
        trust_flow_min: vm.quote.trust_flow_min,
        trust_flow_max: vm.quote.trust_flow_max,
        ahrefs_dr_min: vm.quote.ahrefs_dr_min,
        ahrefs_dr_max: vm.quote.ahrefs_dr_max
      }
      ContentService.createContent(reqBody, function (response) {
        $mdDialog.hide(response);
        toastr.success('Quote successfuly created', 'Success!');
      }, function (error) {
        $log.error(error);
      });
    }

    function refreshSlider() {
      $timeout(function () {
        $scope.$broadcast('rzSliderForceRender');
      }, 900);
    };

    function instantiateAdvertisers() {
      ContentService.newQuote(function (results) {
        var data = results.data;
        vm.advertisers = angular.fromJson(data.advertisers);
        vm.campaigns = data.campaigns;
        vm.categories = data.categories;
      });
    }

    function queryCategories(query) {
      var categoriesFiltered = vm.categories.filter(createFilterForCategories(query));
      return categoriesFiltered;
    }

    function createFilterForCategories(query) {
      var uppercaseQuery = query.toUpperCase();
      return function filterFn(category) {
        return category.name.indexOf(uppercaseQuery) !== -1;
      }
    }

    function filterByAdvertiser(element) {
      return vm.advertiser.id === element.advertiser_id
    }

    function querySearchAdvertisers(query) {
      return AdvertisersService.filterAdvertiser(query, vm.campaign && vm.campaign.id).then(function (response) {
        return orderBy(angular.fromJson(response.data.advertisers), 'email');
      })
    }

    function createFilterForAdvertisers(query) {
      var lowercaseQuery = angular.lowercase(query);
      return function filterFn(advertiser) {
        return (advertiser.email.indexOf(lowercaseQuery) === 0);
      };
    }

    function querySearchCampaign(query) {
      return CampaignService.filterCampaigns(query, vm.advertiser && vm.advertiser.id).then(function (response) {
        return angular.fromJson(response.data.campaigns);
      });
    }

    function createFilterForCampaigns(query) {
      var lowercaseQuery = angular.lowercase(query);
      return function filterFn(campaign) {
        return (campaign.name.indexOf(lowercaseQuery) === 0);
      };
    }

    function campaignSelected(campaign) {
      if (campaign) {
        vm.quote.categories = campaign.categories;
      } else {
        vm.quote.categories = [];
      }
    }
  }
})();
