(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('NewEmailTemplateController', NewEmailTemplateController);

  NewEmailTemplateController.$inject = ['$mdDialog', '$log', '$scope', '$timeout', 'toastr', 'EmailTemplateService', 'email'];

  /* @ngInject */
  function NewEmailTemplateController($mdDialog, $log, $scope, $timeout, toastr, EmailTemplateService, email) {
    var vm = this;

    vm.email = email;
    vm.days = _.times(21)

    // Accessible attributes
    vm.emailTemplate = {
      email_id: email.id,
      days_delayed: 0,
      active: true,
      collate: false
    }

    // Accessible functions
    vm.cancel = function () {
      $mdDialog.cancel();
    }

    vm.submit = function (form) {
      EmailTemplateService.createEmailTemplate(vm.emailTemplate, function (response) {
        $mdDialog.hide(response);
        toastr.success('Email template successfuly created', 'Success!');
      }, function (error) {
        $log.error(error);
      });
    }

    $scope.contentChanged = function (editor, html, text, delta, oldDelta, source) {
      vm.emailTemplate.template = html;
    }

  }
})();
