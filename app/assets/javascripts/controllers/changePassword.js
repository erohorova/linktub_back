(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('ChangePasswordController', ChangePasswordController);

  ChangePasswordController.$inject = ['$scope', '$mdDialog', 'title'];

  /* @ngInject */
  function ChangePasswordController($scope, $mdDialog, title) {
    var vm = this;

    vm.title = title;

    vm.hide = function () {
      $mdDialog.hide();
    }

    vm.cancel = function () {
      $mdDialog.cancel();
    }

    vm.answer = function (password, repeat_password) {
      $mdDialog.hide({
        password: password,
        repeat_password: repeat_password
      });
    }

    vm.cantChange = function () {
      return angular.isUndefined(vm.password) ||
        angular.isUndefined(vm.repeat_password) ||
        vm.password !== vm.repeat_password;
    }
  }
})();