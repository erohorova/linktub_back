(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('AuthorsController', AuthorsController);

  AuthorsController.$inject = ['$mdDialog', '$log', '$window', 'toastr', 'AuthorsService', 'MessagesService', 'UsersService', 'AdminUserService', 'PUBLISHER_STATES'];

  /* @ngInject */
  function AuthorsController($mdDialog, $log, $window, toastr, AuthorsService, MessagesService, UsersService, AdminUserService, PUBLISHER_STATES) {
    var vm = this;

    // Accessible attributes
    vm.author = {};
    vm.authors = [];
    vm.categories = [];
    vm.addCategories = false;
    vm.selectedCategories = [];
    vm.categoriesChanged = false;

    vm.query = {
      filter: '',
      order: 'created_at',
      limit: 10,
      page: 1,
      search: false
    };

    // Accessible functions
    vm.showConfirmToUpdate = showConfirmToUpdate;
    vm.showConfirmToTerminate = showConfirmToTerminate;
    vm.showConfirmToActivate = showConfirmToActivate;
    vm.getAuthors = getAuthors;
    vm.showUpdatePassword = showUpdatePassword;
    vm.becomeAuthor = becomeAuthor;
    vm.isTerminated = isTerminated;
    vm.isActivated = isActivated;
    vm.promptNewAuthor = promptNewAuthor;
    vm.search = search;
    vm.getInvoicesLabel = getInvoicesLabel;
    vm.toggleConfirm = toggleConfirm;
    vm.checkEmail = checkEmail;
    vm.isSelected = isSelected;
    vm.toggleCategory = toggleCategory;
    vm.querySearch = querySearch;

    activate();

    // Functions
    function activate() {
      instantiateAuthor();
      instantiateAuthors();
      instantiateCategories();
    }

    function instantiateAuthor() {
      if (angular.isDefined(window.author)) {
        vm.author = angular.fromJson(window.author);
        vm.author.invoices = [];
      } else {
        vm.author = {}
      }
    }

    function isTerminated(author) {
      return author.state === PUBLISHER_STATES.TERMINATED;
    }

    function isActivated(author) {
      return author.state === PUBLISHER_STATES.ACTIVE;
    }

    function promptNewAuthor(ev) {
      $mdDialog.show({
        controller: 'NewAuthorController',
        controllerAs: 'ctrl',
        targetEvent: ev,
        clickOutsideToClose: false,
        templateUrl: 'newAuthorDialog.html'
      }).then(function (response) {
        vm.getAuthors(1, vm.query.limit);
      }, function () {
        // Cancelled, added this callback to prevent warning
        // TODO: Improve this callback
      });
    }

    function getInvoicesLabel() {
      return 'Invoices (' + vm.author.invoices.length + ')';
    }

    function becomeAuthor() {
      AuthorsService.loginAs(vm.author.id, function (resp) {
        $window.location.href = '/admin/dashboard';
      }, function (error) {
        toastr.error('Error while signing in as ' + vm.author.full_name + ' . Try again later.');
        $log.error(error);
      });
    }

    function instantiateAuthors() {
      if (angular.isDefined(window.authors)) {
        vm.authors = angular.fromJson(window.authors);
      } else {
        vm.authors = []
      }
    }

    function instantiateCategories() {
      if (angular.isDefined(window.categories)) {
        vm.categories = angular.fromJson(window.categories);
      } else {
        vm.categories = [];
      }
    }

    function getAuthors(page, limit) {
      vm.query.page = page;
      vm.query.limit = limit;
      vm.search();
    }

    function search() {
      AuthorsService.indexAuthors(vm.query.page, vm.query.limit, vm.query.filter, function (response) {
        var data = response.data;
        vm.authors = angular.fromJson(data.authors);
        vm.results = data.results;
      });
    }

    function showConfirmToTerminate(ev, author) {
      if (author.articles_count > 0) {
        var confirm = $mdDialog.confirm()
          .title('Author still has articles left to be finished!')
          .textContent('Please re-assign content before terminating this account!')
          .ariaLabel('Terminate author')
          .targetEvent(ev)
          .ok('OK');

        $mdDialog.show(confirm);
      } else {
        var confirm = $mdDialog.confirm()
          .title('Would you like to terminate this account?')
          .ariaLabel('Terminate author')
          .targetEvent(ev)
          .ok('Terminate')
          .cancel('Cancel');

        $mdDialog.show(confirm).then(function (result) {
          AuthorsService.terminateAuthor(author.id, function (response) {
            vm.getAuthors(1, vm.query.limit);
          });
        }, function () {
          // Cancelled
        });
      }
    };

    function showConfirmToActivate(ev, author) {
      var confirm = $mdDialog.confirm()
        .title('Would you like to re activate this account?')
        .ariaLabel('Rehire author')
        .targetEvent(ev)
        .ok('Yes')
        .cancel('No');

      $mdDialog.show(confirm).then(function (result) {
        AuthorsService.activateAuthor(author.id, function (response) {
          vm.getAuthors(1, vm.query.limit);
        });
      }, function () {
        // Cancelled
      });
    };

    function showConfirmToUpdate(ev, form, author) {
      var confirm = $mdDialog.confirm()
        .title('Are you sure you want to update this Author?')
        .ariaLabel('Update author')
        .targetEvent(ev)
        .ok('Update')
        .cancel('Cancel');

      $mdDialog.show(confirm).then(function (result) {
        // Update author
        AuthorsService.updateAuthor(author, function (response) {
          form.$setPristine();
        }, function (error) {
          $log.error(error);
        })
      }, function () {
        // Cancelled
      });
    }

    function toggleConfirm(ev, author) {
      var confirm = $mdDialog.confirm()
        .title('Would you like to manually confirm this account?')
        .ariaLabel('Confirm author')
        .targetEvent(ev)
        .ok('Yes')
        .cancel('No');

      $mdDialog.show(confirm).then(function (result) {
        AuthorsService.confirmAccount(author.id, function (response) {
          toastr.success('Account has been confirmed!');
          vm.getAuthors(1, vm.query.limit);
        });
      }, function () {
        // Cancelled
        author.confirmed = false;
      });
    }

    function showUpdatePassword(ev) {
      $mdDialog.show({
        controller: 'ChangePasswordController',
        controllerAs: 'ctrl',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: true,
        templateUrl: 'changePasswordDialog.html',
        locals: {
          title: 'Change author password'
        }
      }).then(function (user) {
        // Send update to user
        AuthorsService.updateAuthor({
          id: vm.author.id,
          password: user.password,
          password_confirmation: user.repeat_password
        });
      }, function () {
        // Action cancelled
      });
    }

    function checkEmail(form) {
      AdminUserService.checkDuplicateEmail(vm.author, function (response) {
        if (angular.isDefined(response.data)) {
          var data = response.data;
          if (angular.isDefined(data.email_exists)) {
            form.email.$setValidity('emailtaken', !data.email_exists);
          }
        }
      }, function (error) {
        $log.error(error);
      });
    }

    function isSelected(category) {
      return _.findIndex(vm.author.categories, ['name', category.name]) !== -1;
    }

    function toggleCategory(category) {
      var pos = _.findIndex(vm.author.categories, ['name', category.name]);
      if (pos < 0) {
        vm.author.categories.push(category)
      } else {
        vm.author.categories.splice(pos, 1);
      }
      vm.categoriesChanged = true;
    }

    function querySearch(criteria) {
      var categoriesFiltered = vm.categories.filter(createFilterFor(criteria));
      return categoriesFiltered;
    }

    function createFilterFor(query) {
      var uppercaseQuery = query.toUpperCase();
      return function filterFn(category) {
        return category.name.indexOf(uppercaseQuery) !== -1;
      }
    }
  }
})();
