(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('QuickNotesController', QuickNotesController);

  QuickNotesController.$inject = ['$log', '$mdDialog', 'resource'];

  /* @ngInject */
  function QuickNotesController($log, $mdDialog, resource) {
    var vm = this;

    activate();

    function activate() {
      vm.notes = resource.comments;
    }

    vm.hide = function () {
      $mdDialog.hide();
    }

    vm.cancel = function () {
      $mdDialog.cancel();
    }

    vm.submit = function (message, form) {
      $mdDialog.hide(message);
    }
  }
})();
