(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('AdvOrdersController', AdvOrdersController);

  AdvOrdersController.$inject = ['$log', '$window', '$mdDialog', '$filter', 'toastr', 'TransactionsService'];

  /* @ngInject */
  function AdvOrdersController($log, $window, $mdDialog, $filter, toastr, TransactionsService) {
    var vm = this;

    // Variables.
    vm.transactions = [];
    vm.query = { filter: '', limit: 50, page: 1, search: false, dateFrom: '', dateTo: '' };
    vm.perPage = 50;

    // Functions.
    vm.search = search;

    loadTransactions();

    function loadTransactions() {
      if (angular.isDefined(window.transactions)) {
        vm.transactions = angular.fromJson(window.transactions);
        if (angular.isDefined(window.results)) {
          vm.results = parseInt(window.results);
        }
      }
    }

    function search() {
      TransactionsService.index(vm.query, function (response) {
        vm.transactions = JSON.parse(response.data.transactions);
      }, function (error) {
        toastr.error(error.data.errors);
      });
    }
  }
})();
