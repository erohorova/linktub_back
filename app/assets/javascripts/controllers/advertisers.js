(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('AdvertisersController', AdvertisersController);

  AdvertisersController.$inject = ['$mdDialog', '$window', '$log', '$mdExpansionPanel',
    'toastr', 'AdvertisersService', 'MessagesService', 'UsersService', 'CampaignService',
    'PUBLISHER_STATES', 'CAMPAIGN_STATES', 'AgentsService', '$scope'
  ];

  /* @ngInject */
  function AdvertisersController($mdDialog, $window, $log, $mdExpansionPanel,
    toastr, AdvertisersService, MessagesService, UsersService,
    CampaignService, PUBLISHER_STATES, CAMPAIGN_STATES, AgentsService, $scope) {

    var vm = this;

    // Accessible attributes
    vm.advertiser = {};
    vm.advertisers = [];
    vm.agents = [];

    vm.query = {
      order: 'created_at',
      limit: 10,
      page: 1,
      search: false
    };

    $scope.selectOpened = false;
    $scope.noAgent = { id: null };

    // Accessible functions
    vm.showConfirm = showConfirm;
    vm.showConfirmWithNotes = showConfirmWithNotes;
    vm.getAdvertisers = getAdvertisers;
    vm.getMessagesLabel = getMessagesLabel;
    vm.getCampaignsLabel = getCampaignsLabel;
    vm.getActiveContentLabel = getActiveContentLabel;
    vm.showUpdatePassword = showUpdatePassword;
    vm.becomeAdvertiser = becomeAdvertiser;
    vm.isTerminated = isTerminated;
    vm.repliedMessage = repliedMessage;
    vm.showCategories = showCategories;
    vm.downloadReport = downloadReport;
    vm.downloadCampaignReport = downloadCampaignReport;
    vm.toggleConfirm = toggleConfirm;
    vm.search = search;
    vm.createCampaign = createCampaign;
    vm.reActivate = reActivate;
    vm.checkEmail = checkEmail;
    vm.pendingCampaign = pendingCampaign;
    vm.activeCampaign = activeCampaign;
    vm.requestStateChangeConfirmation = requestStateChangeConfirmation;
    vm.decodeMessage = decodeMessage;
    vm.showAgentConfirm = showAgentConfirm;
    activate();

    // Functions
    function activate() {
      instantiateAdvertiser();
      instantiateAdvertisers();
      instantiateCategories();
      instantiateActiveContent();
      instantiateAgents();

      $mdExpansionPanel().waitFor('messagesPanel').then(function (instance) {
        instance.collapse();
      });
    }

    function instantiateAdvertiser() {
      if (angular.isDefined($window.advertiser)) {
        vm.advertiser = angular.fromJson($window.advertiser);
        if (angular.isDefined($window.messages)) {
          vm.advertiser.messages = angular.fromJson($window.messages)
        }
      } else {
        vm.advertiser = {}
      }
    }

    function instantiateAgents() {
      if (angular.isDefined($window.agents)) {
        var parsedAgents = angular.fromJson($window.agents);
        if (vm.advertiser.agent_id) {
          var agents = [vm.advertiser.agent];
          agents = agents.concat(parsedAgents);
          vm.agents = agents;
        } else {
          vm.agents = parsedAgents;
        }
        if (angular.isDefined($window.messages)) {
          vm.advertiser.messages = angular.fromJson($window.messages)
        }
      } else {
        vm.agents = [];
      }
    }

    function reActivate(ev, advertiser) {
      AdvertisersService.changeAdvertiserState(advertiser.id, 'activate', null, function (response) {
        advertiser.state = 'active';
      }, function (error) {
        $log.error(error);
      })
    }

    function instantiateActiveContent() {
      if (angular.isDefined($window.active_content)) {
        vm.active_content = angular.fromJson($window.active_content);
      }
    }

    function instantiateCategories() {
      if (angular.isDefined($window.categories)) {
        vm.categories = angular.fromJson($window.categories);
      }
    }

    function getMessagesLabel() {
      return "Messages (" + vm.advertiser.messages.length + ")";
    }

    function getCampaignsLabel() {
      return "Campaigns (" + vm.advertiser.campaigns.length + ")";
    }

    function getActiveContentLabel() {
      return "Active Content (" + vm.active_content.length + ")";
    }

    function createCampaign(ev) {
      $mdDialog.show({
        controller: 'NewCampaignController',
        controllerAs: 'ctrl',
        templateUrl: 'newCampaignDialog.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: false,
        locals: {
          advertiser: vm.advertiser,
          categories: vm.categories,
        }
      })
      .then(function (response) {
        if (response.data && response.data.campaign) {
            vm.advertiser.campaigns.push(JSON.parse(response.data.campaign));
        }
      }, function () {
        // Action cancelled
      });
    }

    function repliedMessage(message, form) {
      message.response = encodeURIComponent(message.response);
      MessagesService.replyMessage(message, function () {
        vm.advertiser.messages.forEach(function (element) {
          if (element.id === message.id) {
            element.reply_message = message.response;
            form.$setPristine();
            form.$setUntouched();
            return;
          }
        })
      }, function (error) {
        $log.error(error);
      })
    }

    function isTerminated(advertiser) {
      return advertiser.state === PUBLISHER_STATES.TERMINATED;
    }

    function showCategories(ev, campaign) {
      $mdDialog.show({
          controller: 'CategoriesController',
          controllerAs: 'ctrl',
          templateUrl: 'categoriesDialog.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose: true,
          locals: {
            campaign: campaign,
            categories: vm.categories
          }
        })
        .then(function (categories) {
          CampaignService.updateCampaign(campaign.id, {
            categories: categories
          }, function (response) {
            toastr.success('The campaign has been updated successfuly!', 'Successful');
          }, function (response) {
            $log.error('Unable to update!');
          })
        }, function () {
          // Action cancelled
        });
    }

    function becomeAdvertiser() {
      $window.open($window.redirect_url + '?token=' +
        vm.advertiser.user.authentication_token + '&role=advertiser', '_blank');
    }

    function instantiateAdvertisers() {
      if (angular.isDefined($window.advertisers)) {
        vm.advertisers = angular.fromJson($window.advertisers);
      } else {
        vm.advertisers = []
      }
    }

    function getAdvertisers(page, limit) {
      vm.query.page = page;
      vm.query.limit = limit;
      vm.promise = AdvertisersService.indexAdvertisers(page, limit, function (response) {
        vm.advertisers = response.data;
      }).$promise;
    }

    function showConfirmWithNotes(ev, advertiser) {
      // Appending dialog to document.body to cover sidenav in docs app
      var confirm = $mdDialog.prompt()
        .title('Would you like to terminate this account?')
        .placeholder('Notes')
        .ariaLabel('Notes')
        .clickOutsideToClose(true)
        .targetEvent(ev)
        .required(true)
        .ok('Ok')
        .cancel('Cancel');

      $mdDialog.show(confirm).then(function (result) {
        // Terminate account with advertiser.id
        AdvertisersService.changeAdvertiserState(advertiser.id, 'terminate', result, function (response) {
          advertiser.state = 'terminated';
        }, function (error) {
          $log.error(error);
        })
      }, function () {
        // Action cancelled
      });
    };

    function showConfirm(ev, form, user) {
      var confirm = $mdDialog.confirm()
        .title('Are you sure you want to update this Advertiser?')
        .ariaLabel('Update advertiser')
        .targetEvent(ev)
        .ok('Update')
        .cancel('Cancel');

      $mdDialog.show(confirm).then(function (result) {
        // Update advertiser
        UsersService.updateUser(user, function (response) {
          form.$setPristine();
        }, function (error) {
          $log.error(error);
        })
      }, function () {
        // Cancelled
      });
    }

    function showUpdatePassword(ev) {
      $mdDialog.show({
        controller: 'ChangePasswordController',
        controllerAs: 'ctrl',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: true,
        templateUrl: 'changePasswordDialog.html',
        locals: {
          title: "Change advertiser password"
        }
      }).then(function (user) {
        // Send update to user
        UsersService.updateUser({
          id: vm.advertiser.user.id,
          password: user.password,
          password_confirmation: user.repeat_password
        });
      }, function () {
        // Action cancelled
      });
    }

    function toggleConfirm(ev, advertiser) {
      var confirm = $mdDialog.confirm()
        .title('Would you like to manually confirm this account?')
        .ariaLabel('Confirm advertiser')
        .targetEvent(ev)
        .ok('Yes')
        .cancel('No');

      $mdDialog.show(confirm).then(function () {
        AdvertisersService.confirmAccount(advertiser.id, function () {
          toastr.success('Account has been confirmed!');
          vm.getAdvertisers(1, vm.query.limit);
        });
      }, function () {
        // Cancelled
        advertiser.confirmed = false;
      });
    }

    function search() {
      AdvertisersService.indexAdvertisers(vm.query.page, vm.query.limit, vm.query.filter, function (response) {
        var data = response.data;
        vm.advertisers = angular.fromJson(data.advertisers);
        vm.results = data.results;
      });
    }

    function downloadReport() {
      $window.open(vm.advertiser.active_content_spreadsheet_doc.url, '_blank');
    }

    function downloadCampaignReport(campaign) {
      $window.open(campaign.campaign_report_spreadsheet_doc.url, '_blank');
    }

    function checkEmail(form) {
      UsersService.checkDuplicateEmail(vm.advertiser.user, function (response) {
        if (angular.isDefined(response.data)) {
          var data = response.data;
          if (angular.isDefined(data.email_exists)) {
            form.email.$setValidity('emailtaken', !data.email_exists);
          }
        }
      }, function (error) {
        $log.error(error);
      });
    }

    function pendingCampaign(campaign) {
      return campaign.state !== CAMPAIGN_STATES.ACTIVE && campaign.state !== CAMPAIGN_STATES.REJECTED;
    }

    function activeCampaign(campaign) {
      return campaign.state === CAMPAIGN_STATES.ACTIVE;
    }

    function requestStateChangeConfirmation(ev, campaign) {
      var action = this.activeCampaign(campaign) ? 'decline' : 'approve';
      var confirm = $mdDialog.confirm()
        .title('Would you like to ' + action + ' this campaign?')
        .ariaLabel('Confirm change')
        .targetEvent(ev)
        .ok('Yes')
        .cancel('No');

      $mdDialog.show(confirm).then(function () {
        switch (campaign.state) {
          case CAMPAIGN_STATES.ACTIVE:
            CampaignService.declineCampaign(campaign.id, function (result) {
              campaign.state = result.data.new_state;
              toastr.success('The campaign has been declined.', 'Success!');
            }, function () {
              toastr.error('The campaign has already been declined!');
            });
            break;
          case CAMPAIGN_STATES.REJECTED:
            CampaignService.approveCampaign(campaign.id, function (result) {
              campaign.state = result.data.new_state;
              toastr.success('The campaign has been approved.', 'Success!');
            }, function () {
              toastr.error('The campaign has already been approved!');
            });
        }
      }, function () {
        // User cancels, nothing needs to be done.
      });
    }

    function decodeMessage(message) {
      return decodeURIComponent(message);
    }

    function showAgentConfirm(ev, selectOpened) {
      if (!selectOpened) return;

      var confirm = $mdDialog.confirm()
        .title('Are you sure?')
        .ariaLabel('Update agent')
        .targetEvent(ev)
        .ok('Yes')
        .cancel('No');

      $mdDialog.show(confirm).then(function (result) {
        AdvertisersService.reassignAgent($scope.selectedAgent.id, vm.advertiser.id, function (response) {
        }, function (error) {
          $log.error(error);
        })
      }, function () {
        $scope.selectedAgent = vm.advertiser.agent;
      });
    }
  }
})();
