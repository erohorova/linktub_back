(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('AdminUsersController', AdminUsersController);

  AdminUsersController.$inject = ['$mdDialog', '$mdExpansionPanel', 'AdminUserService'];

  /* @ngInject */
  function AdminUsersController($mdDialog, $mdExpansionPanel, AdminUserService) {
    var vm = this;

    // Accessible attributes
    vm.adminUsers = [];
    vm.query = {
      filter: '',
      order: 'created_at',
      limit: 10,
      page: 1,
      search: false
    };

    // Accessible functions
    vm.promptNewAdmin = promptNewAdmin;
    vm.getAdminUsers = getAdminUsers;
    vm.search = search;

    initialize();

    //functions
    function initialize() {
      instantiateAdminUsers();
    }

    function instantiateAdminUsers() {
      if (angular.isDefined(window.adminUsers)) {
        vm.adminUsers = angular.fromJson(window.adminUsers);

        if (angular.isDefined(window.results)) {
          vm.results = parseInt(window.results);
        }
      } else {
        vm.adminUsers = [];
      }
    }

    function search() {
      AdminUserService.indexAdminUsers(vm.query.page, vm.query.limit, vm.query.filter, function (response) {
        var data = response.data;
        vm.adminUsers = angular.fromJson(data.admin_users);
        vm.results = data.results;
      });
    }

    function getAdminUsers(page, limit) {
      vm.query.page = page;
      vm.query.limit = limit;
      vm.search();
    }

    function promptNewAdmin(ev) {
      $mdDialog.show({
        controller: 'NewAdminController',
        controllerAs: 'ctrl',
        targetEvent: ev,
        clickOutsideToClose: false,
        templateUrl: 'newAdminDialog.html'
      }).then(function (response) {
        vm.adminUsers.splice(0, 0, response.data.admin);
      }, function () {
        // Cancelled, added this callback to prevent warning
        // TODO: Improve this callback
      });
    }
  }
})();
