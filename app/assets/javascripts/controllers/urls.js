(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('UrlsController', UrlsController);

  UrlsController.$inject = ['$mdDialog', '$log', '$mdExpansionPanel', '$mdEditDialog', 'toastr', 'UrlsService',
    'MessagesService', 'UsersService', 'URL_STATES'
  ];

  /* @ngInject */
  function UrlsController($mdDialog, $log, $mdExpansionPanel, $mdEditDialog, toastr, UrlsService,
    MessagesService, UsersService, URL_STATES) {
    var vm = this;

    // Accessible attributes
    vm.url = {};
    vm.urls = [];
    vm.query = {
      filter: '',
      order: 'created_at',
      limit: 10,
      page: 1,
      search: false,
      scope: 'pending'
    };

    vm.pendingResults = 0;
    vm.changedPriceResults = 0;
    vm.pausedResults = 0;

    vm.states = [{
      label: 'all',
      payload: 'all'
    }, {
      label: 'pending',
      payload: 'pending'
    }, {
      label: 'Price Changed',
      payload: 'under_review'
    }, {
      label: 'paused',
      payload: 'paused'
    }, {
      label: 'active',
      payload: 'active'
    }, {
      label: 'rejected',
      payload: 'rejected'
    }, {
      label: 'resubmitted',
      payload: 'resubmitted'
    }, {
      label: 'suspended',
      payload: 'cancelled'
    }];

    vm.actions = [{
      id: 0,
      name: 'Approve',
      state: 'active'
    }, {
      id: 1,
      name: 'Reject',
      state: 'reject'
    }, {
      id: 2,
      name: 'Suspend',
      state: 'cancel'
    }];

    // Accessible functions
    vm.showConfirm = showConfirm;
    vm.showConfirmWithNotes = showConfirmWithNotes;
    vm.getUrls = getUrls;
    vm.showUpdatePassword = showUpdatePassword;
    vm.modifyScope = modifyScope;
    vm.search = search;
    vm.editShownPrice = editShownPrice;
    vm.editDescription = editDescription;
    vm.editable = editable;
    vm.isActive = isActive;
    vm.performAction = performAction;
    vm.showCounter = showCounter;
    vm.toggleField = toggleField;

    activate();

    // Functions
    function activate() {
      instantiateUrl();
      instantiateUrls();
    }

    function instantiateUrl() {
      if (angular.isDefined(window.url)) {
        vm.url = angular.fromJson(window.url);
      } else {
        vm.url = {}
      }
    }

    function showCounter(state) {
      switch (state.payload) {
      case URL_STATES.PENDING:
        return vm.pendingResults;
      case URL_STATES.UNDER_REVIEW:
        return vm.changedPriceResults;
      case URL_STATES.PAUSED:
        return vm.pausedResults;
      default:
        return -1;
      }
    }

    function editable(url) {
      return url.state === URL_STATES.PENDING;
    }

    function editShownPrice(ev, url) {
      var promise = $mdEditDialog.small({
        modelValue: url.shown_price,
        placeholder: 'Change the price',
        save: function (input) {
          url.shown_price = input.$modelValue;
          UrlsService.updateUrl(url.id, {
            shown_price: url.shown_price
          }, function () {
            toastr.success('Updated Successfuly');
            vm.search();
          }, function (error) {
            $log.error(error);
          });
        },
        targetEvent: ev,
        type: 'number',
        validators: {
          'aria-label': 'Edit price',
          'min': '0'
        }
      });

      promise.then(function (ctrl) {
        var input = ctrl.getInput();

        input.$viewChangeListeners.push(function () {
          input.$setValidity('test', input.$modelValue !== 'test');
        });
      });
    }

    function editDescription(ev, url) {
      $mdDialog.show({
          controller: 'EditDescriptionController',
          controllerAs: 'ctrl',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose: true,
          templateUrl: 'editDescriptionDialog.html',
          locals: {
            title: 'Change the description',
            description: url.description
          }
        })
        .then(function (response) {
          url.description = response.description;
          UrlsService.updateUrl(url.id, {
            description: url.description
          }, function () {
            toastr.success('Updated Successfuly');
          }, function (error) {
            $log.error(error);
          });
        }, function () {
          // Cancelled
        });
    }

    function instantiateUrls() {
      if (angular.isDefined(window.urls)) {
        vm.urls = angular.fromJson(window.urls);
        if (angular.isDefined(window.results)) {
          vm.results = parseInt(window.results);
        }

        if (angular.isDefined(window.pending_results)) {
          vm.pendingResults = parseInt(window.pending_results);
        }

        if (angular.isDefined(window.changed_price_results)) {
          vm.changedPriceResults = parseInt(window.changed_price_results);
        }

        if (angular.isDefined(window.pending_results)) {
          vm.pausedResults = parseInt(window.paused_results);
        }
      } else {
        vm.urls = []
      }
    }

    function getUrls(page, limit) {
      vm.query.page = page;
      vm.query.limit = limit;
      vm.search();
    }

    function search() {
      UrlsService.indexUrls(vm.query, function (response) {
        var data = response.data;
        vm.urls = angular.fromJson(data.urls);
        vm.results = data.results;
        vm.pendingResults = data.pending_results;
        vm.changedPriceResults = data.changed_price_results;
        vm.pausedResults = data.pausedResults;
      });
    }

    function modifyScope() {
      vm.query.page = 1;
      vm.search();
      if (vm.query.scope && vm.query.scope === URL_STATES.ACTIVE) {
        _.remove(vm.actions, ['id', 0]);
      } else if (_.findIndex(vm.actions, ['id', 0]) === -1) {
        vm.actions.unshift({
          id: 0,
          name: 'Approve',
          state: URL_STATES.ACTIVE
        });
      }
    }

    function showConfirmWithNotes(ev, url) {
      // Appending dialog to document.body to cover sidenav in docs app
      var confirm = $mdDialog.prompt()
        .title('Would you like to terminate this account?')
        .placeholder('Notes')
        .ariaLabel('Notes')
        .clickOutsideToClose(true)
        .targetEvent(ev)
        .required(true)
        .ok('Ok')
        .cancel('Cancel');

      $mdDialog.show(confirm).then(function (result) {
        // Terminate account with url.id
        UrlsService.terminateUrl(url.id, result, function (response) {
          url.state = 'terminated';
        }, function (error) {
          $log.error(error);
        })
      }, function () {
        // Action cancelled
      });
    };

    function showConfirm(ev, form, user) {
      var confirm = $mdDialog.confirm()
        .title('Are you sure you want to update this Url?')
        .ariaLabel('Update url')
        .targetEvent(ev)
        .ok('Update')
        .cancel('Cancel');

      $mdDialog.show(confirm).then(function (result) {
        // Update url
        UsersService.updateUser(user, function (response) {
          form.$setPristine();
        }, function (error) {
          $log.error(error);
        })
      }, function () {
        // Cancelled
      });
    }

    function showUpdatePassword(ev) {
      $mdDialog.show({
        controller: 'ChangePasswordController',
        controllerAs: 'ctrl',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: true,
        templateUrl: 'changePasswordDialog.html',
        locals: {
          title: 'Change url password'
        }
      });
    }

    function isActive() {
      return vm.query.scope === 'active';
    }

    function toggleField(ev, url, field) {
      var originalValue = !url[field];
      var confirm = $mdDialog.confirm()
        .title('Would you like to manually change ' + field + '?')
        .ariaLabel('Confirm chanage ' + field)
        .targetEvent(ev)
        .ok('Yes')
        .cancel('No');

      $mdDialog.show(confirm).then(function () {
        var params = {};
        params[field] = url[field];
        UrlsService.updateUrl(url.id, params, function () {
          toastr.success('Updated Successfuly');
        }, function (err) {
          $log.error(err);
        });
      }, function () {
        url[field] = originalValue;
      });
    }

    function chanageState(actionId, url) {
      UrlsService.updateUrlState(url.id, _.find(vm.actions, ['id', parseInt(actionId)]).state, function (response) {
        vm.getUrls(1, vm.query.limit);
        toastr.success('Successfully ' + _.find(vm.actions, ['id', parseInt(actionId)]).name + ' the URL');
      }, function (error) {
        $log.error(error);
      });
    }

    function performAction(actionId, url) {
      if (actionId == 1) {
        if (url.state === URL_STATES.REJECTED) {
          toastr.error('Can\'t reject a rejected url');
          return;
        }

        $mdDialog.show({
          controller: 'QuickNotesController',
          controllerAs: 'ctrl',
          templateUrl: 'notesDialog.html',
          clickOutsideToClose: false,
          locals: {
            resource: {
              comments: []
            }
          }
        }).then(function (message) {
          UrlsService.createNote(url.id, {
            message: message
          }, function () {
            chanageState(actionId, url);
          }, function (error) {
            $log.error(error);
          });
        }, function () {
          // Cancelled
        });
        url.action = null;
      } else {
        chanageState(actionId, url);
      }
    }
  }
})();
