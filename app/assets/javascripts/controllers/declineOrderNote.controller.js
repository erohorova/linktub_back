(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('DeclineOrderNoteController', DeclineOrderNoteController);

  DeclineOrderNoteController.$inject = ['$mdDialog', 'attrs'];

  /* @ngInject */
  function DeclineOrderNoteController($mdDialog, attrs) {
    var vm = this;

    activate();

    function activate() {
      vm.title = attrs.title;
      vm.content = attrs.content;
    }

    vm.hide = function () {
      $mdDialog.hide();
    }

    vm.cancel = function () {
      $mdDialog.cancel();
    }

    vm.submit = function (message, form) {
      form.$setPristine();
      form.$setUntouched();
      $mdDialog.hide(message);
    }
  }
})();
