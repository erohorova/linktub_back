(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('EditEmailTemplateController', EditEmailTemplateController);

  EditEmailTemplateController.$inject = ['$mdDialog', '$scope', '$timeout', 'toastr', 'EmailTemplateService', 'emailTemplateId'];

  /* @ngInject */
  function EditEmailTemplateController($mdDialog, $scope, $timeout, toastr, EmailTemplateService, emailTemplateId) {
    var vm = this;

    vm.emailTemplateId = emailTemplateId;

    // Accessible attributes
    vm.emailTemplate = {}
    vm.days = _.times(21)

    initialize();

    function initialize() {
      find(vm.emailTemplateId);
    }

    function find(emailTemplateId) {
      EmailTemplateService.showEmailTemplate(emailTemplateId, function (response) {
        var data = response.data;
        vm.emailTemplate = angular.fromJson(data.email_template);
      });
    }

    // Accessible functions
    vm.cancel = function () {
      $mdDialog.cancel();
    }

    vm.submit = function (form) {
      EmailTemplateService.updateEmailTemplate(vm.emailTemplate.id, vm.emailTemplate, function (response) {
        $mdDialog.hide(response);
        toastr.success('Email template successfuly created', 'Success!');
      }, function (error) {
        $log.error(error);
      });
    }

    vm.contentChanged = function (editor, html, text, delta, oldDelta, source) {
      vm.emailTemplate.template = html;
    }

  }
})();
