(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('NewAdminController', NewAdminController);

  NewAdminController.$inject = ['$mdDialog', 'AdminUserService'];

  /* @ngInject */
  function NewAdminController($mdDialog, AdminUserService) {
    var vm = this;

    // Attributes
    vm.admin = {}

    // Functions
    vm.cancel = function () {
      $mdDialog.cancel();
    }

    vm.refreshErrors = function (form, input) {
      if (form.repeatPassword.$invalid && input === 'repeatPassword') {
        form.repeatPassword.$setValidity('no-match', true);
      }
      if (form.email.$invalid && input === "email") {
        form.email.$setValidity('emailtaken', true);
      }
    }

    vm.submit = function (form) {
      if (!vm.isSamePassword()) {
        form.repeatPassword.$setValidity('no-match', false);
        vm.change = true;
      } else {
        AdminUserService.createAdmin(vm.admin, function (response) {
          $mdDialog.hide(response);
        }, function (error) {
          if (angular.isDefined(error.data)) {
            if (angular.isDefined(error.data.email)) {
              form.email.$setValidity('emailtaken', false);
            }
          }
        });
      }
    }

    vm.isSamePassword = function () {
      return vm.admin.password === vm.admin.repeat_password;
    }
  }
})();