(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('AgentsController', AgentsController);

  AgentsController.$inject = ['$mdDialog', '$log', '$window', 'toastr', 'AgentsService', 'MessagesService', 'UsersService', 'AdminUserService', 'PUBLISHER_STATES'];

  /* @ngInject */
  function AgentsController($mdDialog, $log, $window, toastr, AgentsService, MessagesService, UsersService, AdminUserService, PUBLISHER_STATES) {
    var vm = this;

    // Accessible attributes
    vm.agent = {};
    vm.Agents = [];
    vm.categories = [];
    vm.addCategories = false;
    vm.selectedCategories = [];
    vm.categoriesChanged = false;

    vm.query = {
      filter: '',
      order: 'created_at',
      limit: 10,
      page: 1,
      search: false
    };

    // Accessible functions
    vm.showConfirmToUpdate = showConfirmToUpdate;
    vm.showConfirmToTerminate = showConfirmToTerminate;
    vm.showConfirmToActivate = showConfirmToActivate;
    vm.getAgents = getAgents;
    vm.showUpdatePassword = showUpdatePassword;
    vm.isTerminated = isTerminated;
    vm.isActivated = isActivated;
    vm.promptNewAgent = promptNewAgent;
    vm.search = search;
    vm.toggleConfirm = toggleConfirm;
    vm.checkEmail = checkEmail;
    vm.isSelected = isSelected;
    vm.toggleCategory = toggleCategory;
    vm.querySearch = querySearch;

    activate();

    // Functions
    function activate() {
      instantiateAgent();
      instantiateAgents();
      instantiateCategories();
    }

    function instantiateAgent() {
      if (angular.isDefined(window.agent)) {
        vm.agent = angular.fromJson(window.agent);
      } else {
        vm.agent = {}
      }
    }

    function isTerminated(agent) {
      return agent.state === PUBLISHER_STATES.TERMINATED;
    }

    function isActivated(agent) {
      return agent.state === PUBLISHER_STATES.ACTIVE;
    }

    function promptNewAgent(ev) {
      $mdDialog.show({
        controller: 'NewAgentController',
        controllerAs: 'ctrl',
        targetEvent: ev,
        clickOutsideToClose: false,
        templateUrl: 'newAgentDialog.html'
      }).then(function (response) {
        vm.getAgents(1, vm.query.limit);
      }, function () {
        // Cancelled, added this callback to prevent warning
        // TODO: Improve this callback
      });
    }

    function instantiateAgents() {
      if (angular.isDefined(window.agents)) {
        vm.agents = angular.fromJson(window.agents);
      } else {
        vm.agents = []
      }
    }

    function instantiateCategories() {
      if (angular.isDefined(window.categories)) {
        vm.categories = angular.fromJson(window.categories);
      } else {
        vm.categories = [];
      }
    }

    function getAgents(page, limit) {
      vm.query.page = page;
      vm.query.limit = limit;
      vm.search();
    }

    function search() {
      AgentsService.indexAgents(vm.query.page, vm.query.limit, vm.query.filter, function (response) {
        var data = response.data;
        vm.agents = angular.fromJson(data.agents);
        vm.results = data.results;
      });
    }

    function showConfirmToTerminate(ev, agent) {
      if (agent.articles_count > 0) {
        var confirm = $mdDialog.confirm()
          .title('Agent still has articles left to be finished!')
          .textContent('Please re-assign content before terminating this account!')
          .ariaLabel('Terminate agent')
          .targetEvent(ev)
          .ok('OK');

        $mdDialog.show(confirm);
      } else {
        var confirm = $mdDialog.confirm()
          .title('Would you like to terminate this account?')
          .ariaLabel('Terminate agent')
          .targetEvent(ev)
          .ok('Terminate')
          .cancel('Cancel');

        $mdDialog.show(confirm).then(function (result) {
          AgentsService.terminateAgent(agent.id, function (response) {
            vm.getAgents(1, vm.query.limit);
          });
        }, function () {
          // Cancelled
        });
      }
    };

    function showConfirmToActivate(ev, agent) {
      var confirm = $mdDialog.confirm()
        .title('Would you like to re activate this account?')
        .ariaLabel('Rehire agent')
        .targetEvent(ev)
        .ok('Yes')
        .cancel('No');

      $mdDialog.show(confirm).then(function (result) {
        AgentsService.activateAgent(agent.id, function (response) {
          vm.getAgents(1, vm.query.limit);
        });
      }, function () {
        // Cancelled
      });
    };

    function showConfirmToUpdate(ev, form, agent) {
      var confirm = $mdDialog.confirm()
        .title('Are you sure you want to update this Agent?')
        .ariaLabel('Update agent')
        .targetEvent(ev)
        .ok('Update')
        .cancel('Cancel');

      $mdDialog.show(confirm).then(function (result) {
        // Update agent
        AgentsService.updateAgent(agent, function (response) {
          form.$setPristine();
        }, function (error) {
          $log.error(error);
        })
      }, function () {
        // Cancelled
      });
    }

    function toggleConfirm(ev, agent) {
      var confirm = $mdDialog.confirm()
        .title('Would you like to manually confirm this account?')
        .ariaLabel('Confirm agent')
        .targetEvent(ev)
        .ok('Yes')
        .cancel('No');

      $mdDialog.show(confirm).then(function (result) {
        AgentsService.confirmAccount(agent.id, function (response) {
          toastr.success('Account has been confirmed!');
          vm.getAgents(1, vm.query.limit);
        });
      }, function () {
        // Cancelled
        agent.status_confirmed = false;
      });
    }

    function showUpdatePassword(ev) {
      $mdDialog.show({
        controller: 'ChangePasswordController',
        controllerAs: 'ctrl',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: true,
        templateUrl: 'changePasswordDialog.html',
        locals: {
          title: 'Change agent password'
        }
      }).then(function (user) {
        // Send update to user
        AgentsService.updateAgent({
          id: vm.agent.id,
          password: user.password,
          password_confirmation: user.repeat_password
        });
      }, function () {
        // Action cancelled
      });
    }

    function checkEmail(form) {
      AdminUserService.checkDuplicateEmail(vm.agent, function (response) {
        if (angular.isDefined(response.data)) {
          var data = response.data;
          if (angular.isDefined(data.email_exists)) {
            form.email.$setValidity('emailtaken', !data.email_exists);
          }
        }
      }, function (error) {
        $log.error(error);
      });
    }

    function isSelected(category) {
      return _.findIndex(vm.agent.categories, ['name', category.name]) !== -1;
    }

    function toggleCategory(category) {
      var pos = _.findIndex(vm.agent.categories, ['name', category.name]);
      if (pos < 0) {
        vm.agent.categories.push(category)
      } else {
        vm.agent.categories.splice(pos, 1);
      }
      vm.categoriesChanged = true;
    }

    function querySearch(criteria) {
      var categoriesFiltered = vm.categories.filter(createFilterFor(criteria));
      return categoriesFiltered;
    }

    function createFilterFor(query) {
      var uppercaseQuery = query.toUpperCase();
      return function filterFn(category) {
        return category.name.indexOf(uppercaseQuery) !== -1;
      }
    }
  }
})();
