(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('UrlReassignController', UrlReassignController);

  UrlReassignController.$inject = ['$log', '$window', '$mdDialog', '$filter', 'toastr', 'UrlsService', 'PublishersService'];

  /* @ngInject */
  function UrlReassignController($log, $window, $mdDialog, $filter, toastr, UrlsService, PublishersService) {
    var vm = this;

    // URL search variables.
    vm.showUrlsTable = true;
    vm.urls = [];
    vm.urlToReassign = {};
    vm.query = {
      filter: '',
      limit: 10,
      page: 1,
      search: false,
      scope: 'all'
    };

    // Pub search variables.
    vm.pubSearchCriteria = '';
    vm.publishers = [];
    vm.newPublisher = {};
    vm.newProfile = {};
    vm.selectedProfile = {};
    vm.rowOfSelectedProfile = -1;

    // Functions.
    vm.reAssign = reAssign;
    vm.search = search;
    vm.searchPubs = searchPubs;
    vm.selectedProfileChanged = selectedProfileChanged;
    vm.selectPub = selectPub;
    vm.publisherSelected = publisherSelected;
    vm.profileChosen = profileChosen;
    vm.update = update;

    loadUrls();

    function loadUrls() {
      if (angular.isDefined(window.urls)) {
        vm.urls = angular.fromJson(window.urls);
        if (angular.isDefined(window.results)) {
          vm.results = parseInt(window.results);
        }
      }
    }

    function reAssign(ev, url) {
      vm.urlToReassign = url;
      vm.showUrlsTable = false;
    }

    function search() {
      UrlsService.indexUrls(vm.query, function (response) {
        var data = response.data;
        vm.urls = angular.fromJson(data.urls);
        vm.results = data.results;
      });
    }

    function searchPubs() {
      resetNewPubSelection();

      PublishersService.indexPublishers(1, 10, vm.pubSearchCriteria, function (response) {
        vm.publishers = JSON.parse(response.data.publishers);
      });
    }

    function resetNewPubSelection() {
      vm.rowOfSelectedProfile = -1;
      vm.newPublisher = {};
      vm.newProfile = {};
    }

    function selectedProfileChanged(i) {
      vm.rowOfSelectedProfile = i;
    }

    function selectPub(ev, publisher) {
      vm.newPublisher = publisher;
      vm.pubSearchCriteria = '';
    }

    function publisherSelected() {
      return JSON.stringify(vm.newPublisher) !== JSON.stringify({});
    }

    function profileChosen(profile) {
      vm.selectedProfile = profile;
    }

    function update(ev) {
      UrlsService.updatePub(vm.urlToReassign.id, {
        price: vm.urlToReassign.price,
        shown_price: vm.urlToReassign.shown_price,
        new_pub_id: vm.newPublisher.id,
        new_profile_id: vm.selectedProfile.id
      }, function (response) {
        toastr.success('URL updated');
        $window.location.reload();
      }, function (error) {
        toastr.error(error.data.errors);
      });
    }
  }
})();
