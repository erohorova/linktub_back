(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('EditDescriptionController', EditDescriptionController);

  EditDescriptionController.$inject = ['$scope', '$mdDialog', 'title', 'description'];

  /* @ngInject */
  function EditDescriptionController($scope, $mdDialog, title, description) {
    var vm = this;

    vm.title = title;
    vm.description = description;

    vm.hide = function () {
      $mdDialog.hide();
    }

    vm.cancel = function () {
      $mdDialog.cancel();
    }

    vm.answer = function (description) {
      $mdDialog.hide({
        description: description
      });
    }
  }
})();