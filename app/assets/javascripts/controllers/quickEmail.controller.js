(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('QuickEmailController', QuickEmailController);

  QuickEmailController.$inject = ['$log', '$mdDialog', 'toastr', 'PublishersService', 'resource'];

  /* @ngInject */
  function QuickEmailController($log, $mdDialog, toastr, PublishersService, resource) {
    var vm = this;

    activate();

    function activate() {
      vm.email = {
        to: resource.publisher.email,
        doc_url: resource.ad.blog_post_doc.url
      };
    }

    vm.cancel = function () {
      $mdDialog.cancel();
    }

    vm.submit = function () {
      PublishersService.email(resource.publisher.id, vm.email, function () {
        $mdDialog.cancel();
        toastr.success('Email is going to be send!');
      }, function (err) {
        $log.error(err);
      });
    }
  }
})();
