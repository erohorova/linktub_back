(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('ActionItemsController', ActionItemsController);

  ActionItemsController.$inject = ['$log', '$mdDialog', '$window', 'toastr', 'AdsService', 'REFUNDED_CONSTANT'];

  /* @ngInject */
  function ActionItemsController($log, $mdDialog, $window, toastr, AdsService, REFUNDED_CONSTANT) {
    var vm = this;

    // Variables.
    vm.ads = [];
    vm.results = 0;
    vm.pagination = { limit: 50, page: 1 };

    // Functions.
    vm.adState = adState;
    vm.downloadFile = downloadFile;
    vm.remove = remove;
    vm.hasFile = hasFile;

    loadAds();

    function loadAds() {
      if (angular.isDefined(window.ads)) {
        vm.ads = angular.fromJson(window.ads);
        if (angular.isDefined(window.results)) {
          vm.results = parseInt(window.results);
        }
      }
    }

    function adState(ad) {
      return ad.rejected_reason === REFUNDED_CONSTANT.REFUNDED ? 'Refunded' : 'Declined';
    }

    function downloadFile(ad) {
      $window.open(ad.blog_post_doc.url, '_blank');
    }

    function remove(ad, ev) {
      var confirm = $mdDialog.confirm()
        .title('Are you sure you want to remove this ad?')
        .textContent('The ad is going to be removed from this list.')
        .ariaLabel('Remove ad')
        .targetEvent(ev)
        .ok('Yes')
        .cancel('No');
      return $mdDialog.show(confirm).then(function () {
        AdsService.seen(ad.id, function (response) {
          ad.seen = true;
        }, function (error) {
          toastr.error(error.data.errors)
        });
      }, function () {
        $log.log('Confirm dialog dismissed');
      });
    }

    function hasFile(doc) {
      return doc !== null;
    }
  }
})();
