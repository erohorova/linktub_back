(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('ReportsController', ReportsController);

  ReportsController.$inject = ['$log', '$scope', 'DashboardService', 'PayoutsService', '$mdDialog', 'ORDERS_PAYLOADS', 'OrdersService', 'OrdersActionsService', 'toastr', '$window', '$controller'];

  /* @ngInject */
  function ReportsController($log,  $scope, DashboardService, PayoutsService, $mdDialog, ORDERS_PAYLOADS, OrdersService, OrdersActionsService, toastr, $window, $controller) {
    var vm = this;
    var AdvertisersController = $controller('AdvertisersController', {
      $scope: {}
    });
    var OrdersController = $controller('OrdersController');
    var PayoutsController = $controller('PayoutsController');
    var showConfirmDialog = OrdersController.showConfirmDialog;
    var showPromptDialog = OrdersController.showPromptDialog;
    var showNotesDialog = OrdersController.showNotesDialog;
    var payoutsResults = 0;

    $scope.agents = {};

    // Accessible attributes
    vm.accountsCount = 0;
    vm.showReportsMenu = true;
    vm.showAdvNoCampaignTable = false;
    vm.showAgingAdvAccountsTable = false;
    vm.showSalesTable = false;
    vm.showAgingOrdersTable = false;
    vm.showPayoutsTable = false;
    vm.showPubNoProfileTable = false;
    vm.days = [
      {title: 'All', value: 'all'},
      {title: 'Last 30 days', value: '30'},
      {title: 'Last 60 days', value: '60'},
      {title: 'Last 90 days', value: '90'}
    ];
    vm.newLeadsLimit = 20;
    vm.newLeadsPage = 1;
    vm.agingOrdersLimit = 10;
    vm.agingOrdersPage = 1;
    vm.payoutsLimit = 10;
    vm.payoutsPage = 1;
    vm.query = {
      filter: '',
      order: 'created_at',
      limit: 10,
      page: 1
    };

    // Functions
    vm.openAdvNoCampaignTable = openAdvNoCampaignTable;
    vm.openAgingAdvAccountsTable = openAgingAdvAccountsTable;
    vm.openSalesTable = openSalesTable;
    vm.openAgingOrdersTable = openAgingOrdersTable;
    vm.openPayoutsTable = openPayoutsTable;
    vm.openPubNoProfileTable = openPubNoProfileTable;
    vm.openNewLeadTable = openNewLeadTable;
    vm.resetTables = resetTables;
    vm.downloadAdvReport = downloadAdvReport;
    vm.downloadPublishersReport = downloadPublishersReport;
    vm.downloadSalesOrders = downloadSalesOrders;
    vm.filterAdvsByPeriod = filterAdvsByPeriod;
    vm.filterPublishersByPeriod = filterPublishersByPeriod;
    vm.filterSalesOrdersByDate = filterSalesOrdersByDate;
    vm.searchLateOrders = searchLateOrders;
    vm.searchAgingAdvertisers = searchAgingAdvertisers;
    vm.getAdvsNoCampaign = getAdvsNoCampaign;
    vm.getLPublishersNoProfile = getLPublishersNoProfile;
    vm.getSalesOrders = getSalesOrders;
    vm.getLateOrders = getLateOrders;
    vm.getAgingAdvertisers = getAgingAdvertisers;
    vm.getNewLeads = getNewLeads;
    vm.getAds = getAds;
    vm.showAgentConfirm = showAgentConfirm;
    vm.handleActionSelection = handleActionSelection;
    vm.pay = PayoutsController.pay;
    vm.cancel = PayoutsController.cancel;
    vm.showConfirmWithNotes = AdvertisersController.showConfirmWithNotes;
    vm.reActivate = AdvertisersController.reActivate;
    vm.showMessages = OrdersController.showMessages;

    activate();

    function activate() {
      var nowDate = new Date();
      $scope.orderStartEta = new Date(nowDate.setDate(nowDate.getDate() - 7));
      $scope.orderEndEta = new Date();
      $scope.selectedPeriod = vm.days[0];
      searchLateOrders(true);
      instantiateAds();
      getNewLeadsCount();
    }

    function instantiateAds() {
      if (angular.isDefined($window.ads)) {
        vm.ads = angular.fromJson($window.ads);
      }

      if (angular.isDefined($window.results)) {
        payoutsResults = parseInt($window.results);
      }

      if (angular.isDefined($window.payout_total)) {
        vm.payoutTotal = parseInt($window.payout_total);
      }
    }

    function toggleReportsMenu(value) {
      vm.showReportsMenu = value;
    }

    function addPagingInfo(page, limit) {
      vm.query.page = page;
      vm.query.limit = limit;
    }

    function downloadFile(url) {
      $window.open(url, '_blank');
    }

    function resetTables() {

      var nowDate = new Date();

      $scope.orderStartEta = new Date(nowDate.setDate(nowDate.getDate() - 7));
      $scope.orderEndEta = new Date();
      $scope.today = new Date();
      $scope.selectedPeriod = vm.days[0];
      vm.showAdvNoCampaignTable = false;
      vm.showAgingAdvAccountsTable = false;
      vm.showSalesTable = false;
      vm.showAgingOrdersTable = false;
      vm.showPayoutsTable = false;
      vm.showPubNoProfileTable = false;
      vm.showNewLeadTable = false;
      vm.newLeadsLimit = 20;
      vm.newLeadsPage = 1;
      vm.agingOrdersLimit = 10;
      vm.agingOrdersPage = 1;
      vm.payoutsLimit = 10;
      vm.payoutsPage = 1;
      vm.query = {
        filter: '',
        order: 'created_at',
        limit: 10,
        page: 1,
        search: false,
        end_date: null,
        start_date: null
      };

      toggleReportsMenu(true);
    }

    function openAdvNoCampaignTable() {
      resetTables();
      toggleReportsMenu(false);
      vm.showAdvNoCampaignTable = true;
      getAllAdvNoCampaign();
    }

    function openAgingAdvAccountsTable() {
      resetTables();
      toggleReportsMenu(false);
      vm.showAgingAdvAccountsTable = true;
      searchAgingAdvertisers();
    }

    function openSalesTable() {
      resetTables();
      toggleReportsMenu(false);
      vm.showSalesTable = true;
      filterSalesOrdersByDate();
    }

    function openAgingOrdersTable() {
      resetTables();
      toggleReportsMenu(false);
      vm.showAgingOrdersTable = true;
    }

    function openPayoutsTable() {
      resetTables();
      toggleReportsMenu(false);
      vm.showPayoutsTable = true;
      instantiateAds();
    }

    function openPubNoProfileTable() {
      resetTables();
      toggleReportsMenu(false);
      vm.showPubNoProfileTable = true;
      getAllPublishersNoProfile();
    }

    function openNewLeadTable() {
      resetTables();
      toggleReportsMenu(false);
      vm.showNewLeadTable = true;
      vm.accountsCount = 0;
      getNewLeads();
    }

    // Functions for 'Advertisers without a campaign approved' table
    function getAllAdvNoCampaign() {
      DashboardService.indexAllAdvertisers(vm.query, function (response) {
        var data = response.data;
        vm.avdsWithoutCamp = angular.fromJson(data.chosen_advertisers);
        vm.results = data.total_count;
        vm.agents = data.agents;
      });
    }

    function definePeriod() {
      if ($scope.selectedPeriod.value === 'all') {
        vm.query.end_date = null;
        vm.query.start_date = null;
      } else {
        var nowDate = new Date();
        vm.query.end_date = moment().format('YYYY-MM-DD');
        vm.query.start_date = moment(nowDate.setDate(nowDate.getDate() - $scope.selectedPeriod.value)).format('YYYY-MM-DD');
      }
    }

    function filterAdvsByPeriod() {
      if (!vm.showAdvNoCampaignTable) return;
      definePeriod();
      DashboardService.filterAdvsByPeriod(vm.query, function (response) {
        var data = response.data;
        vm.avdsWithoutCamp = angular.fromJson(data.chosen_advertisers);
        vm.results = data.total_count;
      });
    }

    function downloadAdvReport() {
      definePeriod();
      DashboardService.downloadAdvReport(vm.query, function (response) {
        downloadFile(response.data.urls_spreadsheet_doc.url);
      }, function (err) {
        $log.error(err);
      });
    }

    function getAdvsNoCampaign(page, limit) {
      addPagingInfo(page, limit);
      vm.filterAdvsByPeriod();
    }

    // Functions for 'Publishers without a profile approved' table
    function getAllPublishersNoProfile() {
      DashboardService.indexAllPublishers(vm.query, function (response) {
        var data = response.data;
        vm.publishers = angular.fromJson(data.chosen_publishers);
        vm.results = data.total_count;
      });
    }

    function downloadPublishersReport() {
      definePeriod();
      DashboardService.downloadPubsNoProfileReport(vm.query, function (response) {
        downloadFile(response.data.urls_spreadsheet_doc.url);
      }, function (err) {
        $log.error(err);
      });
    }

    function filterPublishersByPeriod() {
      if (!vm.showPubNoProfileTable) return;
      definePeriod();
      DashboardService.filterPublishersByPeriod(vm.query, function (response) {
        var data = response.data;
        vm.publishers = angular.fromJson(data.chosen_publishers);
        vm.results = data.total_count;
      });
    }

    function getLPublishersNoProfile(page, limit) {
      addPagingInfo(page, limit);
      filterPublishersByPeriod();
    }

    // Functions for 'Sales / commission' table
    function filterSalesOrdersByDate() {
      if (!vm.showSalesTable) return;
      vm.query.end_date = moment($scope.orderEndEta).format('YYYY-MM-DD');
      vm.query.start_date = moment($scope.orderStartEta).format('YYYY-MM-DD');
      DashboardService.indexSalesOrders(vm.query, function(response) {
        var data = response.data;
        vm.sales_orders = JSON.parse(data.orders);
        vm.results = data.total_count;
        vm.totalRevenue = data.total_amount;
      }, function (error) {
        toastr.error('', error.data.errors);
      });
    }

    function downloadSalesOrders() {
      DashboardService.downloadSalesOrders(vm.query, function (response) {
        downloadFile(response.data.urls_spreadsheet_doc.url);
      }, function (err) {
        $log.error(err);
      });
    }

    function getSalesOrders(page, limit) {
      addPagingInfo(page, limit);
      filterSalesOrdersByDate();
    }

    // Functions for 'Aging orders' table
    function searchLateOrders(initiateLateOrders) {
      var params = vm.query;
      params.limit = vm.agingOrdersLimit;
      params.page = vm.agingOrdersPage;
      DashboardService.indexLateOrders(params, function(response) {
        var data = response.data;
        if (initiateLateOrders) {
          vm.lateOrdersResults = data.results;
          vm.payoutsResults = payoutsResults;
        }
        vm.lateOrders = JSON.parse(data.orders);
        setUpActions();
      }, function (error) {
        toastr.error('', error.data.errors);
      });
    }

    function setUpActions() {
      _.forEach(vm.lateOrders, function (order) {
        order.actions = OrdersActionsService.getActions();
        order.actions.push({
          label: 'Reminder',
          payload: ORDERS_PAYLOADS.EMAIL_PUB
        });
        if(!order.ad.full_pub_url) {
          _.remove(order.actions, function(action) {
            return action.payload === ORDERS_PAYLOADS.APPROVE;
          });
        }
      });
    }

    function getLateOrders(page, limit) {
      addPagingInfo(page, limit);
      searchLateOrders();
    }

    function removeOrderFromTable(order) {
      vm.lateOrders = vm.lateOrders.filter(function(el) { return (el.id !== order.id) });
      if (vm.lateOrdersResults > 0) vm.lateOrdersResults -= 1;
    }

    function handleActionSelection(ev, order, payload) {
      switch (payload) {
        case ORDERS_PAYLOADS.APPROVE:
          showConfirmDialog(ev, order, {
            title: 'Approving Order?',
            content: 'Are you sure you want to approve this order?.',
            aria: 'approving'
          }, function (result) {
            // YES
            OrdersService.approveOrder(order.id, function (response) {
              // Success
              removeOrderFromTable(order);
            }, function (err) {
              // Error
              $log.error(err);
            });
          });
          break;
        case ORDERS_PAYLOADS.DECLINE:
          showPromptDialog(ev, order, {
            title: 'Declining Order?',
            content: 'Give reason why you are declining this!',
            aria: 'declining'
          }, function (message) {
            OrdersService.declineOrder(order.id, message, function (response) {
              // Success
              removeOrderFromTable(order);
            }, function (err) {
              // Error
              $log.error(err);
            });
          });
          break;
        case ORDERS_PAYLOADS.DOWNLOAD:
          downloadFile(order.ad.blog_post_doc.url);
          break;
        case ORDERS_PAYLOADS.REFUND:
          showNotesDialog(ev, order, {
            title: 'Refund Order?',
            content: "Are you sure you want to refund this order?. This will refund the advertiser and remove payout to publisher." +
                " The content will download automatically. Be sure to save the content.",
            aria: 'refunding'
          }, function (result) {
            OrdersService.refundOrder(order.id, result,  function (response) {
              // Success
              removeOrderFromTable(order);
            }, function (err) {
              // Error
              toastr.error('', err.data.error);
            });
          }, function() {
            //Dialog dismissed
          });
          break;
        case ORDERS_PAYLOADS.EMAIL_PUB:
          $mdDialog.show({
            controller: 'QuickEmailController',
            controllerAs: 'ctrl',
            templateUrl: 'quickEmailDialog.html',
            targetEvent: ev,
            clickOutsideToClose: false,
            locals: {
              resource: order
            }
          });
      }
      order.action = null;
    }

    // Functions for 'Aging advertisers account' table
    function searchAgingAdvertisers() {
      DashboardService.indexAgingAdvertisers(vm.query, function(response) {
        var data = response.data;
        vm.aging_advertisers = JSON.parse(data.chosen_advertisers);
        vm.results = data.total_count;
      }, function (error) {
        toastr.error('', error.data.errors);
      });
    }

    function getAgingAdvertisers(page, limit) {
      if (!vm.showAgingAdvAccountsTable) return;
      addPagingInfo(page, limit);
      vm.promise = DashboardService.indexAgingAdvertisers(page, limit, function (response) {
        var data = response.data;
        vm.aging_advertisers = JSON.parse(data.advertisers);
        vm.results = data.total_count;
      }).$promise;
    }

    // Functions for 'Payouts' table
    function getAds(page, limit) {
      var params = vm.query;
      params.page = page;
      params.limit = limit;
      PayoutsService.indexAds(params, function (response) {
        vm.ads = JSON.parse(response.data.ads);
      }, function (error) {
        $log.error(error);
      });
    }

    // Functions for 'New Lead' table
    function getNewLeadsCount() {
      DashboardService.getNewLeadsСount(function(response) {
        vm.accountsCount = response.data.count;
      }, function (error) {
        toastr.error('', error.data.errors);
      });
    }

    function getNewLeads() {
      var params = {
        limit: vm.newLeadsLimit,
        page: vm.newLeadsPage
      };
      DashboardService.getNewLeads(params, function (response) {
        var data = response.data;
        vm.newAccounts = JSON.parse(data.new_accounts);
        vm.agents = data.agents;
        vm.accountsResults = data.results;
      }, function (error) {
        toastr.error('', error.data.errors);
      });
    }

    function showAgentConfirm(ev, selectOpened, agentId, advertiser) {
      if (!selectOpened) return;

      var confirm = $mdDialog.confirm()
        .title('Are you sure?')
        .ariaLabel('Update agent')
        .targetEvent(ev)
        .ok('Yes')
        .cancel('No');

      $mdDialog.show(confirm).then(function (result) {
        AdvertisersService.reassignAgent(agentId, advertiser.id, function (response) {
          vm.newAccounts = vm.newAccounts.filter(function(el) { return (el.id !== advertiser.id) });
        }, function (error) {
          $log.error(error);
        })
      }, function () {
        $scope.agents[advertiser.id] = advertiser.agent;
      });
    }

  }
})();
