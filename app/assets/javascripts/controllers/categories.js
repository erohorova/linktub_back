(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('CategoriesController', CategoriesController);

  CategoriesController.$inject = ['$log', '$mdDialog', '$scope', '$timeout', 'campaign', 'categories'];

  /* @ngInject */
  function CategoriesController($log, $mdDialog, $scope, $timeout, campaign, categories) {
    var vm = this;

    vm.isSelected = isSelected;
    vm.toggleCategory = toggleCategory;
    vm.querySearch = querySearch;

    activate();

    function activate() {
      instantiateCategories();
    }

    function instantiateCategories() {
      vm.categories = categories;
      vm.selectedCategories = campaign.categories;
    }

    function isSelected(category) {
      return _.findIndex(vm.selectedCategories, ['name', category.name]) !== -1;
    }

    function toggleCategory(category) {
      var pos = _.findIndex(vm.selectedCategories, ['name', category.name]);
      if (pos < 0) {
        vm.selectedCategories.push(category)
      } else {
        vm.selectedCategories.splice(pos, 1);
      }
    }

    function querySearch(criteria) {
      var categoriesFiltered = vm.categories.filter(createFilterFor(criteria));
      return categoriesFiltered;
    }

    function createFilterFor(query) {
      var uppercaseQuery = query.toUpperCase();
      return function filterFn(category) {
        return category.name.indexOf(uppercaseQuery) !== -1;
      }
    }

    // Functions
    vm.cancel = function () {
      $mdDialog.cancel();
    }

    vm.submit = function () {
      $mdDialog.hide(vm.selectedCategories);
    }
  }
})();
