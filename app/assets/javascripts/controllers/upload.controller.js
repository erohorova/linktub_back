(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('UploadController', UploadController);

  UploadController.$inject = ['$log', '$mdDialog', '$rootScope'];

  /* @ngInject */
  function UploadController($log, $mdDialog, $rootScope) {
    var vm = this;
    // Setup to avoid everything except word documents with at most 25mb
    vm.fileExt = $rootScope.quoteUploading ? 'xlsx' : '.doc, .docx';
    vm.fileMax = '25MB';

    // Functions
    vm.getInputLabel = getInputLabel;

    vm.cancel = function () {
      $mdDialog.cancel();
    }

    vm.submit = function () {
      $mdDialog.hide(vm.file);
    }

    function getInputLabel() {
      return vm.file === null ? 'Select a file to upload' : 'Selected file name';
    }
  }
})();