(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('SocialHandlesController', SocialHandlesController);

  SocialHandlesController.$inject = ['$window', '$log', '$mdDialog', '$scope', '$timeout', '$mdEditDialog', 'toastr', 'SocialHandlesService'];

  /* @ngInject */
  function SocialHandlesController($window, $log, $mdDialog, $scope, $timeout, $mdEditDialog, toastr, SocialHandlesService) {
    var vm = this;

    // Accessible attributes
    vm.social_handles = [];
    vm.query = {
      filter: '',
      order: 'submitted_date',
      limit: 10,
      page: 1,
      search: false,
      scope: 'all'
    };
    vm.actions = [{
      id: 0,
      name: 'Approve',
      state: 'active'
    }, {
      id: 1,
      name: 'Reject',
      state: 'reject'
    }, {
      id: 2,
      name: 'Suspend',
      state: 'cancel'
    }];

    vm.states = [{
      label: 'all',
      payload: 'all'
    }, {
      label: 'pending',
      payload: 'pending'
    }, {
      label: 'Price Changed',
      payload: 'under_review'
    }, {
      label: 'paused',
      payload: 'paused'
    }, {
      label: 'active',
      payload: 'active'
    }, {
      label: 'rejected',
      payload: 'rejected'
    }, {
      label: 'resubmitted',
      payload: 'resubmitted'
    }, {
      label: 'suspended',
      payload: 'cancelled'
    }];

    //Accessible Functions
    vm.search = search;
    vm.getSocialHandles = getSocialHandles;
    vm.editPrice = editPrice;
    vm.performAction = performAction;
    vm.modifyScope = modifyScope;
    vm.isActive = isActive;

    activate();

    function activate() {
      instantiateLinks();
    }

    function instantiateLinks() {
      if (angular.isDefined(window.social_handles)) {
        vm.social_handles = angular.fromJson(window.social_handles);
      }

      if (angular.isDefined(window.results)) {
        vm.results = parseInt(window.results);
      }
    }

    function search() {
      SocialHandlesService.indexSocialHandles(vm.query, function (response) {
        var data = response.data;
        vm.social_handles = angular.fromJson(data.social_handles);
        vm.results = data.results;
      });
    }

    function getSocialHandles(page, limit) {
      vm.query.page = page;
      vm.query.limit = limit;
      vm.search();
    }

    function editPrice(ev, social_handle) {
      var editDialog = {
        modelValue: social_handle.price,
        placeholder: 'Price',
        save: function (input) {
          social_handle.price = input.$modelValue;
          SocialHandlesService.updateSocialHandle(social_handle, function (response) {
            var data = angular.fromJson(response.data.social_handle);
            social_handle.price = data.price;
          }, function (error) {
            $log.error(error);
          });
        },
        targetEvent: ev,
        title: 'Price',
        type: 'number',
        validators: {
          'aria-label': 'Edit price',
          'ng-required': 'true',
          'min': '0'
        },
        messages: {
          required: 'You must supply a price.',
          url: 'This is not a valid price.'
        }
      };
      var promise;
      promise = $mdEditDialog.large(editDialog).then(function (ctrl) {
        var input = ctrl.getInput();
        input.$viewChangeListeners.push(function () {
          input.$setValidity('price', angular.isDefined(social_handle.price));
        });
      });
    }

    function performAction(actionId, social_handle) {
      SocialHandlesService.changeState(social_handle.id, vm.actions[actionId].state, function (response) {
        vm.getSocialHandles(1, vm.query.limit);
        toastr.success('Successfully ' + vm.actions[actionId].name + ' social handle');
      }, function (error) {
        $log.error(error);
      });
    }

    function modifyScope() {
      vm.query.page = 1;
      vm.search();
    }

    function isActive() {
      return vm.query.scope === 'active';
    }
  }
})();
