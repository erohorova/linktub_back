(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('EmailsController', EmailsController);

  EmailsController.$inject = ['$log', '$window', '$mdDialog', '$mdEditDialog', '$scope', 'toastr', 'EmailService', 'EmailTemplateService'];

  /* @ngInject */
  function EmailsController($log, $window, $mdDialog, $mdEditDialog, $scope, toastr, EmailService, EmailTemplateService) {
    var vm = this;

    // Accessible attributes
    vm.emails = [];
    vm.emailTemplates = [];
    vm.getEmails = getEmails;
    vm.getEmailTemplates = getEmailTemplates;
    vm.query = {
      filter: '',
      order: '',
      limit: 10,
      page: 1,
      search: false
    };
    vm.search = search;
    vm.searchEmailTemplates = searchEmailTemplates;
    vm.promptNewEmail = promptNewEmail;
    vm.promptNewEmailTemplate = promptNewEmailTemplate;
    vm.promptEditEmailTemplate = promptEditEmailTemplate;
    vm.update = update;
    vm.destroy = destroy;
    vm.receiverTypes = ['AdminUser', 'Advertiser', 'Author', 'Publisher'];
    vm.destroyTemplate = destroyTemplate;

    initialize();

    function initialize() {
      instantiateEmails();
      instantiateEmail();
    }

    function instantiateEmails() {
      if (angular.isDefined(window.emails)) {
        vm.emails = angular.fromJson(window.emails);

        if (angular.isDefined(window.results)) {
          vm.results = parseInt(window.results);
        }
      } else {
        vm.emails = [];
      }
    }

    function instantiateEmail() {
      if (angular.isDefined(window.email)) {
        vm.email = angular.fromJson(window.email);

        if (angular.isDefined(window.emailTemplates)) {
          vm.emailTemplates = angular.fromJson(window.emailTemplates);
        }
        if (angular.isDefined(window.emailVars)) {
          vm.emailVars = angular.fromJson(window.emailVars);
        }
      } else {
        vm.email = {};
        vm.emailVars = {};
        vm.emailTemplates = [];
      }
    }

    function getEmails(page, limit) {
      vm.query.page = page;
      vm.query.limit = limit;
      vm.search();
    }

    function getEmailTemplates(emailId) {
      vm.searchEmailTemplates(emailId);
    }

    function search() {
      EmailService.indexEmail(vm.query, function (response) {
        var data = response.data;
        vm.emails = angular.fromJson(data.emails);
        vm.results = data.results;
      });
    }

    function searchEmailTemplates(emailId) {
      EmailTemplateService.indexEmailTemplate(emailId, function (response) {
        var data = response.data;
        vm.emailTemplates = angular.fromJson(data.email_templates);
        vm.results = data.results;
      });
    }

    function promptNewEmail(ev) {
      $mdDialog.show({
        controller: 'NewEmailController',
        controllerAs: 'ctrl',
        targetEvent: ev,
        clickOutsideToClose: false,
        templateUrl: 'newEmailDialog.html'
      }).then(function (response) {
        vm.getEmails();
      }, function () {});
    }

    function promptNewEmailTemplate(ev) {
      $mdDialog.show({
        controller: 'NewEmailTemplateController',
        controllerAs: 'ctrl',
        targetEvent: ev,
        clickOutsideToClose: false,
        templateUrl: 'newEmailTemplateDialog.html',
        locals: {
          email: vm.email
        }
      }).then(function (response) {
        vm.getEmailTemplates(vm.email.id);
      }, function () {});
    }

    function promptEditEmailTemplate(ev, emailTemplateId) {
      $mdDialog.show({
        controller: 'EditEmailTemplateController',
        controllerAs: 'ctrl',
        targetEvent: ev,
        clickOutsideToClose: false,
        templateUrl: 'newEmailTemplateDialog.html',
        locals: {
          emailTemplateId: emailTemplateId
        }
      }).then(function (response) {
        vm.getEmailTemplates(vm.email.id);
      }, function () {});
    }

    function update(form) {
      EmailService.updateEmail(vm.email.id, vm.email, function (response) {
        $mdDialog.hide(response);
        toastr.success('Email successfuly updated', 'Success!');
      }, function (error) {
        $log.error(error);
      });
    }

    function destroy(emailId, ev) {
      var confirm = $mdDialog.confirm()
        .title('Are you sure?')
        .textContent('You are removing this for good.')
        .targetEvent(ev)
        .ariaLabel('decline')
        .ok('Yes')
        .cancel('No');

      $mdDialog.show(confirm).then(function () {
        EmailService.destroyEmail(emailId, function (response) {
          toastr.success(response.data.message);
          $window.location.href = '/admin/emails';
        }, function (err) {
          $log.error(err);
        });
      }, function () {
        // Cancelled
      });
    }

    function destroyTemplate(emailId, emailTemplateId, ev) {
      var confirm = $mdDialog.confirm()
        .title('Are you sure?')
        .textContent('You are removing this for good.')
        .targetEvent(ev)
        .ariaLabel('decline')
        .ok('Yes')
        .cancel('No');

      $mdDialog.show(confirm).then(function (note) {
        EmailTemplateService.destroyEmailTemplate(emailTemplateId, function (response) {
          toastr.success(response.data.message);
          $window.location.href = '/admin/emails/' + emailId;
        }, function (err) {
          $log.error(err);
        });
      }, function () {
        // Cancelled
      });
    }
  }
})();
