(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('PayoutsController', PayoutsController);

  PayoutsController.$inject = ['$log', '$window', '$mdDialog', '$filter', 'toastr', 'PayoutsService'];

  /* @ngInject */
  function PayoutsController($log, $window, $mdDialog, $filter, toastr, PayoutsService) {
    var vm = this;

    vm.ads = [];
    vm.query = {
      filter: '',
      order: 'approved_date',
      limit: 10,
      page: 1,
      search: false
    };

    vm.pay = pay;
    vm.cancel = cancel;
    vm.getAds = getAds;

    activate();

    function activate() {
      instantiateAds();
    }

    function instantiateAds() {
      if (angular.isDefined($window.ads)) {
        vm.ads = angular.fromJson($window.ads);
      }

      if (angular.isDefined($window.results)) {
        vm.results = parseInt($window.results);
      }

      if (angular.isDefined($window.payout_total)) {
        vm.payoutTotal = parseInt($window.payout_total);
      }
    }

    function pay(ad, index, ev) {
      var payment_email = ad.publisher.payment_method === 'skrill' ? ad.publisher.skrill_email : ad.publisher.paypal_email
      var confirm = $mdDialog.confirm()
        .title('Are you sure?')
        .textContent('You are about to pay ' +
          $filter('currency')(ad.url.price, '$', '0') + ' to ' + payment_email + '.')
        .targetEvent(ev)
        .ariaLabel('decline')
        .ok('Yes')
        .cancel('No');

      $mdDialog.show(confirm).then(function (note) {
        PayoutsService.pay(ad.id, function (response) {
          toastr.success(response.data.message, '', {
            timeOut: 1000
          });
          vm.ads.splice(index, 1);
          vm.payoutTotal = response.data.payout_total;
        }, function (response) {
          toastr.error(response.data.error);
          $log.error(response);
        });
      }, function () {
        // Cancelled
      });
    }

    function cancel(adId, index, ev) {
      var confirm = $mdDialog.confirm()
        .title('Are you sure?')
        .textContent('You are canceling this for good.')
        .targetEvent(ev)
        .ariaLabel('decline')
        .ok('Yes')
        .cancel('No');

      $mdDialog.show(confirm).then(function (note) {
        PayoutsService.cancel(adId, function (response) {
          toastr.success(response.data.message, '', {
            timeOut: 1000
          });
          vm.ads.splice(index, 1);
          vm.payoutTotal = response.data.payout_total;
        }, function (response) {
          toastr.error(response.data.error);
          $log.error(response);
        });
      }, function () {
        // Cancelled
      });
    }

    function getAds(page, limit) {
      vm.query.page = page;
      vm.query.limit = limit;

      PayoutsService.indexAds(vm.query, function (response) {
        vm.ads = JSON.parse(response.data.ads);
      }, function (error) {
        $log.error(error);
      });
    }
  }
})();
