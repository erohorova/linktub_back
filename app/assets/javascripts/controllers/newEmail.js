(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('NewEmailController', NewEmailController);

  NewEmailController.$inject = ['$mdDialog', '$log', 'toastr', 'EmailService'];

  /* @ngInject */
  function NewEmailController($mdDialog, $log, toastr, EmailService) {
    var vm = this;

    // Accessible attributes
    vm.email = {
      response_required: false,
      active: true
    }
    vm.receiverTypes = ['AdminUser', 'Advertiser', 'Author', 'Publisher']

    // Accessible functions
    vm.cancel = function () {
      $mdDialog.cancel();
    }

    vm.submit = function (form) {
      EmailService.createEmail(vm.email, function (response) {
        $mdDialog.hide(response);
        toastr.success('Email successfuly created', 'Success!');
      }, function (error) {
        $log.error(error);
      });
    }
  }
})();
