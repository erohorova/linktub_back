(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('NewAuthorController', NewAuthorController);

  NewAuthorController.$inject = ['$mdDialog', 'AuthorsService'];

  /* @ngInject */
  function NewAuthorController($mdDialog, AuthorsService) {
    var vm = this;

    // Accessible attributes
    vm.author = {}

    // Accessible functions
    vm.cancel = function () {
      $mdDialog.cancel();
    }

    vm.refreshErrors = function (form, input) {
      if (form.repeatPassword.$invalid && input === 'repeatPassword') {
        form.repeatPassword.$setValidity('no-match', true);
      }
      if (form.email.$invalid && input === "email") {
        form.email.$setValidity('emailtaken', true);
      }
    }

    vm.submit = function (form) {
      if (!vm.isSamePassword()) {
        form.repeatPassword.$setValidity('no-match', false);
        vm.change = true;
      } else {
        AuthorsService.createAuthor(vm.author, function (response) {
          $mdDialog.hide(response);
        }, function (error) {
          if (angular.isDefined(error.data)) {
            if (angular.isDefined(error.data.email)) {
              form.email.$setValidity('emailtaken', false);
            }
          }
        });
      }
    }

    vm.isSamePassword = function () {
      return vm.author.password === vm.author.repeat_password;
    }
  }
})();