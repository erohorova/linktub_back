(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('AuthorInvoicesController', AuthorInvoicesController);

  AuthorInvoicesController.$inject = ['$log', '$window', '$mdDialog', 'toastr', 'AuthorInvoiceService'];

  /* @ngInject */
  function AuthorInvoicesController($log, $window, $mdDialog, toastr, AuthorInvoiceService) {
    var vm = this;

    vm.author_invoices = [];
    vm.query = {
      filter: '',
      order: 'created_at',
      limit: 10,
      page: 1,
      search: false
    };

    vm.createInvoice = createInvoice;
    vm.getInvoices = getInvoices;
    vm.search = search;
    vm.showOnlineDocument = showOnlineDocument;
    vm.downloadDocument = downloadDocument;

    activate();

    function activate() {
      instantiateAuthorInvoices();
    }

    function instantiateAuthorInvoices() {
      if (angular.isDefined($window.author_invoices)) {
        vm.author_invoices = angular.fromJson($window.author_invoices);
      }

      if (angular.isDefined($window.results)) {
        vm.results = parseInt($window.results);
      }
    }

    function search() {
      AuthorInvoiceService.indexInvoices(vm.query, function (response) {
        var data = response.data;
        vm.author_invoices = angular.fromJson(data.author_invoices);
        vm.results = data.results;
      });
    }

    function getInvoices(page, limit) {
      vm.query.page = page;
      vm.query.limit = limit;
      vm.search();
    }

    function createInvoice(ev) {
      // Appending dialog to document.body to cover sidenav in docs app
      var confirm = $mdDialog.confirm()
        .title('Are you sure?')
        .textContent('Your invoice is created with your approved articles since last time.')
        .ariaLabel('invoice')
        .targetEvent(ev)
        .ok('Yes!')
        .cancel('Cancel');

      $mdDialog.show(confirm).then(function () {
        AuthorInvoiceService.createInvoice(function (response) {
          toastr.success('Invoice created!');
          vm.search();
        }, function (err) {
          $log.error(err);
        })
      }, function () {
        // Cancelled, added this callback to prevent warning
        // TODO: Improve this callback
      });
    }

    function showOnlineDocument(invoice) {
      $window.open("https://docs.google.com/viewer?url=" + invoice.attached_report.url, '_blank');
    }

    function downloadDocument(invoice) {
      var fileUrl = invoice.attached_report.url;
      fileUrl = fileUrl.indexOf('.xlsx?') !== -1 ? fileUrl.split('?')[0] : fileUrl;
      $window.open(fileUrl, '_blank');
    }
  }
})();
