(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('OrdersController', OrdersController);

  OrdersController.$inject = ['$log', '$window', '$mdDialog', 'toastr', 'OrdersActionsService', 'OrdersService', 'ORDERS_PAYLOADS'];

  /* @ngInject */
  function OrdersController($log, $window, $mdDialog, toastr, OrdersActionsService, OrdersService, ORDERS_PAYLOADS) {
    var vm = this;

    vm.orders = [];
    vm.results = 0;
    vm.query = {
      filter: '',
      order: 'created_at',
      limit: 10,
      page: 1,
      search: false
    };

    vm.gettingOld = gettingOld;
    vm.handleActionSelection = handleActionSelection;
    vm.showMessages = showMessages;
    vm.search = search;
    vm.getOrders = getOrders;
    vm.showConfirmDialog = showConfirmDialog;
    vm.showPromptDialog = showPromptDialog;
    vm.showNotesDialog = showNotesDialog;

    activate();

    function activate() {
      instantiateOrders();
      vm.actions = OrdersActionsService.getActions();
    }

    function getOrders(page, limit) {
      vm.query.page = page;
      vm.query.limit = limit;
      vm.search();
    }

    function search() {
      OrdersService.indexOrders(vm.query, function(response) {
        vm.orders = JSON.parse(response.data.orders);
        vm.results = response.data.results;
        setUpActions();
      }, function (error) {
        toastr.error('', error.data.errors);
      });
    }

    function instantiateOrders() {
      if (angular.isDefined(window.orders)) {
        vm.orders = angular.fromJson(window.orders);
        setUpActions();
      }

      if (angular.isDefined(window.results)) {
        vm.results = parseInt(window.results);
      }
    }

    function setUpActions() {
      _.forEach(vm.orders, function (order) {
        order.actions = OrdersActionsService.getActions();
        if (gettingOld(order.ad.approved_date)) {
          order.actions.push({
            label: 'Reminder',
            payload: ORDERS_PAYLOADS.EMAIL_PUB
          });
        }

        if(!order.ad.full_pub_url) {
          _.remove(order.actions, function(action) {
            return action.payload === ORDERS_PAYLOADS.APPROVE;
          });
        }
      });
    }

    function gettingOld(date) {
      return date ? (moment(moment()).diff(moment(date), 'days') >= 15) : false;
    }

    function handleActionSelection(ev, order, payload) {
      switch (payload) {
      case ORDERS_PAYLOADS.APPROVE:
        showConfirmDialog(ev, order, {
          title: 'Approving Order?',
          content: 'Are you sure you want to approve this order?.',
          aria: 'approving'
        }, function (result) {
          // YES
          OrdersService.approveOrder(order.id, function (response) {
            // Success
            $window.location.href = '/admin/orders';
          }, function (err) {
            // Error
            $log.error(err);
          });
        });
        break;
      case ORDERS_PAYLOADS.DECLINE:
        showPromptDialog(ev, order, {
          title: 'Declining Order?',
          content: 'Give reason why you are declining this!',
          aria: 'declining'
        }, function (message) {
          OrdersService.declineOrder(order.id, message, function (response) {
            // Success
            $window.location.href = '/admin/orders';
          }, function (err) {
            // Error
            $log.error(err);
          });
        });
        break;
      case ORDERS_PAYLOADS.DOWNLOAD:
        $window.open(order.ad.blog_post_doc.url, '_blank');
        break;
      case ORDERS_PAYLOADS.REFUND:
        showNotesDialog(ev, order, {
          title: 'Refund order?',
          content: "Are you sure you want to refund this order?. This will refund the advertiser and remove payout to publisher." +
            " The content will download automatically. Be sure to save the content.",
          aria: 'refunding'
        }, function (result) {
          OrdersService.refundOrder(order.id, result,  function (response) {
            // Success
            $window.location.href = '/admin/orders';
          }, function (err) {
            // Error
            toastr.error('', err.data.error);
          });
        }, function() {
          //Dialog dismissed
        });
        break;
      case ORDERS_PAYLOADS.EMAIL_PUB:
        $mdDialog.show({
          controller: 'QuickEmailController',
          controllerAs: 'ctrl',
          templateUrl: 'quickEmailDialog.html',
          targetEvent: ev,
          clickOutsideToClose: false,
          locals: {
            resource: order
          }
        });
      }
      order.action = null;
    }

    function showPromptDialog(ev, order, info, successCallback, cancelCallback) {
      $mdDialog.show({
        controller: 'DeclineOrderNoteController',
        controllerAs: 'ctrl',
        templateUrl: 'declineOrderNoteDialog.html',
        targetEvent: ev,
        clickOutsideToClose: false,
        locals: {
          attrs: info
        }
      }).then(function (result) {
        successCallback ? successCallback(result) : null;
      }, function () {
        cancelCallback ? cancelCallback : null;
      });
    }

    function showNotesDialog(ev, order, info, successCallback, cancelCallback) {
      $mdDialog.show({
        controller: 'RefundNotesController',
        controllerAs: 'ctrl',
        templateUrl: 'refundNotesDialog.html',
        targetEvent: ev,
        clickOutsideToClose: false,
        locals: {
          attrs: info
        }
      }).then(successCallback, cancelCallback);
    }

    function showConfirmDialog(ev, order, info, successCallback, cancelCallback) {
      var confirm = $mdDialog.confirm()
        .title(info.title)
        .htmlContent(info.content)
        .ariaLabel(info.aria)
        .targetEvent(ev)
        .ok('Yes')
        .cancel('No');

      $mdDialog.show(confirm).then(function (result) {
        successCallback ? successCallback(result) : null;
      }, function () {
        cancelCallback ? cancelCallback : null;
      });
    }

    function showMessages(ev, order) {
      if (order.ad && order.ad.comments.length > 0) {
        $mdDialog.show({
          controller: 'OrderNotesController',
          controllerAs: 'ctrl',
          templateUrl: 'orderNotesDialog.html',
          targetEvent: ev,
          clickOutsideToClose: false,
          locals: {
            attrs: {
              ad: order.ad,
              allowInput: false,
              title: 'Notes'
            }
          }
        });
      } else {
        toastr.info('There aren\'t comments for this order!');
      }
    }
  }
})();
