(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('CampaignsController', CampaignsController);

  CampaignsController.$inject = ['$log', '$mdDialog', 'toastr', 'CampaignService'];

  /* @ngInject */
  function CampaignsController($log, $mdDialog, toastr, CampaignService) {
    var vm = this;

    vm.campaigns = [];
    vm.query = {
      filter: '',
      order: 'created_at', // from the oldest to the newest
      limit: 10,
      page: 1,
      search: false
    };

    vm.search = search;
    vm.getCampaigns = getCampaigns;
    vm.approve = approve;
    vm.decline = decline;
    vm.showCategories = showCategories;

    activate();

    function activate() {
      instantiateCampaigns();
      instantiateCategories();
    }

    function instantiateCampaigns() {
      if (angular.isDefined(window.campaigns)) {
        vm.campaigns = angular.fromJson(window.campaigns);
      }

      if (angular.isDefined(window.results)) {
        vm.results = parseInt(window.results);
      }
    }

    function instantiateCategories() {
      if (angular.isDefined(window.categories)) {
        vm.categories = angular.fromJson(window.categories);
      }
    }

    function showCategories(ev, campaign) {
      $mdDialog.show({
          controller: 'CategoriesController',
          controllerAs: 'ctrl',
          templateUrl: 'categoriesDialog.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose: true,
          locals: {
            campaign: campaign,
            categories: vm.categories
          }
        })
        .then(function (categories) {
          CampaignService.updateCampaign(campaign.id, {
            categories: categories
          }, function (response) {
            toastr.success('The campaign has been updated successfuly!', 'Successful');
          }, function (response) {
            $log.error('Unable to update!');
          })
        }, function () {
          // Action cancelled
        });
    }

    function approve(ev, campaign) {
      var confirm = $mdDialog.confirm()
        .title('Are you sure?')
        .textContent('You are about to approve this campaign.')
        .ariaLabel('approve campaign')
        .clickOutsideToClose(true)
        .targetEvent(ev)
        .ok('Approve')
        .cancel('Cancel');

      $mdDialog.show(confirm).then(function (result) {
        // Approve
        CampaignService.approveCampaign(campaign.id, function (result) {
          vm.getCampaigns(1, vm.query.limit);
          toastr.success('The campaign has been approved.', 'Success!');
        }, function () {
          toastr.error('The campaign has already been approved!');
        });
      }, function () {
        // Action cancelled
      });
    }

    function decline(ev, campaign) {
      var confirm = $mdDialog.confirm()
        .title('Are you sure?')
        .textContent('You are about to decline this campaign.')
        .ariaLabel('decline campaign')
        .clickOutsideToClose(true)
        .targetEvent(ev)
        .ok('Decline')
        .cancel('Cancel');

      $mdDialog.show(confirm).then(function (result) {
        // Decline
        CampaignService.declineCampaign(campaign.id, function (result) {
          vm.getCampaigns(1, vm.query.limit);
          toastr.success('The campaign has been declined.', 'Success!');
        }, function () {
          toastr.error('The campaign has already been declined!');
        });
      }, function () {
        // Action cancelled
      });
    }

    function getCampaigns(page, limit) {
      vm.query.page = page;
      vm.query.limit = limit;
      vm.search();
    }

    function search() {
      CampaignService.indexCampaign(vm.query.page, vm.query.limit, vm.query.filter, function (response) {
        var data = response.data;
        vm.campaigns = angular.fromJson(data.campaigns);
        vm.results = data.results;
      });
    }
  }
})();
