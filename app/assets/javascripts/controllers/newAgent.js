(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('NewAgentController', NewAgentController);

  NewAgentController.$inject = ['$mdDialog', 'AgentsService'];

  /* @ngInject */
  function NewAgentController($mdDialog, AgentsService) {
    var vm = this;

    // Accessible attributes
    vm.agent = {};

    // Accessible functions
    vm.cancel = function () {
      $mdDialog.cancel();
    };

    vm.refreshErrors = function (form, input) {
      if (form.repeatPassword.$invalid && input === 'repeatPassword') {
        form.repeatPassword.$setValidity('no-match', true);
      }
      if (form.email.$invalid && input === "email") {
        form.email.$setValidity('emailtaken', true);
      }
    };

    vm.submit = function (form) {
      if (!vm.isSamePassword()) {
        form.repeatPassword.$setValidity('no-match', false);
        vm.change = true;
      } else {
        AgentsService.createAgent(vm.agent, function (response) {
          $mdDialog.hide(response);
        }, function (error) {
          if (angular.isDefined(error.data)) {
            if (angular.isDefined(error.data.email)) {
              form.email.$setValidity('emailtaken', false);
            }
          }
        });
      }
    }

    vm.isSamePassword = function () {
      return vm.agent.password === vm.agent.repeat_password;
    }
  }
})();