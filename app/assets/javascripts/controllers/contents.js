(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('ContentsController', ContentsController);

  ContentsController.$inject = ['$window', '$mdDialog', '$scope', '$timeout', '$mdEditDialog', '$log', 'toastr', 'ContentService', 'UrlsService', 'ArticlesService', 'CONTENT_STATES', 'URL_STATES', '$rootScope'];

  /* @ngInject */
  function ContentsController($window, $mdDialog, $scope, $timeout, $mdEditDialog, $log, toastr, ContentService, UrlsService, ArticlesService, CONTENT_STATES, URL_STATES, $rootScope) {
    var vm = this;

    // Accessible attributes
    vm.contentsUrl = '/admin/contents';
    vm.showNewUrls = false;
    vm.quotes = [];
    vm.query = {
      filter: '',
      order: 'created_at',
      limit: 10,
      page: 1,
      search: false
    };
    vm.queryAssigned = {
      filter: '',
      order: 'created_at',
      limit: 10,
      page: 1,
      search: false
    };
    vm.querySelected = {
      filter: '',
      order: '-domain_authority',
      limit: 5,
      page: 1,
      search: false
    };
    vm.queryNew = {
      filter: '',
      order: '-domain_authority',
      limit: 100,
      page: 1,
      search: false
    };
    vm.categories = [];
    vm.selectedCategories = [];
    vm.domainAuthority = {
      options: {
        floor: 10,
        ceil: 100,
        step: 10,
        minRange: 10,
        noSwitching: true,
        hideLimitLabels: true,
        hidePointerLabels: true,
        showTicksValues: true
      }
    };
    vm.trustFlow = {
      options: {
        floor: 10,
        ceil: 100,
        step: 10,
        minRange: 10,
        noSwitching: true,
        hideLimitLabels: true,
        hidePointerLabels: true,
        showTicksValues: true
      }
    };
    vm.ahrefsDr = {
      options: {
        floor: 0,
        ceil: 100,
        step: 10,
        minRange: 10,
        noSwitching: true,
        hideLimitLabels: true,
        hidePointerLabels: true,
        showTicksValues: true
      }
    };
    vm.selected = [];
    vm.aditional_keywords = [];
    vm.searchText = '';

    // Functions
    vm.refreshSlider = refreshSlider;
    vm.querySearchPublishers = querySearchPublishers;
    vm.querySearchUrls = querySearchUrls;
    vm.search = search;
    vm.getQuotes = getQuotes;
    vm.getAssignedQuotes = getAssignedQuotes;
    vm.contentSearch = contentSearch;
    vm.modifyCampaignList = modifyCampaignList;
    vm.getQuoteUrls = getQuoteUrls;
    vm.querySearchCampaign = querySearchCampaign;
    vm.placements = placements;
    vm.queryCategories = queryCategories;
    vm.modifyPrices = modifyPrices;
    vm.editPrice = editPrice;
    vm.createQuotes = createQuotes;
    vm.sendToContent = sendToContent;
    vm.createCartItems = createCartItems;
    vm.selectedAmount = selectedAmount;
    vm.getStateIcon = getStateIcon;
    vm.getStateTooltip = getStateTooltip;
    vm.promptNewQuote = promptNewQuote;
    vm.transformAdditionalKeyword = transformAdditionalKeyword;
    vm.deleteQuote = deleteQuote;
    vm.pauseUrl = pauseUrl;
    vm.isActive = isActive;
    vm.downloadSelectedUrls = downloadSelectedUrls;
    vm.uploadFile = uploadFile;

    activate();

    function activate() {
      instantiateQuote();
      instantiateQuotes();
      instantiateCategories();
      instantiatePublishers();
      instantiateCampaigns();
      instantiateUrls();
      refreshSlider();
    }

    function getStateIcon(state) {
      switch (state) {
      case CONTENT_STATES.REQUESTED:
        return 'hourglass_empty';
      case CONTENT_STATES.RECEIVED:
        return 'check_circle';
      default:
        return 'help'
      }
    }

    function getStateTooltip(state) {
      switch (state) {
      case CONTENT_STATES.REQUESTED:
        return 'Click Search to add URLs';
      case CONTENT_STATES.RECEIVED:
        return 'Content already assigned';
      default:
        return 'help'
      }
    }

    function promptNewQuote(ev) {
      $mdDialog.show({
        controller: 'NewContentController',
        controllerAs: 'dctrl',
        targetEvent: ev,
        clickOutsideToClose: false,
        templateUrl: 'newContentDialog.html'
      }).then(function (response) {
        vm.quotes.splice(0, 0, angular.fromJson(response.data.content));
      }, function () {
        // Cancelled, added this callback to prevent warning
        // TODO: Improve this callback
      });
    }

    function instantiateCampaigns() {
      if (angular.isDefined(window.campaigns)) {
        vm.campaigns = angular.fromJson(window.campaigns);
      }
    }

    function instantiateQuote() {
      if (angular.isDefined(window.quote)) {
        vm.quote = angular.fromJson(window.quote);
        if (angular.isDefined(vm.quote)) {
          vm.selectedCategories = vm.quote.categories;
          _.map(vm.quote.content_urls, function (content_url) {
            var url = _.find(vm.quote.urls, function (url) {
              return url.id === content_url.url_id
            });
            url.shown_price = content_url.shown_price;
          })
        }
      }
    }

    function selectedAmount() {
      return _.reduce(_.flatMap(vm.selected, 'shown_price'), function (sum, n) {
        return sum + n;
      }, 0);
    }

    function editPrice(ev, url) {
      var promise = $mdEditDialog.small({
        modelValue: url.shown_price,
        placeholder: 'Change the price',
        save: function (input) {
          url.shown_price = input.$modelValue;
        },
        targetEvent: ev,
        type: 'number',
        validators: {
          'aria-label': 'Edit price',
          'min': url.price
        }
      });

      promise.then(function (ctrl) {
        var input = ctrl.getInput();

        input.$viewChangeListeners.push(function () {
          input.$setValidity('test', input.$modelValue !== 'test');
        });
      });
    }

    function createQuotes(ev, selectedUrls) {
      if (!canBeSubmitted(selectedUrls)) {
        toastr.error('Check your selected URLs', 'Empty or duplicated campaigns');
      } else {
        ContentService.assign(vm.quote.id, {
          selectedUrls: selectedUrls
        }, function () {
          toastr.success('The URLs have been assigned', 'Successful!');
          $window.location.href = vm.contentsUrl;
        })
      }
    }

    function canBeSubmitted(selectedUrls) {
      var all, notRepeated;
      var canBeSubmitted = _.every(selectedUrls, function (url) {
        all = _.every(url.campaigns, function (campaign) {
          return angular.isDefined(campaign) && campaign !== null;
        });
        notRepeated = _.uniqBy(url.campaigns, 'id').length === url.campaigns.length;
        return all && notRepeated;
      });

      return canBeSubmitted;
    }

    function sendToContent(ev, selectedUrls) {
      if (!canBeSubmitted(selectedUrls)) {
        toastr.error('Check your selected URLs', 'Empty or duplicated campaigns');
      } else {
        ContentService.assign(vm.quote.id, {
          selectedUrls: selectedUrls
        }, function () {
          ArticlesService.bulkCreate(vm.quote.id, selectedUrls, function (response) {
            toastr.success('The URLs have been sent to content', 'Successful!');
            $window.location.href = vm.contentsUrl;
          }, function (error) {
            toastr.error(error.data.errors);
          });
        });
      }
    }

    function createCartItems(ev, selectedUrls) {
      var all, notRepeated;
      var canBeSubmitted = _.every(selectedUrls, function (url) {
        all = _.every(url.campaigns, function (campaign) {
          return angular.isDefined(campaign) && campaign !== null;
        });
        notRepeated = _.uniqBy(url.campaigns, 'id').length === url.campaigns.length;
        return all && notRepeated;
      });
      if (!canBeSubmitted) {
        toastr.error('Check your selected URLs', 'Empty or duplicated campaigns');
      } else {
        ContentService.createOrder(vm.quote.id, {
          selectedUrls: selectedUrls
        }, function () {
          toastr.success('The URLs have moved to cart item', 'Successful!');
          $window.location.href = vm.contentsUrl;
        })
      }
    }

    function modifyPrices() {
      var change = vm.quote.agency_content ? -50 : 50;
      ContentService.updateAgencyContent(vm.quote.id, vm.quote, function (response) {
        _.map(vm.queryUrls, function (url) {
          url.shown_price += change;
        });
      }, function (error) {
        vm.quote.agency_content = !vm.quote.agency_content;
        $log.error(error);
      });
    }

    function queryCategories(query) {
      var categoriesFiltered = vm.categories.filter(createFilterForCategories(query));
      categoriesFiltered.map(function (item) {
        var uppercaseQuery = query.toUpperCase();
        var upperKeyword = item.keyword && item.keyword.toUpperCase();
        if (upperKeyword && (upperKeyword.indexOf(uppercaseQuery) !== -1)) {
          item.byKeywords = true;
          var keywordArray = item.keyword.split(', ');
          keywordArray = keywordArray.filter(keywordsFilter(query));
          keywordArray.forEach(function (keyItem) {
            var currentItem = Object.assign({}, item);
            currentItem.activeKey = currentItem.name;
            currentItem.name = keyItem;
            currentItem.keepCase = keyItem;
            currentItem.name = currentItem.name.toUpperCase();
            delete currentItem.$$hashKey;
            categoriesFiltered.push(currentItem);
          });
          categoriesFiltered = categoriesFiltered.filter(function (element) {
            return (!element.byKeywords || element.activeKey)
          });
        }
      });
      return categoriesFiltered;
    }

    function createFilterForCategories(query) {
      var uppercaseQuery = query.toUpperCase();
      return function filterFn(category) {
        var uppercaseKeyword = category.keyword && category.keyword.toUpperCase();
        var nameResult = category.name && category.name.indexOf(uppercaseQuery) !== -1;
        var keyResult = uppercaseKeyword && uppercaseKeyword.indexOf(uppercaseQuery) !== -1;
        return nameResult || keyResult;
      }
    }

    function keywordsFilter(query) {
      var uppercaseQuery = query.toUpperCase();
      return function filterFunction(keyword) {
        var upperKeyword = keyword && keyword.toUpperCase();
        return upperKeyword.indexOf(uppercaseQuery) !== -1;
      }
    }

    function instantiateCategories() {
      if (angular.isDefined(window.categories)) {
        vm.categories = angular.fromJson(window.categories);
      }
    }

    function instantiatePublishers() {
      if (angular.isDefined(window.publishers)) {
        vm.publishers = angular.fromJson(window.publishers);
      }
    }

    function instantiateQuotes() {
      if (angular.isDefined(window.quotes)) {
        vm.quotes = angular.fromJson(window.quotes);
      }

      if (angular.isDefined(window.assigned_quotes)) {
        vm.assignedQuotes = angular.fromJson(window.assigned_quotes);
      }

      if (angular.isDefined(window.results)) {
        vm.results = parseInt(window.results);
      }

      if (angular.isDefined(window.assignedResults)) {
        vm.assignedResults = parseInt(window.assignedResults);
      }
    }

    function instantiateUrls() {
      if (angular.isDefined(window.urls)) {
        vm.urls = angular.fromJson(window.urls);
      }
    }

    function placements() {
      return _.reduce(_.flatMap(vm.selected, function (selected) {
        return selected.campaigns.length;
      }), function (sum, n) {
        return sum + n;
      }, 0);
    }

    function modifyCampaignList(url) {
      if (url.repeats > 20) {
        toastr.warning('No more than 20 campaigns per URL');
        url.repeats = 20;
      }
      url.campaigns = [];
      for (var i = 0; i < url.repeats; i++) {
        var campaign = angular.copy(vm.quote.campaign);
        campaign.searchTextUrl = null;
        url.campaigns.push(campaign);
      }
    }

    function getQuoteUrls(page, limit) {
      vm.querySelected.page = page;
      vm.querySelected.limit = limit;
      return vm.quotes.urls;
    }

    function search() {
      ContentService.indexContent(vm.query.page, vm.query.limit, vm.query.filter, function (response) {
        var data = response.data;
        vm.quotes = angular.fromJson(data.quotes);
        vm.results = data.results;
      });
    }

    function searchAssigned() {
      ContentService.indexContent(vm.queryAssigned, function (response) {
        var data = response.data;
        vm.assignedQuotes = angular.fromJson(data.quotes);
        vm.assignedResults = data.results;
      });
    }

    function contentSearch() {
      var categories = _.filter(vm.selectedCategories, function (category) {
        return !category.byKeywords;
      });
      categories = _.map(categories, function (category) {
        return category.name;
      });
      var keywords = (vm.aditional_keywords.length === 0) ? '' : vm.aditional_keywords;
      var words = _.concat(categories, keywords);
      var publisherId = vm.publisher !== null ? vm.publisher.id : null;
      var urlId = vm.url !== null ? vm.url.id : null;
      ContentService.filter(vm.quote.id, {
        publisher_id: publisherId,
        url_id: urlId,
        words: words,
        keywords: keywords,
        domain_authority_min: vm.quote.domain_authority_min,
        domain_authority_max: vm.quote.domain_authority_max,
        trust_flow_min: vm.quote.trust_flow_min,
        trust_flow_max: vm.quote.trust_flow_max,
        ahrefs_dr_min: vm.quote.ahrefs_dr_min,
        ahrefs_dr_max: vm.quote.ahrefs_dr_max
      }, function (response) {
        vm.queryUrls = angular.fromJson(response.data.urls);
        angular.forEach(vm.queryUrls, function (url) {
          var campaign = angular.copy(vm.quote.campaign);
          campaign.searchTextUrl = null;
          url.campaigns = [campaign];
          url.shown_price = vm.quote.agency_content ? url.shown_price - 50 : url.shown_price;
        });
        vm.showNewUrls = true;
      })
    }

    function querySearchPublishers(query) {
      var results = query ? vm.publishers.filter(createFilterForPublisher(query)) : vm.publishers;
      return results;
    }

    function querySearchCampaign(query) {
      var results = query ? vm.campaigns.filter(createFilterForCampaigns(query)) : vm.campaigns;
      return results;
    }

    function querySearchUrls(query) {
      var results = query ? vm.urls.filter(createFilterForUrl(query)) : vm.urls;
      return results;
    }

    function createFilterForPublisher(query) {
      var lowercaseQuery = angular.lowercase(query);
      return function filterFn(publisher) {
        return (publisher.email.indexOf(lowercaseQuery) === 0);
      };
    }

    function createFilterForCampaigns(query) {
      var lowercaseQuery = angular.lowercase(query);
      return function filterFn(campaign) {
        return (angular.lowercase(campaign.name).indexOf(lowercaseQuery) >= 0);
      };
    }

    function createFilterForUrl(query) {
      var lowercaseQuery = angular.lowercase(query);
      return function filterFn(url) {
        return (url.href.indexOf(lowercaseQuery) > 0);
      };
    }

    function getQuotes(page, limit) {
      vm.query.page = page;
      vm.query.limit = limit;
      vm.search();
    }

    function deleteQuote(quoteId, ev) {
      var confirm = $mdDialog.confirm()
        .title('Are you sure?')
        .textContent('You are removing this for good.')
        .targetEvent(ev)
        .ariaLabel('decline')
        .ok('Yes')
        .cancel('No');

      $mdDialog.show(confirm).then(function () {
        ContentService.deleteContent(quoteId, function () {
          var pos = _.findIndex(vm.quotes, ['id', quoteId]);
          vm.quotes.splice(pos, 1);
          toastr.success('Quote has been removed!');
        }, function (err) {
          $log.error(err)
        })
      }, function () {
        // Cancelled
      });
    }

    function getAssignedQuotes(page, limit) {
      vm.queryAssigned.page = page;
      vm.queryAssigned.limit = limit;
      vm.queryAssigned.scope = CONTENT_STATES.RECEIVED;
      vm.search();
    }

    function refreshSlider() {
      $timeout(function () {
        $scope.$broadcast('rzSliderForceRender');
      }, 900);
    };

    function transformAdditionalKeyword(chip) {
      if((typeof chip) === 'string') {
        return chip.toUpperCase();
      } else {
        return chip.name.toUpperCase();
      }
    }

    function pauseUrl(url, ev) {
      var confirm = $mdDialog.confirm()
        .title('Are you sure you want to pause the URL?')
        .targetEvent(ev)
        .ariaLabel('decline')
        .ok('Yes')
        .cancel('No');

      $mdDialog.show(confirm).then(function () {
        UrlsService.updateUrlState(url.id, 'pause', function () {
          url.state = URL_STATES.PAUSED
          toastr.success('URL has been paused');
        }, function (err) {
          toastr.error('Error while pausing URL. Try again later.');
          $log.error(err)
        })
      }, function () {
        // Cancelled
      });
    }

    function isActive(url) {
      return url.state === URL_STATES.ACTIVE;
    }

    function getRowClass(url) {
      if (url.already_bought) {
        return 'bought';
      } else if (url.already_quoted) {
        return 'quoted';
      }

      return '';
    }

    function downloadSelectedUrls() {
      ContentService.downloadSelectedUrls(vm.quote.id, function (response) {
        $window.open(response.data.urls_spreadsheet_doc.url, '_blank');
      }, function (err) {
        $log.error(err);
      });
    }

    function uploadFile(ev, id) {
      $rootScope.quoteUploading = true;
      $mdDialog.show({
        controller: 'UploadController',
        controllerAs: 'ctrl',
        templateUrl: 'uploadDialog.html',
        targetEvent: ev,
        clickOutsideToClose: false
      }).then(function (response) {
        $rootScope.quoteUploading = false;
        ContentService.uploadFile(id, response, function (response) {
          $window.location.reload();
        }, function (err) {
          $log.error(err);
          toastr.error(err.data.error + '. Please refresh the screen.', 'Can\'t upload file');
        });
      }, function () {
        // Cancelled, added this callback to prevent warning
        // TODO: Improve this callback
      });
    }

  }
})();
