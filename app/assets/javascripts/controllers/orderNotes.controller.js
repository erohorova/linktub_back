(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('OrderNotesController', OrderNotesController);

  OrderNotesController.$inject = ['$log', '$mdDialog', 'attrs'];

  /* @ngInject */
  function OrderNotesController($log, $mdDialog, attrs) {
    var vm = this;

    vm.getCancelText = getCancelText;

    activate();

    function activate() {
      vm.allowInput = attrs.allowInput;
      vm.title = attrs.title;
      vm.notes = attrs.ad.comments;
    }

    vm.hide = function () {
      $mdDialog.hide();
    }

    vm.cancel = function () {
      $mdDialog.cancel();
    }

    vm.submit = function (message, form) {
      form.$setPristine();
      form.$setUntouched();
      $mdDialog.hide(message);
    }

    function getCancelText() {
      return vm.allowInput ? 'Cancel' : 'Ok, I Understand';
    }
  }
})();
