(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('AdvReassignController', AdvReassignController);

  AdvReassignController.$inject = ['$log', '$window', '$mdDialog', '$filter', 'toastr', 'AdvertisersService', 'AdsService'];

  /* @ngInject */
  function AdvReassignController($log, $window, $mdDialog, $filter, toastr, AdvertisersService, AdsService) {
    var vm = this;

    // Variables.
    vm.showAdsTable = true;
    vm.ads = [];
    vm.query = {
      filter: '',
      limit: 10,
      page: 1,
      search: false
    };
    vm.selectedAd = {};
    vm.advSearchCriteria = '';
    vm.advertisers = [];
    vm.newAdvertiser = {};
    vm.newCampaign = {};
    vm.selectedCampaign = {};
    vm.rowOfSelectedCampaign = -1;

    // Functions.
    vm.search = search;
    vm.reAssign = reAssign;
    vm.searchAdvs = searchAdvs;
    vm.advertiserSelected = advertiserSelected;
    vm.selectAdv = selectAdv;
    vm.selectedCampaignChanged = selectedCampaignChanged;
    vm.selectCampaign = selectCampaign;
    vm.update = update;
    vm.getAds = getAds;


    loadAds();

    function loadAds() {
      if (angular.isDefined(window.ads)) {
        vm.ads = angular.fromJson(window.ads);
        if (angular.isDefined(window.results)) {
          vm.results = parseInt(window.results);
        }
      }
    }

    function getAds(page, limit) {
      vm.query.page = page;
      vm.query.limit = limit;
      vm.search();
    }

    function search() {
      AdsService.search(vm.query, function (response) {
        vm.ads = JSON.parse(response.data.ads);
      }, function (error) {
        toastr.error(error.data.errors, 'Oops!');
      });
    }

    function reAssign(ev, ad) {
      vm.selectedAd = ad;
      vm.showAdsTable = false;
    }

    function searchAdvs() {
      resetSelection();

      AdvertisersService.indexAdvertisers(1, 10, vm.advSearchCriteria, function (response) {
        vm.advertisers = JSON.parse(response.data.advertisers);
      });
    }

    function resetSelection() {
      vm.newAdvertiser = {};
      vm.newCampaign = {};
      vm.selectedCampaign = {};
    }

    function advertiserSelected() {
      return JSON.stringify(vm.newAdvertiser) !== JSON.stringify({});
    }

    function selectAdv(ev, adv) {
      vm.newAdvertiser = adv;
      vm.advSearchCriteria = '';
    }

    function selectedCampaignChanged(i) {
      vm.rowOfSelectedCampaign = i;
    }

    function selectCampaign(campaign) {
      vm.selectedCampaign = campaign;
    }

    function update(ev) {
      AdsService.update(vm.selectedAd.id, vm.newAdvertiser.id, vm.selectedCampaign.id,  function (response) {
        toastr.success('Advertiser updated');
        $window.location.reload();
      }, function (error) {
        toastr.error(error.data.errors);
      });
    }
  }
})();
