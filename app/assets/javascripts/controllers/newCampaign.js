(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('NewCampaignController', NewCampaignController);

  NewCampaignController.$inject = ['$mdDialog', '$log', 'AdvertisersService', 'advertiser', 'categories'];

  /* @ngInject */
  function NewCampaignController($mdDialog, $log, AdvertisersService, advertiser, categories) {
    var vm = this;

    // Accessible attributes
    vm.campaign = {};
    vm.isSelected = isSelected;
    vm.toggleCategory = toggleCategory;
    vm.querySearch = querySearch;

    activate();

    function activate() {
      instantiateCategories();
    }

    function instantiateCategories() {
      vm.categories = categories;
      vm.campaign.categories = [];
    }

    function isSelected(category) {
      return _.findIndex(vm.campaign.categories, ['name', category.name]) !== -1;
    }

    function toggleCategory(category) {
      var pos = _.findIndex(vm.campaign.categories, ['name', category.name]);
      if (pos < 0) {
        vm.campaign.categories.push(category)
      } else {
        vm.campaign.categories.splice(pos, 1);
      }
    }

    function querySearch(criteria) {
      var categoriesFiltered = vm.categories.filter(createFilterFor(criteria));
      return categoriesFiltered;
    }

    function createFilterFor(query) {
      var uppercaseQuery = query.toUpperCase();
      return function filterFn(category) {
        return category.name.indexOf(uppercaseQuery) !== -1;
      }
    }

    // Accessible functions
    vm.cancel = function () {
      $mdDialog.cancel();
    }

    vm.submit = function (form) {
      vm.campaign.categories_ids = vm.campaign.categories.map(function (category) {
        return category.id;
      });
      vm.campaign.links_attributes = {'0': vm.campaign.links_attributes};
      AdvertisersService.createCampaign(advertiser.id, vm.campaign, function (response) {
        $mdDialog.hide(response);
      }, function (err) {
        $log.error(err);
      });
    }
  }
})();
