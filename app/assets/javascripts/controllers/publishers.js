(function () {
  'use strict';

  angular
    .module('linktub')
    .controller('PublishersController', PublishersController);

  PublishersController.$inject = ['$mdDialog', '$window', '$log', '$mdExpansionPanel', 'toastr',
    'PublishersService', 'MessagesService', 'UsersService', 'ProfileService', 'PUBLISHER_STATES'
  ];

  /* @ngInject */
  function PublishersController($mdDialog, $window, $log, $mdExpansionPanel, toastr, PublishersService,
    MessagesService, UsersService, ProfileService, PUBLISHER_STATES) {
    var vm = this;

    // Accessible attributes
    vm.publisher = {};
    vm.publishers = [];

    vm.query = {
      order: 'created_at',
      limit: 10,
      page: 1,
      search: false
    };

    // Accessible functions
    vm.showConfirm = showConfirm;
    vm.showConfirmWithNotes = showConfirmWithNotes;
    vm.getPublishers = getPublishers;
    vm.showUpdatePassword = showUpdatePassword;
    vm.becomePublisher = becomePublisher;
    vm.isTerminated = isTerminated;
    vm.repliedMessage = repliedMessage;
    vm.getMessagesLabel = getMessagesLabel;
    vm.getProfilesLabel = getProfilesLabel;
    vm.showCategories = showCategories;
    vm.downloadReport = downloadReport;
    vm.toggleConfirm = toggleConfirm;
    vm.search = search;
    vm.reActivate = reActivate;
    vm.checkEmail = checkEmail;
    vm.decodeMessage = decodeMessage;

    activate();

    // Functions
    function activate() {
      instantiatePublisher();
      instantiatePublishers();
      instantiateCategories();

      $mdExpansionPanel().waitFor('messagesPanel').then(function (instance) {
        instance.collapse();
      });
    }

    function instantiatePublisher() {
      if (angular.isDefined($window.publisher)) {
        vm.publisher = angular.fromJson($window.publisher);
        if (angular.isDefined($window.messages)) {
          vm.publisher.messages = angular.fromJson($window.messages)
        }
      } else {
        vm.publisher = {}
      }
    }

    function instantiateCategories() {
      if (angular.isDefined($window.categories)) {
        vm.categories = angular.fromJson($window.categories);
      }
    }

    function reActivate(ev, publisher) {
      PublishersService.changePublisherState(publisher.id, 'activate', null, function (response) {
        publisher.state = 'active';
      }, function (error) {
        $log.error(error);
      })
    }

    function search() {
      PublishersService.indexPublishers(vm.query.page, vm.query.limit, vm.query.filter, function (response) {
        var data = response.data;
        vm.publishers = angular.fromJson(data.publishers);
        vm.results = data.results;
      });
    }

    function repliedMessage(message, form) {
      message.response = encodeURIComponent(message.response);
      MessagesService.replyMessage(message, function () {
        vm.publisher.messages.forEach(function (element) {
          if (element.id === message.id) {
            element.reply_message = message.response;
            form.$setPristine();
            form.$setUntouched();
            return;
          }
        })
      }, function (error) {
        $log.error(error);
      })
    }

    function getMessagesLabel() {
      return 'Messages (' + vm.publisher.messages.length + ')';
    }

    function getProfilesLabel() {
      return 'Profiles (' + vm.publisher.profiles.length + ')';
    }

    function isTerminated(publisher) {
      return publisher.state === PUBLISHER_STATES.TERMINATED;
    }

    function becomePublisher() {
      $window.open($window.redirect_url + '?token=' +
        vm.publisher.user.authentication_token + '&role=publisher', '_blank');
    }

    function instantiatePublishers() {
      if (angular.isDefined($window.publishers)) {
        vm.publishers = angular.fromJson($window.publishers);
      } else {
        vm.publishers = []
      }
    }

    function getPublishers(page, limit) {
      vm.query.page = page;
      vm.query.limit = limit;
      vm.promise = PublishersService.indexPublishers(page, limit, function (response) {
        vm.publishers = response.data;
      }).$promise;
    }

    function showConfirmWithNotes(ev, publisher) {
      // Appending dialog to document.body to cover sidenav in docs app
      var confirm = $mdDialog.prompt()
        .title('Would you like to terminate this account?')
        .placeholder('Notes')
        .ariaLabel('Notes')
        .clickOutsideToClose(true)
        .targetEvent(ev)
        .required(true)
        .ok('Ok')
        .cancel('Cancel');

      $mdDialog.show(confirm).then(function (result) {
        // Terminate account with publisher.id
        PublishersService.changePublisherState(publisher.id, 'terminate', result, function (response) {
          publisher.state = 'terminated';
        }, function (error) {
          $log.error(error);
        })
      }, function () {
        // Action cancelled
      });
    };

    function showConfirm(ev, form, user) {
      var confirm = $mdDialog.confirm()
        .title('Are you sure you want to update this Publisher?')
        .ariaLabel('Update publisher')
        .targetEvent(ev)
        .ok('Update')
        .cancel('Cancel');

      $mdDialog.show(confirm).then(function (result) {
        // Update publisher
        UsersService.updateUser(user, function (response) {
          form.$setPristine();
        }, function (error) {
          $log.error(error);
        })
      }, function () {
        // Cancelled
      });
    }

    function showUpdatePassword(ev) {
      $mdDialog.show({
        controller: 'ChangePasswordController',
        controllerAs: 'ctrl',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: true,
        templateUrl: 'changePasswordDialog.html',
        locals: {
          title: 'Change publisher password'
        }
      }).then(function (user) {
        // Send update to user
        UsersService.updateUser({
          id: vm.publisher.user.id,
          password: user.password,
          password_confirmation: user.repeat_password
        });
      }, function () {
        // Action cancelled
      });
    }

    function showCategories(ev, profile) {
      $mdDialog.show({
        controller: 'CategoriesController',
        controllerAs: 'ctrl',
        templateUrl: 'categoriesDialog.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose: true,
        locals: {
          campaign: profile,
          categories: vm.categories
        }
      }).then(function (categories) {
        ProfileService.updateProfile(profile.id, {
          categories: categories
        }, function (response) {
          toastr.success('The profile has been updated successfuly!', 'Successful');
        }, function (error) {
          toastr.error('Cannot update profile. Please try again');
          $log.error('Unable to update!');
        });
      }, function () {
        $log.log('Action cancelled');
      });
    }

    function toggleConfirm(ev, publisher) {
      var confirm = $mdDialog.confirm()
        .title('Would you like to manually confirm this account?')
        .ariaLabel('Confirm publisher')
        .targetEvent(ev)
        .ok('Yes')
        .cancel('No');

      $mdDialog.show(confirm).then(function (result) {
        PublishersService.confirmAccount(publisher.id, function (response) {
          toastr.success('Account has been confirmed!');
          vm.getPublishers(1, vm.query.limit);
        });
      }, function () {
        // Cancelled
        publisher.confirmed = false;
      });
    }

    function downloadReport() {
      $window.open(vm.publisher.active_content_spreadsheet_doc.url, '_blank');
    }

    function checkEmail(form) {
      UsersService.checkDuplicateEmail(vm.publisher.user, function (response) {
        if (angular.isDefined(response.data)) {
          var data = response.data;
          if (angular.isDefined(data.email_exists)) {
            form.email.$setValidity('emailtaken', !data.email_exists);
          }
        }
      }, function (error) {
        $log.error(error);
      });
    }

    function decodeMessage(message) {
      return decodeURIComponent(message);
    }
  }
})();
