(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('OrdersActionsService', OrdersActionsService);

  OrdersActionsService.$inject = ['$log', 'ORDERS_PAYLOADS'];

  /* @ngInject */
  function OrdersActionsService($log, ORDERS_PAYLOADS) {
    var service = {
      getActions: getActions
    };

    return service;

    function getActions() {
      return [{
        label: 'Approve',
        payload: ORDERS_PAYLOADS.APPROVE
      }, {
        label: 'Decline',
        payload: ORDERS_PAYLOADS.DECLINE
      }, {
        label: 'Download',
        payload: ORDERS_PAYLOADS.DOWNLOAD
      }, {
        label: 'Refund',
        payload: ORDERS_PAYLOADS.REFUND
      }];
    }
  }
})();
