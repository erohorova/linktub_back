(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('TransactionsService', TransactionsService);

  TransactionsService.$inject = ['$http'];

  /* @ngInject */
  function TransactionsService($http) {
    var URL = '/admin/transactions';

    var service = {
      index: index
    };

    return service;

    function index(query, successCallback, errorCallback) {
      return $http.get(URL, {
        params: {
          page: query.page,
          limit: query.limit,
          q: query.filter,
          from: query.dateFrom,
          to: query.dateTo
        }
      }).then(successCallback, errorCallback);
    }
  }
})();
