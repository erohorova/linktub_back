(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('AuthorInvoiceService', AuthorInvoiceService);

  AuthorInvoiceService.$inject = ['$http'];

  /* @ngInject */
  function AuthorInvoiceService($http) {
    var URL = '/admin/author_invoices';
    var URL_JSON = '/admin/author_invoices.json';

    var service = {
      indexInvoices: indexInvoices,
      createInvoice: createInvoice,
    };

    return service;

    function indexInvoices(query, successCallback, errorCallback) {
      $http.get(URL_JSON, {
        params: {
          page: query.page,
          limit: query.limit,
          scope: query.scope,
          q: query.filter
        }
      }).then(successCallback, errorCallback);
    }

    function createInvoice(successCallback, errorCallback) {
      $http.post(URL_JSON).then(successCallback, errorCallback);
    }
  }
})();
