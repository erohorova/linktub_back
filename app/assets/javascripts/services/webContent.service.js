(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('WebContentService', WebContentService);

  WebContentService.$inject = ['$http'];

  /* @ngInject */
  function WebContentService($http) {
    var URL_JSON = '/admin/web_contents.json';
    var URL = '/admin/web_contents';

    var service = {
      indexWebContent: indexWebContent,
      updateWebContent: updateWebContent,
      changeState: changeState,
      updateCounter: updateCounter
    };

    return service;

    function indexWebContent(query, successCallback) {
      $http.get(URL_JSON, {
        params: {
          page: query.page,
          limit: query.limit,
          scope: query.scope,
          q: query.filter
        }
      }).then(successCallback);
    }

    function updateWebContent(web_content, successCallback, errorCallback) {
      $http.put(URL + '/' + web_content.id, {
        web_content: {
          price: web_content.price
        }
      }).then(successCallback, errorCallback);
    }

    function changeState(id, newState, successCallback, errorCallback) {
      return $http.put(URL + '/' + id + '/state', {
        state: newState
      }).then(successCallback, errorCallback);
    }

    function updateCounter(successCallback) {
      $http.get(URL + '/counter').then(successCallback);
    }
  }
})();