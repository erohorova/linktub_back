(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('ContentService', ContentService);

  ContentService.$inject = ['$http', 'Upload'];

  /* @ngInject */
  function ContentService($http, Upload) {
    var URL_JSON = '/admin/contents.json';
    var URL = '/admin/contents';

    var service = {
      indexContent: indexContent,
      filter: filter,
      assign: assign,
      updateCounter: updateCounter,
      newQuote: newQuote,
      createContent: createContent,
      createOrder: createOrder,
      deleteContent: deleteContent,
      updateAgencyContent: updateAgencyContent,
      downloadSelectedUrls: downloadSelectedUrls,
      uploadFile: uploadFile
    };

    return service;

    function indexContent(page, limit, filter, successCallback) {
      return $http.get(URL_JSON + '?page=' + page + '&limit=' + limit + '&q=' + filter).then(successCallback);
    }

    function filter(contentId, params, successCallback, errorCallback) {
      $http.get(URL + '/' + contentId + '/filter', {
        params: {
          'words[]': params.words,
          'keywords[]': params.keywords,
          publisher_id: params.publisher_id,
          url_id: params.url_id,
          domain_authority_min: params.domain_authority_min,
          domain_authority_max: params.domain_authority_max,
          trust_flow_min: params.trust_flow_min,
          trust_flow_max: params.trust_flow_max,
          ahrefs_dr_min: params.ahrefs_dr_min,
          ahrefs_dr_max: params.ahrefs_dr_max
        }
      }).then(successCallback, errorCallback);
    }

    function assign(contentId, params, successCallback) {
      $http.post(URL + '/' + contentId + '/assign', {
        urls: params.selectedUrls
      }).then(successCallback);
    }

    function updateCounter(successCallback) {
      $http.get(URL + '/counter').then(successCallback);
    }

    function newQuote(successCallback) {
      $http.get(URL + '/new').then(successCallback);
    }

    function createContent(content, successCallback, errorCallback) {
      $http.post(URL_JSON, {
        content: content
      }).then(successCallback, errorCallback);
    }

    function createOrder(contentId, params, successCallback, errorCallback) {
      $http.post(URL + '/' + contentId + '/order', {
        urls: params.selectedUrls
      }).then(successCallback, errorCallback);
    }

    function deleteContent(contentId, successCallback, errorCallback) {
      $http.delete(URL + '/' + contentId).then(successCallback, errorCallback);
    }

    function updateAgencyContent(contentId, params, successCallback, errorCallback) {
      $http.put(URL + '/' + contentId, {
        agency_content: params.agency_content
      }).then(successCallback, errorCallback);
    }

    function downloadSelectedUrls(contentId, successCallback, errorCallback) {
      $http.get(URL + '/' + contentId + '/report').then(successCallback, errorCallback);
    }

    function uploadFile(contentId, file, successCallback, errorCallback) {
      Upload.upload({
        url: URL + '/' + contentId + '/upload',
        method: 'POST',
        data: {
          spreadsheet: file
        }
      }).then(successCallback, errorCallback);
    }

  }
})();
