(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('AuthorsService', AuthorsService);

  AuthorsService.$inject = ['$http'];

  /* @ngInject */
  function AuthorsService($http) {
    var URL_JSON = '/admin/authors.json';
    var URL = '/admin/authors';

    var service = {
      indexAuthors: indexAuthors,
      createAuthor: createAuthor,
      updateAuthor: updateAuthor,
      terminateAuthor: terminateAuthor,
      activateAuthor: activateAuthor,
      confirmAccount: confirmAccount,
      loginAs: loginAs
    };

    return service;

    function indexAuthors(page, limit, filter, successCallback) {
      $http.get(URL_JSON, {
        params: {
          page: page,
          limit: limit,
          q: filter
        }
      }).then(successCallback);
    }

    function createAuthor(author, successCallback, errorCallback) {
      $http.post(URL_JSON, {
        author: author
      }).then(successCallback, errorCallback);
    }

    function updateAuthor(author, successCallback, errorCallback) {
      $http.put(URL + '/' + author.id, {
        author: author
      }).then(successCallback, errorCallback);
    }

    function terminateAuthor(authorId, successCallback, errorCallback) {
      $http.put(URL + '/' + authorId + '/state', {
        state: 'terminate'
      }).then(successCallback, errorCallback);
    }

    function activateAuthor(authorId, successCallback, errorCallback) {
      $http.put(URL + '/' + authorId + '/state', {
        state: 'activate'
      }).then(successCallback, errorCallback);
    }

    function confirmAccount(authorId, successCallback, errorCallback) {
      $http.put(URL + '/' + authorId + '/confirm').then(successCallback, errorCallback);
    }

    function loginAs(authorId, successCallback, errorCallback) {
      $http.post(URL + '/' + authorId + '/login_as').then(successCallback, errorCallback);
    }
  }
})();
