(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('EmailService', EmailService);

  EmailService.$inject = ['$http'];

  /* @ngInject */
  function EmailService($http) {
    var URL_JSON = '/admin/emails.json';
    var URL = '/admin/emails';

    var service = {
      indexEmail: indexEmail,
      newEmail: newEmail,
      createEmail: createEmail,
      updateEmail: updateEmail,
      destroyEmail: destroyEmail
    };

    return service;

    function indexEmail(query, successCallback) {
      return $http.get(URL_JSON, {
        params: {
          page: query.page,
          limit: query.limit,
          order_by: query.orderBy,
          order: query.order,
          q: query.filter
        }
      }).then(successCallback);
    }

    function newEmail(successCallback) {
      $http.get(URL + '/new').then(successCallback);
    }

    function createEmail(email, successCallback, errorCallback) {
      $http.post(URL, {
        email: email
      }).then(successCallback, errorCallback);
    }

    function updateEmail(emailId, email, successCallback, errorCallback) {
      $http.put(URL + '/' + emailId, {
        email: email
      }).then(successCallback, errorCallback);
    }

    function destroyEmail(emailId, successCallback, errorCallback) {
      $http.delete(URL + '/' + emailId)
      .then(successCallback, errorCallback);
    }
  }
})();
