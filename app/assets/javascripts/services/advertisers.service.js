(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('AdvertisersService', AdvertisersService);

  AdvertisersService.$inject = ['$http'];

  /* @ngInject */
  function AdvertisersService($http) {
    var URL_JSON = '/admin/advertisers.json';
    var URL = '/admin/advertisers';

    var service = {
      indexAdvertisers: indexAdvertisers,
      changeAdvertiserState: changeAdvertiserState,
      filterAdvertiser: filterAdvertiser,
      confirmAccount: confirmAccount,
      createCampaign: createCampaign,
      reassignAgent: reassignAgent
    };

    return service;

    function indexAdvertisers(page, limit, filter, successCallback) {
      $http.get(URL_JSON, {
        params: {
          page: page,
          limit: limit,
          q: filter
        }
      }).then(successCallback);
    }

    function changeAdvertiserState(advertiserId, transition, note, successCallback, errorCallback) {
      $http.put(URL + '/' + advertiserId, {
        transition: transition,
        note: note
      }).then(successCallback, errorCallback);
    }

    function filterAdvertiser(query, campaignId) {
      return $http.get(URL_JSON + '?page=' + 1 + '&limit=' + 10, {
        params: {
          q: query,
          campaign_id: campaignId
        }
      });
    }

    function confirmAccount(advertiserId, successCallback, errorCallback) {
      $http.put(URL + '/' + advertiserId + '/confirm').then(successCallback, errorCallback);
    }

    function createCampaign(advertiserId, campaign, successCallback, errorCallback) {
      $http.post(URL + '/' + advertiserId + '/campaigns', {
        campaign: campaign
      }).then(successCallback, errorCallback);
    }

    function reassignAgent(agentId, advertiserId, successCallback, errorCallback) {
      $http.put(URL + '/' + advertiserId + '/reassign_agent', {
        agent_id: agentId
      }).then(successCallback, errorCallback);
    }

  }
})();
