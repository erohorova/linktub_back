(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('DashboardService', DashboardService);

  DashboardService.$inject = ['$http'];

  /* @ngInject */
  function DashboardService($http) {
    var ADV_NO_CAMP_URL = '/admin/advertisers/choose_without_campaigns';
    var PUB_NO_PROFILE_URL = '/admin/publishers/choose_without_profiles';
    var ADV_FILTER_URL = '/admin/advertisers/filter_and_download';
    var PUB_FILTER_URL = '/admin/publishers/filter_and_download';
    var ACCEPTED_ORDER_URL = '/admin/orders/accepted_orders';
    var LATE_ORDER_URL = '/admin/orders/outdated_orders';
    var AGING_ADV_URL = '/admin/advertisers/aging_adv_accounts';
    var NEW_LEADS_URL = '/admin/advertisers/new_leads';
    var NEW_LEADS_COUNT_URL = '/admin/advertisers/count';

    var service = {
      indexAllAdvertisers: indexAllAdvertisers,
      indexAllPublishers: indexAllPublishers,
      indexSalesOrders: indexSalesOrders,
      downloadAdvReport: downloadAdvReport,
      downloadPubsNoProfileReport: downloadPubsNoProfileReport,
      downloadSalesOrders: downloadSalesOrders,
      indexLateOrders: indexLateOrders,
      filterAdvsByPeriod: filterAdvsByPeriod,
      filterPublishersByPeriod: filterPublishersByPeriod,
      indexAgingAdvertisers: indexAgingAdvertisers,
      getNewLeadsСount: getNewLeadsСount,
      getNewLeads: getNewLeads
    };

    return service;

    function downloadSalesOrders(query, successCallback, errorCallback) {
      $http.get(ACCEPTED_ORDER_URL, {
        params: {
          download: true,
          start_date: query.start_date,
          end_date: query.end_date
        }
      }).then(successCallback, errorCallback);
    }

    function indexAllAdvertisers(query, successCallback) {
     $http.get(ADV_NO_CAMP_URL, {
        params: {
          page: query.page,
          limit: query.limit,
          q: query.filter
        }
      }).then(successCallback);
    }

    function indexAllPublishers(query, successCallback) {
      $http.get(PUB_NO_PROFILE_URL, {
        params: {
          page: query.page,
          limit: query.limit,
          q: query.filter
        }
      }).then(successCallback);
    }

    function indexSalesOrders(query, successCallback, errorCallback) {
      $http.get(ACCEPTED_ORDER_URL, {
        params: {
          page: query.page,
          limit: query.limit,
          q: query.filter,
          download: false,
          start_date: query.start_date,
          end_date: query.end_date
        }
      }).then(successCallback, errorCallback);
    }

    function indexLateOrders(query, successCallback, errorCallback) {
      $http.get(LATE_ORDER_URL, {
        params: {
          page: query.page,
          limit: query.limit,
          q: query.filter,
          download: false
        }
      }).then(successCallback, errorCallback);
    }

    function downloadAdvReport(query, successCallback, errorCallback) {
      $http.get(ADV_FILTER_URL, {
        params: {
          download: true,
          start_date: query.start_date,
          end_date: query.end_date
        }
      }).then(successCallback, errorCallback);
    }

    function downloadPubsNoProfileReport(query, successCallback, errorCallback) {
      $http.get(PUB_FILTER_URL, {
        params: {
          download: true,
          start_date: query.start_date,
          end_date: query.end_date
        }
      }).then(successCallback, errorCallback);
    }

    function filterAdvsByPeriod(query, successCallback) {
      $http.get(ADV_FILTER_URL, {
        params: {
          page: query.page,
          limit: query.limit,
          q: query.filter,
          start_date: query.start_date,
          end_date: query.end_date,
          download: false
        }
      }).then(successCallback);
    }

    function filterPublishersByPeriod(query, successCallback) {
      $http.get(PUB_FILTER_URL, {
        params: {
          page: query.page,
          limit: query.limit,
          q: query.filter,
          start_date: query.start_date,
          end_date: query.end_date,
          download: false
        }
      }).then(successCallback);
    }

    function indexAgingAdvertisers(query, successCallback, errorCallback) {
      $http.get(AGING_ADV_URL, {
        params: {
          page: query.page,
          limit: query.limit,
          q: query.filter
        }
      }).then(successCallback, errorCallback);
    }

    function getNewLeadsСount(successCallback, errorCallback) {
      $http.get(NEW_LEADS_COUNT_URL).then(successCallback, errorCallback);
    }

    function getNewLeads(params, successCallback, errorCallback) {
      $http.get(NEW_LEADS_URL, {
        params: params
      }).then(successCallback, errorCallback);
    }
  }
})();
