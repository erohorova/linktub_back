(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('PublishersService', PublishersService);

  PublishersService.$inject = ['$http'];

  /* @ngInject */
  function PublishersService($http) {
    var URL_JSON = '/admin/publishers.json';
    var URL = '/admin/publishers';

    var service = {
      indexPublishers: indexPublishers,
      changePublisherState: changePublisherState,
      confirmAccount: confirmAccount,
      email: email
    };

    return service;

    function indexPublishers(page, limit, filter, successCallback) {
      $http.get(URL_JSON, {
        params: {
          page: page,
          limit: limit,
          q: filter
        }
      }).then(successCallback);
    }

    function changePublisherState(publisherId, transition, note, successCallback, errorCallback) {
      $http.put(URL + '/' + publisherId, {
        transition: transition,
        note: note
      }).then(successCallback, errorCallback);
    }

    function email(publisherId, email, successCallback, errorCallback) {
      $http.post(URL + '/' + publisherId + '/email', {
        email: email
      }).then(successCallback, errorCallback);
    }

    function confirmAccount(publisherId, successCallback, errorCallback) {
      $http.put(URL + '/' + publisherId + '/confirm').then(successCallback, errorCallback);
    }
  }
})();
