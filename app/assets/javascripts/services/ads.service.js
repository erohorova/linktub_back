(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('AdsService', AdsService);

  AdsService.$inject = ['$http'];

  /* @ngInject */
  function AdsService($http) {
    var URL = '/admin/ads';
    var URL_JSON = '/admin/ads.json';

    var service = {
      indexAds: indexAds,
      approveAd: approveAd,
      declineAd: declineAd,
      pauseAd: pauseAd,
      search: search,
      update: update,
      seen: seen,
      updateUnresponsiveCounter: updateUnresponsiveCounter,
      updateActionItemsCounter: updateActionItemsCounter,
      remindPublisher: remindPublisher
    };

    return service;

    function indexAds(query, successCallback, errorCallback) {
      $http.get(URL_JSON, {
        params: {
          page: query.page,
          limit: query.limit,
          scope: query.scope,
          q: query.filter
        }
      }).then(successCallback, errorCallback);
    }

    function approveAd(adId, successCallback, errorCallback) {
      return $http.put(URL + '/' + adId + '/state', {
        state: 'approved'
      }).then(successCallback, errorCallback);
    }

    function declineAd(adId, notes, successCallback, errorCallback) {
      return $http.put(URL + '/' + adId + '/state', {
        state: 'rejected',
        notes: notes
      }).then(successCallback, errorCallback);
    }

    function pauseAd(adId, successCallback, errorCallback) {
      return $http.put(URL + '/' + adId + '/state', {
        state: 'pause'
      }).then(successCallback, errorCallback);
    }

    function search(query, successCallback, errorCallback) {
      return $http.get(URL + '/search', {
        params: {
          page: query.page,
          limit: query.limit,
          q: query.filter
        }
      }).then(successCallback, errorCallback);
    }

    function update(adId, advId, campaignId, successCallback, errorCallback) {
      return $http.put(URL + '/' + adId, {
        adv_id: advId,
        campaign_id: campaignId
      }).then(successCallback, errorCallback);
    }

    function seen(adId, successCallback, errorCallback) {
      return $http.put(URL + '/' + adId + '/seen').then(successCallback, errorCallback);
    }

    function updateActionItemsCounter(successCallback) {
      return $http.get(URL + '/action_items_counter').then(successCallback);
    }

    function updateUnresponsiveCounter(successCallback) {
      return $http.get(URL + '/unresponsive_ads_counter').then(successCallback);
    }

    function remindPublisher(adId, successCallback, errorCallback) {
      return $http.put(URL + '/' + adId + '/remind').then(successCallback, errorCallback);
    }
  }
})();
