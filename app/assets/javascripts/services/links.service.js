(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('LinksService', LinksService);

  LinksService.$inject = ['$http'];

  /* @ngInject */
  function LinksService($http) {
    var URL = '/admin/links.json';
    var URL2 = '/admin/links';

    var service = {
      indexLinks: indexLinks,
      approveLink: approveLink,
      declineLink: declineLink
    };

    return service;

    function indexLinks(page, limit, filter, successCallback) {
      return $http.get(URL + '?page=' + page + '&limit=' + limit + '&q=' + filter).then(successCallback);
    }

    function approveLink(linkId, successCallback, errorCallback) {
      return $http.put(URL2 + '/' + linkId + '/state', {
        state: 'approve'
      }).then(successCallback, errorCallback);
    }

    function declineLink(linkId, successCallback, errorCallback) {
      return $http.put(URL2 + '/' + linkId + '/state', {
        state: 'decline'
      }).then(successCallback, errorCallback);
    }
  }
})();
