(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('PayoutsService', PayoutsService);

  PayoutsService.$inject = ['$http'];

  /* @ngInject */
  function PayoutsService($http) {
    var URL      = '/admin/payouts';
    var URL_JSON = '/admin/payouts.json';

    var service = {
      indexAds: indexAds,
      pay: pay,
      cancel: cancel
    };

    return service;

    function indexAds(query, successCallback, errorCallback) {
      $http.get(URL_JSON, {
        params: {
          page: query.page,
          limit: query.limit,
        }
      }).then(successCallback, errorCallback);
    }

    function pay(adId, successCallback, errorCallback) {
      $http.put(URL + '/' + adId + '/pay').then(successCallback, errorCallback);
    }

    function cancel(adId, successCallback, errorCallback) {
      $http.delete(URL + '/' + adId).then(successCallback, errorCallback);
    }
  }
})();
