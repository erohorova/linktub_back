(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('UsersService', UsersService);

  UsersService.$inject = ['$http'];

  /* @ngInject */
  function UsersService($http) {
    var URL = '/admin/users';

    var service = {
      updateUser: updateUser,
      checkDuplicateEmail: checkDuplicateEmail
    };

    return service;

    function updateUser(user, successCallback, errorCallback) {
      $http.put(URL + '/' + user.id, {
        user: user
      }).then(successCallback, errorCallback);
    }

    function checkDuplicateEmail(user, successCallback, errorCallback) {
      $http.get(URL + '/check_email', {
        params: {
          id: user.id,
          email: user.email
        }
      }).then(successCallback, errorCallback);
    }
  }
})();
