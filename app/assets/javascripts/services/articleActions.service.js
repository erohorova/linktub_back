(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('ArticleActionsService', ArticleActionsService);

  ArticleActionsService.$inject = ['ArticlesService', 'ARTICLE_STATES', 'ARTICLE_PAYLOADS'];

  /* @ngInject */
  function ArticleActionsService(ArticlesService, ARTICLE_STATES, ARTICLE_PAYLOADS) {
    var service = {
      defineActionsForAdmins: defineActionsForAdmins,
      defineActionsForAuthors: defineActionsForAuthors,
      performSelectedAction: performSelectedAction,
      getOrderLetter: getOrderLetter,
      getLabelForPayload: getLabelForPayload
    };

    return service;

    /* Given an article's state this function should return
       the availabe actions to perfomr by an admin*/
    function defineActionsForAdmins(state) {
      switch (state) {
      case ARTICLE_STATES.DRAFT:
        return [{
          label: getLabelForPayload(ARTICLE_PAYLOADS.ASSIGN),
          payload: ARTICLE_PAYLOADS.ASSIGN
        }, {
          label: getLabelForPayload(ARTICLE_PAYLOADS.CANCEL),
          payload: ARTICLE_PAYLOADS.CANCEL
        }];
      case ARTICLE_STATES.ASSIGNED:
        return [{
          label: getLabelForPayload(ARTICLE_PAYLOADS.REJECT_BY_ADMIN),
          payload: ARTICLE_PAYLOADS.REJECT_BY_ADMIN
        }];
      case ARTICLE_STATES.COMPLETED:
        return [{
          label: getLabelForPayload(ARTICLE_PAYLOADS.APPROVE),
          payload: ARTICLE_PAYLOADS.APPROVE
        }, {
          label: getLabelForPayload(ARTICLE_PAYLOADS.REJECT_BY_ADMIN),
          payload: ARTICLE_PAYLOADS.REJECT_BY_ADMIN
        }, {
          label: getLabelForPayload(ARTICLE_PAYLOADS.CANCEL),
          payload: ARTICLE_PAYLOADS.CANCEL
        }];
      case ARTICLE_STATES.AUTHOR_REJECTED:
        return [{
          label: getLabelForPayload(ARTICLE_PAYLOADS.ASSIGN),
          payload: ARTICLE_PAYLOADS.ASSIGN
        }, {
          label: getLabelForPayload(ARTICLE_PAYLOADS.CANCEL),
          payload: ARTICLE_PAYLOADS.CANCEL
        }];
      case ARTICLE_STATES.AGENCY_REJECTED:
        return [{
          label: getLabelForPayload(ARTICLE_PAYLOADS.ASSIGN),
          payload: ARTICLE_PAYLOADS.ASSIGN
        }, {
          label: getLabelForPayload(ARTICLE_PAYLOADS.CANCEL),
          payload: ARTICLE_PAYLOADS.CANCEL
        }];
      }
    }

    /* Given an article's state this function should return
       the availabe actions to perfomr */
    function defineActionsForAuthors(state) {
      switch (state) {
      case ARTICLE_STATES.ASSIGNED:
        return [{
          label: getLabelForPayload(ARTICLE_PAYLOADS.COMPLETE),
          payload: ARTICLE_PAYLOADS.COMPLETE
        }, {
          label: getLabelForPayload(ARTICLE_PAYLOADS.REJECT_BY_AUTHOR),
          payload: ARTICLE_PAYLOADS.REJECT_BY_AUTHOR
        }];
      case ARTICLE_STATES.ADMIN_REJECTED:
        return [{
          label: getLabelForPayload(ARTICLE_PAYLOADS.COMPLETE),
          payload: ARTICLE_PAYLOADS.COMPLETE
        }, {
          label: getLabelForPayload(ARTICLE_PAYLOADS.REJECT_BY_AUTHOR),
          payload: ARTICLE_PAYLOADS.REJECT_BY_AUTHOR
        }];
      case ARTICLE_STATES.AGENCY_REJECTED:
        return [{
          label: getLabelForPayload(ARTICLE_PAYLOADS.COMPLETE),
          payload: ARTICLE_PAYLOADS.COMPLETE
        }, {
          label: getLabelForPayload(ARTICLE_PAYLOADS.REJECT_BY_AUTHOR),
          payload: ARTICLE_PAYLOADS.REJECT_BY_AUTHOR
        }];
      default:
        []
      }
    }

    function getLabelForPayload(payload) {
      switch (payload) {
        case ARTICLE_PAYLOADS.ASSIGN:
          return 'Assign to author';
        case ARTICLE_PAYLOADS.CANCEL:
          return 'Cancel Order';
        case ARTICLE_PAYLOADS.APPROVE:
          return 'Approve to Agency';
        case ARTICLE_PAYLOADS.REJECT_BY_ADMIN:
          return 'Reject to author';
        case ARTICLE_PAYLOADS.REJECT_BY_AUTHOR:
          return 'Cancel Order';
        case ARTICLE_PAYLOADS.COMPLETE:
          return 'Send to admin';
        default:
          ''
      }
    }

    /* Given an article's state perform a new action */
    function performSelectedAction(article, successCallback, errorCallback) {
      ArticlesService.updateArticleState(article, article.action.payload).then(successCallback, errorCallback);
    }

    function getOrderLetter(state) {
      switch (state) {
      case ARTICLE_STATES.AUTHOR_REJECTED:
        return 'A'
      case ARTICLE_STATES.DRAFT:
        return 'B'
      case ARTICLE_STATES.ADMIN_REJECTED:
        return 'C'
      case ARTICLE_STATES.AGENCY_REJECTED:
        return 'D'
      case ARTICLE_STATES.ASSIGNED:
        return 'E'
      case ARTICLE_STATES.APPROVED:
        return 'F'
      default:
        return 'Z'
      }
    }
  }
})();
