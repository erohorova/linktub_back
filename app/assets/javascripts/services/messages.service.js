(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('MessagesService', MessagesService);

  MessagesService.$inject = ['$http'];

  /* @ngInject */
  function MessagesService($http) {
    var URL = '/admin/messages';

    var service = {
      replyMessage: replyMessage
    };

    return service;

    function replyMessage(message, successCallback, errorCallback) {
      $http.put(URL + '/' + message.id, {
        message: {
          reply_message: message.response
        }
      }).then(successCallback, errorCallback);
    }
  }
})();