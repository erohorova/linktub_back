(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('UrlsService', UrlsService);

  UrlsService.$inject = ['$http', 'Upload'];

  /* @ngInject */
  function UrlsService($http, Upload) {
    var URL_JSON = '/admin/urls.json';
    var URL = '/admin/urls';

    var service = {
      indexUrls: indexUrls,
      upadateFile: upadateFile,
      updateUrl: updateUrl,
      updateUrlAd: updateUrlAd,
      createNote: createNote,
      updateUrlState: updateUrlState,
      updateCounter: updateCounter,
      updatePub: updatePub
    };

    return service;

    function indexUrls(query, successCallback) {
      $http.get(URL_JSON, {
        params: {
          page: query.page,
          limit: query.limit,
          scope: query.scope,
          q: query.filter
        }
      }).then(successCallback);
    }

    function upadateFile(urlId, params, successCallback, errorCallback) {
      Upload.upload({
        url: URL + '/' + urlId,
        method: 'PUT',
        data: {
          url: {
            ad_attributes: {
              id: params.id,
              blog_post_doc: params.file
            }
          }
        }
      }).then(successCallback, errorCallback);
    }

    function createNote(urlId, params, successCallback, errorCallback) {
      $http.post(URL + '/' + urlId + '/comments', {
        url: {
          comment: {
            comment: params.message
          }
        }
      }).then(successCallback, errorCallback);
    }

    function updateUrlAd(urlId, params, successCallback, errorCallback) {
      $http.put(URL + '/' + urlId, {
        url: {
          ad_attributes: {
            id: params.id,
            blog_post_title: params.title,
            full_pub_url: params.target,
            link_text: params.anchor
          }
        }
      }).then(successCallback, errorCallback);
    }

    function updateUrl(urlId, params, successCallback, errorCallback) {
      $http.put(URL + '/' + urlId, {
        url: {
          eta: params.eta,
          shown_price: params.shown_price,
          description: params.description,
          allow_blog_post: params.allow_blog_post,
          allow_home_page: params.allow_home_page,
          follow_link: params.follow_link
        }
      }).then(successCallback, errorCallback);
    }

    function updateUrlState(campaignId, state, successCallback, errorCallback) {
      return $http.put(URL + '/' + campaignId + '/state', {
        state: state
      }).then(successCallback, errorCallback);
    }

    function updateCounter(successCallback) {
      $http.get(URL + '/counter').then(successCallback);
    }

    function updatePub(urlId, params, successCallback, errorCallback) {
      return $http.put(URL + '/' + urlId + '/update_pub', params).then(successCallback, errorCallback);
    }
  }
})();
