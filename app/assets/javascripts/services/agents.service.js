(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('AgentsService', AgentsService);

  AgentsService.$inject = ['$http'];

  /* @ngInject */
  function AgentsService($http) {
    var URL_JSON = '/admin/agents.json';
    var URL = '/admin/agents';

    var service = {
      indexAgents: indexAgents,
      createAgent: createAgent,
      updateAgent: updateAgent,
      terminateAgent: terminateAgent,
      activateAgent: activateAgent,
      confirmAccount: confirmAccount
    };

    return service;

    function indexAgents(page, limit, filter, successCallback) {
      $http.get(URL_JSON, {
        params: {
          page: page,
          limit: limit,
          q: filter
        }
      }).then(successCallback);
    }

    function createAgent(agent, successCallback, errorCallback) {
      $http.post(URL_JSON, {
        agent: agent
      }).then(successCallback, errorCallback);
    }

    function updateAgent(agent, successCallback, errorCallback) {
      $http.put(URL + '/' + agent.id, {
        agent: agent
      }).then(successCallback, errorCallback);
    }

    function terminateAgent(agentId, successCallback, errorCallback) {
      $http.put(URL + '/' + agentId + '/state', {
        state: 'terminate'
      }).then(successCallback, errorCallback);
    }

    function activateAgent(agentId, successCallback, errorCallback) {
      $http.put(URL + '/' + agentId + '/state', {
        state: 'activate'
      }).then(successCallback, errorCallback);
    }

    function confirmAccount(agentId, successCallback, errorCallback) {
      $http.put(URL + '/' + agentId + '/confirm').then(successCallback, errorCallback);
    }

  }
})();
