(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('SocialHandlesService', SocialHandlesService);

  SocialHandlesService.$inject = ['$http'];

  /* @ngInject */
  function SocialHandlesService($http) {
    var URL_JSON = '/admin/social_handles.json';
    var URL = '/admin/social_handles';

    var service = {
      indexSocialHandles: indexSocialHandles,
      updateSocialHandle: updateSocialHandle,
      changeState: changeState,
      updateCounter: updateCounter
    };

    return service;

    function indexSocialHandles(query, successCallback) {
      $http.get(URL_JSON, {
        params: {
          page: query.page,
          limit: query.limit,
          scope: query.scope,
          q: query.filter
        }
      }).then(successCallback);
    }

    function updateSocialHandle(social_handle, successCallback, errorCallback) {
      $http.put(URL + '/' + social_handle.id, {
        social_handle: {
          price: social_handle.price
        }
      }).then(successCallback, errorCallback);
    }

    function changeState(id, newState, successCallback, errorCallback) {
      return $http.put(URL + '/' + id + '/state', {
        state: newState
      }).then(successCallback, errorCallback);
    }

    function updateCounter(successCallback) {
      $http.get(URL + '/counter').then(successCallback);
    }
  }
})();