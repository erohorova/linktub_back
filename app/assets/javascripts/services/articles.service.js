(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('ArticlesService', ArticlesService);

  ArticlesService.$inject = ['$http', 'Upload'];

  /* @ngInject */
  function ArticlesService($http, Upload) {
    var URL_JSON = '/admin/articles.json';
    var URL = '/admin/articles';

    var service = {
      indexArticle: indexArticle,
      upadateFile: upadateFile,
      updateCounter: updateCounter,
      updateArticle: updateArticle,
      updateAuthor: updateAuthor,
      updateArticleAd: updateArticleAd,
      updateArticleLink: updateArticleLink,
      createNote: createNote,
      updateArticleState: updateArticleState,
      deleteArticle: deleteArticle,
      bulkCreate: bulkCreate
    };

    return service;

    function indexArticle(query, successCallback) {
      $http.get(URL_JSON, {
        params: {
          page: query.page,
          limit: query.limit,
          scope: query.scope,
          q: query.filter
        }
      }).then(successCallback);
    }

    function upadateFile(articleId, params, successCallback, errorCallback) {
      Upload.upload({
        url: URL + '/' + articleId,
        method: 'PUT',
        data: {
          article: {
            ad_attributes: {
              id: params.id,
              blog_post_doc: params.file
            }
          }
        }
      }).then(successCallback, errorCallback);
    }

    function createNote(articleId, params, successCallback, errorCallback) {
      $http.post(URL + '/' + articleId + '/comments', {
        article: {
          comment: {
            comment: params.message
          }
        }
      }).then(successCallback, errorCallback);
    }

    function updateArticleAd(articleId, params, successCallback, errorCallback) {
      $http.put(URL + '/' + articleId, {
        article: {
          ad_attributes: {
            id: params.id,
            blog_post_title: params.title,
            full_pub_url: params.target,
            link_text: params.anchor
          }
        }
      }).then(successCallback, errorCallback);
    }

    function updateArticleLink(articleId, params, successCallback, errorCallback) {
      $http.put(URL + '/' + articleId, {
        article: {
          link_attributes: {
            id: params.id,
            href: params.target,
          }
        }
      }).then(successCallback, errorCallback);
    }

    function updateArticle(articleId, params, successCallback, errorCallback) {
      $http.put(URL + '/' + articleId, {
        article: {
          eta: params.eta
        }
      }).then(successCallback, errorCallback);
    }

    function updateAuthor(articleId, params, successCallback, errorCallback) {
      $http.put(URL + '/' + articleId, {
        article: {
          author_id: params.author_id
        }
      }).then(successCallback, errorCallback);
    }

    function updateCounter(successCallback) {
      $http.get(URL + '/counter').then(successCallback);
    }

    function updateArticleState(article, state, successCallback, errorCallback) {
      return $http.put(URL + '/' + article.id + '/state', {
        state: state
      }).then(successCallback, errorCallback);
    }

    function deleteArticle(articleId, successCallback, errorCallback) {
      return $http.delete(URL + '/' + articleId).then(successCallback, errorCallback);
    }

    function bulkCreate(quote_id, urls, successCallback, errorCallback) {
      $http.post(URL + '/bulk_create', {
        quote_id: quote_id,
        urls: urls
      }).then(successCallback, errorCallback);
    }
  }
})();
