(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('SessionService', SessionService);

  SessionService.$inject = ['$http'];

  /* @ngInject */
  function SessionService($http) {
    var URL = '/admin/logout';

    var service = {
      signOut: signOut
    };

    return service;

    function signOut(successCallback) {
      return $http.get(URL).then(successCallback);
    }
  }
})();