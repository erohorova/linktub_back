(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('EmailTemplateService', EmailTemplateService);

  EmailTemplateService.$inject = ['$http'];

  /* @ngInject */
  function EmailTemplateService($http) {
    var URL_JSON = '/admin/email_templates.json';
    var URL = '/admin/email_templates';

    var service = {
      indexEmailTemplate: indexEmailTemplate,
      newEmailTemplate: newEmailTemplate,
      createEmailTemplate: createEmailTemplate,
      showEmailTemplate: showEmailTemplate,
      updateEmailTemplate: updateEmailTemplate,
      destroyEmailTemplate: destroyEmailTemplate
    };

    return service;

    function indexEmailTemplate(emailId, successCallback) {
      return $http.get(URL_JSON + '?email_id=' + emailId).then(successCallback);
    }

    function newEmailTemplate(successCallback) {
      $http.get(URL + '/new').then(successCallback);
    }

    function createEmailTemplate(emailTemplate, successCallback, errorCallback) {
      $http.post(URL, {
        email_template: emailTemplate
      }).then(successCallback, errorCallback);
    }

    function showEmailTemplate(emailTemplateId, successCallback) {
      return $http.get(URL + '/' + emailTemplateId + '.json').then(successCallback);
    }

    function updateEmailTemplate(emailTemplateId, emailTemplate, successCallback, errorCallback) {
      $http.put(URL + '/' + emailTemplateId, {
        email_template: emailTemplate
      }).then(successCallback, errorCallback);
    }

    function destroyEmailTemplate(emailId, successCallback, errorCallback) {
      $http.delete(URL + '/' + emailId).then(successCallback, errorCallback);
    }
  }
})();
