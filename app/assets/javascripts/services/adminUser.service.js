(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('AdminUserService', AdminUserService);

  AdminUserService.$inject = ['$http'];

  /* @ngInject */
  function AdminUserService($http) {
    var URL = '/admin/admin_users.json';

    var service = {
      indexAdminUsers: indexAdminUsers,
      createAdmin: createAdmin,
      checkDuplicateEmail: checkDuplicateEmail
    };

    return service;

    function indexAdminUsers(page, limit, filter, successCallback) {
      return $http.get(URL + '?page=' + page + '&limit=' + limit + '&q=' + filter).then(successCallback);
    }

    function createAdmin(admin, successCallback, errorCallback) {
      $http.post(URL, {
        admin_user: {
          phone: admin.phone,
          email: admin.email,
          last_name: admin.lastName,
          first_name: admin.name,
          password: admin.password
        }
      }).then(successCallback, errorCallback);
    }

    function checkDuplicateEmail(admin, successCallback, errorCallback) {
      $http.get(URL + '/check_email', {
        params: {
          id: admin.id,
          email: admin.email
        }
      }).then(successCallback, errorCallback);
    }
  }
})();
