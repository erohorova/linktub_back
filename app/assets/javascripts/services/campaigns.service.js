(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('CampaignService', CampaignService);

  CampaignService.$inject = ['$http'];

  /* @ngInject */
  function CampaignService($http) {
    var URL_JSON = '/admin/campaigns.json';
    var URL = '/admin/campaigns';

    var service = {
      indexCampaign: indexCampaign,
      approveCampaign: approveCampaign,
      declineCampaign: declineCampaign,
      updateCampaign: updateCampaign,
      filterCampaigns: filterCampaigns,
      updateCounter: updateCounter
    };

    return service;

    function indexCampaign(page, limit, filter, successCallback) {
      return $http.get(URL_JSON + '?page=' + page + '&limit=' + limit + '&q=' + filter).then(successCallback);
    }

    function approveCampaign(campaignId, successCallback, errorCallback) {
      return $http.put(URL + '/' + campaignId + '/state', {
        state: 'active'
      }).then(successCallback, errorCallback);
    }

    function declineCampaign(campaignId, successCallback, errorCallback) {
      return $http.put(URL + '/' + campaignId + '/state', {
        state: 'reject'
      }).then(successCallback, errorCallback);
    }

    function updateCampaign(campaignId, campaign, successCallback, errorCallback) {
      return $http.put(URL + '/' + campaignId, {
        campaign: campaign
      }).then(successCallback, errorCallback);
    }

    function filterCampaigns(query, advertiserId) {
      return $http.get(URL + '/filter' + '?page=' + 1 + '&limit=' + 10, {
        params: {
          q: query,
          advertiser_id: advertiserId
        }
      });
    }

    function updateCounter(successCallback, errorCallback) {
      return $http.get(URL + '/counter').then(successCallback, errorCallback);
    }
  }
})();
