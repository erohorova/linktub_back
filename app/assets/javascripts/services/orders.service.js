(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('OrdersService', OrdersService);

  OrdersService.$inject = ['$http'];

  /* @ngInject */
  function OrdersService($http) {
    var URL = '/admin/orders';
    var URL_JSON = '/admin/orders.json';
    var LATE_URL_JSON = '/admin/outdated_orders.json';
    var LATE_URL = '/admin/outdated_orders';

    var service = {
      approveOrder: approveOrder,
      declineOrder: declineOrder,
      refundOrder: refundOrder,
      indexOrders: indexOrders
    };

    return service;

    function approveOrder(orderId, successCallback, errorCallback) {
      $http.put(URL + '/' + orderId + '/state', {
        state: 'complete'
      }).then(successCallback, errorCallback);
    }

    function declineOrder(orderId, message, successCallback, errorCallback) {
      $http.put(URL + '/' + orderId + '/state', {
        state: 'decline',
        message: message
      }).then(successCallback, errorCallback);
    }

    function refundOrder(orderId, notes, successCallback, errorCallback) {
      $http.put(URL + '/' + orderId + '/state', {
        state: 'refund',
        notes: notes
      }).then(successCallback, errorCallback);
    }

    function indexOrders(query, successCallback, errorCallback) {
      $http.get(URL_JSON, {
        params: {
          page: query.page,
          limit: query.limit,
          scope: query.scope,
          q: query.filter
        }
      }).then(successCallback, errorCallback);
    }

  }
})();
