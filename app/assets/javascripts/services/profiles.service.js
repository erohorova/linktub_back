(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('ProfileService', ProfileService);

  ProfileService.$inject = ['$http'];

  /* @ngInject */
  function ProfileService($http) {
    var URL = '/admin/profiles';

    var service = {
      updateProfile: updateProfile
    };

    return service;

    function updateProfile(profileId, profile, successCallback, errorCallback) {
      return $http.put(URL + '/' + profileId, {
        profile: profile
      }).then(successCallback, errorCallback);
    }
  }
})();
