// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require angular/angular
//= require angular-animate/angular-animate
//= require angular-aria/angular-aria
//= require angular-messages/angular-messages
//= require angular-material/angular-material
//= require angular-material-sidemenu/dest/angular-material-sidemenu
//= require angular-material-data-table/dist/md-data-table.min
//= require angular-material-expansion-panel/dist/md-expansion-panel.min
//= require angularjs-slider/dist/rzslider.min
//= require angular-rails-templates
//= require angular-toastr/dist/angular-toastr.min
//= require angular-toastr/dist/angular-toastr.tpls.min
//= require moment
//= require angular-moment/angular-moment
//= require angular-sanitize/angular-sanitize
//= require ng-file-upload/dist/ng-file-upload.min
//= require quill/dist/quill.min
//= require ng-quill/dist/ng-quill
//= require angular-notification-icons/dist/angular-notification-icons
//= require_tree ../templates
//= require_tree .
