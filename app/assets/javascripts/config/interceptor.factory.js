(function () {
  'use strict';

  angular
    .module('linktub')
    .factory('Interceptor', interceptor);

  interceptor.$inject = ['$q', '$rootScope'];

  /* @ngInject */
  function interceptor($q, $rootScope) {
    ////////// Attributes //////////
    var xhrCount = 0;

    var interceptor = {
      request: function (config) {
        setupCredentials(config);
        incrementCounter(config);
        return config;
      },

      requestError: function (config) {
        decrementCounter(config);
        return config;
      },

      response: function (res) {
        decrementCounter(res.config);
        return res;
      },

      responseError: function (res) {
        decrementCounter(res.config);
        return $q.reject(res);
      }
    }

    return interceptor;

    ////////// Functions //////////

    function setupCredentials(config) {
      config.headers['X-CSRF-Token'] = $rootScope.authenticity_token;
      config.headers['Accept'] = 'application/json';
    }

    function incrementCounter(config) {
      if (!config.ignoreLoading) {
        xhrCount++;
        updateStatus();
      }
    }

    function decrementCounter(config) {
      if (!config.ignoreLoading) {
        xhrCount--;
        updateStatus();
      }
    }

    function updateStatus() {
      $rootScope.isLoading = xhrCount > 0;
    }
  }
})();