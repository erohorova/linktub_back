(function () {
  'use strict';

  angular
    .module('linktub')
    .constant('CAMPAIGN_STATES', {
      REJECTED: 'rejected',
      ACTIVE: 'active'
    })
    .constant('CONTENT_STATES', {
      REQUESTED: 'requested',
      RECEIVED: 'received'
    })
    .constant('PUBLISHER_STATES', {
      TERMINATED: 'terminated',
      ACTIVE: 'active'
    })
    .constant('ARTICLE_STATES', {
      ALL: 'all',
      DRAFT: 'draft',
      DONE: 'done',
      ASSIGNED: 'assigned',
      COMPLETED: 'completed',
      ADMIN_REJECTED: 'admin_rejected',
      AUTHOR_REJECTED: 'author_rejected',
      AGENCY_REJECTED: 'agency_rejected',
      APPROVED: 'admin_approved',
      CANCELLED: 'cancelled',
      PAST_ETA: 'past_eta'
    })
    .constant('ARTICLE_PAYLOADS', {
      ASSIGN: 'assign',
      APPROVE: 'approve',
      COMPLETE: 'complete',
      REJECT_BY_ADMIN: 'reject_by_admin',
      REJECT_BY_AUTHOR: 'reject_by_author',
      REJECT_BY_AGENCY: 'reject_by_agency',
      CANCEL: 'cancel',
    })
    .constant('ORDERS_PAYLOADS', {
      APPROVE: 'approve',
      DECLINE: 'decline',
      DOWNLOAD: 'download',
      REFUND: 'refund',
      EMAIL_PUB: 'email_pub',
    })
    .constant('ADS_PAYLOADS', {
      APPROVE: 'approve',
      DECLINE: 'decline',
      DOWNLOAD: 'download',
    })
    .constant('URL_STATES', {
      PENDING: 'pending',
      ACTIVE: 'active',
      REJECTED: 'rejected',
      UNDER_REVIEW: 'under_review',
      PAUSED: 'paused',
    })
    .constant('AD_STATES', {
      PENDING: 'pending',
      ACTIVE: 'active',
      REJECTED: 'rejected',
      UNDER_REVIEW: 'under_review',
      PAUSED: 'paused',
    })
    .constant('REFUNDED_CONSTANT', {
      REFUNDED: 'Refunded'
    })
    .constant('ARTICLES_COPY_ATTRIBUTES', {
      TITLE: 'title',
      URL: 'url',
      ANCHOR: 'anchor'
    });
})();
