(function () {
  'use strict';

  angular
    .module('linktub', [
      'ngMessages',
      'ngAria',
      'ngMaterial',
      'ngMaterialSidemenu',
      'md.data.table',
      'material.components.expansionPanels',
      'templates',
      'rzModule',
      'toastr',
      'angularMoment',
      'ngFileUpload',
      'ngSanitize',
      'ngQuill',
      'angular-notification-icons'
    ])
    .config(config);

  config.$inject = ['$mdThemingProvider', '$httpProvider', 'toastrConfig', '$mdDateLocaleProvider'];

  /** @ngInject */
  function config($mdThemingProvider, $httpProvider, toastrConfig, $mdDateLocaleProvider) {
    $mdThemingProvider.theme('default')
      .primaryPalette('orange')
      .accentPalette('green');

    // Setup toastr
    toastrConfig.allowHtml = true;
    toastrConfig.timeOut = 5000;
    toastrConfig.positionClass = 'toast-top-right';
    toastrConfig.closeButton = true;
    toastrConfig.progressBar = true;
    toastrConfig.newestOnTop = true;

    // Setup interceptor for $http
    $httpProvider.interceptors.push('Interceptor');

    $mdDateLocaleProvider.formatDate = function (date) {
      return moment(date).isValid() ? moment(date).format('MM/DD/YYYY') : '';
    };
  }
})();
