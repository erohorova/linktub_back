(function () {
  'use strict';

  angular
    .module('linktub')
    .filter('humanize', function () {
      return function (input) {
        input = input || '';
        return input.replace(/_/g, '\u00a0');
      };
    });
})();