(function () {
  'use strict';

  angular
    .module('linktub')
    .filter('camelize', camelize);

  function camelize() {
    return camelizeFilter

    function camelizeFilter(input) {
      return input.replace(/_/g, ' ')
    }
  }
})();