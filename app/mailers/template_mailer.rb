class TemplateMailer < ApplicationMailer

  def email(email, subject, html)
    @email = email
    @subject = subject
    @html = html
    mail(to: email, subject: @subject)
  end

end
