class LinktubDeviseMailer < Devise::Mailer
  helper :application # gives access to all helpers defined within `application_helper`.
  include Devise::Controllers::UrlHelpers # Optional. eg. `confirmation_url`
  # default template_path: 'devise/mailer' # to make sure that your mailer uses the devise views

  def confirmation_instructions(record, token, opts = {})
    email_vars = {}
    email_vars[:confirmation_url] = confirmation_url(record, confirmation_token: token)
    email_vars[:first_name] = record.first_name
    email_vars[:last_name] = record.last_name
    Email.schedule("#{record.class.name.downcase}_account_confirmation", record.id, email_vars)
  end
end
