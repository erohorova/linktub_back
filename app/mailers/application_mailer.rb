class ApplicationMailer < ActionMailer::Base
  default from: "admin@scalefluence.com"
  layout 'mailer'
end
