class QuickMailer < ApplicationMailer
  def email(params)
    @body = params[:body]
    attachments['content.docx'] = open(params[:doc_url]).read if params[:doc_url].present?
    mail(to: params[:email], subject: params[:subject], cc: params[:cc])
  end
end
