# encoding: utf-8

module Emails
  class ResponsesController < ApplicationController
    def show
      @email_trigger = EmailTrigger.includes(:email).find(params[:id])
      @email_trigger.update(response_completed: true)
      @redirect_url = @email_trigger.redirect_url
      redirect_to @redirect_url
    end
  end
end
