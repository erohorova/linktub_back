# encoding: utf-8

module Api
  module V1
    class ApiController < ApplicationController
      include Concerns::Authenticable
      include Pundit

      layout false
      respond_to :json

      rescue_from ActiveRecord::RecordNotFound,        with: :render_not_found
      rescue_from ActiveRecord::RecordInvalid,         with: :render_record_invalid
      rescue_from ActionController::RoutingError,      with: :render_not_found
      rescue_from ActionController::UnknownController, with: :render_not_found
      rescue_from AbstractController::ActionNotFound,  with: :render_not_found
      rescue_from PermissionsHelper::ForbiddenAccess,  with: :render_forbidden_access

      # Pundit errors
      rescue_from Pundit::NotAuthorizedError,          with: :render_forbidden_access

      def status
        render json: { online: true }
      end

      def render_forbidden_access(exception)
        logger.info(exception) # for logging
        render json: { error: 'Not Authorized' }, status: :forbidden
      end

      def render_not_found(exception)
        logger.info(exception) # for logging
        render json: { error: "Couldn't find the record" }, status: :not_found
      end

      def render_record_invalid(exception)
        logger.info(exception) # for logging
        render json: { errors: exception.record.errors.as_json }, status: :bad_request
      end

      %w(advertiser publisher).each do |key|
        define_method "authenticate_#{key}!" do
          return true if current_user.send(key.to_sym)
          render json: { errors: ["You need to provide a valid token for #{key}"] }, status: :unauthorized
        end
        define_method "current_#{key}" do
          current_user && current_user.send(key)
        end
      end
    end
  end
end
