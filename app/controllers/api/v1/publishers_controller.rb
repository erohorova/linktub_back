# encoding: utf-8

module Api
  module V1
    class PublishersController < ApiController
      before_action :authenticate_user!
      before_action :authenticate_publisher!, only: :show
      helper_method :publisher

      def show
        @profiles = current_publisher.profiles.order(created_at: :desc).limit(3)
      end

      def create
        @publisher = current_user.create_publisher!
      end

      private

      def publisher_params
        params.require(:publisher).permit
      end

      def publisher
        @publisher ||= current_publisher
      end
    end
  end
end
