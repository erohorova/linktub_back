# encoding: utf-8

module Api
  module V1
    class NotificationsController < ApiController
      before_action :authenticate_user!
      helper_method :notification

      def index
        @notifications = current_user.notifications
      end

      def destroy_type
        if received_type?
          current_user.public_send("#{params[:type]}_notifications").destroy_all
          head :no_content
        else
          render json: { error: "Couldn't find the type" }, status: :not_found
        end
      end

      private

      def received_type?
        params[:type].present? && Notification.types.include?(params[:type])
      end

      def notification_params
        params.require(:notification).permit(:description, :notification_type, :state, :user_id)
      end
    end
  end
end
