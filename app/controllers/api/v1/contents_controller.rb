# encoding: utf-8

module Api
  module V1
    class ContentsController < ApiController
      before_action :authenticate_advertiser!
      helper_method :content

      def index
        @contents = received_scope? ? current_advertiser.received_content : current_advertiser.requested_content
        Email.unschedule('advertiser_content_received', current_advertiser.user_id)
      end

      def create
        @content = current_advertiser.contents.create!(content_params.merge(created_by: Content::ADVERTISER))
      end

      def update
        content.update! content_params
      end

      def report
        content = Content.find(params[:content_id])
        render json: { urls_spreadsheet_doc: content.urls_spreadsheet_doc } if content.update_urls_spreadsheet
      end

      def upload
        content = Content.find(params[:content_id])
        result = content.bulk_upload_from_document(params[:spreadsheet])
        if result[:status]
          head :ok
        else
          render json: { error: result[:error] }, status: :unprocessable_entity
        end
      end

      private

      def content_params
        params.require(:content).permit(:campaign_id, :trust_flow_min, :trust_flow_max, :domain_authority_min,
                                        :domain_authority_max, :ahrefs_dr_min, :ahrefs_dr_max, :agency_content,
                                        category_ids: [])
      end

      def received_scope?
        params[:scope].present? && params[:scope] == 'received'
      end

      def content
        @content ||= Content.find(params[:id])
      end
    end
  end
end
