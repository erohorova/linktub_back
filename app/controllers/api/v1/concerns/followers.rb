module Api
  module V1
    module Concerns
      module Followers
        extend ActiveSupport::Concern

        SocialHandle::SOCIALS.each do |key|
          define_method "#{key}_followers" do
            return render json: { error: 'No Url provided.' }, status: :bad_request unless params[:url]
            result = socials_service.followers(params[:url])
            render json: { followers: result }, status: :ok
          end
        end
      end
    end
  end
end
