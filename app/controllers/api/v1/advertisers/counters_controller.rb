# encoding: utf-8

module Api
  module V1
    module Advertisers
      class CountersController < ApiController
        before_action :authenticate_advertiser!
        helper_method :advertiser

        private

        def advertiser
          @advertiser ||= current_advertiser
        end
      end
    end
  end
end
