# encoding: utf-8

module Api
  module V1
    module Advertisers
      class ArticlesController < ApiController
        before_filter :authenticate_advertiser!
        helper_method :article

        def index
          @articles = current_advertiser.articles.content_pipe
          @zip = current_advertiser.content_pipe_zip.url[/[^?]+/]
        end

        def bulk_create
          @articles = []
          url_ids = params[:articles].map { |param| param[:url_id] }
          content_ids = params[:articles].map { |param| param[:content_id] }
          @contents = Content.where(id: content_ids).group_by(&:id)
          @urls = Url.where(id: url_ids).group_by(&:id)
          params[:articles].each do |article_params|
            ActiveRecord::Base.transaction do
              dispose_url_from_content(multiple_articles_params(article_params))
              new_article = current_advertiser.articles.create!(
                multiple_articles_params(article_params.except(:content_id))
              )
              campaign_id = article_params[:ad_attributes][:campaign_id]
              href = article_params[:href]
              new_article.link = current_advertiser.campaigns.find(campaign_id).links.find_or_create_by!(href: href)
              new_article.ad.link = new_article.link
              new_article.ad.advertiser = current_advertiser
              create_comment(new_article, article_params[:notes]) if article_params[:notes].present?
              new_article.save!
              @articles << new_article
            end
          end
        end

        def create_orders
          articles = current_advertiser.articles.where(id: params[:articles_ids])
          articles.map do |article|
            current_advertiser.orders.create! ad: article.ad, url: article.ad.url, created_from: params[:created_from]
            article.done
            current_advertiser.destroy_content_notification
          end
          head :no_content
        end

        def update
          create_comment(article, params[:article][:notes]) if params[:article][:notes].present?
          if single_article_params[:ad_attributes].present?
            campaign_id = single_article_params[:ad_attributes][:campaign_id]
            if params[:article][:full_pub_url].present?
              href = params[:article][:full_pub_url]
              article.link = current_advertiser.campaigns.find(campaign_id).links.find_or_create_by!(href: href)
            end
            article.update! single_article_params
          end
          article.reject_by_agency if params[:rewrite]
        end

        private

        def dispose_url_from_content(params)
          content = @contents[params[:content_id]].first
          content.urls.delete(@urls[params[:url_id]].first)
          content.done if content.urls.empty?
        end

        def single_article_params
          params.require(:article).permit(
            :url_id, :link_id, :eta, :notes,
            ad_attributes: %i(id blog_post_title link_text full_pub_url
                              blog_post_body blog_post_doc blog_post_image_1
                              blog_post_image_2 url_id campaign_id link_id _destroy)
          )
        end

        def multiple_articles_params(params)
          params.permit(
            :url_id, :link_id, :eta, :content_id,
            ad_attributes: %i(id blog_post_title link_text full_pub_url
                              blog_post_body blog_post_doc blog_post_image_1
                              blog_post_image_2 url_id campaign_id link_id _destroy)
          )
        end

        def create_comment(article, body)
          article.comments.create!(
            comment: body,
            user: current_user,
            role: current_advertiser.class.name
          )
        end

        def article
          @article ||= current_advertiser.articles.find(params[:id])
        end
      end
    end
  end
end
