# encoding: utf-8

module Api
  module V1
    module Advertisers
      class OrdersController < Api::V1::ApiController
        before_action :authenticate_advertiser!
        helper_method :order

        def index
          @orders = current_advertiser.pending_orders.includes(
            :url, ad: [:link, campaign: [:categories, links: [:urls]]]
          ).each do |order|
            content_ids = current_advertiser.contents.where(campaign_id: order.ad&.campaign_id).pluck(:id)
            content_url = order.url.content_urls.find_by(content_id: content_ids)
            order.url.shown_price = content_url.shown_price if content_url
          end
          Email.unschedule('advertiser_article_done', current_advertiser.user_id)
        end

        def create
          @order = current_advertiser.orders.create! order_params
        end

        def bulk_create
          content = Content.find(params[:orders][:content_id])
          urls = Url.where(id: params[:orders][:url_ids])
          @orders = urls.map do |url|
            content.urls.delete(url)
            current_advertiser.orders.create!(url_id: url.id, created_from: params[:orders][:created_from])
          end
          content.done if content.urls.empty?
        end

        def update
          order.update! order_params
        end

        def destroy
          order.destroy!
          head :no_content
        end

        def destroy_list
          current_advertiser.remove_orders_articles_from_cart(orders_ids)
          current_advertiser.orders.where(id: orders_ids).destroy_all
          head :no_content
        end

        def already_ordered
          orders = current_advertiser.orders.where(url_id: params[:url_id])
          render json: { already_ordered: orders.count >= 2,
                         campaign_name: orders.count >= 2 ? orders.offset(1).first.ad.campaign.name : nil }
        end

        def already_ordered_for_campaign
          ads = current_advertiser.ads.where(url_id: params[:url_id], campaign_id: params[:campaign_id])
          render json:  { already_ordered: ads.count > 0,
                          campaign_name: ads.count > 0 ? ads.first.campaign.name : nil
                        }
        end

        private

        def order_params
          params.require(:order).permit(
            :url_id, :created_from, :searched_campaign_id,
            ad_attributes: %i(id blog_post_title link_text
                              blog_post_body blog_post_doc blog_post_image_1
                              blog_post_image_2 url_id url_price campaign_id link_id _destroy)
          )
        end

        def orders_ids
          params[:orders_ids] || []
        end

        def order
          @order ||= Order.find(params[:id])
        end
      end
    end
  end
end
