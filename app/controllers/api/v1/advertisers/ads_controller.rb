# encoding: utf-8

module Api
  module V1
    module Advertisers
      class AdsController < ApiController
        before_filter :authenticate_advertiser!
        helper_method :ad

        def index
          if received_state?
            @ads = current_advertiser.public_send(params[:state].to_s)
            is_rej_or_canc_cont = params[:state].to_s == 'rejected_or_cancelled_content'
            @url = is_rej_or_canc_cont ? nil : current_advertiser.public_send("#{params[:state]}_spreadsheet").url[/[^?]+/]
          else
            @ads = current_advertiser.ads
            @url = nil
          end
        end

        def update
          ad.update! ad_params
          ad.pending || ad.resubmit
        end

        def destroy
          ad.destroy!
          head :no_content
        end

        private

        def received_state?
          %w(active_content awaiting_publication rejected_content rejected_or_cancelled_content).include?(params[:state])
        end

        def ad_params
          params.require(:ad).permit(:blog_post_title, :link_text,
                                     :blog_post_body, :blog_post_doc, :blog_post_image_1,
                                     :blog_post_image_2, :url_id, :campaign_id, :link_id)
        end

        def ad
          @ad ||= Ad.find(params[:id])
        end
      end
    end
  end
end
