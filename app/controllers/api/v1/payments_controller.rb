# encoding: utf-8

module Api
  module V1
    class PaymentsController < ApiController
      before_action :authenticate_advertiser!

      def token
        @token = transaction_service.token
      end

      def payment
        render_no_token_error unless payment_params[:payment_token] || payment_params[:payment_nonce]
        @result = if payment_params[:payment_token]
                    transaction_service.payment_token(payment_params[:payment_token], orders_ids)
                  else
                    transaction_service.payment_nonce(payment_params[:payment_nonce], payment_params[:last_four],
                                                      payment_params[:store_payment_method], orders_ids)
                  end
        @result.try(:success?).nil? ? head(:ok) : render('error', status: :bad_request)
      end

      private

      def payment_params
        params.require(:payment).permit(:orders_ids, :last_four, :payment_nonce,
                                        :payment_token, :payment_nonce, :store_payment_method)
      end

      def orders_ids
        params[:payment][:orders_ids]
      end

      def transaction_service
        TransactionService.new(current_user)
      end

      def render_no_token_error
        render json: { error: 'No payment token or nonce provided by client.' }, status: :bad_request && return
      end
    end
  end
end
