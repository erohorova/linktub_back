# encoding: utf-8

module Api
  module V1
    class InvoicesController < ApiController
      before_action :authenticate_advertiser!

      def index
        @invoices = current_advertiser
                      .invoices(include: :orders)
                      .order('created_at DESC')
                      .page(params[:page])
                      .per(params[:limit])
        @total_count = current_advertiser.invoices.count
      end

      private

      def advertiser
        @advertiser ||= current_advertiser
      end
    end
  end
end
