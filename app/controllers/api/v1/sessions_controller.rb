# encoding: utf-8

module Api
  module V1
    class SessionsController < Devise::SessionsController
      include Concerns::Authenticable

      skip_before_filter :verify_authenticity_token, if: :json_request?
      skip_before_filter :verify_signed_out_user, only: [:destroy]

      rescue_from PermissionsHelper::ForbiddenAccess, with: :render_forbidden_access

      # POST /resource/sign_in
      def create
        resource = warden.authenticate! scope: resource_name, recall: "#{controller_path}#failure"
        raise PermissionsHelper::ForbiddenAccess if resource.confirmed_at.nil?
        if resource&.advertiser.try(:active?) || resource&.publisher.try(:active?)
          sign_in_and_redirect(resource_name, resource)
        else
          failure('Your account has been suspended')
        end
      end

      def sign_in_and_redirect(resource_or_scope, resource = nil)
        scope = Devise::Mapping.find_scope! resource_or_scope
        resource ||= resource_or_scope
        sign_in(scope, resource) unless warden.user(scope) == resource
        render 'resource', locals: { resource: resource }
      end

      # DELETE /resource/sign_out
      def destroy
        # sign out and avoid resetting the session
        Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name)
        head :no_content
      end

      def failure(message = nil)
        render json: { errors: [message] }, status: :bad_request
      end

      private

      def user_params
        params.require(:user).permit(
          :username, :first_name,
          :last_name, :email
        )
      end

      def render_forbidden_access(exception)
        logger.info(exception) # for logging
        render json: { error: 'You must confirm your email before signing in.' }, status: :forbidden
      end

      protected

      def json_request?
        request.format.json?
      end
    end
  end
end
