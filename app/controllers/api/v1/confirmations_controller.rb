# encoding: utf-8

module Api
  module V1
    class ConfirmationsController < Devise::ConfirmationsController
      skip_before_filter :verify_authenticity_token, if: :json_request?

      # GET /resource/confirmation?confirmation_token=abcdef
      def show
        self.resource = resource_class.confirm_by_token(params[:confirmation_token])
        after_url = after_confirmation_path_for(resource_name, resource)
        if resource.errors.empty?
          unschedule_emails(resource)
          respond_to do |format|
            format.html { redirect_to after_url }
            format.json { render json: { notice: flash[:notice], redirect_path: after_url } }
          end
        else
          respond_to do |format|
            format.html { redirect_to after_url }
            format.json { render json: { error: resource.errors }, status: :bad_request }
          end
        end
      end

      private

      def unschedule_emails(resource)
        Email.unschedule('publisher_account_confirmation', resource.id)
        Email.unschedule('advertiser_account_confirmation', resource.id)
        Email.unschedule('publisher_change_email', resource.id)
        Email.unschedule('advertiser_change_email', resource.id)
      end

      def after_confirmation_path_for(_resource_name, _resource)
        params[:redirect_url]
      end

      protected

      def json_request?
        request.format.json?
      end
    end
  end
end
