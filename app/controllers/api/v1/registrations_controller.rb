# encoding: utf-8

module Api
  module V1
    class RegistrationsController < Devise::RegistrationsController
      before_action :configure_permitted_parameters, only: :create
      before_action :create_user, only: :create
      skip_before_filter :verify_authenticity_token, if: :json_request?

      def create
        resource_saved = resource.save
        if resource_saved
          save_success
          schedule_emails
          render json: { token: resource.authentication_token, email: resource.email }
        else
          save_fail
          render json: { error: resource.errors }, status: :bad_request
        end
      end

      protected

      def create_user
        build_resource(sign_up_params)
        # Add publisher to user
        resource.build_publisher if params[:publisher].present?
        # Add advertiser to user
        resource.build_advertiser if params[:advertiser].present?
      end

      def save_success
        if resource.active_for_authentication?
          sign_up(resource_name, resource)
        else
          expire_data_after_sign_in!
        end
      end

      def save_fail
        clean_up_passwords resource
        @validatable = devise_mapping.validatable?
        @minimum_password_length = resource_class.password_length.min if @validatable
      end

      def schedule_emails
        url = Email.find_by(slug: 'publisher_account_confirmation').try(:redirect_url) if params[:publisher].present?
        url = Email.find_by(slug: 'advertiser_account_confirmation').try(:redirect_url) if params[:advertiser].present?
        email_vars = {
          first_name: resource.first_name,
          last_name: resource.last_name,
          email: resource.email,
          confirmation_url: "#{user_confirmation_url(confirmation_token: resource.confirmation_token)}&redirect_url=#{url}"
        }

        if params[:publisher].present?
          Email.schedule('publisher_account_confirmation', resource.id, email_vars)
          Email.schedule('publisher_my_account', resource.id, email_vars) unless resource.try(:paypal_email)
          Email.schedule('publisher_submit_url_reminder', resource.id, email_vars)
        elsif params[:advertiser].present?
          Email.schedule('advertiser_account_confirmation', resource.id, email_vars)
          Email.schedule('advertiser_url_submission', resource.id, email_vars)
        end
      end

      def configure_permitted_parameters
        devise_parameter_sanitizer.for :sign_up do |params|
          params.permit(
            :first_name, :last_name,
            :email, :password,
            :password_confirmation,
            :phone
          )
        end
      end

      def json_request?
        request.format.json?
      end
    end
  end
end
