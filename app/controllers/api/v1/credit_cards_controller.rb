# encoding: utf-8

module Api
  module V1
    class CreditCardsController < ApiController
      before_filter :authenticate_advertiser!
      helper_method :credit_card

      def index
        @credit_cards = current_advertiser.credit_cards
      end

      def create
        @result = credit_card_service.create_payment_method(credit_card_params[:payment_nonce])
        @result.try(:success?) ? head(:ok) : render('error', status: :bad_request)
      end

      def update
        @result = credit_card_service.update_payment_method(
          credit_card_params[:payment_nonce], credit_card_params[:payment_token]
        )
        @result.try(:success?) ? head(:ok) : render('error', status: :bad_request)
      end

      def destroy
        credit_card.destroy!
        head :no_content
      end

      private

      def credit_card_service
        CreditCardsService.new(current_user)
      end

      def credit_card_params
        params.require(:credit_card).permit(:payment_token, :payment_nonce)
      end

      def credit_card
        @credit_card ||= current_advertiser.credit_cards.find(params[:id])
      end
    end
  end
end
