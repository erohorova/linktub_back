# encoding: utf-8

module Api
  module V1
    module Publishers
      class AdsController < ApiController
        before_filter :authenticate_publisher!
        helper_method :ad

        def index
          @ads = if received_state?
                   current_publisher.public_send("#{params[:state]}_ads")
                 elsif params[:state].present?
                   render json: { error: 'Invalid state' }, status: :bad_request
                 else
                   current_publisher.ads
                 end

          if params[:state].present? && Ad.states[params[:state].to_sym] == Ad.states[:approved_by_publisher]
            @ads.concat(current_publisher.completed_orders.where(completed_by: nil).map{ |o| o.ad })
          end
        end

        def update
          @ad = Ad.find(params[:id])
          @ad.update! ad_params
          @ad.comments.create!(comment_params) if params[:ad][:notes].present?
          @ad.approved_by_publisher
          unschedule_emails if params[:ad][:full_pub_url].present?
        end

        def approve
          if ad.approved_by_publisher
            schedule_emails
            head(:no_content)
          else
            render(json: { error: 'Not approved' }, status: :bad_request)
          end
        end

        def reject
          if ad.reject
            ad.update! reject_params
            ad.comments.create!(comment_params) unless ad.cancelled?
            if ad.editable
              ad.send_advertiser_content_rewrite_email
            else
              ad.order.remove_from_transaction
            end
            head :no_content
          else
            render json: { error: 'Not rejected' }, status: :bad_request
          end
        end

        private

        def reject_params
          params.require(:ad).permit(:rejected_reason, :notes, :editable)
        end

        def ad_params
          params.require(:ad).permit(:full_pub_url)
        end

        def received_state?
          params[:state].present? && Ad.states.include?(params[:state])
        end

        def comment_params
          { comment: params[:ad][:notes], user: current_user, role: current_publisher.class.name }
        end

        def ad
          @ad ||= Ad.find(params[:ad_id])
        end

        def schedule_emails
          email_vars = ad.attributes.symbolize_keys
          email_vars[:advertiser_first_name] = ad.advertiser.try(:first_name)
          email_vars[:advertiser_last_name] = ad.advertiser.try(:last_name)
          email_vars[:publisher_first_name] = ad.publisher.try(:first_name)
          email_vars[:publisher_last_name] = ad.publisher.try(:last_name)
          email_vars[:campaign_name] = ad.campaign.try(:name)
          Email.schedule('publisher_submit_post', ad.publisher.try(:user_id), email_vars)
        end

        def unschedule_emails
          Email.unschedule('publisher_submit_post', ad.publisher.try(:user_id))
        end
      end
    end
  end
end
