# encoding: utf-8

module Api
  module V1
    module Publishers
      class MessagesController < ApiController
        before_action :authenticate_publisher!
        helper_method :message

        def index
          @messages = current_publisher.messages.page(params[:page])
          @unread_count = current_publisher.messages.unread.count
          @pages = @messages.total_pages
          @results = @messages.total_count
          unschedule_emails
        end

        def create
          @message = current_publisher.messages.build message_params
          @message.author = 'admin'
          @message.save!
          notify_admin
        end

        def read
          @message = Message.find(params[:message_id])
          @message.read
          head :no_content
        end

        private

        def message
          @message ||= Message.find(params[:id])
        end

        def message_params
          params.require(:message).permit(:subject, :question_message, :reply_message)
        end

        def notify_admin
          email_vars = {
            name: message.user.full_name,
            email: message.user.email,
          }

          Email.schedule('publisher_admin_message', AdminUser.first.try(:id), email_vars)
        end

        def unschedule_emails
          Email.unschedule('admin_publisher_message', current_publisher.user_id)
        end
      end
    end
  end
end
