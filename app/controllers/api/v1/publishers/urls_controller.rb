# encoding: utf-8

module Api
  module V1
    module Publishers
      class UrlsController < Api::V1::ApiController
        before_action :authenticate_publisher!, only: :update
        helper_method :url

        def index
          @urls = if received_states?
                    params[:states].flat_map { |state| current_publisher.public_send("#{state}_urls") }
                  elsif params[:states].present?
                    render json: { error: 'Invalid state/s' }, status: :bad_request
                  else
                    current_publisher.urls
                  end
        end

        def update
          if url.active? && (url_params[:paused] == 'true')
            url.pause
          end
          url.active if url.paused? && url_params[:paused] != 'true'
          url.update! url_params.except(:paused) if url.active? || url.rejected?
          if url_params.key?(:price)
            schedule_emails_for_price_change
          end
        end

        private

        def url_params
          params.require(:url).permit(:href, :price, :description, :author_page, :follow_link, :paused)
        end

        def url
          @url ||= current_publisher.urls.find(params[:id])
        end

        def received_states?
          params[:states].present? && params[:states].all? { |s| Url.states.include?(s) }
        end

        def schedule_emails_for_price_change
          email_vars = {
            first_name: url.publisher.first_name,
            last_name: url.publisher.last_name,
            email: url.publisher.email,
            url: url.href,
            url_price: url.price
          }
          Email.schedule("admin_url_price_changed", AdminUser.first.try(:id), email_vars)
        end
      end
    end
  end
end
