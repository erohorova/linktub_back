# encoding: utf-8

module Api
  module V1
    module Publishers
      class OrdersController < Api::V1::ApiController
        before_action :authenticate_publisher!
        helper_method :order

        def index
          @orders = scoped_orders.includes(
            :url, ad: [:link, :payout, campaign: [:categories, links: [:urls]]]
          )
        end

        private

        def scoped_orders
          current_publisher.completed_orders.where(completed_by: 'admin').union(current_publisher.cancelled_orders)
        end
      end
    end
  end
end
