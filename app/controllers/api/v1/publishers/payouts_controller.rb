module Api
  module V1
    module Publishers
      class PayoutsController < Api::V1::ApiController
        before_filter :authenticate_publisher!

        def show
          date_from = DateTime.parse(params[:date_from]).beginning_of_day
          date_to = DateTime.parse(params[:date_to]).end_of_day
          @items = current_publisher.payout_items(date_from, date_to)
          @total_earnings = @items.sum('urls.price').to_i
          @projected_earnings = current_publisher.projected_earnings
          @last_month_payouts = current_publisher.last_month_payouts
        end
      end
    end
  end
end
