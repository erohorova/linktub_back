# encoding: utf-8

module Api
  module V1
    module Publishers
      class CountersController < ApiController
        before_action :authenticate_publisher!
        helper_method :publisher

        private

        def publisher
          @publisher ||= current_publisher
        end
      end
    end
  end
end
