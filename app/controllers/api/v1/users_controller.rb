# encoding: utf-8

module Api
  module V1
    class UsersController < Api::V1::ApiController
      before_action :authenticate_user!, except: :check_email
      helper_method :user

      # PUT /api/v1/users/:id
      def update
        old_email = current_user.email
        password_param ? current_user.update_with_password(user_params) : current_user.update(user_params)
        errors = current_user.errors
        if errors.empty? && email_param.present? && email_param != old_email
          current_user.invalidate_token
          current_user.unconfirmed
          schedule_emails
        end
        return render json: { error: errors.as_json }, status: :bad_request unless errors.empty?
      end

      def check_email
        @user_exists = User.where(email: params[:email]).exists?
      end

      private

      def render_bad_request
        head :bad_request
      end

      def password_param
        params[:user][:current_password]
      end

      def email_param
        params[:user][:email]
      end

      def user_params
        params.require(:user).permit(
          :first_name, :last_name, :email,
          :company, :country, :phone, :password, :password_confirmation,
          :current_password, :address_line_1, :address_line_2,
          :city, :state, :zip_code, :paypal_email, :skrill_email,
          :payment_method
        )
      end

      def user
        @user ||= current_user
      end

      def schedule_emails
        role = current_user.roles.first
        return if role.blank?
        url = Email.find_by(slug: "#{role}_change_email").try(:redirect_url)
        email_vars = {
          first_name: current_user.first_name,
          last_name: current_user.last_name,
          email: current_user.email,
          confirmation_url: "#{user_confirmation_url(confirmation_token: current_user.confirmation_token)}&redirect_url=#{url}"
        }
        Email.schedule("#{role}_change_email", current_user.id, email_vars)
        Email.unschedule('publisher_my_account', current_user.id) if current_user.try(:paypal_email).present?
      end
    end
  end
end
