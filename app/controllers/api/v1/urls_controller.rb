# encoding: utf-8

module Api
  module V1
    class UrlsController < Api::V1::ApiController
      before_action :authenticate_advertiser!, only: :index
      before_action :authenticate_publisher!,  only: :update
      helper_method :url

      def index
        return render json: { error: 'Cannot search until you have URLs approved' },
                      status: :forbidden unless current_advertiser.auto_approved?
        @urls = Url.search(params[:words], params[:keywords], filters_params)&.order(params[:sort_by])&.page(params[:page])
        @pages = @urls&.total_pages
        @results = @urls&.total_count
      end

      def update
        url.pause  if url.active? && (params[:url][:pause] == 'true')
        url.active if url.paused? && !(params[:url][:pause] == 'true')
        url.update! url_params if url.active? || url.rejected?
      end

      def suggested_price
        render json: { suggested_price: SuggestedPriceService.new.suggest_stringified_price(params[:href]) }
      end

      private

      def filters_params
        filters ||= {}
        params[:shown_price_max] = '10000' if params[:shown_price_max].to_i >= 1000 # TODO: Improve this code
        params[:ahrefs_lrd_max] = '1000000' if params[:shown_price_max].to_i >= 1000000
        filters_params = %w(domain_authority trust_flow citation_flow shown_price ahrefs_dr ahrefs_lrd)
        filters_params.each do |search_param|
          filters[search_param] = [params["#{search_param}_min".to_sym].to_i,
                                   params["#{search_param}_max".to_sym].to_i]
        end
        filters
      end

      def url_params
        params.require(:url).permit(:href, :price, :description, :author_page, :follow_link)
      end

      def url
        @url ||= current_publisher.profiles.find(params[:profile_id]).urls.find(params[:id])
      end
    end
  end
end
