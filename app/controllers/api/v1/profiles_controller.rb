# encoding: utf-8

module Api
  module V1
    class ProfilesController < ApiController
      include Concerns::Followers

      before_action :authenticate_publisher!
      helper_method :profile

      def index
        @profiles = current_publisher.profiles.limit(params[:limit])
      end

      def create
        @profile = current_publisher.profiles.build profile_params
        manage_extra_params
        @profile.save!
        schedule_emails
      end

      def names
        @names = current_publisher.profiles.pluck(:name)
      end

      def meta_description
        @description = MetaInspector.new(params[:url]).description if params[:url]
        show_error('We can\'t find the Meta Description. Please write your own.') if @description.nil?
      rescue MetaInspector::RequestError
        show_error('We can\'t find the Meta Description. Please write your own.')
      rescue ArgumentError
        show_error('Domain name must not start with a dot.')
      rescue MetaInspector::ParserError
        show_error('Absolute URI missing hierarchical.')
      end

      def ga_muv
        return render json: { error: 'No Google Analytics Url provided.' }, status: :bad_request unless params[:url]
        result = socials_service.visitors(params[:url])
        if result
          render json: { visitors: result }, status: :ok
        else
          render json: { error: 'Error retrieving the information.' }, status: :bad_request
        end
      end

      def domain_information
        return render json: { error: 'No Url provided.' }, status: :bad_request unless params[:url]
        result = socials_service.domain_information(params[:url])
        render json: result, status: :ok
      end

      def twitter_followers
        twitter_service = TwitterService.new
        followers_count = twitter_service.get_followers_count(params[:handle])
        return render json: { error: 'No Twitter user found.' }, status: :bad_request unless followers_count
        render json: { count: followers_count }
      end

      # If there is a spreadsheet present in params the bulk upload all urls
      def update
        profile.update profile_params
        manage_extra_params
        profile.save!
      end

      def update_spreadsheet
        profile = current_publisher.profiles.find(params[:profile_id])
        if profile.bulk_upload_urls(spreadsheet_param, params[:profile_type])
          head(:ok)
        else
          render json: { error: 'Please look at all the columns before uploading and make sure they are filled out.' }, status: :bad_request
        end
      rescue ActiveRecord::RecordInvalid => exception
        render json: { error: exception.message }, status: :bad_request
      end

      def destroy
        profile.destroy!
        head :no_content
      end

      private

      def profile
        @profile ||= current_publisher.profiles.find(params[:id])
      end

      def socials_service
        SocialsService.new
      end

      def categories_param
        params[:profile][:categories_ids]
      end

      def urls_params
        params[:profile][:urls_attributes]
      end

      def spreadsheet_param
        params[:spreadsheet]
      end

      def profile_params
        params.require(:profile).permit(
          :name, :image, :profile_type,
          :categories_ids, :content_type,
          facebook_handles_attributes: %i(id url price _destroy),
          youtube_handles_attributes: %i(id url price _destroy),
          twitter_handles_attributes: %i(id url price _destroy),
          pinterest_handles_attributes: %i(id url price _destroy),
          instagram_handles_attributes: %i(id url price _destroy),
          urls_attributes: %i(id href price description author_page follow_link allow_home_page
            allow_blog_post required_length _destroy),
          web_content_attributes: %i(id href price ga_account_id ga_profile_id website_url muv _destroy)
        )
      end

      def show_error(message)
        render json: { error: message }, status: :bad_request
      end

      def manage_extra_params
        @profile.toggle_categories categories_param.values          if categories_param.present?
        @profile.toggle_urls urls_params                            if urls_params.present?
        @profile.bulk_upload_file(spreadsheet_param, @profile.urls, @profile.contributor?) if spreadsheet_param.present?
      end

      def schedule_emails
        # TODO: restrict if the publishers first profile has been rejected
        # or the publishers account has been deleted by admin
        email_vars = {
          first_name: current_publisher.first_name,
          last_name: current_publisher.last_name,
          email: current_publisher.email,
          profile_name: @profile.name,
          profile_type: @profile.profile_type
        }
        Email.schedule('publisher_new_profile', current_publisher.user_id, email_vars)
        Email.schedule('admin_new_profile', AdminUser.first.try(:id), email_vars)
        Email.unschedule('publisher_submit_url_reminder', current_publisher.user_id)
      end

    end
  end
end
