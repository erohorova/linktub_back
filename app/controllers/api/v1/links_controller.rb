# encoding: utf-8

module Api
  module V1
    class LinksController < ApiController
      before_action :authenticate_advertiser!
      helper_method :link

      def index
        @links = campaign.links
      end

      def create
        @link = campaign.links.create! link_params
      end

      def orders_built
        @orders_built = Link.find(params[:link_id]).ads.includes(:url)
      end

      def destroy
        authorize link
        link.destroy!
        head :no_content
      end

      private

      def link_params
        params.require(:link).permit(:href)
      end

      def link
        @link ||= Link.find(params[:id])
      end

      def campaign
        current_advertiser.campaigns.find(params[:campaign_id])
      end
    end
  end
end
