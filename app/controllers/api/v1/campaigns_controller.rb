# encoding: utf-8

module Api
  module V1
    class CampaignsController < ApiController
      before_action :authenticate_advertiser!
      before_action :set_campaign, only: %i[show update destroy]

      def index
        @campaigns = scoped_campaigns
        @campaigns = @campaigns.limit(params[:limit]) if params[:limit].present?
      end

      def create
        @campaign = current_advertiser.campaigns.build campaign_params
        manage_extra_params
        @campaign.save!
        Email.unschedule('advertiser_url_submission', current_advertiser.user_id)
      end

      def names
        @names = current_advertiser.active_campaigns_desc_updated_at.pluck(:name)
      end

      def show
        links_id = @campaign.advertiser.active_content.of_campaign(@campaign.id).map(&:link_id)
        @links = Link.id_in(links_id).order(updated_at: :desc).page(params[:page])
        @pages = @links.total_pages
        @results = @links.total_count
      end

      def update_spreadsheet
        if spreadsheet_param.present?
          @campaign = current_advertiser.campaigns.find(params[:campaign_id])
          @campaign.bulk_upload_file(spreadsheet_param, @campaign.links, false)
          head :ok
        else
          render json: { error: 'No file was uploaded' }, status: :not_found
        end
      end

      # If there is a spreadsheet present in params the bulk upload all urls
      def update
        @campaign.update campaign_params.except(:links_attributes)
        @link = @campaign.links.find_or_create_by!(href: links_hrefs[0]) if links_hrefs.present?
        manage_extra_params
        @campaign.save!
      end

      def destroy
        @campaign.destroy!
        head :no_content
      end

      private

      def set_campaign
        @campaign = Campaign.find(params[:id])
      end

      def campaign_params
        params.require(:campaign).permit(:name, :image, :categories_ids, links_attributes: %i(id href))
      end

      def categories_param
        params[:categories_ids] if params[:campaign]
      end

      def links_hrefs
        campaign_params[:links_attributes]&.values&.map { |k| k[:href] }
      end

      def spreadsheet_param
        params[:spreadsheet]
      end

      def manage_extra_params
        @campaign.toggle_categories categories_param.values if categories_param.present?
        @campaign.bulk_upload_file(spreadsheet_param, @campaign.links, false) if spreadsheet_param.present?
      end

      def scoped_campaigns
        if params[:scope].present? && available_scope?
          current_advertiser.try("#{params[:scope].to_sym}_campaigns")
        else
          current_advertiser.campaigns_desc_updated_at
        end
      end

      def available_scope?
        Campaign.state_machine.states.map(&:name).include?(params[:scope].to_sym)
      end
    end
  end
end
