# encoding: utf-8

module Api
  module V1
    class AdvertisersController < ApiController
      before_action :authenticate_user!
      before_action :authenticate_advertiser!, only: :show
      helper_method :advertiser

      def create
        @advertiser = current_user.create_advertiser!
      end

      private

      def advertiser_params
        params.require(:advertiser).permit(:billable_date, :suggested_link_email_sent_at, :any_approved)
      end

      def advertiser
        @advertiser ||= current_advertiser
      end
    end
  end
end
