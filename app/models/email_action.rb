# == Schema Information
#
# Table name: email_actions
#
#  id                :integer          not null, primary key
#  email_trigger_id  :integer
#  email_template_id :integer
#  send_at           :datetime         not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  sent              :boolean          default(FALSE)
#  collate           :boolean          default(FALSE)
#  receiver_id       :integer
#  receiver_type     :string
#
# Indexes
#
#  index_email_actions_on_collate                        (collate)
#  index_email_actions_on_email_template_id              (email_template_id)
#  index_email_actions_on_email_trigger_id               (email_trigger_id)
#  index_email_actions_on_receiver_type_and_receiver_id  (receiver_type,receiver_id)
#

class EmailAction < ActiveRecord::Base
  # Associations
  belongs_to :email_template
  belongs_to :email_trigger
  belongs_to :receiver, polymorphic: true

  # Validattions
  validates :send_at, presence: true

  # Scopes
  scope :scheduled_for_today, -> { where(send_at: (Time.zone.now.beginning_of_day..Time.zone.now.end_of_day)).where(sent: false) }
  scope :scheduled_for_past_hours, -> (times) { where(send_at: (Time.zone.now - times.hour..Time.zone.now)).where(sent: false) }
  scope :single_emails_scheduled_for_today, -> { scheduled_for_today.where(collate: false) }
  scope :already_sent, -> { where(sent: true) }
  scope :collated_emails_scheduled_for_today, lambda {
    select('distinct on (receiver_id, receiver_type, email_template_id) *').scheduled_for_today.where(collate: true)
  }
  scope :collated_emails_scheduled_for_past_hours, lambda { |times|
    select('distinct on (receiver_id, receiver_type, email_template_id) *').scheduled_for_past_hours(times).where(collate: true)
  }
  scope :collated_together_not_sent, lambda { |action|
    where(receiver: action.receiver, email_template_id: action.email_template_id, sent: false, collate: true)
  }

  # Delegates
  delegate :response_completed?, to: :email_trigger

  def self.find_actions(slug, days)
    where(email_template_id: Email.find_by(slug: slug).email_templates.pluck(:id), send_at: days.from_now.beginning_of_day..days.from_now.end_of_day, sent: false)
  end

  def self.deliver_force(slug, days)
    find_actions(slug, days).map { |a| Email.deliver(a.id) }
  end
end
