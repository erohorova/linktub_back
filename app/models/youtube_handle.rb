# == Schema Information
#
# Table name: social_handles
#
#  id              :integer          not null, primary key
#  url             :string
#  price           :float
#  network         :string
#  type            :string
#  state           :integer
#  profile_id      :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  suggested_price :float            default(0.0)
#  shown_price     :float            default(0.0)
#
# Indexes
#
#  index_social_handles_on_network     (network)
#  index_social_handles_on_profile_id  (profile_id)
#  index_social_handles_on_state       (state)
#  index_social_handles_on_type        (type)
#

class YoutubeHandle < SocialHandle
  # Validations
  validates :url, social_url: { handler: name }
end
