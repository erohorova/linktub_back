# == Schema Information
#
# Table name: admin_users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  type                   :string
#  first_name             :string
#  last_name              :string
#  phone                  :string
#  state                  :integer
#  terminated_date        :date
#  activated_date         :date
#  paypal_email           :string
#  confirmation_token     :string
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  skrill_email           :string
#
# Indexes
#
#  index_admin_users_on_confirmation_token    (confirmation_token) UNIQUE
#  index_admin_users_on_email                 (email) UNIQUE
#  index_admin_users_on_first_name            (first_name)
#  index_admin_users_on_last_name             (last_name)
#  index_admin_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_admin_users_on_type                  (type)
#

class Agent < AdminUser
  include Ransackable
  self.table_name = 'admin_users'

  # Associations
  has_many :advertisers

  #Delegators
  delegate :count, to: :advertisers, prefix: true

  def sti_name
    'Agent'
  end

  def status_confirmed
    confirmed_at.present?
  end
end
