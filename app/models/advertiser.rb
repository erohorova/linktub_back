# == Schema Information
#
# Table name: advertisers
#
#  id                                            :integer          not null, primary key
#  billable_date                                 :date
#  suggested_link_email_sent_at                  :datetime
#  any_approved                                  :boolean          default(FALSE)
#  created_at                                    :datetime         not null
#  updated_at                                    :datetime         not null
#  awaiting_publication_spreadsheet_file_name    :string
#  awaiting_publication_spreadsheet_content_type :string
#  awaiting_publication_spreadsheet_file_size    :integer
#  awaiting_publication_spreadsheet_updated_at   :datetime
#  rejected_content_spreadsheet_file_name        :string
#  rejected_content_spreadsheet_content_type     :string
#  rejected_content_spreadsheet_file_size        :integer
#  rejected_content_spreadsheet_updated_at       :datetime
#  active_content_spreadsheet_file_name          :string
#  active_content_spreadsheet_content_type       :string
#  active_content_spreadsheet_file_size          :integer
#  active_content_spreadsheet_updated_at         :datetime
#  user_id                                       :integer
#  email                                         :string
#  content_pipe_zip_file_name                    :string
#  content_pipe_zip_content_type                 :string
#  content_pipe_zip_file_size                    :integer
#  content_pipe_zip_updated_at                   :datetime
#  activated_date                                :datetime
#  terminated_date                               :datetime
#  state                                         :integer
#  auto_approved                                 :boolean          default(FALSE)
#  advertisers_report_spreadsheet_file_name      :string
#  advertisers_report_spreadsheet_content_type   :string
#  advertisers_report_spreadsheet_file_size      :integer
#  advertisers_report_spreadsheet_updated_at     :datetime
#  agent_id                                      :integer
#
# Indexes
#
#  index_advertisers_on_agent_id  (agent_id)
#  index_advertisers_on_state     (state)
#  index_advertisers_on_user_id   (user_id)
#

class Advertiser < AccountType
  include ScopesForAds
  include ScopesForOrders
  include ScopesForContents
  include ScopesForArticles
  include AccountTypeStates
  include AccountTypeStates
  include Ransackable
  include ActiveContentManager

  EXCEL_MIME = 'application/vnd.ms-excel'.freeze
  ZIP_MIME = 'application/zip'.freeze
  HEADERS = ['Date purchased', 'Campaign', 'Publisher', 'DA', 'Cost', 'Destination URL', 'Anchor Text', 'Live Link'].freeze
  NO_PURCHASE_TIME = 60.days.freeze
  ADMIN_HEADERS = ['Account Create Date', 'Adv Email', 'First / Last Name', 'Assigned Agent'].freeze

  # Relationships
  belongs_to :user
  belongs_to :author, class_name: 'AdminUser'
  belongs_to :agent, class_name: 'AdminUser'
  has_many   :ads
  has_many   :campaigns
  has_many   :links, -> { distinct }, through: :campaigns
  has_many   :orders
  has_many   :contents
  has_many   :transactions, as: :payer
  has_many   :invoices
  has_many   :articles

  # Attachments
  has_attached_file :rejected_content_spreadsheet
  has_attached_file :awaiting_publication_spreadsheet
  has_attached_file :advertisers_report_spreadsheet
  has_attached_file :content_pipe_zip

  validates_attachment :rejected_content_spreadsheet, content_type: { content_type: EXCEL_MIME }
  validates_attachment :awaiting_publication_spreadsheet, content_type: { content_type: EXCEL_MIME }
  validates_attachment :advertisers_report_spreadsheet, content_type: { content_type: EXCEL_MIME }
  validates_attachment :content_pipe_zip, content_type: { content_type: ZIP_MIME }

  # Delegates
  delegate :credit_cards, :email, :messages, :notifications,
           :first_name, :last_name, :full_name, to: :user

  # Lifecycle's callbacks
  after_create :reminder_submission

  # Scopes
  scope :active_with_active_campaigns, -> { active.joins(:campaigns).where("campaigns.state = #{Campaign.states[:active]}").uniq }
  scope :without_content,              -> { where(any_approved: false) }
  scope :by_campaign_id,               ->(id) { joins(:campaigns).where(campaigns: { id: id }) }

  def create_invoice(orders)
    orders.map(&:hold)
    invoices.create! orders: orders
  end

  def active_ads
    ads.approved_advertiser_not_cancelled
  end

  def pending_ads
    ads.where(state: pending_ads_states)
  end

  def declined_ads
    ads.rejected
  end

  def active_campaigns
    campaigns.active
  end

  def active_campaigns_desc_updated_at
    campaigns.active_desc_updated_at
  end

  def campaigns_desc_updated_at
    campaigns.desc_updated_at
  end

  def pending_links
    active_campaigns.flat_map(&:pending_links)
  end

  def approved_links
    active_campaigns.flat_map(&:approved_links)
  end

  def active_content
    active_ads
  end

  def awaiting_publication
    pending_ads
  end

  def requested_content
    contents.requested.order(id: :desc)
  end

  def received_content
    contents.received.order(id: :desc)
  end

  def rejected_content
    declined_ads
  end

  def rejected_or_cancelled_content
    declined_ads.union(cancelled_ads).not_seen
  end

  ## Notification's count methods
  def active_content_notifications_count
    user.active_content_notifications.count
  end

  def awaiting_pubs_notifications_count
    user.awaiting_publication_notifications.count
  end

  def approved_links_notifications_count
    user.urls_approved_notifications.count
  end

  def rejected_content_notifications_count
    user.rejected_content_notifications.count
  end

  def new_quotes_notifications_count
    user.quotes_notifications.count
  end

  def new_articles_notifications_count
    user.articles_notifications.count
  end

  ## Count's methods
  def active_content_count
    active_ads.count
  end

  def awaiting_pubs_count
    pending_ads.count
  end

  def approved_links_count
    active_campaigns.flat_map(&:approved_links).count
  end

  def rejected_content_count
    declined_ads.count
  end

  def rejected_or_cancelled_content_count
    rejected_or_cancelled_content.count
  end

  def new_message_count
    messages.unread.count
  end

  def aging_accounts
    date = Time.now - 60.days
    invoices.where('orders_accepted_count > ? AND created_at > ?', 0, date).count.zero?
  end

  def days_last_order
    last_order_date = invoices.where('orders_accepted_count > ?', 0)
                              .order(:created_at).last&.created_at
    (Time.now.to_date - last_order_date.to_date).to_i if last_order_date.present?
  end

  def update_campaign_report_spreadsheet(campaign)
    campaign.update_campaign_report_spreadsheet
  end

  def destroy_content_notification
    notifications.articles.sample&.delete
  end

  def remove_orders_articles_from_cart(orders_ids)
    orders.where(id: orders_ids).find_each { |o| o.ad&.article&.update!(removed_from_cart: true) }
  end

  def update_active_content_spreadsheet
    self.active_content_spreadsheet = SpreadsheetService.create('active_content') do |sheet|
      sheet.add_row HEADERS
      active_content.order(requested_date: :desc).find_each do |resource|
        sheet.add_row [resource.requested_date.strftime('%m/%d/%Y'), resource.campaign.name,
                       resource.url.href, resource.url.domain_authority, resource.url.shown_price,
                       resource.link.href, resource.link_text, resource.full_pub_url]
      end
    end

    active_content_spreadsheet.instance_write(:content_type, EXCEL_MIME)
    active_content_spreadsheet.instance_write(:file_name, 'active_content.xlsx')
    save!
  end

  def update_advertisers_report_spreadsheet(results)
    self.advertisers_report_spreadsheet = SpreadsheetService.create('advertisers_report') do |sheet|
      sheet.add_row ADMIN_HEADERS
      sheet.column_widths 25, 25, 25, 25
      results.each do |result|
        agent = result.agent.present? ? result.agent&.full_name : 'No agent'
        sheet.add_row [result.created_at.strftime('%m/%d/%Y'), result.email, result.user.full_name, agent]
      end
    end
    advertisers_report_spreadsheet.instance_write(:content_type, EXCEL_MIME)
    advertisers_report_spreadsheet.instance_write(:file_name, 'advertisers_report.xlsx')
    save!
  end

  def advertisers_report_spreadsheet_doc
    { name: advertisers_report_spreadsheet.original_filename, url: advertisers_report_spreadsheet.url[/[^?]+/] } if advertisers_report_spreadsheet.present?
  end

  def update_zip_file
    tempfile = Tempfile.new(["scalefluence_content_pipe_#{SecureRandom.hex}", '.zip'], Rails.root.join('tmp'))
    names = admin_approved_articles.map { |a| a.ad.blog_post_doc.file.filename if a.ad.blog_post_doc.file.present? }
    repeated_names = names.select { |n| names.count(n) > 1 }.uniq
    Zip::File.open(tempfile.path, Zip::File::CREATE) do |zipfile|
      admin_approved_articles.each_with_index do |article, index|
        next unless article.ad.blog_post_doc.file.present?

        number = (repeated_names.include?(article.ad.blog_post_doc.file.filename) && index != 0) ? "(#{++index})" : ''
        Tempfile.open('doc') do |temp|
          file = open(article.ad.blog_post_doc.file.url, 'rb')
          temp.write(file.read.force_encoding(Encoding::UTF_8))
          temp.close

          zipfile.add("#{number} #{article.ad.blog_post_doc.file.filename}", temp.path)
        end
      end
    end
    self.content_pipe_zip = StringIO.new(File.read(tempfile.path))
    content_pipe_zip.instance_write(:content_type, ZIP_MIME)
    content_pipe_zip.instance_write(:file_name, "scalefluence_content_pipe_#{SecureRandom.hex}.zip")
    save!
  end

  def self.inner_joins
    joins('LEFT JOIN campaigns ON advertisers.id = campaigns.advertiser_id')
      .joins('LEFT JOIN links ON campaigns.id = links.campaign_id')
      .joins(:user).distinct
  end

  # Ransackable overridden methods
  def self.format_query
    cols = columns.select do |c|
      %i[string text].include?(c.type)
    end

    # generate query fragment and add publisher email to the query
    cols.map { |c| "#{table_name}.#{c.name} ILIKE ?" }.join(' OR ') +
      ' OR users.first_name ILIKE ?' \
      ' OR users.last_name ILIKE ?' \
      ' OR users.email ILIKE ?' \
      ' OR links.href ILIKE ?' \
  end

  private

  def pending_ads_states
    Ad.states.deep_symbolize_keys.fetch_values(:pending, :resubmitted, :rejected_by_admin)
  end

  def reminder_submission
    email_vars = attributes.symbolize_keys
    email_vars[:advertiser_first_name] = first_name
    email_vars[:advertiser_last_name] = last_name
    Email.schedule('advertiser_link_submission_reminder', user_id, email_vars)
  end
end
