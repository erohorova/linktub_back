# == Schema Information
#
# Table name: orders
#
#  id                   :integer          not null, primary key
#  advertiser_id        :integer
#  url_id               :integer
#  ad_id                :integer
#  completed_date       :datetime
#  state                :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  invoice_id           :integer
#  created_from         :string
#  transaction_id       :integer
#  completed_by         :string
#  searched_campaign_id :integer
#
# Indexes
#
#  index_orders_on_ad_id          (ad_id)
#  index_orders_on_advertiser_id  (advertiser_id)
#  index_orders_on_invoice_id     (invoice_id)
#  index_orders_on_state          (state)
#  index_orders_on_url_id         (url_id)
#

class Order < ActiveRecord::Base
  include OrderStates
  include Ransackable

  # Associations
  belongs_to :advertiser
  belongs_to :url
  belongs_to :ad
  belongs_to :invoice
  belongs_to :transaction_order, foreign_key: 'transaction_id', class_name: 'Transaction', dependent: :destroy
  has_one :publisher, through: :url

  # Lifecycle's callbacks
  before_update :set_advertiser
  before_destroy :change_article_state

  # Delegates
  delegate :price, to: :url
  delegate :admin_payout_date, to: :ad

  # Nested attributes
  accepts_nested_attributes_for :ad, reject_if: :all_blank

  # Scopes
  scope :all_but_pending, lambda {
    joins(:ad).where('ads.state = ? OR ads.state = ?', Ad.states[:approved_by_publisher], Ad.states[:approved])
              .where(state: [states[:on_hold], states[:completed]]).where(completed_by: nil).uniq
  }
  scope :on_hold_or_completed_for_url, lambda { |url_id|
    where(url_id: url_id).where(state: [states[:on_hold], states[:completed]])
  }
  scope :cancelled_or_bought_for_url, lambda { |url_id|
    where(url_id: url_id).where(state: [states[:cancelled], states[:bought]])
  }
  scope :paid, lambda {
     joins(:ad)
    .joins('INNER JOIN payouts ON payouts.ad_id = ads.id')
    .where('payouts.id IS NOT NULL')
  }

  def decline(message)
    ad.reject_by_admin
    ad.update!(rejected_reason: 'Declined by admin', editable: true)
    ad.comments.create!(comment: message, role: AdminUser::ADMIN)
  end

  def email_vars
    email_vars = attributes.symbolize_keys
    email_vars[:advertiser_first_name] = advertiser.try(:first_name)
    email_vars[:advertiser_last_name] = advertiser.try(:last_name)
    email_vars[:publisher_first_name] = publisher.try(:first_name)
    email_vars[:publisher_last_name] = publisher.try(:last_name)
    email_vars[:content_url] = ad&.blog_post_doc&.url
    email_vars
  end

  def email_vars_publisher_cancelled
    vars_cancelled = email_vars
    vars_cancelled[:blog_post_url] = ad&.try(:blog_post_title)
    vars_cancelled[:pub_url] = ad&.try(:full_pub_url)
    vars_cancelled[:notes] = ad&.try(:notes)
    vars_cancelled
  end

  def refund(notes)
    transaction_service.refund(self)
    send_refund_email(notes)
    ad.cancel
    ad.update!(editable: false, full_pub_url: "Refunded #{Time.zone.now.strftime('%m-%d-%Y')} #{notes}")
    cancel
  end

  def profile_categories
    url.profile.categories
  end

  def profile_name
    url.profile.name
  end

  def amount
    content_ids = advertiser.contents.where(campaign_id: ad&.campaign_id).pluck(:id)
    content_url = url.content_urls.find_by(content_id: content_ids)
    content_url&.shown_price || url.shown_price
  end

  def set_advertiser
    ad.update!(advertiser: advertiser)
  end

  def remove_from_transaction
    transaction_order.orders.delete(self)
    transaction_order.amount = (transaction_order.amount.to_i - url.shown_price.to_i).to_s
    transaction_order.save!
  end

  # Ransackable overridden methods
  def self.format_query
    cols = columns.select do |c|
      %i(string text).include?(c.type)
    end

    # generate query fragment and add publisher email to the query
    cols.map { |c| "#{table_name}.#{c.name} ILIKE ?" }.join(' OR ') +
      ' OR urls.href ILIKE ?' \
      ' OR publishers.email ILIKE ?' \
      ' OR advertisers.email ILIKE ?' \
  end

  def self.inner_joins
    joins(:url).joins(:advertiser)
      .joins('INNER JOIN profiles ON profiles.id = urls.profile_id')
      .joins('INNER JOIN publishers ON publishers.id = profiles.publisher_id').distinct
  end

  private

  def change_article_state
    ad&.article&.admin_approved!
  end

  def transaction_service
    TransactionService.new(AdminUser.default_admin_user)
  end

  def send_refund_email(notes)
    adv_subject = "Hey #{advertiser.first_name}, we refunded an order for you"
    adv_body = "#{advertiser.first_name},<br /><br />an order has been refunded today. Here are the notes: <br /><br />#{notes}<br /><br />" +
           "The blogger was #{ad.full_pub_url} and it was for the #{ad.campaign.name} campaign. <br />" +
           "The amount refunded was U$S #{url.shown_price.to_s}. <br />Please email admin@scalefluence.com to discuss doing a replacement." +
           "<br /> <br />We appreciate your understanding! <br /><br />-- The Scalefluence Team --";
    doc_url = ad.article&.blog_post_doc ? ad.article.blog_post_doc[:url] : nil

    pub_subject = "Order refunded"
    pub_body = "Hey #{publisher.first_name},<br/>We have decided to refund this article. The content is attached.<br/><br/>" +
               "It was going on this URL: #{ad.full_pub_url}.<br/><br/>Do NOT publish this article. The advertiser has cancelled this order." +
               "<br/><br/>Sorry for the inconvenience!<br/><br/>-- The Scalefluence Team --";

    adv_email_params = {
      body: adv_body,
      email: advertiser.try(:email),
      subject: adv_subject,
      doc_url: doc_url,
      cc: AdminUser.default_admin_user.try(:email)
    }

    pub_email_params = {
      body: pub_body,
      email: publisher.try(:email),
      subject: pub_subject,
      doc_url: doc_url,
      cc: AdminUser.default_admin_user.try(:email)
    }

    QuickMailer.email(adv_email_params).deliver_now
    QuickMailer.email(pub_email_params).deliver_now
  end
end
