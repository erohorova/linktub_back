# == Schema Information
#
# Table name: transactions
#
#  id                          :integer          not null, primary key
#  braintree_transaction_id    :string
#  payer_id                    :integer
#  payer_type                  :string
#  receiver_id                 :integer
#  receiver_type               :string
#  description                 :text
#  amount                      :string
#  state                       :integer
#  voided_at                   :datetime
#  declined_at                 :datetime
#  rejected_at                 :datetime
#  authorized_at               :datetime
#  submitted_for_settlement_at :datetime
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  is_refund                   :boolean          default(FALSE)
#  amount_submitted            :string
#
# Indexes
#
#  index_transactions_on_payer_type_and_payer_id        (payer_type,payer_id)
#  index_transactions_on_receiver_type_and_receiver_id  (receiver_type,receiver_id)
#

class Transaction < ActiveRecord::Base
  include TransactionStates
  include Ransackable

  # Associations
  belongs_to :payer, polymorphic: true
  belongs_to :receiver, polymorphic: true
  has_many :orders

  scope :completed, lambda {
    where(state: [states[:authorized], states[:submitted_for_settlement], states[:settled]])
  }

  scope :between_dates, -> (from, to) { where(created_at: DateTime.parse(from).beginning_of_day..DateTime.parse(to).end_of_day) }

  # returns the charged amount if settlement was to be done at the moment of execution
  def amount_at_the_moment
    orders_with_approved_ad = orders.select { |o| o.ad.approved_by_publisher? }
    orders_with_approved_ad.map(&:amount).reduce(0, :+)
  end

  def ready?
    orders.any? { |o| o.ad.approved_by_publisher? }
  end

  def self.inner_joins
    joins(:orders)
     .joins('INNER JOIN ads ON ads.id = orders.ad_id')
     .joins('INNER JOIN advertisers ON advertisers.id = orders.advertiser_id')
     .joins('INNER JOIN campaigns ON campaigns.id = ads.campaign_id')
     .joins('INNER JOIN users ON users.id = advertisers.user_id').distinct
  end

  def self.format_query
    cols = columns.select { |c| %i(string text).include?(c.type) }
    cols.map { |c| "#{table_name}.#{c.name} ILIKE ?" }.join(' OR ') +
     ' OR advertisers.email ILIKE ?' \
     ' OR users.first_name ILIKE ?' \
     ' OR users.last_name ILIKE ?' \
     ' OR campaigns.name ILIKE ?'
  end
end
