# encoding: utf-8
# == Schema Information
#
# Table name: ads
#
#  id                   :integer          not null, primary key
#  link_text            :string
#  requested_date       :datetime
#  approved_date        :datetime
#  rejected_date        :datetime
#  reconsidered_date    :datetime
#  cancelled_date       :datetime
#  admin_cancelled      :boolean          default(FALSE)
#  advertiser_cancelled :boolean          default(FALSE)
#  publisher_cancelled  :boolean          default(FALSE)
#  url_price            :decimal(, )      default(0.0)
#  blog_post            :string           default("blog_post")
#  blog_post_title      :string
#  blog_post_body       :string
#  quote_id             :string
#  full_pub_url         :string
#  attach_word_document :boolean          default(TRUE)
#  blog_post_doc        :string
#  blog_post_image_1    :string
#  blog_post_image_2    :string
#  state                :integer
#  rejected_reason      :string
#  notes                :text
#  editable             :boolean
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  advertiser_id        :integer
#  link_id              :integer
#  url_id               :integer
#  campaign_id          :integer
#  rejected_counter     :integer          default(0)
#  ordered_date         :date
#  seen                 :boolean          default(FALSE)
#
# Indexes
#
#  index_ads_on_advertiser_id  (advertiser_id)
#  index_ads_on_campaign_id    (campaign_id)
#  index_ads_on_link_id        (link_id)
#  index_ads_on_state          (state)
#  index_ads_on_url_id         (url_id)
#

class Ad < ActiveRecord::Base
  include AdStates
  include Ransackable
  acts_as_commentable

  # Relationships
  belongs_to :advertiser
  belongs_to :link
  belongs_to :url
  has_one    :publisher, through: :url
  belongs_to :campaign
  has_one    :order, dependent: :nullify
  has_one    :article
  has_one    :payout

  # Constants
  DELAY_WINDOW = 72.hours.freeze
  REJECTED_BY_REFUND = 'Refunded'.freeze

  # Uploaders
  mount_uploader :blog_post_doc, DocumentUploader
  mount_uploader :blog_post_image_1, ImageUploader
  mount_uploader :blog_post_image_2, ImageUploader

  # Nested attributes for ...
  accepts_nested_attributes_for :comments, allow_destroy: true, reject_if: :all_blank

  # Callback
  after_save :update_zip_file

  # Scopes
  scope :approved_advertiser_not_cancelled, -> { approved.union(approved_by_publisher).union(cancelled) } # Advertiser cancelled
  scope :general_approved_ads, -> { where(state: [states[:approved], states[:approved_by_publisher]]) }
  scope :unresponsive, ->(start_date) { where('requested_date <= ?', start_date) }
  scope :reassignable, -> { where(state: [states[:cart_item], states[:pending], states[:approved_by_publisher], states[:reconsidered], states[:resubmitted]]) }
  scope :declined_or_refunded, -> { where(state: [states[:rejected], states[:cancelled], states[:rejected_by_admin]]) }
  scope :not_seen, -> { where(seen: false) }
  scope :of_campaign, ->(campaign_id) { where(campaign_id: campaign_id) }

  # Delegates
  delegate :price, to: :url

  def admin_payout_date
    paid_date = Ad.states[state] == Ad.states[:paid] ? Payout.find_by(publisher: publisher, ad: self) : nil
    paid_date&.created_at
  end

  def self.inner_joins
    joins(:url).joins(:advertiser).joins(:campaign)
  end

  def self.format_query
    cols = columns.select { |c| %i(string text).include?(c.type) && %w(full_pub_url link_text).include?(c.name) }

    cols.map { |c| "#{table_name}.#{c.name} ILIKE ?" }.join(' OR ') +
    ' OR urls.href ILIKE ?' \
    ' OR advertisers.email ILIKE ?' \
    ' OR campaigns.name ILIKE ?'
  end

  def send_advertiser_content_rewrite_email
    email_vars = attributes.symbolize_keys
    email_vars[:advertiser_first_name] = advertiser.try(:first_name)
    email_vars[:advertiser_last_name] = advertiser.try(:last_name)
    email_vars[:publisher_first_name] = publisher.try(:first_name)
    email_vars[:publisher_last_name] = publisher.try(:last_name)
    email_vars[:campaign_name] = campaign.try(:name)
    Email.schedule('advertiser_content_rewrite', advertiser.try(:user_id), email_vars)
  end

  private

  def update_zip_file
    article.update_zip_file if article.present? && blog_post_doc_changed?
  end
end
