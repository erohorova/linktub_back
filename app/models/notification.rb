# == Schema Information
#
# Table name: notifications
#
#  id                :integer          not null, primary key
#  description       :text
#  user_id           :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  notification_type :integer
#
# Indexes
#
#  index_notifications_on_notification_type  (notification_type)
#  index_notifications_on_user_id            (user_id)
#

class Notification < ActiveRecord::Base
  # Available notifications' types
  enum types: {
    active_content: 1,
    awaiting_publication: 2,
    urls_approved: 3,
    rejected_content: 4,
    active_ads: 5,
    new_orders: 6,
    pricing_and_pausing: 7,
    resubmitted_content: 8,
    quotes: 9,
    articles: 10,
    messages: 11
  }

  Notification.types.each do |name, value|
    scope name, -> { where(notification_type: value) }
  end

  # Get the humanize name
  def type
    Notification.types.key(notification_type)
  end

  # Relationships
  belongs_to :user
end
