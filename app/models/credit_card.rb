# == Schema Information
#
# Table name: credit_cards
#
#  id               :integer          not null, primary key
#  masked_number    :string
#  braintree_token  :string
#  cardholder_name  :string
#  card_type        :string
#  expiration_month :integer
#  expiration_year  :integer
#  image_url        :string
#  street_address   :string
#  extended_address :string
#  city             :string
#  state            :string
#  zip_code         :string
#  country_name     :string
#  user_id          :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
# Indexes
#
#  index_credit_cards_on_braintree_token  (braintree_token)
#  index_credit_cards_on_user_id          (user_id)
#

class CreditCard < ActiveRecord::Base
  # Relationships
  belongs_to :user

  # Validattions
  validates :user, :masked_number, :braintree_token, presence: true
  validates :braintree_token, uniqueness: true
  validates :masked_number, uniqueness: true
end
