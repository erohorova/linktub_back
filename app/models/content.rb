# == Schema Information
#
# Table name: contents
#
#  id                            :integer          not null, primary key
#  advertiser_id                 :integer
#  campaign_id                   :integer
#  state                         :integer          not null
#  domain_authority_min          :integer
#  domain_authority_max          :integer
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#  urls_spreadsheet_file_name    :string
#  urls_spreadsheet_content_type :string
#  urls_spreadsheet_file_size    :integer
#  urls_spreadsheet_updated_at   :datetime
#  trust_flow_min                :integer
#  trust_flow_max                :integer
#  agency_content                :boolean          default(FALSE)
#  created_by                    :string
#  deleted_at                    :datetime
#  ahrefs_dr_min                 :integer
#  ahrefs_dr_max                 :integer
#
# Indexes
#
#  index_contents_on_advertiser_id  (advertiser_id)
#  index_contents_on_campaign_id    (campaign_id)
#  index_contents_on_deleted_at     (deleted_at)
#  index_contents_on_state          (state)
#

class Content < ActiveRecord::Base
  include ContentStates
  include Ransackable
  include ScopesForLinks
  include ExcelQuoteManager
  acts_as_paranoid without_default_scope: true

  # Constants
  EXCEL_MIME = 'application/vnd.ms-excel'.freeze
  HEADERS = ['Unique Identifier', 'Campaign', 'Publisher', 'Price', 'MOZ DA', 'MOZ PA', 'Majestic TF',
             'Majestic CF', 'Ahrefs DR', 'Ahrefs LRD', 'Do-Follow?', 'Home Page Links?', 'Only Blog Post Links?',
             'Topic', 'Destination URL', 'Anchor', 'Special Instructions'].freeze
  ADMIN = 'admin'.freeze
  ADVERTISER = 'advertiser'.freeze
  MAX_LENGTH = 31

  # Validations
  validates :domain_authority_min, numericality: { greater_than: 0, less_than_or_equal_to: :domain_authority_max }
  validates :domain_authority_max, numericality: { less_than_or_equal_to: 100 }
  validates :trust_flow_min,       numericality: { greater_than: 0, less_than_or_equal_to: :trust_flow_max }
  validates :trust_flow_max,       numericality: { less_than_or_equal_to: 100 }

  # Associations
  belongs_to :advertiser
  belongs_to :campaign
  has_many   :categorizings, as: :categorizable
  has_many   :content_links, dependent: :destroy
  has_many   :content_urls
  has_many   :categories,    through: :categorizings
  has_many   :links,         through: :content_links
  has_many   :urls,          through: :content_urls, after_add: :update_urls_spreadsheet,
                             after_remove: :update_urls_spreadsheet

  # Scopes...
  scope :ordered_by_date,    -> { order(created_at: :desc) }
  scope :pending,            -> { where(state: content_pending_states) }
  scope :active,             -> { without_deleted } # acts_as_paranoid function.
  scope :deleted,            -> { only_deleted } # acts_as_paranoid function.

  # Cloning config
  amoeba do
    enable
    include_association %i[categories links]
    exclude_association :content_urls
    nullify :campaign_id
  end

  # Attachments
  has_attached_file    :urls_spreadsheet
  validates_attachment :urls_spreadsheet, content_type: { content_type: EXCEL_MIME }

  def configure_rows(sheet)
    # Custom styles
    align_left = sheet.workbook.styles.add_style alignment: { horizontal: :left }
    currency = sheet.workbook.styles.add_style num_fmt: 5
    unlocked = sheet.workbook.styles.add_style locked: false

    urls.each do |url|
      content_url = url.content_urls.find_by(content_id: id, url_id: url.id)
      sheet.add_row [
        url.id, campaign.name, url.href, content_url.shown_price, url.domain_authority,
        url.page_authority, url.trust_flow, url.citation_flow, url.ahrefs_dr, url.ahrefs_lrd,
        url.follow_link.humanize, url.allow_home_page.humanize, url.allow_blog_post.humanize,
        nil, nil, nil, nil
      ], style: [
        align_left, nil, nil, currency, nil, nil, nil, nil, nil, nil, nil, nil, nil,
        unlocked, unlocked, unlocked, unlocked
      ]
    end
  end

  def self.find_or_duplicate(content, campaign, url)
    if content.campaign_id == campaign[:id]
      ContentUrl.create!(content_id: content.id, url_id: url[:id], shown_price: url[:shown_price])
      content
    else
      found_content = Content.find_by(campaign_id: campaign[:id])
      if found_content.nil?
        found_content = content.amoeba_dup
        found_content.advertiser_id = Campaign.find(campaign[:id]).advertiser_id
        found_content.campaign_id = campaign[:id]
        found_content.save!
      end
      found_content.urls.push(Url.find(url[:id]))
      found_content.content_urls.last.update!(shown_price: url[:shown_price])
      found_content
    end
  end

  def urls_spreadsheet_doc
    { name: urls_spreadsheet.original_filename, url: urls_spreadsheet.url[/[^?]+/] } if urls_spreadsheet.present?
  end

  def self.inner_joins
    joins(:advertiser).joins(:campaign)
                      .joins('INNER JOIN users ON users.id = advertisers.user_id')
                      .joins('INNER JOIN links ON links.campaign_id = campaigns.id').distinct
  end

  # Ransackable overridden methods
  def self.format_query
    cols = columns.select { |c| %i[string text].include?(c.type) }

    # generate query fragment and add publisher email to the query
    cols.map { |c| "#{table_name}.#{c.name} ILIKE ?" }.join(' OR ') +
      ' OR campaigns.name ILIKE ?' \
      ' OR links.href ILIKE ?' \
      ' OR advertisers.email ILIKE ?' \
      ' OR users.first_name ILIKE ?' \
      ' OR users.last_name ILIKE ?' \
  end

  def first_approve_link
    campaign.approved_links.first
  end

  def self.content_pending_states
    Content.states.deep_symbolize_keys
           .fetch_values(:requested, :received)
  end

  def report_name
    "#{Date.today.month}.#{Date.today.day} #{campaign.name}".first(MAX_LENGTH).strip
  end

  def update_urls_spreadsheet(_param = nil)
    self.urls_spreadsheet = SpreadsheetService.create(report_name) do |sheet|
      # Make the file protected
      sheet.sheet_protection do |protection|
        protection.password = ENV['EXCEL_PASSWORD']
        protection.auto_filter = false
      end

      sheet.add_row HEADERS
      sheet.column_widths 20, 25, 25, 16, 16, 16, 16, 16, 16, 16, 16, 20, 25, 25, 30, 25, 30

      configure_rows(sheet)

      # Cell range styling
      range = "A1:K#{sheet.rows.count}"
      sheet.add_style 'A1:Q1', b: true, alignment: { horizontal: :left }, sz: 13
      sheet.add_style range, sz: 12, wrap_text: true
      sheet.add_style 'N1:Q1', bg_color: '90CAF9'
      sheet.add_style 'O1:P1', bg_color: 'FFD54F'
      sheet.add_border 'A1:Q1'
    end

    urls_spreadsheet.instance_write(:content_type, EXCEL_MIME)
    urls_spreadsheet.instance_write(:file_name, "#{report_name}.xlsx")
    save!
  end
end
