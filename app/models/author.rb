# == Schema Information
#
# Table name: admin_users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  type                   :string
#  first_name             :string
#  last_name              :string
#  phone                  :string
#  state                  :integer
#  terminated_date        :date
#  activated_date         :date
#  paypal_email           :string
#  confirmation_token     :string
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  skrill_email           :string
#
# Indexes
#
#  index_admin_users_on_confirmation_token    (confirmation_token) UNIQUE
#  index_admin_users_on_email                 (email) UNIQUE
#  index_admin_users_on_first_name            (first_name)
#  index_admin_users_on_last_name             (last_name)
#  index_admin_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_admin_users_on_type                  (type)
#

class Author < AdminUser
  include ScopesForArticles
  include Ransackable
  self.table_name = 'admin_users'

  # Associations
  has_many :articles
  has_many :author_invoices, foreign_key: 'admin_user_id'
  has_many :categorizings, as: :categorizable
  has_many :categories, through: :categorizings

  # Delegators
  delegate :count, to: :articles, prefix: true

  # Scopes
  scope :confirmed_active, -> { active.where.not(confirmed_at: nil) }

  def sti_name
    'Author'
  end

  def self.format_query
    cols = columns.select { |c| %i(string text).include?(c.type) }

    # generate query fragment and add publisher email to the query
    cols.map { |c| "#{table_name}.#{c.name} ILIKE ?" }.join(' OR ')
  end

  def generate_invoice
    latest = author_invoices.last
    articles = admin_approved_articles
    articles = articles.where('approved_date > ?', latest.created_at) if latest
    invoice = author_invoices.build
    invoice.articles << articles
    invoice.approved_articles_count = articles.count
    invoice.upload_spreadsheet(articles)
    invoice
  end

  private

  def author_states
    Article.states.deep_symbolize_keys
           .fetch_values(:assigned, :completed, :agency_rejected, :admin_rejected, :admin_approved)
  end
end
