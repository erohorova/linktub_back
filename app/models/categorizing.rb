# == Schema Information
#
# Table name: categorizings
#
#  id                 :integer          not null, primary key
#  category_id        :integer
#  categorizable_id   :integer
#  categorizable_type :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_categorizings_on_categorizable_type_and_categorizable_id  (categorizable_type,categorizable_id)
#  index_categorizings_on_category_id                              (category_id)
#

class Categorizing < ActiveRecord::Base
  # Associations
  belongs_to :categorizable, polymorphic: true
  belongs_to :category
end
