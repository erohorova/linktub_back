# == Schema Information
#
# Table name: links
#
#  id                :integer          not null, primary key
#  href              :string
#  approved_date     :date
#  suspended_date    :date
#  flagged_date      :date
#  archived_date     :date
#  reconsidered_date :date
#  declined_date     :date
#  page_rank         :integer          default(0)
#  alexa_rank        :integer          default(0)
#  back_links        :integer          default(0)
#  domain_age        :float            default(0.0)
#  state             :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  campaign_id       :integer
#
# Indexes
#
#  index_links_on_campaign_id  (campaign_id)
#  index_links_on_state        (state)
#  index_links_on_updated_at   (updated_at)
#

class Link < ActiveRecord::Base
  include LinkStates
  include Ransackable

  # Relationships
  belongs_to :campaign, autosave: true, touch: true
  has_many   :ads,      dependent: :destroy
  has_many   :urls,     through: :ads

  # Validations
  validates :href, presence: true, url: true

  # Lifecycle's callbacks
  after_create :update_state
  after_update :update_state

  # Scopes
  scope :id_in,                 ->(ids) { where(id: ids) }
  scope :links_desc_updated_at, -> { order(updated_at: :desc) }
  scope :not_archived,          -> { where.not(state: 5) }
  scope :pending,               -> { where(state: %w(1 2)) }

  def self.inner_joins
    joins(:campaign).joins('INNER JOIN advertisers ON advertisers.id = campaigns.advertiser_id')
                    .joins('INNER JOIN users ON users.id = advertisers.user_id')
  end

  # Ransackable overridden methods
  def self.format_query
    cols = columns.select { |c| %i(string text).include?(c.type) && c.name == 'href' }

    # generate query fragment and add publisher email to the query
    cols.map { |c| "#{table_name}.#{c.name} ILIKE ?" }.join(' OR ') +
      ' OR advertisers.email ILIKE ?' \
      ' OR users.phone ILIKE ?' \
      ' OR campaigns.name ILIKE ?'
  end

  def approved_advertiser?
    campaign&.advertiser&.auto_approved?
  end

  def approve_advertiser
    campaign&.advertiser&.update! auto_approved: true
  end

  private

  def update_state
    approve if can_approve? && campaign&.advertiser&.auto_approved?
  end
end
