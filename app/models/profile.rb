# == Schema Information
#
# Table name: profiles
#
#  id           :integer          not null, primary key
#  name         :string
#  image        :string
#  profile_type :string
#  total_earned :float            default(0.0)
#  total_orders :integer          default(0)
#  total_sites  :integer          default(0)
#  products     :text
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  publisher_id :integer
#  content_type :string           default("collaborate"), not null
#
# Indexes
#
#  index_profiles_on_profile_type  (profile_type)
#  index_profiles_on_publisher_id  (publisher_id)
#

class Profile < ActiveRecord::Base
  include AccountItems
  include ScopesForUrls
  acts_as_spread_sheet_manager

  # Constants
  PROFILE_TYPES = %w(blogger influencer broker contributor).freeze
  HEADERS = %w(href follow_link allow_home_page allow_blog_post first_length second_length third_length fourth_length price).freeze
  HEADERS_CONTRIBUTOR = %w(href author_page description follow_link allow_home_page allow_blog_post first_length second_length third_length fourth_length price).freeze
  CONTENT_TYPES = %w(own ghosted collaborate).freeze

  # Serializers
  serialize :products

  # Associations
  belongs_to :publisher
  has_many   :categorizings, as: :categorizable
  has_many   :categories, through: :categorizings
  has_many   :urls, dependent: :nullify
  has_many   :ads, through: :urls
  has_many   :social_handles
  has_many   :facebook_handles
  has_many   :youtube_handles
  has_many   :twitter_handles
  has_many   :pinterest_handles
  has_many   :instagram_handles
  has_one    :web_content

  # Callbacks
  # after_save :update_products
  after_create :remove_submission_reminder

  # Validations
  validates :name, uniqueness: { scope: :publisher_id }, presence: true
  validates :profile_type, presence: true, inclusion: { in: PROFILE_TYPES }
  validates :content_type, presence: true, inclusion: { in: CONTENT_TYPES }

  # Nested attributes_for
  accepts_nested_attributes_for :urls,              allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :web_content,       allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :facebook_handles,  allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :youtube_handles,   allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :twitter_handles,   allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :pinterest_handles, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :instagram_handles, allow_destroy: true, reject_if: :all_blank

  def toggle_urls(urls_params)
    urls_params.each do |_, url|
      urls.find(url['id']).pause  if url[:state] == 'paused'
      urls.find(url['id']).active if url[:state] == 'active'
    end
  end

  PROFILE_TYPES.each do |key|
    define_method "#{key}?" do
      return profile_type == key
    end
  end

  def total_orders
    # TODO: Change this to get better performance
    value = urls.map { |u| u.completed_orders.count }.reduce(:+)
    value || 0
  end

  def total_earned
    # TODO: Change this to get better performance
    value = urls.flat_map { |u| u.completed_orders.map(&:price) }.reduce(:+)
    value || 0
  end

  def total_ads
    # TODO: Change this to get better performance
    ads.approved.count
  end

  def bulk_upload_urls(file, type = 'broker')
    urls_params = case type
                    when 'contributor'
                      parse_params_from_file(file, HEADERS_CONTRIBUTOR.map(&:to_sym))
                    when 'broker'
                      parse_params_from_file(file, HEADERS.map(&:to_sym))
                    end

    return false unless urls_params.present? && urls_params.any?
    new_urls = []
    ActiveRecord::Base.transaction do
      urls_params.each do |params|
        params = params[:description].nil? ? params.merge(description: 'No description provided') : params
        params = params.delete(:first_length).present? ? params.merge(required_length: '750-1000') : params
        params = params.delete(:second_length).present? ? params.merge(required_length: '1000-1250') : params
        params = params.delete(:third_length).present? ? params.merge(required_length: '1250-1500') : params
        params = params.delete(:fourth_length).present? ? params.merge(required_length: '1500+') : params
        return unless params[:required_length].present? && params[:price].present?
        new_urls << urls.create_with(params).find_or_create_by!(href: params[:href])
      end
    end
    delay.set_metadescription(new_urls) if type == 'broker'
  end

  private

  def set_metadescription(urls)
    urls.each do |url|
      begin
        desc = MetaInspector.new(url.href).description
        url.update!(description: desc) if desc
      rescue MetaInspector::Error
        next
      end
    end
  end

  def update_products
    prods = []
    prods << { name: 'sponsored' } if any_sponsored_product?
    prods << { name: 'social' }    if any_social_product?
    prods << { name: 'search' }    if any_search_product?
    update_column(:products, prods)
  end

  def any_sponsored_product?
    web_content && web_content.href.present? && web_content.price > 0
  end

  def any_social_product?
    social_handles.count > 0
  end

  def any_search_product?
    urls.where('price > 0').count > 0
  end

  def remove_submission_reminder
    Email.unschedule('publisher_url_submission_reminder', publisher&.user_id)
  end
end
