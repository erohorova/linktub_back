# == Schema Information
#
# Table name: web_contents
#
#  id              :integer          not null, primary key
#  href            :string
#  price           :float
#  state           :integer
#  profile_id      :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  approved_date   :date
#  declined_date   :date
#  reconsider_date :date
#  archived_date   :date
#  activated_date  :date
#  paused_date     :date
#  suggested_price :float            default(0.0)
#  shown_price     :float            default(0.0)
#  ga_account_id   :string
#  ga_profile_id   :string
#  website_url     :string
#  muv             :integer
#
# Indexes
#
#  index_web_contents_on_profile_id  (profile_id)
#  index_web_contents_on_state       (state)
#

class WebContent < ActiveRecord::Base
  include ActAsProduct
  include Ransackable

  # Associations
  belongs_to :profile

  # Validations
  validates :href, :price, presence: true

  # Lifecycle's callbacks
  before_save :pricing, if: :price_changed?

  # Ransackable overridden methods
  def self.format_query
    cols = columns.select { |c| %i(string text).include?(c.type) && c.name == 'href' }

    # generate query fragment and add publisher email to the query
    cols.map { |c| "#{table_name}.#{c.name} ILIKE ?" }.join(' OR ') + ' OR publishers.email ILIKE ?'
  end

  def self.inner_joins
    joins(:profile).joins('INNER JOIN publishers ON publishers.id = profiles.publisher_id')
  end

  private

  def pricing
    self.shown_price = price * 2
    self.suggested_price = SuggestedPriceService.new.suggest_price(self.href)
  end
end
