# == Schema Information
#
# Table name: account_types
#
#  id         :integer          not null, primary key
#  type       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class AccountType < ActiveRecord::Base
  include Ransackable

  acts_as_commentable

  # This is an abstract class
  self.abstract_class = true

  # Lifecycle's callbacks
  before_create :set_user_email
  after_create :reminder_submission

  def confirmed
    user&.confirmed_at.present?
  end

  def confirm
    user&.confirm
  end

  # Ransackable overridden methods
  def self.format_query
    cols = columns.select do |c|
      %i(string text).include?(c.type) &&
        %w(first_name last_name email).include?(c.name)
    end

    # generate query fragment and add publisher email to the query
    main_sqlquery = cols.map { |c| "#{table_name}.#{c.name} ILIKE ?" }.join(' OR ')
    joins_query = ' OR users.first_name ILIKE ? OR users.last_name ILIKE ?'
    main_sqlquery + joins_query
  end

  def self.inner_joins
    joins(:user)
  end

  def send_quick_email(email_params)
    QuickMailer.email(email_params.merge(email: email)).deliver_now
  end

  def self.safe_transition?(transition)
    state_machine.events.map(&:name).include?(transition.to_sym)
  end

  private

  def set_user_email
    self.email = user.email
  end
end
