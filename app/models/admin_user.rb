# == Schema Information
#
# Table name: admin_users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  type                   :string
#  first_name             :string
#  last_name              :string
#  phone                  :string
#  state                  :integer
#  terminated_date        :date
#  activated_date         :date
#  paypal_email           :string
#  confirmation_token     :string
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  skrill_email           :string
#
# Indexes
#
#  index_admin_users_on_confirmation_token    (confirmation_token) UNIQUE
#  index_admin_users_on_email                 (email) UNIQUE
#  index_admin_users_on_first_name            (first_name)
#  index_admin_users_on_last_name             (last_name)
#  index_admin_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_admin_users_on_type                  (type)
#

class AdminUser < ActiveRecord::Base
  include Ransackable
  include Accountable
  include AccountTypeStates

  # Constants
  ADMIN = 'Admin'.freeze
  AUTHOR = 'Author'.freeze
  AGENT = 'Agent'.freeze

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  # Relationships
  has_many :articles
  has_many :advertisers
  has_many :config, as: :configable

  # Scopes
  scope :just_admins, -> { where(type: nil) }

  def full_name
    "#{first_name} #{last_name}"
  end

  def pending_articles
    articles.where(state: AdminUser.author_states)
  end

  def pending_articles_count
    pending_articles.count
  end

  # Ransackable overridden methods
  def self.format_query
    cols = columns.select do |c|
      %i(string text).include?(c.type) &&
        %w(first_name last_name email).include?(c.name)
    end

    # generate query fragment and add publisher email to the query
    cols.map { |c| "#{table_name}.#{c.name}  ILIKE ?" }.join(' OR ')
  end

  def admin?
    type.nil?
  end

  def confirmed
    confirmed_at.present?
  end

  def author?
    type == AUTHOR
  end

  def agent?
    type == AGENT
  end

  def role
    type.nil? ? ADMIN : AUTHOR
  end

  def self.author_states
    Article.states.deep_symbolize_keys
           .fetch_values(:assigned, :agency_rejected, :admin_rejected, :admin_approved)
  end

  def self.default_admin_user
    AdminUser.find_by_email(ENV['ADMIN_EMAIL'])
  end
end
