# == Schema Information
#
# Table name: social_handles
#
#  id              :integer          not null, primary key
#  url             :string
#  price           :float
#  network         :string
#  type            :string
#  state           :integer
#  profile_id      :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  suggested_price :float            default(0.0)
#  shown_price     :float            default(0.0)
#
# Indexes
#
#  index_social_handles_on_network     (network)
#  index_social_handles_on_profile_id  (profile_id)
#  index_social_handles_on_state       (state)
#  index_social_handles_on_type        (type)
#

class SocialHandle < ActiveRecord::Base
  include ActAsProduct
  include Ransackable

  # Inheritance
  self.inheritance_column = :type

  # Constants
  SOCIALS = %i(facebook twitter instagram youtube pinterest).freeze

  # Associations
  belongs_to :profile

  # Validations
  validates :url, :price, presence: true
  validates :url, uniqueness: { scope: :profile_id }

  # Lifecycle's callbacks
  before_validation :store_network
  before_save       :pricing, if: :price_changed?

  SOCIALS.each do |social|
    define_method "#{social}?" do
      return regex(social).match(url).to_a.second == social.to_s
    end
  end

  # Ransackable overridden methods
  def self.format_query
    cols = columns.select { |c| %i(string text).include?(c.type) && c.name == 'url' || c.name == 'network' }

    # generate query fragment and add publisher email to the query
    cols.map { |c| "#{table_name}.#{c.name} ILIKE ?" }.join(' OR ') + ' OR publishers.email ILIKE ?'
  end

  def self.inner_joins
    joins(:profile).joins('INNER JOIN publishers ON publishers.id = profiles.publisher_id')
  end

  private

  def store_network
    return unless url.present?
    url_with_protocol = url.start_with?('http://', 'https://') ? url : "http://#{url}"
    self.network = URI.parse(url_with_protocol)&.host&.sub(%r{/^https?\:\/\//}, '')&.sub(/^www./, '')&.sub(/.com/, '')
  end

  def regex(social)
    %r{(?:https?:\/\/)?(?:www\.)?(mbasic.#{social}|m\.#{social}|#{social})\.(com|me)\/(?:(?:\w\.)*#!\/)?(?:pages\/)?(?:[\w\-\.]*\/)*([\w\-\.]*)}
  end

  def pricing
    self.shown_price = price * 2
    # self.suggested_price = SuggestedPriceService.new.suggest_price(self.url)
  end
end
