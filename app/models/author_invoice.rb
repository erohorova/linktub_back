# == Schema Information
#
# Table name: author_invoices
#
#  id                               :integer          not null, primary key
#  approved_articles_count          :integer
#  admin_user_id                    :integer
#  paid                             :boolean          default(FALSE)
#  created_at                       :datetime         not null
#  updated_at                       :datetime         not null
#  invoice_spreadsheet_file_name    :string
#  invoice_spreadsheet_content_type :string
#  invoice_spreadsheet_file_size    :integer
#  invoice_spreadsheet_updated_at   :datetime
#
# Indexes
#
#  index_author_invoices_on_admin_user_id  (admin_user_id)
#

class AuthorInvoice < ActiveRecord::Base
  EXCEL_MIME = 'application/vnd.ms-excel'.freeze
  HEADERS = ['Date Ordered', 'Date approved', 'Publisher URL', 'Title', 'Destination', 'Anchor'].freeze

  # Relationships
  belongs_to :author, foreign_key: 'admin_user_id'
  has_many   :articles

  # Scopes
  scope :ordered_by_date, -> { order(created_at: :desc) }

  # Attachments
  has_attached_file :invoice_spreadsheet
  validates_attachment :invoice_spreadsheet, content_type: { content_type: EXCEL_MIME }

  def attached_report
    { name: invoice_spreadsheet.original_filename, url: invoice_spreadsheet.url } if invoice_spreadsheet.present?
  end

  def upload_spreadsheet(articles)
    self.invoice_spreadsheet = SpreadsheetService.create('invoice_report') do |sheet|
      sheet.add_row HEADERS
      articles.each do |article|
        sheet.add_row [article.created_at, article.approved_date, article.url.href,
                       article.ad.blog_post_title, article.url.href, article.ad.link_text]
      end
    end

    invoice_spreadsheet.instance_write(:content_type, EXCEL_MIME)
    invoice_spreadsheet.instance_write(:file_name, 'invoice_report.xlsx')
    save!
  end
end
