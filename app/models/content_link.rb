# == Schema Information
#
# Table name: content_links
#
#  id         :integer          not null, primary key
#  content_id :integer
#  link_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_content_links_on_content_id  (content_id)
#  index_content_links_on_link_id     (link_id)
#

class ContentLink < ActiveRecord::Base
  # Associations
  belongs_to :content
  belongs_to :link
end
