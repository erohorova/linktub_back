# == Schema Information
#
# Table name: configs
#
#  id              :integer          not null, primary key
#  category        :string(191)
#  name            :string(191)
#  data_type       :string
#  value           :text
#  configable_type :string(191)
#  configable_id   :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  config_index  (configable_type,configable_id,category,name) UNIQUE
#

class Config < ActiveRecord::Base

  belongs_to :configable, polymorphic: true

  validates_uniqueness_of :name, scope: %i[configable_type configable_id category]
  validates_presence_of :data_type, :value

end
