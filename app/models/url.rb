# == Schema Information
#
# Table name: urls
#
#  id                               :integer          not null, primary key
#  href                             :string
#  description                      :text
#  price                            :float            default(0.0)
#  follow_link                      :boolean          default(FALSE)
#  author_page                      :string
#  state                            :integer
#  approved_date                    :date
#  declined_date                    :date
#  reconsider_date                  :date
#  archived_date                    :date
#  activated_date                   :date
#  paused_date                      :date
#  domain_age                       :float            default(0.0)
#  domain_authority                 :integer          default(0)
#  page_authority                   :integer          default(0)
#  page_rank                        :integer          default(0)
#  alexa_rank                       :integer          default(0)
#  back_links                       :integer          default(0)
#  trust_flow                       :integer          default(0)
#  citation_flow                    :integer          default(0)
#  profile_categories_names         :text
#  created_at                       :datetime         not null
#  updated_at                       :datetime         not null
#  profile_id                       :integer
#  search_categories_vector         :tsvector
#  search_description_vector        :tsvector
#  allow_home_page                  :boolean          default(FALSE)
#  allow_blog_post                  :boolean          default(FALSE)
#  suggested_price                  :float            default(0.0)
#  shown_price                      :float            default(0.0)
#  required_length                  :string           default("750-1000"), not null
#  profile_categories_keyword       :string
#  search_categories_keyword_vector :tsvector
#  ahrefs_dr                        :integer
#  ahrefs_lrd                       :integer
#  semrush_traffic                  :integer
#
# Indexes
#
#  index_urls_on_alexa_rank            (alexa_rank)
#  index_urls_on_citation_flow         (citation_flow)
#  index_urls_on_domain_authority      (domain_authority)
#  index_urls_on_price                 (price)
#  index_urls_on_profile_id            (profile_id)
#  index_urls_on_state                 (state)
#  index_urls_on_trust_flow            (trust_flow)
#  urls_search_categories_keyword_idx  (search_categories_keyword_vector)
#  urls_search_description_idx         (search_description_vector)
#  urls_search_idx                     (search_categories_vector)
#

class Url < ActiveRecord::Base
  include ActAsProduct
  include Ransackable
  include ScopesForOrders
  acts_as_commentable

  # Constans
  MAX_SEARCH = 500

  # Callbacks
  before_validation :update_search_attributes
  before_create     :set_domain_information,   if: :domain_info_empty?
  before_save       :pricing,                  if: :price_changed?
  after_commit      :set_metadescription,      if: :description_blank?, on: :create

  # Relationship
  belongs_to :profile, autosave: true, touch: true
  has_one    :publisher,    through: :profile
  has_many   :categories,   through: :profile
  has_many   :content_urls, dependent: :destroy
  has_many   :contents,     through: :content_urls
  has_many   :orders, dependent: :destroy
  has_many   :ads

  # Nested attributes for ...
  accepts_nested_attributes_for :comments, allow_destroy: true, reject_if: :all_blank

  # Scopes
  scope :pricing_and_pausing,       -> { where(state: [states[:active], states[:paused], states[:pending]]) }
  scope :pending_and_price_changed, -> { where(state: [states[:pending], states[:under_review]]) }

  scope :deleted_for_campaign, lambda { |campaign_id| joins(:contents).where('contents.campaign_id = ? AND contents.deleted_at IS NOT NULL', campaign_id) }

  scope :bought_for_campaign, lambda { |campaign_id|
    deleted_for_campaign(campaign_id)
    .joins(:ads)
    .where('ads.campaign_id = ? AND ads.state = ?', campaign_id, Ad.states[:approved])
    .joins('INNER JOIN orders ON orders.ad_id = ads.id')
    .where('orders.state = ?', Order.states[:completed])
  }

  scope :quoted_for_campaign_with_order, lambda { |campaign_id|
    deleted_for_campaign(campaign_id)
    .joins('LEFT OUTER JOIN ads ON ads.url_id = urls.id')
    .where('ads.campaign_id = ?', campaign_id)
    .joins('LEFT OUTER JOIN orders ON orders.ad_id = ads.id')
    .where.not('orders.state = ?', Order.states[:completed])
  }

  scope :quoted_for_campaign_without_order, lambda { |campaign_id|
    deleted_for_campaign(campaign_id).uniq
  }

  scope :quoted_for_campaing, lambda { |campaign_id| Url.quoted_for_campaign_with_order(campaign_id).union(Url.quoted_for_campaign_without_order(campaign_id)) }

  # Validations
  validates :href, presence: true, uniqueness: { scope: :profile_id }, url: true
  validates :author_page, url: true, allow_blank: true

  def profile_name
    profile.name
  end

  def publisher_email
    publisher.email
  end

  # Ransackable overridden methods
  def self.format_query
    cols = columns.select { |c| %i(string text).include?(c.type) && (c.name == 'href' || c.name == 'description') }

    # generate query fragment and add publisher email to the query
    cols.map { |c| "#{table_name}.#{c.name} ILIKE ?" }.join(' OR ') + ' OR publishers.email ILIKE ?'
  end

  def self.inner_joins
    joins(:profile).joins(:publisher)
  end

  def self.already_bought_for_campaign(url_id, campaign_id)
    Url.bought_for_campaign(campaign_id).exists?(url_id)
  end

  def self.already_quoted_for_campaign(url_id, campaign_id)
    Url.quoted_for_campaing(campaign_id).exists?(url_id)
  end

  private

  def update_search_attributes
    self.profile_categories_names = categories.map(&:name).join(' ')
    self.profile_categories_keyword = categories.map(&:keyword).join(' ')
  end

  def domain_info_empty?
    trust_flow.zero? && domain_authority.zero?
  end

  def description_blank?
    description.blank?
  end

  def set_shown_price
    self.shown_price = price * 2
  end

  def set_domain_information
    info = SocialsService.new.domain_information(href)
    self.trust_flow = info[:trust_flow]
    self.domain_authority = info[:domain_authority]
    self.citation_flow = info[:citation_flow]
    self.page_authority = info[:page_authority]
  end

  def set_metadescription
    metadescription = MetaInspector.new(href).description
    update! description: metadescription
  rescue MetaInspector::Error
    update! description: 'This will be set by the admin'
  end

  def pricing
    self.shown_price = price * 2
    self.suggested_price = SuggestedPriceService.new.suggest_price(href)
    review if can_review? && !pending?
  end

  # Setup methods to be handle on background
  handle_asynchronously :set_metadescription
end
