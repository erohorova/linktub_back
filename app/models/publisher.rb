# == Schema Information
#
# Table name: publishers
#
#  id                                         :integer          not null, primary key
#  user_id                                    :integer
#  avatar                                     :string
#  created_at                                 :datetime         not null
#  updated_at                                 :datetime         not null
#  email                                      :string
#  activated_date                             :datetime
#  terminated_date                            :datetime
#  state                                      :integer
#  active_content_spreadsheet_file_name       :string
#  active_content_spreadsheet_content_type    :string
#  active_content_spreadsheet_file_size       :integer
#  active_content_spreadsheet_updated_at      :datetime
#  publishers_report_spreadsheet_file_name    :string
#  publishers_report_spreadsheet_content_type :string
#  publishers_report_spreadsheet_file_size    :integer
#  publishers_report_spreadsheet_updated_at   :datetime
#
# Indexes
#
#  index_publishers_on_state    (state)
#  index_publishers_on_user_id  (user_id)
#

class Publisher < AccountType
  include ScopesForAds
  include ScopesForOrders
  include ScopesForUrls
  include AccountTypeStates
  include ActiveContentManager

  EXCEL_MIME = 'application/vnd.ms-excel'.freeze
  HEADERS = ['Publisher URL', 'Publisher Payout', 'Live Blog Post URL', 'Date Paid'].freeze
  ADMIN_HEADERS = ['Account Create Date', 'Pub Email', 'First / Last Name'].freeze

  # Associations
  belongs_to :user
  has_many   :profiles, dependent: :destroy
  has_many   :urls,                    through: :profiles
  has_many   :orders, -> { distinct }, through: :urls
  has_many   :ads,    -> { distinct }, through: :orders

  # Delegates
  delegate :messages, :first_name, :last_name,
           :paypal_email, :skrill_email, :payment_method,
           :full_name, to: :user

  # Attachments
  has_attached_file :publishers_report_spreadsheet

  validates_attachment :publishers_report_spreadsheet, content_type: { content_type: EXCEL_MIME }

  # Lifecycle's callbacks
  after_create :reminder_submission

  # Scopes
  scope :has_active_url, -> { joins(:urls).where(urls: { state: Url.states[:active] }) }

  # Uploaders
  mount_uploader :avatar, ImageUploader

  def pricing_and_pausing_urls
    urls.pricing_and_pausing
  end

  def general_approved_ads
    ads.general_approved_ads
  end

  def active_ads_notifications_count
    user.active_ads_notifications.count
  end

  def new_orders_notifications_count
    user.new_orders_notifications.count
  end

  def pricing_and_pausing_urls_notifications_count
    user.pricing_and_pausing_notifications.count
  end

  def resubmitted_content_notifications_count
    user.resubmitted_content_notifications.count
  end

  def payout_items(date_from, date_to)
    completed_orders.paid.joins(:ad).joins('INNER JOIN payouts ON payouts.ad_id = ads.id').where('payouts.created_at BETWEEN ? AND ?', date_from, date_to)
  end

  def total_earnings(date_from, date_to)
    payout_items(date_from, date_to).joins(:ad).joins(:url).sum('urls.price').to_i
  end

  def last_month_payouts
    payout_items(1.month.ago.beginning_of_day, Time.zone.now.beginning_of_day).joins(:ad).sum('urls.price').to_i
  end

  def projected_earnings
    on_hold_orders.joins(:ad).sum('urls.price').to_i
  end

  def report_name
    "publisher report - #{email}"
  end

  # Ransackable overridden methods
  def self.format_query
    cols = columns.select do |c|
      %i(string text).include?(c.type)
    end

    # generate query fragment and add publisher email to the query
    cols.map { |c| "#{table_name}.#{c.name} ILIKE ?" }.join(' OR ') +
      ' OR users.first_name ILIKE ?' \
      ' OR users.last_name ILIKE ?' \
      ' OR users.email ILIKE ?' \
      ' OR urls.href ILIKE ?' \
  end

  def self.inner_joins
    joins('LEFT JOIN profiles ON publishers.id = profiles.publisher_id')
      .joins('LEFT JOIN urls ON profiles.id = urls.profile_id')
      .joins(:user).distinct
  end

  def self.send_payout_reminders
    publishers_sent_last_month = EmailAction.already_sent
                                            .where(['email_actions.send_at > ?', 1.month.ago])
                                            .joins(email_template: :email)
                                            .where(emails: { slug: 'publisher_change_payout_reminder' })
                                            .map(&:receiver_id)

    where.not(user_id: publishers_sent_last_month)
         .has_active_url
         .uniq.each(&:send_payout_reminder)
  end

  def send_payout_reminder
    email_vars = attributes.symbolize_keys
    email_vars[:first_name] = try(:first_name)
    email_vars[:last_name] = try(:last_name)
    Email.schedule('publisher_change_payout_reminder', user_id, email_vars)
  end

  def update_active_content_spreadsheet
    self.active_content_spreadsheet = SpreadsheetService.create(report_name) do |sheet|
      sheet.add_row HEADERS
      ads.where(state: [Ad.states[:approved], Ad.states[:approved_by_publisher], Ad.states[:paid]])
         .order(requested_date: :desc).find_each do |resource|
        paid_date = Ad.states[resource.state] == Ad.states[:paid] ? Payout.find_by(publisher: self, ad: resource) : nil
        sheet.add_row [resource.url.href, resource.url.price,
                       resource.full_pub_url, paid_date&.created_at]
      end
    end

    active_content_spreadsheet.instance_write(:content_type, EXCEL_MIME)
    active_content_spreadsheet.instance_write(:file_name, "#{report_name}.xlsx")
    save!
  end

  def update_publishers_report_spreadsheet(publishers)
    self.publishers_report_spreadsheet = SpreadsheetService.create('publishers_report') do |sheet|
      sheet.add_row ADMIN_HEADERS
      sheet.column_widths 25, 25, 25

      publishers.each do |publisher|
        sheet.add_row [publisher.created_at.strftime('%m/%d/%Y'), publisher.email, publisher&.user.full_name]
      end
    end

    publishers_report_spreadsheet.instance_write(:content_type, EXCEL_MIME)
    publishers_report_spreadsheet.instance_write(:file_name, 'publishers_report.xlsx')
    save!
  end

  def publishers_report_spreadsheet_doc
    { name: publishers_report_spreadsheet.original_filename, url: publishers_report_spreadsheet.url[/[^?]+/] } if publishers_report_spreadsheet.present?
  end

  private

  def reminder_submission
    email_vars = attributes.symbolize_keys
    email_vars[:publisher_first_name] = first_name
    email_vars[:publisher_last_name] = last_name
    Email.schedule('publisher_url_submission_reminder', user_id, email_vars)
  end
end
