# == Schema Information
#
# Table name: campaigns
#
#  id                                       :integer          not null, primary key
#  name                                     :string
#  image                                    :string
#  state                                    :integer
#  created_at                               :datetime         not null
#  updated_at                               :datetime         not null
#  advertiser_id                            :integer
#  campaign_report_spreadsheet_file_name    :string
#  campaign_report_spreadsheet_content_type :string
#  campaign_report_spreadsheet_file_size    :integer
#  campaign_report_spreadsheet_updated_at   :datetime
#
# Indexes
#
#  index_campaigns_on_advertiser_id  (advertiser_id)
#  index_campaigns_on_state          (state)
#  index_campaigns_on_updated_at     (updated_at)
#

class Campaign < ActiveRecord::Base
  include AccountItems
  include Ransackable
  include CampaignStates
  include ActAsSpreadsheetManager
  include ScopesForLinks

  EXCEL_MIME = 'application/vnd.ms-excel'.freeze
  HEADERS_CAMPAIGN_REPORT = ['Date Ordered', 'Campaign', 'Pub URL', 'DA', 'TF', 'CT',
                             'Live Blog Post', 'Destination URL', 'Anchor Text', 'Price'].freeze
  MAX_LENGTH = 31

  # Relationships
  belongs_to :advertiser
  has_many   :links, dependent: :destroy
  has_many   :categorizings, as: :categorizable
  has_many   :categories, through: :categorizings
  has_many   :content, dependent: :nullify

  # Validations
  validates :name, uniqueness: { scope: :advertiser_id }, presence: true

  # Scopes
  scope :active_desc_updated_at, -> { active.order(updated_at: :desc) }
  scope :desc_updated_at,        -> { order(updated_at: :desc) }

  # Lifecycle's callbacks
  after_create :check_auto_approvement

  # Nested attributes_for
  accepts_nested_attributes_for :links, reject_if: :all_blank

  # Attachments
  has_attached_file :campaign_report_spreadsheet
  validates_attachment :campaign_report_spreadsheet, content_type: { content_type: EXCEL_MIME }

  def campaign_report_spreadsheet_doc
    return unless campaign_report_spreadsheet.present?

    { name: campaign_report_spreadsheet.original_filename, url: campaign_report_spreadsheet.url[/[^?]+/] }
  end

  def self.inner_joins
    joins(:advertiser).joins('INNER JOIN links ON links.campaign_id = campaigns.id').distinct
  end

  # Ransackable overridden methods
  def self.format_query
    cols = columns.select do |c|
      %i(string text).include?(c.type)
    end

    # generate query fragment and add publisher email to the query
    cols.map { |c| "#{table_name}.#{c.name} ILIKE ?" }.join(' OR ') +
      ' OR links.href ILIKE ?' \
      ' OR advertisers.email ILIKE ?' \
  end

  def first_created_link
    links.first
  end

  def links_attributes=(hash)
    hash&.each do |_, links_values|
      links << Link.find_or_create_by(href: links_values[:href]) if links_values.present?
    end
  end

  def pending_links
    links.pending
  end

  def not_archived_links
    links.not_archived
  end

  def links_desc_updated_at
    links.links_desc_updated_at
  end

  def total_urls
    not_archived_links.count
  end

  def total_links
    links.count
  end

  # TODO: Refactor to return real data
  def total_expended
    '-20000'
  end

  def report_name
    "campaign report - #{name}"
  end

  def update_campaign_report_spreadsheet
    self.campaign_report_spreadsheet = SpreadsheetService.create(report_name) do |sheet|
      sheet.add_row HEADERS_CAMPAIGN_REPORT
      advertiser.active_content.order(requested_date: :desc).find_each do |resource|
        sheet.add_row [resource.ordered_date, resource.campaign.name, resource.url.href,
                       resource.url.domain_authority, resource.url.trust_flow,
                       resource.url.citation_flow, resource.full_pub_url,
                       resource.link.href, resource.link_text, resource.url.shown_price]
      end
    end

    campaign_report_spreadsheet.instance_write(:content_type, EXCEL_MIME)
    campaign_report_spreadsheet.instance_write(:file_name, "#{report_name}.xlsx")
    save!
  end

  private

  def check_auto_approvement
    active if advertiser&.auto_approved
  end
end
