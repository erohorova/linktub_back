# == Schema Information
#
# Table name: content_urls
#
#  id          :integer          not null, primary key
#  content_id  :integer
#  url_id      :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  shown_price :float            default(0.0)
#
# Indexes
#
#  index_content_urls_on_content_id  (content_id)
#  index_content_urls_on_url_id      (url_id)
#

class ContentUrl < ActiveRecord::Base
  belongs_to :content
  belongs_to :url
end
