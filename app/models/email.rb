# == Schema Information
#
# Table name: emails
#
#  id                :integer          not null, primary key
#  name              :string
#  slug              :string
#  notes             :text
#  receiver_type     :string
#  response_required :boolean          default(FALSE)
#  redirect_url      :string
#  active            :boolean          default(TRUE)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class Email < ActiveRecord::Base
  include Ransackable

  # Constants
  EMAIL_TYPES = HashWithIndifferentAccess.new(YAML.load(File.read(Rails.root.join('config', 'emails.yml')))).except(:default)
  RECEIVER_TYPES = %w(AdminUser Advertiser Publisher Author).freeze
  ADMIN_EMAIL = 'admin@scalefluence.com'.freeze
  ADMIN_RECEIVER_TYPE = 'AdminUser'.freeze

  # Associations
  has_many :email_templates, dependent: :destroy
  has_many :email_triggers, dependent: :destroy

  # Callbacks
  before_validation :set_slug

  # Validattions
  validates :name, presence: true, uniqueness: true
  validates :slug, presence: true, uniqueness: true
  validates :receiver_type, presence: true, inclusion: { in: RECEIVER_TYPES }

  def set_slug
    self.slug ||= name.parameterize('_')
  end

  # this method must return a SQL query formated with ? for params
  def self.format_query
    cols = columns.select { |c| %i(string text).include?(c.type) }

    # generate query fragment and add publisher email to the query
    cols.map { |c| "#{table_name}.#{c.name} ILIKE ?" }.join(' OR ')
  end

  def self.render_template(template, vars = {})
    t = Liquid::Template.parse(template)
    t.render(vars.with_indifferent_access)
  end

  # Schedule a receiver the receive a series of emails
  def self.schedule(slug, receiver_id, vars = {})
    return unless receiver_id

    email = where(active: true).where(slug: slug).first
    return unless email

    templates = email.email_templates
    return unless templates

    # Create EmailTrigger from Email
    trigger = email.email_triggers.create(
      receiver_type: email.receiver_type,
      receiver_id: receiver_id,
      vars: vars.merge(redirect_url: email.redirect_url)
    )

    # Loop through active templates and create EmailActions,
    # which schedules the emails for delivery
    templates.active.find_each do |template|
      action = template.email_actions.create(
        receiver_type: email.receiver_type,
        receiver_id: receiver_id,
        email_trigger_id: trigger.id,
        send_at: (Time.zone.now + template.days_delayed.days),
        collate: template.collate
      )

      # If there is no delay and the emails shouldn't be collate, go ahead and deliver
      delay.deliver(action.id) if template.days_delayed == 0 && !template.collate
    end
  end

  # Find and send all the emails scheduled for today
  def self.find_for_delivery
    actions = EmailAction.single_emails_scheduled_for_today
    actions.each do |action|
      next if action.response_completed?
      delay.deliver(action.id)
    end
  end

  def self.find_for_delivery_for_past_hours(hours)
    actions = EmailAction.collated_emails_scheduled_for_past_hours(hours)
    actions.each do |action|
      next if action.response_completed?
      delay.deliver(action.id)
    end
  end

  # Remove email from scheduler
  def self.unschedule(slug, user_id)
    return unless user_id

    email = where(active: true).where(slug: slug).first
    return unless email

    templates = email.email_templates
    return unless templates

    receiver_is_admin = email.receiver_type == ADMIN_RECEIVER_TYPE

    receiver_id = receiver_is_admin ? AdminUser.find_by(email: ENV['ADMIN_EMAIL']).id : user_id

    email_actions = EmailAction.where(
                      email_template_id: templates.ids,
                      receiver_type: email.receiver_type,
                      receiver_id: receiver_id
                    )

    email_actions.includes(:email_trigger)
                 .where('email_triggers.vars @> ?', { 'id': user_id }.to_json) if receiver_is_admin

    email_actions.update_all(sent: true)
  end

  # Schedule a single email action to be sent
  def self.deliver(email_action_id)
    action = EmailAction.includes(:email_template, :email_trigger).find(email_action_id)

    template = action.email_template
    trigger  = action.email_trigger
    email = trigger.receiver.try(:email)

    # If missing data, skip to the next action
    return false unless email && trigger && template

    # Skip this email action if the receiver
    # has already responded to this trigger.
    return false if trigger.response_completed?

    # Build the HTML to send
    vars_hash = trigger.vars.with_indifferent_access
    vars_hash[:response_url] ||= trigger.response_url
    vars_hash[:logo_image]   ||= logo_image

    rendered_subject  = render_template(template.subject, vars_hash)
    rendered_template = render_template(template.template, vars_hash)

    # Send the HTML to the receiver
    TemplateMailer.email(email, rendered_subject, rendered_template).deliver_now
    if vars_hash[:send_to_admin]
      TemplateMailer.email(ADMIN_EMAIL, rendered_subject, rendered_template).deliver_now
    end

    # Mark this email action as sent and its relateds
    if action.collate
      EmailAction.collated_together_not_sent(action).find_each do |email_action|
        email_action.update(sent: true) if email_action.id != action.id
      end
    end

    # EmailAction.mark_as_sent(action.id) if action.collate
    action.update(sent: true)
  end

  def self.logo_image
    url = ActionController::Base.helpers.image_url('logo.png', host: ENV['BASE_URL'])
    "<img src=\"#{url}\" width=\"300px\" />"
  end
end
