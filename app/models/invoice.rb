# == Schema Information
#
# Table name: invoices
#
#  id                                              :integer          not null, primary key
#  orders_count                                    :integer          default(0)
#  orders_accepted_count                           :integer          default(0)
#  total_charged                                   :float            default(0.0)
#  advertiser_id                                   :integer
#  created_at                                      :datetime         not null
#  updated_at                                      :datetime         not null
#  accepted_orders_report_spreadsheet_file_name    :string
#  accepted_orders_report_spreadsheet_content_type :string
#  accepted_orders_report_spreadsheet_file_size    :integer
#  accepted_orders_report_spreadsheet_updated_at   :datetime
#
# Indexes
#
#  index_invoices_on_advertiser_id  (advertiser_id)
#

class Invoice < ActiveRecord::Base
  # Relationships
  belongs_to :advertiser
  has_many   :orders

  EXCEL_MIME = 'application/vnd.ms-excel'.freeze
  HEADERS = ['Date Ordered', 'Agent Name', 'Adv Email', 'Number of Ads', 'Dollar Amount']

  # Attachments
  has_attached_file :accepted_orders_report_spreadsheet

  validates_attachment :accepted_orders_report_spreadsheet, content_type: { content_type: EXCEL_MIME }

  # Scopes
  scope :accepted, -> { where('orders_accepted_count > ?', 0) }

  # Callbacks
  before_save :update_counters

  # Validattions
  validates :advertiser, presence: true

  def update_accepted_orders_report_spreadsheet(accepted_orders)
    self.accepted_orders_report_spreadsheet = SpreadsheetService.create('accepted_orders_report') do |sheet|
      sheet.add_row HEADERS
      sheet.column_widths 25, 25, 25, 25, 25

      accepted_orders.each do |order|
        sheet.add_row [order.created_at.strftime('%m/%d/%Y'), '', order.advertiser&.email,
                       order.orders_accepted_count, order.total_charged]
      end
    end

    accepted_orders_report_spreadsheet.instance_write(:content_type, EXCEL_MIME)
    accepted_orders_report_spreadsheet.instance_write(:file_name, 'accepted_orders_report.xlsx')
    save!
  end

  def accepted_orders_report_spreadsheet_doc
    { name: accepted_orders_report_spreadsheet.original_filename, url: accepted_orders_report_spreadsheet.url[/[^?]+/] } if accepted_orders_report_spreadsheet.present?
  end

  private

  def update_counters
    self.orders_count = orders.length
    self.orders_accepted_count = orders.completed.length
    self.total_charged = orders.completed.map(&:amount).sum
  end
end
