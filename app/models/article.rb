# == Schema Information
#
# Table name: articles
#
#  id                  :integer          not null, primary key
#  url_id              :integer
#  link_id             :integer
#  ad_id               :integer
#  advertiser_id       :integer
#  state               :integer
#  pending_date        :datetime
#  approved_date       :datetime
#  rejected_date       :datetime
#  cancelled_date      :datetime
#  eta                 :date
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  author_id           :integer
#  assigned_date       :date
#  rewritten_counter   :integer          default(0)
#  removed_from_cart   :boolean          default(FALSE)
#  author_invoice_id   :integer
#  last_action_applied :string
#
# Indexes
#
#  index_articles_on_ad_id              (ad_id)
#  index_articles_on_advertiser_id      (advertiser_id)
#  index_articles_on_author_id          (author_id)
#  index_articles_on_author_invoice_id  (author_invoice_id)
#  index_articles_on_link_id            (link_id)
#  index_articles_on_state              (state)
#  index_articles_on_url_id             (url_id)
#

class Article < ActiveRecord::Base
  include ArticleStates
  include Ransackable
  acts_as_commentable

  # Relationships
  belongs_to :url
  belongs_to :link
  belongs_to :ad, dependent: :destroy
  belongs_to :advertiser
  belongs_to :author, class_name: 'AdminUser'
  belongs_to :author_invoice

  # Nested attributes for ...
  accepts_nested_attributes_for :ad, allow_destroy: true, reject_if: :all_blank
  accepts_nested_attributes_for :link, reject_if: :all_blank

  # Scopes
  scope :content_pipe,         -> { where(state: advertisers_states) }
  scope :author,               -> { where(state: author_states) }
  scope :rewritten_one,        -> { where(rewritten_counter: 1) }
  scope :rewritten_two,        -> { where(rewritten_counter: 2) }
  scope :removed_from_cart,    -> { where(removed_from_cart: true) }
  scope :all_but_rejected,     -> { where(state: not_rejected_by_admin) }
  scope :assigned_without_eta, -> { assigned.where(eta: nil) }
  scope :assigned_past_eta,    -> { assigned.where('eta <= ?', Time.zone.now.to_date) }

  # Delegates
  delegate :update_zip_file, to: :advertiser
  delegate :user,            to: :advertiser
  delegate :notifications,   to: :advertiser

  validate :author_change, on: :update

  # this method must return a SQL query formated with ? for params
  def self.format_query
    cols = columns.select { |c| %i(string text).include?(c.type) && c.name != 'last_action_applied' }

    # generate query fragment and add publisher email to the query
    cols.map { |c| "#{table_name}.#{c.name} ILIKE ?" }.join(' OR ') +
      ' urls.href ILIKE ?' \
      ' OR campaigns.name ILIKE ?' \
      ' OR ads.blog_post_title ILIKE ?' \
      ' OR ads.link_text ILIKE ?' \
      ' OR ads.full_pub_url ILIKE ?'
  end

  # this method should return any join needed for the query
  def self.inner_joins
    joins(:url).joins('INNER JOIN campaigns ON campaigns.id = ads.campaign_id')
               .joins(:ad)
               .joins(:link)
  end

  def blog_post_doc
    { name: ad.blog_post_doc.file.filename, url: ad.blog_post_doc.file.url } if ad&.blog_post_doc&.file.present?
  end

  def put_back
    advertiser.orders.create! ad: ad, url: ad.url
    article.approve
  end

  def create_note(comment, user, role)
    comments.create!(comment: comment, user: user, role: role)
  end

  def self.advertisers_states
    Article.states.deep_symbolize_keys
           .fetch_values(:draft, :assigned, :completed, :agency_rejected, :author_rejected, :admin_approved)
  end

  def self.author_states
    advertisers_states - [Article.states[:draft]]
  end

  def self.not_rejected_by_admin
    Article.states.deep_symbolize_keys
           .fetch_values(:draft, :assigned, :completed, :admin_rejected, :author_rejected, :admin_approved, :cancelled, :done)
  end

  def author_change
    prohibited_states_for_author_change = [Article.states[:done], Article.states[:admin_approved]]
    return unless prohibited_states_for_author_change.include?(Article.states[state]) && author_id_was != author_id

    errors[:base] << I18n.t('articles.validations.errors.change_author')
  end
end
