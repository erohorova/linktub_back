# == Schema Information
#
# Table name: email_templates
#
#  id           :integer          not null, primary key
#  email_id     :integer
#  template     :text             default("")
#  days_delayed :integer          default(0)
#  active       :boolean          default(TRUE)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  subject      :string
#  collate      :boolean          default(FALSE)
#
# Indexes
#
#  index_email_templates_on_collate   (collate)
#  index_email_templates_on_email_id  (email_id)
#

class EmailTemplate < ActiveRecord::Base
  # Associations
  belongs_to :email
  has_many   :email_actions, dependent: :destroy

  # Validattions
  validates :email_id, presence: true
  validates :subject,  presence: true
  validates :template, presence: true

  # Scopes
  default_scope -> { order('days_delayed ASC') }
  scope :active, -> { where(active: true) }
end
