# encoding: utf-8
# == Schema Information
#
# Table name: users
#
#  id                       :integer          not null, primary key
#  email                    :string           default(""), not null
#  encrypted_password       :string           default(""), not null
#  reset_password_token     :string
#  reset_password_sent_at   :datetime
#  sign_in_count            :integer          default(0), not null
#  current_sign_in_at       :datetime
#  last_sign_in_at          :datetime
#  current_sign_in_ip       :inet
#  last_sign_in_ip          :inet
#  authentication_token     :string           default("")
#  braintree_customer_id    :string
#  created_at               :datetime
#  updated_at               :datetime
#  is_active                :boolean          default(TRUE)
#  terminate_reason         :text
#  first_name               :string           not null
#  last_name                :string           not null
#  address_line_1           :string
#  address_line_2           :string
#  company                  :string
#  country                  :string
#  city                     :string
#  zip_code                 :string
#  phone                    :string
#  current_balance          :float            default(0.0)
#  payout_min               :integer
#  subscribe_to_news_letter :boolean          default(TRUE)
#  how_know_about_us        :string           default("")
#  paypal_email             :string
#  state                    :string
#  confirmation_token       :string
#  confirmed_at             :datetime
#  confirmation_sent_at     :datetime
#  payment_method           :string(191)
#  skrill_email             :string
#
# Indexes
#
#  index_users_on_authentication_token  (authentication_token) UNIQUE
#  index_users_on_confirmation_token    (confirmation_token) UNIQUE
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_first_name            (first_name)
#  index_users_on_last_name             (last_name)
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#

class User < ActiveRecord::Base
  include Authenticable
  include Accountable
  include ScopesForNotifications
  include ScopesForMessages

  # Validations
  validates :first_name, :last_name, presence: true

  # Relationships
  has_one  :advertiser
  has_one  :publisher
  has_many :notifications
  has_many :credit_cards, dependent: :destroy
  has_many :messages,     dependent: :destroy

  # Scopes
  scope :timedout, -> { where('last_sign_in_at < ?', 2.hours.ago) }

  # Life cycle's callbacks
  before_update :update_role_models, if: proc { |user| user.email_changed? }

  def full_name
    "#{first_name} #{last_name}"
  end

  def roles
    [advertiser, publisher].compact.sort_by(&:created_at).map { |role| role.class.to_s.downcase }
  end

  def active_for_authentication?
    is_active
  end

  def terminated?
    advertiser&.terminated? || publisher&.terminated?
  end

  def send_confirmation_notification?
    false
  end

  def create_invoice(orders)
    advertiser.create_invoice(orders)
  end

  private

  def update_role_models
    advertiser.update! email: email if advertiser.present?
    publisher.update! email: email if publisher.present?
  end
end
