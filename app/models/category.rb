# == Schema Information
#
# Table name: categories
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  keyword    :string
#
# Indexes
#
#  index_categories_on_name  (name)
#

class Category < ActiveRecord::Base
  # Validations
  validates :name, presence: true, uniqueness: true

  # Associations
  has_many :categorizings, dependent: :destroy

  def to_s
    name
  end
end
