# encoding: utf-8

module ScopesForNotifications
  extend ActiveSupport::Concern

  included do
    # Define scopes for orders by states
    Notification.types.each do |x, _|
      define_method "#{x}_notifications" do
        notifications.send(x)
      end
    end
  end
end
