module OrderStates
  extend ActiveSupport::Concern

  included do
    state_machine :state, initial: :pending do
      # Before transitions callback
      before_transition on: :complete do |order|
        order.completed_date = Time.zone.now
      end

      after_transition on: :complete do |order|
        order.invoice&.save!
        order.ad.approve if order.ad.can_approve?
      end

      after_transition on: :cancel do |order|
        order.ad.reject
        order.ad.comments.create!(comment: "Refunded #{Time.zone.now.strftime('%m-%d-%Y')}", role: AdminUser::ADMIN)
        order.ad.update!(rejected_reason: Ad::REJECTED_BY_REFUND, editable: false)
      end

      before_transition on: :hold do |order|
        order.ad.pending
      end

      # After all transaction trigger emails
      after_transition do |order|
        email_vars = order.email_vars
        Email.schedule("advertiser_order_#{order.state}", order.advertiser.try(:user_id), email_vars)
        if Order.states[order.state] == Order.states[:cancelled]
          Email.schedule("publisher_order_#{order.state}", order.publisher.try(:user_id), order.email_vars_publisher_cancelled)
        else
          Email.schedule("publisher_order_#{order.state}", order.publisher.try(:user_id), email_vars)
        end
        Email.schedule("admin_order_#{order.state}", AdminUser.first.try(:id), email_vars)
      end

      # Event triggers
      event :cancel do
        transition all => :cancelled
      end

      event :hold do
        transition pending: :on_hold
      end

      event :bought do
        transition %i[pending on_hold] => :bought
      end

      event :complete do
        transition %i[pending on_hold bought] => :completed
      end
    end

    # States
    enum state: { pending: 1, on_hold: 2, completed: 3, cancelled: 4, bought: 5 }
    # Pending: order is in cart.
    # On hold: Adv made checkout, but publisher hasn't completed it's part.
    # Completed: Publisher completed it's part. Purchase is done.
    # An order can be completed even if there are ads that are not approved by publisher yet
    # Cancelled: Dead end, for some reason the order is not going to be completed.
    # Bought: Transaction completed.
  end
end
