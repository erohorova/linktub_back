# encoding: utf-8

module ScopesForContents
  extend ActiveSupport::Concern

  included do
    # Define scopes for contents by states
    Content.states.each do |x, _|
      define_method "#{x}_contents" do
        contents.send(x)
      end
    end
  end
end
