# encoding: utf-8

module ScopesForMessages
  extend ActiveSupport::Concern

  included do
    # Define scopes for messages by states
    Message.states.each do |x, _|
      define_method "#{x}_messages" do
        messages.send(x)
      end
    end
  end
end
