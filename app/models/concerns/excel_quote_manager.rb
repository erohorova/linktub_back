# encoding: utf-8

module ExcelQuoteManager
  extend ActiveSupport::Concern

  HEADERS = {
    id: 0,
    campaign: 1,
    publisher: 2,
    price: 3,
    da: 4,
    pa: 5,
    tf: 6,
    cf: 7,
    ahrefs_dr: 8,
    ahrefs_lrd: 9,
    do_follow: 10,
    home_page: 11,
    allow_blog_post: 12,
    topic: 13,
    destination_url: 14,
    anchor: 15,
    instructions: 16
  }.freeze

  CHARACTERS_NOT_ALLOWED = /[+\"\\\']/.freeze

  def bulk_upload_from_document(document)
    return { status: false, error: 'No document' } if document.blank?

    process_spreadsheet(Roo::Spreadsheet.open(document))
  end

  private

  def empty_row?(row)
    row.last(4).none?
  end

  def empty_required_fileds?(row)
    !row[HEADERS[:destination_url]..HEADERS[:anchor]].all?
  end

  def bad_anchor
    @error_message = 'Anchor text is required. Please check your anchors. If you have a URL, you must have an anchor.'
    raise ActiveRecord::Rollback
  end

  def bad_url
    @error_message = 'Destination URL must include http(s). Please check all your URLs and upload again.'
    raise ActiveRecord::Rollback
  end

  def proccess_row(content)
    return false unless content[HEADERS[:destination_url]].valid_url?

    link = create_link(content[HEADERS[:destination_url]])
    article = create_article(content, link)
    if content[HEADERS[:instructions]].present?
      article.create_note(
        content[HEADERS[:instructions]].gsub(CHARACTERS_NOT_ALLOWED, ''),
        advertiser.user,
        advertiser.class.name
      )
    end
    true
  end

  def create_link(url)
    advertiser.campaigns.find(campaign_id).links.find_or_create_by!(href: url)
  end

  def create_article(content, link)
    advertiser.articles.create!(
      url_id: content[HEADERS[:id]], link_id: link.id, ad_attributes: {
        advertiser_id: advertiser_id,
        campaign_id: campaign_id,
        blog_post_title: content[HEADERS[:topic]]&.gsub(CHARACTERS_NOT_ALLOWED, ''),
        link_text: content[HEADERS[:anchor]]&.gsub(CHARACTERS_NOT_ALLOWED, ''),
        url_id: content[HEADERS[:id]],
        link_id: link.id
      }
    )
  end

  def process_spreadsheet(spreadsheet)
    ActiveRecord::Base.transaction do
      rows_processed = 0
      (2..spreadsheet.last_row).each do |index|
        content = spreadsheet.sheet(0).row(index)
        next if empty_row?(content)

        bad_anchor if empty_required_fileds?(content)
        bad_url unless proccess_row(content)
        rows_processed += 1
        remove_url_from_content(content[HEADERS[:id]])
      end
      return { status: rows_processed.positive?, error: rows_processed.positive? ? '' : 'Blank file, no rows to process' }
    end
    { status: false, error: @error_message || 'Complete at least one row' }
  end

  def remove_url_from_content(url_id)
    ContentUrl.find_by(url_id: url_id, content_id: id).destroy
    done if urls.empty?
  end
end
