# encoding: utf-8

module Ransackable
  extend ActiveSupport::Concern

  included do
    def self.ransackable_scopes(_opts)
      [:containing_text]
    end

    def self.containing_text(query)
      fragment = format_query
      inner_joins.where(fragment, *Array.new(fragment.split('OR').size, "%#{query}%"))
    end

    # this method must return a SQL query formated with ? for params
    def self.format_query
      raise NotImplementedError, 'You must override format_query.'
    end

    # this method should return any join needed for the query
    def self.inner_joins
      all
    end
  end
end
