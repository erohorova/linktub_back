# encoding: utf-8

module ActAsProduct
  extend ActiveSupport::Concern

  included do
    include ProductStates
    include Searchable
    include SitesCounterTriggers
    include Ransackable
    acts_as_commentable

    # Callbacks
    after_update :change_state

    def change_state
      pending unless rejected? || active?
      resubmit if price_changed? && rejected?
    end
  end
end
