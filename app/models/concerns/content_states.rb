# encoding: utf-8

module ContentStates
  extend ActiveSupport::Concern

  included do
    state_machine :state, initial: :requested do
      # Before transitions callback
      before_transition on: :receive do |content|
        Notification.create! user: content.advertiser.user, notification_type: Notification.types[:quotes]
      end

      # After all transaction trigger emails
      after_transition do |content|
        email_vars = content.attributes.symbolize_keys
        email_vars[:advertiser_first_name] = content.advertiser.try(:first_name)
        email_vars[:advertiser_last_name] = content.advertiser.try(:last_name)
        email_vars[:campaign_name] = content.campaign.try(:name)
        Email.schedule("advertiser_content_#{content.state}", content.advertiser.try(:user_id), email_vars)
      end

      # Event triggers
      event :requested do
        transition all => :requested
      end

      event :receive do
        transition requested: :received
      end

      event :done do
        transition all => :done
      end
    end

    # States
    enum state: { requested: 1, received: 2, defined: 3, done: 4 }
  end
end
