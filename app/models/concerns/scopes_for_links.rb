# encoding: utf-8

module ScopesForLinks
  extend ActiveSupport::Concern

  included do
    # Define scopes for links by states
    Link.states.each do |x, _|
      define_method "#{x}_links" do
        links.send(x)
      end
    end
  end
end
