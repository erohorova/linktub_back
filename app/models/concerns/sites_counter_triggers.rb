# encoding: utf-8

module SitesCounterTriggers
  extend ActiveSupport::Concern

  included do
    # Triggers
    trigger.after(:insert) do
      'UPDATE profiles SET total_sites = total_sites + 1 WHERE id = NEW.profile_id;'
    end

    trigger.after(:delete) do
      'UPDATE profiles SET total_sites = total_sites - 1 WHERE id = OLD.profile_id;'
    end

    trigger.after(:update) do
      <<-SQL
        IF NEW.profile_id <> OLD.profile_id THEN
          UPDATE profiles SET total_sites = total_sites - 1 WHERE id = OLD.profile_id;
          UPDATE profiles SET total_sites = total_sites + 1 WHERE id = NEW.profile_id;
        END IF;
      SQL
    end
  end
end
