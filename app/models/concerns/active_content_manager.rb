# encoding: utf-8

module ActiveContentManager
  extend ActiveSupport::Concern

  EXCEL_MIME = 'application/vnd.ms-excel'.freeze

  included do
    has_attached_file    :active_content_spreadsheet
    validates_attachment :active_content_spreadsheet, content_type: { content_type: EXCEL_MIME }
  end

  def active_content_spreadsheet_doc
    return nil unless active_content_spreadsheet.present?
    { name: active_content_spreadsheet.original_filename, url: active_content_spreadsheet.url[/[^?]+/] }
  end
end
