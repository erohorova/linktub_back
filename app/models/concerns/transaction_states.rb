# encoding: utf-8

module TransactionStates
  extend ActiveSupport::Concern
  included do
    state_machine :state, initial: :authorizing do
      # Before transitions callback
      before_transition on: :submitted do |transaction|
        transaction.submitted_for_settlement_at = Time.zone.now
        transaction.amount_submitted = transaction.amount_at_the_moment
        transaction.orders.each { |o| o.complete if Order.states[o.state] != Order.states[:completed] }
      end

      before_transition on: :authorized do |transaction|
        transaction.authorized_at = Time.zone.now
      end

      before_transition on: :voided do |transaction|
        transaction.voided_at = Time.zone.now
      end

      before_transition on: :declined do |transaction|
        transaction.declined_at = Time.zone.now
      end

      before_transition on: :rejected do |transaction|
        transaction.rejected_at = Time.zone.now
      end

      # After all transaction trigger emails
      after_transition do |transaction|
        email_vars = transaction.attributes.symbolize_keys
        Email.schedule("payer_transaction_#{transaction.state}", transaction.payer.try(:user_id), email_vars)
        Email.schedule("receiver_transaction_#{transaction.state}", transaction.receiver.try(:user_id), email_vars)
        Email.schedule("admin_transaction_#{transaction.state}", AdminUser.first.try(:id), email_vars)
      end

      after_transition on: :submitted do |transaction|
        AdvsNoPurchaseJob.set(wait: Advertiser::NO_PURCHASE_TIME).perform_later(transaction.payer.try(:id))
      end

      # Event triggers
      event :authorized do
        transition authorizing: :authorized
      end

      event :rejected do
        transition authorizing: :rejected
      end

      event :declined do
        transition authorizing: :declined
      end

      event :submitted do
        transition authorized: :submitted_for_settlement
      end

      event :voided do
        transition [:authorized, :submitted_for_settlement] => :voided
      end
    end

    # States
    enum state: { authorizing: 1, authorized: 2, submitted_for_settlement: 3,
                  settled: 4, declined: 5, rejected: 6, voided: 7, error: 8 }
  end
end
