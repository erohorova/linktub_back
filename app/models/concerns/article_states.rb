# encoding: utf-8

module ArticleStates
  extend ActiveSupport::Concern

  included do
    state_machine :state, initial: :draft do
      before_transition on: :approve do |article|
        article.approved_date = Time.zone.now
        Notification.create!(
          user: article.user,
          notification_type: Notification.types[:articles]
        )
      end

      after_transition on: :approve do |article|
        article.update_zip_file
      end

      after_transition on: :assign do |article|
        email_vars = article.attributes.symbolize_keys
        email_vars[:advertiser_first_name] = article.advertiser.try(:first_name)
        email_vars[:advertiser_last_name] = article.advertiser.try(:last_name)
        email_vars[:author_first_name] = article.author.try(:first_name)
        email_vars[:author_last_name] = article.author.try(:last_name)
        Email.schedule('author_article_remind_eta', article.author_id, email_vars)
      end

      after_transition on: :complete do |article|
        notifications = article.notifications
        notifications.sample.destroy if notifications.any?
      end

      before_transition on: :assign do |article|
        article.assigned_date = Time.zone.now
      end

      before_transition any => %i(admin_rejected agency_rejected author_rejected) do |article|
        article.rewritten_counter += 1
        if article.rewritten_counter >= 2
          email_vars = article.attributes.symbolize_keys
          email_vars[:advertiser_first_name] = article.advertiser.try(:first_name)
          email_vars[:advertiser_last_name] = article.advertiser.try(:last_name)
          email_vars[:author_first_name] = article.author.try(:first_name)
          email_vars[:author_last_name] = article.author.try(:last_name)
          Email.schedule('advertiser_article_rewrite_2', article.author.try(:user_id), email_vars)
        end
      end

      # After all transaction trigger emails
      after_transition do |article|
        email_vars = article.attributes.symbolize_keys
        email_vars[:advertiser_first_name] = article.advertiser.try(:first_name)
        email_vars[:advertiser_last_name] = article.advertiser.try(:last_name)
        email_vars[:author_first_name] = article.author.try(:first_name)
        email_vars[:author_last_name] = article.author.try(:last_name)
        last_message = article.comments&.last
        email_vars[:notes] = "#{last_message&.role}: #{last_message&.comment}"
        Email.schedule("advertiser_article_#{article.state}", article.advertiser.try(:user_id), email_vars)
        Email.schedule("author_article_#{article.state}", article.author.try(:id), email_vars)
        Email.schedule("admin_article_#{article.state}", AdminUser.first.try(:id), email_vars)
      end

      # Transactions
      event :approve do
        transition %i(completed) => :admin_approved
      end

      event :assign do
        transition %i(draft agency_rejected author_rejected) => :assigned
      end

      event :complete do
        transition %i(draft assigned admin_rejected agency_rejected) => :completed
      end

      event :reject_by_admin do
        transition %i(completed) => :admin_rejected
      end

      event :reject_by_author do
        transition %i(assigned admin_rejected agency_rejected) => :author_rejected
      end

      event :reject_by_agency do
        transition %i(admin_approved cart_removed) => :agency_rejected
      end

      event :cancel do
        transition all - [:cancelled] => :cancelled
      end

      event :done do
        transition all => :done
      end
    end

    enum state: {
      draft: 1,
      assigned: 2,
      completed: 3,
      admin_rejected: 4,
      agency_rejected: 5,
      author_rejected: 6,
      admin_approved: 7,
      cancelled: 8,
      done: 9
    }
  end
end
