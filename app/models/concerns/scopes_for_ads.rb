# encoding: utf-8

module ScopesForAds
  extend ActiveSupport::Concern

  included do
    # Define scopes for ads by states
    Ad.states.each do |x, _|
      define_method "#{x}_ads" do
        ads.send(x)
      end
    end
  end
end
