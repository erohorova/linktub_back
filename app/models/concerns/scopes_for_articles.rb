# encoding: utf-8

module ScopesForArticles
  extend ActiveSupport::Concern

  included do
    # Define scopes for articles by states
    Article.states.each do |x, _|
      define_method "#{x}_articles" do
        articles.send(x)
      end
    end
  end
end
