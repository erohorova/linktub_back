# encoding: utf-8

module Accountable
  extend ActiveSupport::Concern

  %w(advertiser publisher).each do |key|
    define_method "#{key}?" do
      send(key).present?
    end
  end
end
