# encoding: utf-8

module ActAsSpreadsheetManager
  extend ActiveSupport::Concern

  included do
    def bulk_upload_file(file, collection, set_blank_description)
      return if file.blank?
      spreadsheet = Roo::Spreadsheet.open(file)
      sheet = spreadsheet.sheet(0)
      head = sheet.row(1).compact.map { |x| x.parameterize.underscore.to_sym }
      initial_count = collection.count
      errors_array = []
      # Started with 2 to avoid headers
      ActiveRecord::Base.transaction do
        (2..spreadsheet.last_row).each do |r|
          hash_row = Hash[head.zip(sheet.row(r))]
          hash_row[:description] = 'No description provided' if set_blank_description
          item = collection.create sanitize(hash_row, collection)
          errors_array << item.errors.full_messages[0]
        end
      end
      { processed: collection.count - initial_count, expected: spreadsheet.last_row - 1, errors: errors_array }
    end
  end

  def sanitize(hash, resource)
    if resource.name.parameterize.underscore.to_sym == :url
      hash[:follow_link] = true if hash[:do_follow].present?
      hash[:follow_link] = false if hash[:no_follow].present?
      hash.except!(:do_follow, :no_follow)
    else
      hash
    end
  end
end
