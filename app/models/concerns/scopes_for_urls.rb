# encoding: utf-8

module ScopesForUrls
  extend ActiveSupport::Concern

  included do
    # Define scopes for urls by states
    Url.states.each do |x, _|
      define_method "#{x}_urls" do
        urls.send(x)
      end
    end
  end
end
