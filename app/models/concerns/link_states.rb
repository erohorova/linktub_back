# encoding: utf-8

module LinkStates
  extend ActiveSupport::Concern

  included do
    state_machine :state, initial: :new_link do
      # Before transitions callback
      before_transition on: :approve do |link|
        Notification.create!(
          user: link.campaign.advertiser.user,
          notification_type: Notification.types[:urls_approved]
        )
        link.approved_date = Time.zone.now
      end

      before_transition on: :suspend do |link|
        link.suspended_date = Time.zone.now
      end

      before_transition on: :flag do |link|
        link.flagged_date = Time.zone.now
      end

      before_transition on: :archive do |link|
        link.archived_date = Time.zone.now
      end

      before_transition on: :reconsider do |link|
        link.reconsidered_date = Time.zone.now
      end

      before_transition on: [:decline, :review] do |link|
        link.declined_date = Time.zone.now
      end

      # After all transaction trigger emails
      after_transition do |link|
        email_vars = link.attributes.symbolize_keys
        email_vars[:advertiser_first_name] = link.campaign.advertiser.try(:first_name)
        email_vars[:advertiser_last_name] = link.campaign.advertiser.try(:last_name)
        Email.schedule("advertiser_link_#{link.state}", link.campaign.advertiser.try(:user_id), email_vars)
        Email.schedule("admin_link_#{link.state}", AdminUser.first.try(:id), email_vars)
      end

      # Event triggers
      event :decline do
        transition new_link: :under_review
      end

      event :flag do
        transition approved: :flagged
      end

      event :approve do
        transition [:new_link, :reconsidered, :suspended] => :approved
      end

      event :review do
        transition [:flagged, :reconsidered] => :under_review
      end

      event :reconsider do
        transition under_review: :reconsidered
      end

      event :suspend do
        transition [:under_review, :archived] => :suspended
      end

      event :archive do
        transition suspended: :archived
      end
    end

    # States
    enum state: { new_link: 1, under_review: 2, approved: 3,
                  suspended: 4, archived: 5, reconsidered: 6, flagged: 7 }
  end
end
