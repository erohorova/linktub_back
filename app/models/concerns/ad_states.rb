  # encoding: utf-8

module AdStates
  extend ActiveSupport::Concern
  included do
    state_machine :state, initial: :cart_item do
      # Before transitions callback
      before_transition on: :pending do |ad|
        Notification.create! user: ad.advertiser.user, notification_type: Notification.types[:awaiting_publication]
        Notification.create! user: ad.publisher.user, notification_type: Notification.types[:new_orders]
        ad.requested_date = Time.zone.now
      end

      before_transition on: :approve do |ad|
        Notification.create! user: ad.advertiser.user, notification_type: Notification.types[:active_content]
        Notification.create! user: ad.publisher.user, notification_type: Notification.types[:active_ads]
        ad.approved_date = Time.zone.now
      end

      before_transition on: :approved_by_publisher do |ad|
        Notification.create! user: ad.publisher.user, notification_type: Notification.types[:active_ads]
        ad.approved_date = Time.zone.now
      end

      before_transition on: :reconsider do |ad|
        Notification.create! user: ad.advertiser.user, notification_type: Notification.types[:rejected_content]
        ad.reconsidered_date = Time.zone.now
      end

      before_transition on: :reject do |ad|
        Notification.create! user: ad.advertiser.user, notification_type: Notification.types[:rejected_content]
        ad.increment!(:rejected_counter)
        ad.rejected_date = Time.zone.now
      end

      before_transition on: :resubmit do |ad|
        Notification.create! user: ad.publisher.user, notification_type: Notification.types[:resubmitted_content]
      end

      before_transition on: :cancel do |ad|
        ad.cancelled_date = Time.zone.now
      end

      # After all transaction trigger emails
      after_transition do |ad|
        email_vars = ad.attributes.symbolize_keys
        email_vars[:advertiser_first_name] = ad.advertiser.try(:first_name)
        email_vars[:advertiser_last_name] = ad.advertiser.try(:last_name)
        email_vars[:publisher_first_name] = ad.publisher.try(:first_name)
        email_vars[:publisher_last_name] = ad.publisher.try(:last_name)
        email_vars[:url_price] = ad.url_price.to_f
        email_vars[:campaign_name] = ad.campaign.try(:name)
        Email.schedule("advertiser_ad_#{ad.state}", ad.advertiser.try(:user_id), email_vars)
        Email.schedule("publisher_ad_#{ad.state}", ad.publisher.try(:user_id), email_vars)
        Email.schedule("admin_ad_#{ad.state}", AdminUser.first.try(:id), email_vars)
      end

      # After all transaction trigger update for files
      after_transition do |ad|
        ad.advertiser.delay.update_active_content_spreadsheet
        ad.publisher.delay.update_active_content_spreadsheet
        ad.advertiser.delay.update_campaign_report_spreadsheet(ad.campaign)
      end

      # After rejected 2 times do...
      after_transition on: :reject do |ad|
        ad.cancel if ad.rejected_counter > 1
      end

      # Event triggers
      event :pending do
        transition cart_item: :pending
      end

      event :approve do
        transition approved_by_publisher: :approved
      end

      event :approved_by_publisher do
        transition [:pending, :rejected, :resubmitted, :rejected_by_admin] => :approved_by_publisher
      end

      event :reject do
        transition [:pending, :resubmitted, :reconsidered, :approved_by_publisher] => :rejected
      end

      event :reject_by_admin do
        transition approved_by_publisher: :rejected_by_admin
      end

      event :reconsider do
        transition all - [:cancelled] => :reconsidered
      end

      event :resubmit do
        transition rejected: :resubmitted
      end

      event :cancel do
        transition all => :cancelled
      end

      event :pay do
        transition all => :paid
      end
    end

    # States
    enum state: { cart_item: 1, pending: 2, approved_by_publisher: 3, approved: 4,
                  rejected: 5, reconsidered: 6, resubmitted: 7, cancelled: 8,
                  rejected_by_admin: 9, paid: 10 }
  end
end
