# encoding: utf-8

module CampaignStates
  extend ActiveSupport::Concern

  included do
    state_machine :state, initial: :pending do
      # After transitions callback
      after_transition on: :active do |campaign|
        unless campaign.advertiser.auto_approved
          campaign.advertiser.update! auto_approved: true
          email_vars = campaign.attributes.symbolize_keys
          email_vars[:advertiser_first_name] = campaign.advertiser.try(:first_name)
          email_vars[:advertiser_last_name] = campaign.advertiser.try(:last_name)
          Email.schedule("advertiser_campaign_#{campaign.state}", campaign.advertiser.try(:user_id), email_vars)
          Email.unschedule('advertiser_link_submission_reminder', campaign.advertiser.try(:user_id))
        end
        campaign.links.find_each(&:approve)
        AdvsNoPurchaseJob.set(wait: Advertiser::NO_PURCHASE_TIME).perform_later(campaign.advertiser.id) if campaign.advertiser.campaigns.active.count == 1
      end

      # After all transaction trigger emails
      after_transition do |campaign|
        unless campaign.active?
          email_vars = campaign.attributes.symbolize_keys
          email_vars[:advertiser_first_name] = campaign.advertiser.try(:first_name)
          email_vars[:advertiser_last_name] = campaign.advertiser.try(:last_name)
          Email.schedule("advertiser_campaign_#{campaign.state}", campaign.advertiser.try(:user_id), email_vars)
          Email.schedule("admin_campaign_#{campaign.state}", campaign.advertiser.try(:user_id), email_vars)
        end
      end

      event :pending do
        transition all - [:pending] => :pending
      end

      event :active do
        transition all - [:active] => :active
      end

      event :reject do
        transition all - [:rejected] => :rejected
      end
    end

    # States
    enum state: { pending: 1, active: 2, rejected: 3 }
  end
end
