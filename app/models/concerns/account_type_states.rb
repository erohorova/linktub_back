# encoding: utf-8

module AccountTypeStates
  extend ActiveSupport::Concern

  included do
    state_machine :state, initial: :active do
      # Before transitions callback
      before_transition on: :activate do |account|
        account.activated_date = Time.zone.now
      end

      before_transition on: :terminate do |account|
        account.terminated_date = Time.zone.now
      end

      after_transition on: :terminate do |account|
        account.user.invalidate_token if account.class.method_defined?(:user)
      end

      # After all transaction trigger emails
      after_transition do |account|
        email_vars = account.attributes.symbolize_keys
        email_vars[:first_name] = account.try(:first_name)
        email_vars[:last_name] = account.try(:last_name)
        user_id = account.has_attribute?('user_id') ? account.user_id : account.id
        # admin_user, advertiser, publisher
        Email.schedule("#{account.class.name.downcase}_account_#{account.state}", user_id, email_vars)
      end

      # Event triggers
      event :activate do
        transition all => :active
      end

      event :terminate do
        transition [:active] => :terminated
      end
    end

    # States
    enum state: { active: 1, terminated: 2 }
  end
end
