# encoding: utf-8

module Searchable
  extend ActiveSupport::Concern
  include PgSearch

  ALL = 'ALL'.freeze

  included do
    pg_search_scope :search_by_category, against: :profile_categories_names, using: {
      tsearch: { dictionary: 'english', tsvector_column: 'search_categories_vector', any_word: true }
    }

    pg_search_scope :search_by_description, against: :description, using: {
      tsearch: { dictionary: 'english', tsvector_column: 'search_description_vector', prefix: true }
    }

    pg_search_scope :search_by_category_keyword, against: :profile_categories_keyword, using: {
      tsearch: { dictionary: 'english', tsvector_column: 'search_categories_keyword_vector', prefix: true }
    }

    # This method should look for urls with their description or some category in words
    def self.search(words, keywords, filters)

      return none unless words.reject(&:empty?).any? || keywords.reject(&:empty?).any?
      return where(sanitize_params(filters)).active if words.reject(&:empty?).map(&:upcase).include?(ALL)
      search_params = words + keywords
      search_params.map { |x| search_by_description(x) }.reduce(:union)
        .union(keywords.map {|x| search_by_category_keyword(x)}
        .reduce(:union)).union(search_by_category(words))
        .where(sanitize_params(filters)).active
    end

    def self.sanitize_params(filters)
      filters.map { |dom, ran| "#{dom} BETWEEN #{ran[0]} AND #{ran[1]}" }.join(' AND ')
    end
  end
end
