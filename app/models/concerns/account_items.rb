# encoding: utf-8

module AccountItems
  extend ActiveSupport::Concern

  included do
    include PgSearch
    pg_search_scope :search_by_categories, associated_against: {
      categories: [:name]
    }

    # Uploaders
    mount_uploader :image, ImageUploader

    def toggle_categories(categories_ids)
      self.categories = Category.find(categories_ids)
    end
  end
end
