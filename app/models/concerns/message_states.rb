# encoding: utf-8

module MessageStates
  extend ActiveSupport::Concern

  included do
    state_machine :state, initial: :sent do
      # Before transitions callback
      before_transition on: :read do |message|
        message.read_at = Time.zone.now
      end

      before_transition on: :reply do |message|
        Notification.create!(
          user: message.user,
          notification_type: Notification.types[:replied_message]
        )
        message.replied_at = Time.zone.now
      end

      # After all transaction trigger emails
      after_transition do |message|
        email_vars = message.attributes.symbolize_keys
        email_vars[:user_first_name] = message.user.try(:first_name)
        email_vars[:user_last_name] = message.user.try(:last_name)
        Email.schedule("user_message_#{message.state}", message.user_id, email_vars)
      end

      # Event triggers
      event :read do
        transition unread: :read
      end

      event :reply do
        transition sent: :unread
      end
    end

    # States
    enum state: { sent: 1, unread: 2, read: 3 }
  end
end
