# encoding: utf-8

module ScopesForOrders
  extend ActiveSupport::Concern

  included do
    # Define scopes for orders by states
    Order.states.each do |x, _|
      define_method "#{x}_orders" do
        orders.send(x)
      end
    end
  end
end
