# encoding: utf-8

module ProductStates
  extend ActiveSupport::Concern

  included do
    attr_accessor :previous_state

    state_machine :state, initial: :pending do

      before_transition do |product|
        @previous_state = product.state
      end

      # After all transaction trigger emails
      after_transition do |product|
        email_vars = product.attributes.symbolize_keys
        email_vars[:publisher_first_name] = product.profile.publisher.try(:first_name)
        email_vars[:publisher_last_name] = product.profile.publisher.try(:last_name)
        email_vars[:notes] = product.comments.last.try(:comment)
        email_vars[:href] = product.try(:href)
        # url, social_handle, web_content
        Email.schedule("publisher_#{product.class.name.downcase}_#{product.state}", product.profile.publisher.try(:user_id), email_vars)
        Email.schedule("admin_#{product.class.name.downcase}_#{product.state}", AdminUser.first.try(:id), email_vars)
        Email.schedule('url_has_been_paused', AdminUser.default_admin_user.try(:id), email_vars) if product.paused?
        Email.schedule('url_has_been_unpaused', AdminUser.default_admin_user.try(:id), email_vars) if self.states[@previous_state.to_sym] == self.states[:paused] && product.active?
        Email.schedule('publisher_url_suspended', product.profile.publisher.try(:user_id), email_vars) if product.cancelled?
        if @previous_state.to_sym == :under_review
          status = product.active? ? 'approved' : 'declined'
          Email.schedule("publisher_price_change_#{status}", product.profile.publisher.try(:user_id), email_vars)
        end
      end

      # Event triggers
      event :active do
        transition %i[pending paused rejected resubmitted under_review] => :active
      end

      event :pending do
        transition %i[rejected active] => :pending
      end

      event :pause do
        transition %i[pending active] => :paused
      end

      event :reject do
        transition %i[pending active paused resubmitted under_review] => :rejected
      end

      event :resubmit do
        transition %i[active rejected] => :resubmitted
      end

      event :cancel do
        transition all - [:cancelled] => :cancelled
      end

      event :review do
        transition %i[paused active pending rejected] => :under_review
      end
    end

    # States
    enum state: { pending: 1, active: 2, paused: 3, rejected: 4, cancelled: 5, resubmitted: 6, under_review: 7 }
  end
end
