# == Schema Information
#
# Table name: email_triggers
#
#  id                 :integer          not null, primary key
#  email_id           :integer
#  receiver_id        :integer
#  receiver_type      :string
#  vars               :text
#  response_completed :boolean          default(FALSE)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_email_triggers_on_email_id                       (email_id)
#  index_email_triggers_on_receiver_type_and_receiver_id  (receiver_type,receiver_id)
#

class EmailTrigger < ActiveRecord::Base

  EMAIL_DEFAULT_REDIRECT = ENV['BASE_URL'].freeze

  # Associations
  belongs_to :email
  belongs_to :receiver,      polymorphic: true
  has_many   :email_actions

  before_save :set_receiver_type

  # Serializers
  serialize :vars

  # This method is used as an email variable that generates
  # the response URL for the receiver
  def response_url
    "#{ENV['BASE_URL']}/emails/responses/#{self.id}"
  end

  # Render redirect URL with variables
  def redirect_url
    return EMAIL_DEFAULT_REDIRECT unless email and email.redirect_url.present?
    url = Liquid::Template.parse(email.redirect_url)
    url.render(self.vars.with_indifferent_access)
  end

  def set_receiver_type
    if %w[Publisher Advertiser].include?(self.receiver_type)
      self.receiver_type = 'User'
    end
  end

end
