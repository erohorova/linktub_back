class ArticlePolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      if user.admin?
        scope.all
      else
        scope.where(author_id: user.id).where(state: [Article.states[:assigned], Article.states[:rejected]])
      end
    end
  end

  def index?
    admin? || author?
  end

  def show?
    index?
  end

  def update?
    index? && !record.approved?
  end

  def destroy?
    admin?
  end

  def create?
    admin?
  end
end
