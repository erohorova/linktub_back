class AdminUserPolicy < ApplicationPolicy
  def index?
    admin?
  end

  def show?
    admin?
  end

  def update?
    admin? || record.id == user.id
  end

  def destroy?
    admin?
  end

  def create?
    admin?
  end
end
