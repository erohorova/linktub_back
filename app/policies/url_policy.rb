class UrlPolicy < ApplicationPolicy
  def index?
    admin?
  end

  def show?
    admin?
  end

  def update?
    admin?
  end

  def destroy?
    admin?
  end

  def create?
    admin?
  end

  def activate?
    admin?
  end

  def reject?
    admin?
  end

  def cancel?
    admin?
  end

  def pause?
    admin?
  end

  def unpause?
    admin?
  end
end
