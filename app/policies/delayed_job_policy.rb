class DelayedJobPolicy < ApplicationPolicy
  def index?
    true
  end

  def show?
    true
  end

  def update?
    true
  end

  def destroy?
    true
  end

  def create?
    true
  end

  def run?
    true
  end
end
