class PayoutsPolicy < ApplicationPolicy
  def index?
    admin?
  end

  def cancel?
    admin?
  end

  def pay?
    admin?
  end
end
