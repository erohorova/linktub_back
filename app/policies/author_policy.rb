class AuthorPolicy < ApplicationPolicy
  def index?
    admin?
  end

  def show?
    index? || record.id == user.id
  end

  def update?
    index? || record.id == user.id
  end

  def destroy?
    admin?
  end

  def create?
    admin?
  end

  def terminate?
    admin?
  end
end
