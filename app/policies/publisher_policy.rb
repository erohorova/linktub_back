class PublisherPolicy < ApplicationPolicy
  def index?
    admin?
  end

  def show?
    admin?
  end

  def update?
    admin?
  end

  def delete?
    admin?
  end

  def create?
    admin?
  end

  def activate?
    admin?
  end
end
