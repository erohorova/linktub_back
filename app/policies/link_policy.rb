class LinkPolicy < ApplicationPolicy
  def index?
    true
  end

  def approve?
    admin?
  end

  def reject?
    admin?
  end

  def destroy?
    user.advertiser? && user.advertiser.id == record.campaign.advertiser_id
  end
end
