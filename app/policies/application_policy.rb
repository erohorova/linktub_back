class ApplicationPolicy
  attr_reader :user, :record

  class Scope < Struct.new(:user, :scope)
    attr_reader :user, :scope

    def initialize(user, scope)
      @user  = user
      @scope = scope
    end

    def resolve
      scope
    end
  end

  def initialize(user, record)
    @user = user
    @record = record
  end

  def admin?
    (user.is_a? AdminUser) && !author?
  end

  def author?
    user.is_a? Author
  end
end
