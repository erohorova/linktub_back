module ActiveAdmin
  class PagePolicy < ApplicationPolicy
    class Scope < Struct.new(:user, :scope)

      def initialize(user, scope)
        @user = user
        @scope = scope
      end

      def resolve
        scope
      end
    end

    def initialize(user, page)
      @user = user
      @page = page
    end

    def show?
      true
    end

    def index?
      show?
    end
  end
end
