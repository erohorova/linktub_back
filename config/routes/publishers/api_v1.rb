resources :publishers, only: %i[show create update] do
  resources :messages, module: :publishers, only: %i[index show create update destroy] do
    put :read, action: :read
  end
  resources :orders, module: :publishers, only: %i[index update]
  get :counters, controller: 'publishers/counters', action: :show
  resources :ads, module: :publishers, only: %i[index update destroy] do
    put :approve, action: :approve
    put :reject, action: :reject
  end
  resources :notifications, only: [] do
    delete :destroy_type, on: :collection
  end
  resources :urls, module: :publishers, only: %i[index update]
  resources :profiles, except: %i[new edit] do
    resources :urls, only: [:update]
    get :meta_description, on: :collection
    get :ga_muv, on: :collection
    get :names, on: :collection
    get :domain_information, on: :collection
    get :facebook_followers, on: :collection
    get :instagram_followers, on: :collection
    get :youtube_followers, on: :collection
    get :twitter_followers, on: :collection
    get :pinterest_followers, on: :collection
    put :update_spreadsheet
  end
  get :payouts, controller: 'publishers/payouts', action: :show
end
