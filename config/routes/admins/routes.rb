# Admin panel custom routes
namespace :admin do
  namespace :contents do
    get :advertisers_search
    get :campaigns
    get :urls
    get :publishers
    get :counter
    get :search_urls
  end

  controller :payouts do
    delete 'payouts/:id',     action: :cancel, as: :cancel_payout
    put    'payouts/:id/pay', action: :pay,    as: :pay_payout
  end

  controller :articles do
    get 'articles/:id/notes',         action: :notes,         as: :notes
    put 'articles/:id/update_file',   action: :update_file,   as: :article_update_file
    put 'articles/:id/assign_author', action: :assign_author, as: :article_assign_author
    put 'articles/:id/change_state',  action: :change_state,  as: :article_change_state
    put 'articles/:id/put_back',      action: :put_back,      as: :article_put_back
    post 'articles/:id/comments',     action: :comment,       as: :admin_create_note
    put 'articles/:id/state',         action: :state,         as: :admin_state
    get 'articles/counter',           action: :counter,       as: :admin_counter_articles
    post 'articles/bulk_create',      action: :bulk_create,   as: :admin_bulk_create
  end

  controller :authors do
    post 'authors',            action: :create,  as: :admin_create_author
    put 'authors/:id/state',   action: :state,   as: :admin_author_state
    put 'authors/:id/confirm', action: :confirm, as: :admin_author_confirm
  end

  controller :contents do
    post 'contents/:id/assign',              action: :assign,          as: :admin_contents_assign
    get 'contents/advertiser_campaigns/:id', action: :campaigns,       as: :contents_advertiser_campaigns
    post 'contents/:id/save',                action: :save,            as: :contents_advertiser_save
    post 'contents/:id/save_bulk',           action: :save_bulk,       as: :contents_advertiser_save_bulk
    post 'contents/:id/order',               action: :order,           as: :contents_advertiser_order
    get 'contents/contents_search/:id',      action: :contents_search, as: :admin_contents_search
    put 'contents/:id',                      action: :update,          as: :update_admin_content
    delete 'contents/:id',                   action: :destroy,         as: :delete_admin_content
    get 'contents/:id/filter',               action: :filter,          as: :filter_admin_content
    get 'contents/:id/report',               action: :report,          as: :report_admin_content
    post 'contents/:id/upload',              action: :upload,          as: :upload_admin_content
  end

  controller :web_contents do
    put 'web_contents/:id/state', action: :state,   as: :admin_update_web_content
    get 'web_contents/counter',   action: :counter, as: :admin_counter_web_content
  end

  controller :social_handles do
    put 'social_handles/:id/state', action: :state,   as: :admin_update_social_handle
    get 'social_handles/counter',   action: :counter, as: :admin_counter_social_handle
  end

  controller :urls do
    put  'urls/:id/state',      action: :state,      as: :admin_update_state_url
    get  'urls/counter',        action: :counter,    as: :admin_counter_url
    post 'urls/:id/comments',   action: :comment,    as: :admin_create_note_url
    put  'urls/:id/update_pub', action: :update_pub, as: :admin_reassign_pub
  end

  controller :campaigns do
    put 'campaigns/:id/state', action: :state,   as: :campaigns_advertiser_state
    get 'campaigns/filter',    action: :filter,  as: :campaigns_advertiser_filter
    get 'campaigns/counter',   action: :counter, as: :campaigns_counter
  end

  controller :ads do
    put 'ads/:id/state',                action: :state,                           as: :ads_advertiser_state
    put 'ads/:id/seen',                 action: :seen,                            as: :ads_seen
    get 'ads/search',                   action: :search,                          as: :ads_search
    get 'ads/action_items_counter',     action: :update_action_items_counter,     as: :ads_action_items_counter
    get 'ads/unresponsive_ads_counter', action: :update_unresponsive_ads_counter, as: :ads_unresponsive_ads_counter
    put 'ads/:id/remind',               action: :remind,                          as: :ads_remind
  end

  put  'advertisers/:advertiser_id/reply_message/:id', controller: 'advertisers', action: :reply_message,      as: :advertisers_reply_message
  post 'advertisers/:id/campaigns',                    controller: 'advertisers', action: :create_campaign,    as: :advertisers_create_campaign
  post 'publishers/:id/email',                         controller: 'publishers',  action: :email_publisher,    as: :publisher_email
  put  'links/:id/state',                              controller: 'links',       action: :state,              as: :links_advertiser_state
  put  'orders/:id/state',                             controller: 'orders',      action: :state,              as: :orders_advertiser_state
  get   'orders/outdated_orders',                      controller: 'orders',      action: :outdated_orders,    as: :orders_outdated
  get   'orders/accepted_orders',                      controller: 'orders',      action: :accepted_orders,    as: :orders_accepted
  put  'advertisers/:id/confirm',                      controller: 'advertisers', action: :confirm,            as: :admin_advertiser_confirm
  get 'advertisers/choose_without_campaigns',          controller: 'advertisers', action: :choose_without_campaigns, as: :choose_advertisers_without_campaign
  get 'advertisers/filter_and_download',               controller: 'advertisers', action: :filter_and_download,             as: :filter_advertisers
  get 'advertisers/aging_adv_accounts',                controller: 'advertisers', action: :aging_adv_accounts,  as: :adv_accounts_aging
  put 'advertisers/:id/reassign_agent',                controller: 'advertisers', action: :reassign_agent,     as: :agent_reassign
  get 'advertisers/count',                             controller: 'advertisers', action: :count,              as: :new_leads_count
  put  'publishers/:id/confirm',                       controller: 'publishers',  action: :confirm,            as: :admin_publisher_confirm
  get 'publishers/choose_without_profiles',            controller: 'publishers',  action: :choose_without_profiles, as: :choose_publishers_without_profile
  get 'publishers/filter_and_download',                controller: 'publishers',  action: :filter_and_download,             as: :filter_publishers
  get  'users/check_email',                            controller: 'users',       action: :check_email,        as: :admin_check_email
  get  'admin_users/check_email',                      controller: 'admin_users', action: :check_email,        as: :admin_user_check_email
  put 'agents/:id/confirm',                            controller: 'agents',      action: :confirm,            as: :admin_agent_confirm
  put 'agents/:id/state',                              controller: 'agents',      action: :state,              as: :admin_agent_state
  get 'advertisers/new_leads',                         controller: 'advertisers', action: :new_leads,          as: :advertisers_new_leads
end
