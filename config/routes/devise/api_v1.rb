devise_for :admin_users, ActiveAdmin::Devise.config
ActiveAdmin.routes(self)
devise_for :users, path: 'api/v1/users/', controllers: {
  sessions: 'api/v1/sessions',
  registrations: 'api/v1/registrations',
  passwords: 'api/v1/passwords',
  confirmations: 'api/v1/confirmations',
}
