resources :advertisers, only: %i[show create] do
  resources :messages, module: :advertisers, only: %i[index show create update destroy] do
    put :read, action: :read
  end
  get :counters, controller: 'advertisers/counters', action: :show
  resources :articles, module: :advertisers, only: %i[index update] do
    collection do
      post :bulk_create
      post :create_orders
    end
  end
  resources :invoices, only: [:index]
  resources :credit_cards, only: %i[index show create update destroy]
  resources :payments, only: [] do
    collection do
      get :token
      post :pay, action: :payment
    end
  end
  resources :notifications, only: [] do
    delete :destroy_type, on: :collection
  end
  resources :campaigns, except: %i[new edit] do
    resources :links, only: %i[create index]
    get :names, on: :collection
    put :update_spreadsheet
  end
  resources :links, only: [:destroy] do
    get :orders_built
  end
  resources :ads, module: :advertisers, only: %i[index update destroy]
  resources :contents, only: %i[index show create update] do
    get :report
    post :upload
  end
  resources :orders, module: :advertisers, except: %i[new edit] do
    collection do
      get ':url_id/already_ordered', action: :already_ordered
      get ':url_id/:campaign_id/already_ordered_for_campaign', action: :already_ordered_for_campaign
      post :bulk_create
      delete :destroy_list
    end
  end
end
