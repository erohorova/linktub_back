# encoding: utf-8

Railsroot::Application.routes.draw do
  root 'api/v1/api#status'

  # Routes for admin (ajax)
  draw :routes, 'admins'
  # Routes for devise (user & admin)
  draw :api_v1, 'devise'

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :users, only: %i[show update] do
        collection do
          get :check_email
        end
      end
      resources :categories, only: [:index]
      resources :urls, only: [:index] do
        get :suggested_price, on: :collection
      end

      get :visitors, controller: :analytics

      # Routes for advertisers (V1)
      draw :api_v1, 'advertisers'

      # Routes for publishers (V1)
      draw :api_v1, 'publishers'
    end
  end


  get '.well-known/*other', to: 'api/v1/api#status'

  namespace :emails do
    resources :responses, only: [:show]
  end
end
