Skrill::Payment::Configuration.configure do |config|
  config.merchant_email    = Rails.application.secrets.merchant_email
  config.merchant_password = Rails.application.secrets.merchant_password
  config.subject           = 'Payment'
  config.note              = 'Your payment'
  config.currency          = 'USD'
end