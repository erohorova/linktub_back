class String
  def to_bool
    # ActiveRecord::Type::Boolean.new.type_cast_from_user value
    return true if self == true || self =~ /(true|t|yes|y|1|on)$/i
    return false if self == false || blank? || self =~ /(false|f|no|n|nil|0|off)$/i
    raise ArgumentError, "invalid value for Boolean: \"#{self}\""
  end
end