source 'https://rubygems.org'
ruby '2.4.5'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 4.2.8'

# Use postgres as the database for Active Record
gem 'pg', '~> 0.18.2'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'activeadmin', '~> 1.0.0'
gem 'angular-rails-templates', '~> 1.0', '>= 1.0.2'
gem 'annotate', '~> 2.6.5'
gem 'devise', '~> 3.5.1'
gem 'devise-async', '~> 0.10.1'
gem 'skrill-payment', '~> 0.3.3'

# Use delayed jobs
gem 'delayed_job_active_record', '~> 4.0.3'

gem 'active_record_union', '~> 1.2'
gem 'acts_as_commentable', '~> 4.0.2'
gem 'amoeba', '~> 3.1'
gem 'aws-sdk', '~> 2'
gem 'axlsx', '~> 2.1.0.pre'
gem 'axlsx_styler', '~> 0.2.0'
gem 'braintree', '~> 2.70'
gem 'carrierwave', '~> 0.11.2'
gem 'clockwork', '~> 2.0', '>= 2.0.3'
gem 'fog-aws', '~> 1.1'
gem 'font-awesome-rails', '~> 4.7.0.2'
gem 'hairtrigger', '~> 0.2.19'
gem 'haml', '~> 4.0.6'
gem 'jbuilder', '~> 1.2'
gem 'jquery-fileupload-rails', '~> 0.4.7'
gem 'kaminari', '~> 1.0.1'
gem 'koala', '~> 1.10.0rc'
gem 'linkscape', '~> 0.3.2'
gem 'liquid', '~> 3.0', '>= 3.0.6'
gem 'metainspector', '~> 5.4.1'
gem 'momentjs-rails', '~> 2.17', '>= 2.17.1'
gem 'paperclip', '~> 5.0.0'
gem 'paranoia', '~> 2.2'
gem 'pg_search', '~> 1.0.6'
gem 'pry-rails', '~> 0.3.4'
gem 'puma', '~> 3.7.0'
gem 'pundit', '~> 1.1'
gem 'rack-cors', '~> 0.4.0', require: 'rack/cors'
gem 'rails_12factor', '~> 0.0.3', group: :production
gem 'rest-client', '~> 2.0', '>= 2.0.2'
gem 'roo', '~> 2.7.1'
gem 'simple_token_authentication', '~> 1.6.0'
gem 'spinjs-rails', '~> 1.4'
gem 'state_machines', '~> 0.4.0'
gem 'state_machines-activerecord', '~> 0.4.0'
gem 'toastr-rails', '~> 1.0.3'
gem 'twitter', '~> 6.2.0'
gem 'valid_url', '~> 0.0.4'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

group :development do
  # Code analysis tools
  gem 'better_errors', '~> 2.1.1'
  gem 'binding_of_caller', '~> 0.7.2'
  gem 'bullet', '~> 5.4.2'
  gem 'letter_opener', '~> 1.4.1'
  gem 'pry-byebug', '~> 3.3.0'
  gem 'quiet_assets', '~> 1.1.0'
  gem 'rails-erd', '~> 1.4.7'
  gem 'rails_best_practices', '~> 1.19.4'
  gem 'reek', '~> 1.3.6'
  gem 'rubocop', '~> 0.71.0'
end

group :development, :test, :staging do
  gem 'factory_girl_rails', '~> 4.5.0'
  gem 'faker', '~> 1.8', '>= 1.8.4'
  gem 'ffaker', '~> 2.7'
  gem 'rspec-rails', '~> 3.5.1'
  gem 'spork-rails', '~> 4.0.0'
end

group :test do
  gem 'database_cleaner', '~> 1.5.3'
  gem 'shoulda-matchers', '~> 2.8.0'
  gem 'vcr', '~> 4.0.0'
  gem 'webmock', '~> 3.4.2'
end

group :assets do
  gem 'uglifier', '~> 2.7.2'
end

# Use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.1.2'

# Use unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]
