VCR.configure do |config|
  config.cassette_library_dir = Rails.root.join('spec', 'vcr')
  config.hook_into :webmock # or :fakeweb
  config.ignore_localhost = false
end

RSpec.configure do |c|
  c.around(:each, :vcr) do |example|
    name = example.metadata[:full_description].split(/\s+/, 2).join('/').underscore.gsub(%r{/[^\w\/]+/}, '_')
    VCR.use_cassette(name) { example.call }
  end
end
