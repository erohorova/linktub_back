# encoding: utf-8

include Warden::Test::Helpers

module RequestHelpers
  def login(user)
    login_as user, scope: :user
    user
  end

  def login_with_credentials(user = nil)
    user ||= @user
    request.headers['X-USER-TOKEN'] = user.authentication_token
  end

  def response_body(value = nil)
    body = JSON.parse(response.body)
    value ? body[value.to_s] : body
  end
end
