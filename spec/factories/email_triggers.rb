# == Schema Information
#
# Table name: email_triggers
#
#  id                 :integer          not null, primary key
#  email_id           :integer
#  receiver_id        :integer
#  receiver_type      :string
#  vars               :text
#  response_completed :boolean          default(FALSE)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_email_triggers_on_email_id                       (email_id)
#  index_email_triggers_on_receiver_type_and_receiver_id  (receiver_type,receiver_id)
#

FactoryGirl.define do
  factory :email_trigger do
    email_id 1
    association :receiver, factory: :user
    vars YAML::dump({:test=>"My test variable"})
    response_completed false
  end
end
