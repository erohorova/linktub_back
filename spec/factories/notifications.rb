# == Schema Information
#
# Table name: notifications
#
#  id                :integer          not null, primary key
#  description       :text
#  user_id           :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  notification_type :integer
#
# Indexes
#
#  index_notifications_on_notification_type  (notification_type)
#  index_notifications_on_user_id            (user_id)
#

FactoryGirl.define do
  factory :notification do
    user
    description       { Faker::Lorem.paragraph }
    notification_type { rand(1..6) }
  end
end
