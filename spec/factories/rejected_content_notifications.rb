FactoryGirl.define do
  factory :rejected_content_notification, class: Notification, parent: :notification do
    notification_type 'RejectedContentNotification'
  end
end
