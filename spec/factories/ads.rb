# == Schema Information
#
# Table name: ads
#
#  id                   :integer          not null, primary key
#  link_text            :string
#  requested_date       :datetime
#  approved_date        :datetime
#  rejected_date        :datetime
#  reconsidered_date    :datetime
#  cancelled_date       :datetime
#  admin_cancelled      :boolean          default(FALSE)
#  advertiser_cancelled :boolean          default(FALSE)
#  publisher_cancelled  :boolean          default(FALSE)
#  url_price            :decimal(, )      default(0.0)
#  blog_post            :string           default("blog_post")
#  blog_post_title      :string
#  blog_post_body       :string
#  quote_id             :string
#  full_pub_url         :string
#  attach_word_document :boolean          default(TRUE)
#  blog_post_doc        :string
#  blog_post_image_1    :string
#  blog_post_image_2    :string
#  state                :integer
#  rejected_reason      :string
#  notes                :text
#  editable             :boolean
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  advertiser_id        :integer
#  link_id              :integer
#  url_id               :integer
#  campaign_id          :integer
#  rejected_counter     :integer          default(0)
#  ordered_date         :date
#  seen                 :boolean          default(FALSE)
#
# Indexes
#
#  index_ads_on_advertiser_id  (advertiser_id)
#  index_ads_on_campaign_id    (campaign_id)
#  index_ads_on_link_id        (link_id)
#  index_ads_on_state          (state)
#  index_ads_on_url_id         (url_id)
#

FactoryGirl.define do
  factory :ad do
    advertiser
    url
    blog_post_title        { Faker::Book.title }
    link_text              { Faker::Beer.name }
    requested_date         { 1.year.ago }
    approved_date          { 1.year.ago }
    rejected_date          { 1.year.ago }
    reconsidered_date      { 1.year.ago }
    cancelled_date         { 1.year.ago }
    admin_cancelled        false
    advertiser_cancelled   false
    publisher_cancelled    false
    full_pub_url           { Faker::Internet.url }
    state                  { Random.rand(1...Ad.states.size) }

    after(:build) do |evaluator|
      evaluator.notes           = Faker::Lorem.sentence
      evaluator.campaign        = FactoryGirl.create(:campaign, advertiser: evaluator.advertiser)
      evaluator.link            = FactoryGirl.create(:link, campaign: evaluator.campaign)
    end
  end
end
