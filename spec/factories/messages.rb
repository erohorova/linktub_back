# == Schema Information
#
# Table name: messages
#
#  id               :integer          not null, primary key
#  subject          :string
#  question_message :text
#  reply_message    :text
#  author           :string
#  replied_at       :datetime
#  read_at          :datetime
#  state            :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  user_id          :integer
#
# Indexes
#
#  index_messages_on_state    (state)
#  index_messages_on_user_id  (user_id)
#

FactoryGirl.define do
  factory :message do
    subject          { "#{FFaker::Company.bs}?" }
    question_message { FFaker::CheesyLingo.sentence }
    author           'advertiser@mail.com'
    user
  end
end
