# == Schema Information
#
# Table name: categories
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  keyword    :string
#
# Indexes
#
#  index_categories_on_name  (name)
#

FactoryGirl.define do
  factory :category do
    sequence :name do |n|
      "#{Faker::Lorem.word.upcase}#{n}"
    end
  end
end
