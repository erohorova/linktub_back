# == Schema Information
#
# Table name: profiles
#
#  id           :integer          not null, primary key
#  name         :string
#  image        :string
#  profile_type :string
#  total_earned :float            default(0.0)
#  total_orders :integer          default(0)
#  total_sites  :integer          default(0)
#  products     :text
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  publisher_id :integer
#  content_type :string           default("collaborate"), not null
#
# Indexes
#
#  index_profiles_on_profile_type  (profile_type)
#  index_profiles_on_publisher_id  (publisher_id)
#

FactoryGirl.define do
  factory :profile do
    publisher
    profile_type { %w(blogger broker).sample }
    content_type { %w(own ghosted collaborate).sample }
    total_sites 0
    sequence :name do |n|
      "#{Faker::Team.name} - ##{n}"
    end
  end
end
