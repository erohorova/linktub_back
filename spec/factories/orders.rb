# == Schema Information
#
# Table name: orders
#
#  id                   :integer          not null, primary key
#  advertiser_id        :integer
#  url_id               :integer
#  ad_id                :integer
#  completed_date       :datetime
#  state                :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  invoice_id           :integer
#  created_from         :string
#  transaction_id       :integer
#  completed_by         :string
#  searched_campaign_id :integer
#
# Indexes
#
#  index_orders_on_ad_id          (ad_id)
#  index_orders_on_advertiser_id  (advertiser_id)
#  index_orders_on_invoice_id     (invoice_id)
#  index_orders_on_state          (state)
#  index_orders_on_url_id         (url_id)
#

FactoryGirl.define do
  factory :order do
    advertiser
    url
    ad
    state          1
    completed_date Faker::Time.between(1.month.ago, Time.zone.now)
  end
end
