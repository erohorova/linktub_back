# == Schema Information
#
# Table name: credit_cards
#
#  id               :integer          not null, primary key
#  masked_number    :string
#  braintree_token  :string
#  cardholder_name  :string
#  card_type        :string
#  expiration_month :integer
#  expiration_year  :integer
#  image_url        :string
#  street_address   :string
#  extended_address :string
#  city             :string
#  state            :string
#  zip_code         :string
#  country_name     :string
#  user_id          :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
# Indexes
#
#  index_credit_cards_on_braintree_token  (braintree_token)
#  index_credit_cards_on_user_id          (user_id)
#

FactoryGirl.define do
  factory :credit_card do
    masked_number    { "ending in #{Faker::Number.number(4)}" }
    cardholder_name  { "#{Faker::Name.first_name} #{Faker::Name.last_name}" }
    braintree_token  { Faker::Number.number(8) }
    card_type        %w(visa mastercard amex).sample
    expiration_month { rand(1..12).month.from_now.month }
    expiration_year  { rand(1..3).year.from_now.year }
    image_url        { Faker::Avatar.image }
    street_address   { Faker::Address.street_address }
    extended_address { Faker::Address.secondary_address }
    city             { Faker::Address.city }
    state            { Faker::Address.state }
    zip_code         { Faker::Address.zip_code }
    country_name     { Faker::Address.country }
    user
  end
end
