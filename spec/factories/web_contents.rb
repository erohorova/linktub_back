# == Schema Information
#
# Table name: web_contents
#
#  id              :integer          not null, primary key
#  href            :string
#  price           :float
#  state           :integer
#  profile_id      :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  approved_date   :date
#  declined_date   :date
#  reconsider_date :date
#  archived_date   :date
#  activated_date  :date
#  paused_date     :date
#  suggested_price :float            default(0.0)
#  shown_price     :float            default(0.0)
#  ga_account_id   :string
#  ga_profile_id   :string
#  website_url     :string
#  muv             :integer
#
# Indexes
#
#  index_web_contents_on_profile_id  (profile_id)
#  index_web_contents_on_state       (state)
#

FactoryGirl.define do
  factory :web_content do
    sequence :href do |n|
      "#{Faker::Internet.url}/#{n}"
    end
    price { Faker::Number.between(1, 1000) }
    profile
  end
end
