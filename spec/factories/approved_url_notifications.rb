FactoryGirl.define do
  factory :approved_url_notification, class: Notification, parent: :notification do
    notification_type 'ApprovedUrlNotification'
  end
end
