# == Schema Information
#
# Table name: author_invoices
#
#  id                               :integer          not null, primary key
#  approved_articles_count          :integer
#  admin_user_id                    :integer
#  paid                             :boolean          default(FALSE)
#  created_at                       :datetime         not null
#  updated_at                       :datetime         not null
#  invoice_spreadsheet_file_name    :string
#  invoice_spreadsheet_content_type :string
#  invoice_spreadsheet_file_size    :integer
#  invoice_spreadsheet_updated_at   :datetime
#
# Indexes
#
#  index_author_invoices_on_admin_user_id  (admin_user_id)
#

FactoryGirl.define do
  factory :author_invoice do
  end
end
