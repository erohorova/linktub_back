# == Schema Information
#
# Table name: invoices
#
#  id                                              :integer          not null, primary key
#  orders_count                                    :integer          default(0)
#  orders_accepted_count                           :integer          default(0)
#  total_charged                                   :float            default(0.0)
#  advertiser_id                                   :integer
#  created_at                                      :datetime         not null
#  updated_at                                      :datetime         not null
#  accepted_orders_report_spreadsheet_file_name    :string
#  accepted_orders_report_spreadsheet_content_type :string
#  accepted_orders_report_spreadsheet_file_size    :integer
#  accepted_orders_report_spreadsheet_updated_at   :datetime
#
# Indexes
#
#  index_invoices_on_advertiser_id  (advertiser_id)
#

FactoryGirl.define do
  factory :invoice do
    advertiser
    orders_count          { Random.rand(20..30) }
    orders_accepted_count { Random.rand(10..15) }
    total_charged         { Random.rand(100..500) }
  end
end
