# == Schema Information
#
# Table name: articles
#
#  id                  :integer          not null, primary key
#  url_id              :integer
#  link_id             :integer
#  ad_id               :integer
#  advertiser_id       :integer
#  state               :integer
#  pending_date        :datetime
#  approved_date       :datetime
#  rejected_date       :datetime
#  cancelled_date      :datetime
#  eta                 :date
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  author_id           :integer
#  assigned_date       :date
#  rewritten_counter   :integer          default(0)
#  removed_from_cart   :boolean          default(FALSE)
#  author_invoice_id   :integer
#  last_action_applied :string
#
# Indexes
#
#  index_articles_on_ad_id              (ad_id)
#  index_articles_on_advertiser_id      (advertiser_id)
#  index_articles_on_author_id          (author_id)
#  index_articles_on_author_invoice_id  (author_invoice_id)
#  index_articles_on_link_id            (link_id)
#  index_articles_on_state              (state)
#  index_articles_on_url_id             (url_id)
#

FactoryGirl.define do
  factory :article do
    url
    ad
    link
    eta { Faker::Date.between(2.days.from_now, 20.days.from_now) }
    state 1
  end
end
