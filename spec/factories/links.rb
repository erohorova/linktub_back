# == Schema Information
#
# Table name: links
#
#  id                :integer          not null, primary key
#  href              :string
#  approved_date     :date
#  suspended_date    :date
#  flagged_date      :date
#  archived_date     :date
#  reconsidered_date :date
#  declined_date     :date
#  page_rank         :integer          default(0)
#  alexa_rank        :integer          default(0)
#  back_links        :integer          default(0)
#  domain_age        :float            default(0.0)
#  state             :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  campaign_id       :integer
#
# Indexes
#
#  index_links_on_campaign_id  (campaign_id)
#  index_links_on_state        (state)
#  index_links_on_updated_at   (updated_at)
#

FactoryGirl.define do
  factory :link do
    sequence :href do |n|
      "#{Faker::Internet.url}/#{n}"
    end
    campaign
    approved_date            { Faker::Date.between(2.years.ago, Date.today) }
    suspended_date           { Faker::Date.between(2.years.ago, Date.today) }
    flagged_date             { Faker::Date.between(2.years.ago, Date.today) }
    archived_date            { Faker::Date.between(2.years.ago, Date.today) }
    reconsidered_date        { Faker::Date.between(2.years.ago, Date.today) }
    declined_date            { Faker::Date.between(2.years.ago, Date.today) }
    page_rank                { Faker::Number.between(1, 10) }
    alexa_rank               { Faker::Number.between(1, 10) }
    back_links               { Faker::Number.between(1, 10) }
    domain_age               { Faker::Number.decimal(2) }
  end
end
