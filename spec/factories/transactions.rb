# == Schema Information
#
# Table name: transactions
#
#  id                          :integer          not null, primary key
#  braintree_transaction_id    :string
#  payer_id                    :integer
#  payer_type                  :string
#  receiver_id                 :integer
#  receiver_type               :string
#  description                 :text
#  amount                      :string
#  state                       :integer
#  voided_at                   :datetime
#  declined_at                 :datetime
#  rejected_at                 :datetime
#  authorized_at               :datetime
#  submitted_for_settlement_at :datetime
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  is_refund                   :boolean          default(FALSE)
#  amount_submitted            :string
#
# Indexes
#
#  index_transactions_on_payer_type_and_payer_id        (payer_type,payer_id)
#  index_transactions_on_receiver_type_and_receiver_id  (receiver_type,receiver_id)
#

FactoryGirl.define do
  factory :transaction do
    braintree_transaction_id    { Faker::Blockchain::Bitcoin.address }
    payer                       nil
    receiver                    nil
    authorized_at               nil
    submitted_for_settlement_at nil
    description                 { Faker::Lorem.paragraph }
    amount                      { Faker::Number.decimal(4) }
  end
end
