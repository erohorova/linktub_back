# == Schema Information
#
# Table name: email_templates
#
#  id           :integer          not null, primary key
#  email_id     :integer
#  template     :text             default("")
#  days_delayed :integer          default(0)
#  active       :boolean          default(TRUE)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  subject      :string
#  collate      :boolean          default(FALSE)
#
# Indexes
#
#  index_email_templates_on_collate   (collate)
#  index_email_templates_on_email_id  (email_id)
#

FactoryGirl.define do
  factory :email_template do
    email_id 1
    subject "Fake email ##{Random.rand(20..30)}"
    template "<strong>#{Faker::Lorem.sentence}</strong>"
    days_delayed 0
    active true
  end

end
