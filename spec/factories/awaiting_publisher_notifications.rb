FactoryGirl.define do
  factory :awaiting_publisher_notification, class: Notification, parent: :notification do
    notification_type 'AwaitingPublisherNotification'
  end
end
