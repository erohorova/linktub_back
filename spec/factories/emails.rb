# == Schema Information
#
# Table name: emails
#
#  id                :integer          not null, primary key
#  name              :string
#  receiver_type     :string
#  response_required :boolean          default(FALSE)
#  redirect_url      :string
#  active            :boolean          default(TRUE)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  slug              :string
#  notes             :text
#

FactoryGirl.define do
  factory :email do
    name "Fake email"
    redirect_url { Faker::Internet.url }
    receiver_type "AdminUser"
    response_required true
    active true
  end

end
