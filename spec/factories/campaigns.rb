# == Schema Information
#
# Table name: campaigns
#
#  id                                       :integer          not null, primary key
#  name                                     :string
#  image                                    :string
#  state                                    :integer
#  created_at                               :datetime         not null
#  updated_at                               :datetime         not null
#  advertiser_id                            :integer
#  campaign_report_spreadsheet_file_name    :string
#  campaign_report_spreadsheet_content_type :string
#  campaign_report_spreadsheet_file_size    :integer
#  campaign_report_spreadsheet_updated_at   :datetime
#
# Indexes
#
#  index_campaigns_on_advertiser_id  (advertiser_id)
#  index_campaigns_on_state          (state)
#  index_campaigns_on_updated_at     (updated_at)
#

FactoryGirl.define do
  factory :campaign do
    sequence :name do |n|
      "#{Faker::Team.name} - ##{n}"
    end
    advertiser
    state 1

    factory :campaign_with_links do
      # posts_count is declared as a transient attribute and available in
      # attributes on the factory, as well as the callback via the evaluator
      transient do
        links_count 5
      end

      # the after(:create) yields two values; the user instance itself and the
      # evaluator, which stores all values from the factory, including transient
      # attributes; `create_list`'s second argument is the number of records
      # to create and we make sure the user is associated properly to the post
      after(:create) do |campaign, evaluator|
        create_list(:link, evaluator.links_count, campaign: campaign)
      end
    end
  end
end
