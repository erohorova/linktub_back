# == Schema Information
#
# Table name: advertisers
#
#  id                                            :integer          not null, primary key
#  billable_date                                 :date
#  suggested_link_email_sent_at                  :datetime
#  any_approved                                  :boolean          default(FALSE)
#  created_at                                    :datetime         not null
#  updated_at                                    :datetime         not null
#  awaiting_publication_spreadsheet_file_name    :string
#  awaiting_publication_spreadsheet_content_type :string
#  awaiting_publication_spreadsheet_file_size    :integer
#  awaiting_publication_spreadsheet_updated_at   :datetime
#  rejected_content_spreadsheet_file_name        :string
#  rejected_content_spreadsheet_content_type     :string
#  rejected_content_spreadsheet_file_size        :integer
#  rejected_content_spreadsheet_updated_at       :datetime
#  active_content_spreadsheet_file_name          :string
#  active_content_spreadsheet_content_type       :string
#  active_content_spreadsheet_file_size          :integer
#  active_content_spreadsheet_updated_at         :datetime
#  user_id                                       :integer
#  email                                         :string
#  content_pipe_zip_file_name                    :string
#  content_pipe_zip_content_type                 :string
#  content_pipe_zip_file_size                    :integer
#  content_pipe_zip_updated_at                   :datetime
#  activated_date                                :datetime
#  terminated_date                               :datetime
#  state                                         :integer
#  auto_approved                                 :boolean          default(FALSE)
#  advertisers_report_spreadsheet_file_name      :string
#  advertisers_report_spreadsheet_content_type   :string
#  advertisers_report_spreadsheet_file_size      :integer
#  advertisers_report_spreadsheet_updated_at     :datetime
#  agent_id                                      :integer
#
# Indexes
#
#  index_advertisers_on_agent_id  (agent_id)
#  index_advertisers_on_state     (state)
#  index_advertisers_on_user_id   (user_id)
#

FactoryGirl.define do
  factory :advertiser do
    billable_date                { Faker::Date.between(365.days.ago, Date.today) }
    suggested_link_email_sent_at { Faker::Time.between(2.days.ago, Date.today, :all) }
    any_approved                 { Faker::Boolean.boolean }
    user

    factory :advertiser_with_campaigns do
      # posts_count is declared as a transient attribute and available in
      # attributes on the factory, as well as the callback via the evaluator
      transient do
        campaigns_count 5
      end

      # the after(:create) yields two values; the user instance itself and the
      # evaluator, which stores all values from the factory, including transient
      # attributes; `create_list`'s second argument is the number of records
      # to create and we make sure the user is associated properly to the post
      after(:create) do |advertiser, evaluator|
        create_list(:campaign, evaluator.campaigns_count, advertiser: advertiser)
      end
    end

    # factory :advertiser_with_wish_list do
    #   transient do
    #     url_count 5
    #   end
    #
    #   after(:create) do |advertiser, evaluator|
    #     create_list(:url, evaluator.url_count, advertiser: advertiser)
    #   end
    # end
  end
end
