FactoryGirl.define do
  factory :active_content_notification, class: Notification, parent: :notification do
    notification_type 'ActiveContentNotification'
  end
end
