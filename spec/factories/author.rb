FactoryGirl.define do
  factory :author, class: AdminUser, parent: :admin_user do
    type 'Author'
  end
end
