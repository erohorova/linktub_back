# == Schema Information
#
# Table name: publishers
#
#  id                                         :integer          not null, primary key
#  user_id                                    :integer
#  avatar                                     :string
#  created_at                                 :datetime         not null
#  updated_at                                 :datetime         not null
#  email                                      :string
#  activated_date                             :datetime
#  terminated_date                            :datetime
#  state                                      :integer
#  active_content_spreadsheet_file_name       :string
#  active_content_spreadsheet_content_type    :string
#  active_content_spreadsheet_file_size       :integer
#  active_content_spreadsheet_updated_at      :datetime
#  publishers_report_spreadsheet_file_name    :string
#  publishers_report_spreadsheet_content_type :string
#  publishers_report_spreadsheet_file_size    :integer
#  publishers_report_spreadsheet_updated_at   :datetime
#
# Indexes
#
#  index_publishers_on_state    (state)
#  index_publishers_on_user_id  (user_id)
#

FactoryGirl.define do
  factory :publisher do
    user

    factory :publisher_with_profiles do
      transient do
        profiles_count 5
      end

      after(:create) do |publisher, evaluator|
        create_list(:profile, evaluator.profiles_count, publisher: publisher)
      end
    end
  end
end
