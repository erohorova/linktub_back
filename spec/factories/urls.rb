# == Schema Information
#
# Table name: urls
#
#  id                               :integer          not null, primary key
#  href                             :string
#  description                      :text
#  price                            :float            default(0.0)
#  follow_link                      :boolean          default(FALSE)
#  author_page                      :string
#  state                            :integer
#  approved_date                    :date
#  declined_date                    :date
#  reconsider_date                  :date
#  archived_date                    :date
#  activated_date                   :date
#  paused_date                      :date
#  domain_age                       :float            default(0.0)
#  domain_authority                 :integer          default(0)
#  page_authority                   :integer          default(0)
#  page_rank                        :integer          default(0)
#  alexa_rank                       :integer          default(0)
#  back_links                       :integer          default(0)
#  trust_flow                       :integer          default(0)
#  citation_flow                    :integer          default(0)
#  profile_categories_names         :text
#  created_at                       :datetime         not null
#  updated_at                       :datetime         not null
#  profile_id                       :integer
#  search_categories_vector         :tsvector
#  search_description_vector        :tsvector
#  allow_home_page                  :boolean          default(FALSE)
#  allow_blog_post                  :boolean          default(FALSE)
#  suggested_price                  :float            default(0.0)
#  shown_price                      :float            default(0.0)
#  required_length                  :string           default("750-1000"), not null
#  profile_categories_keyword       :string
#  search_categories_keyword_vector :tsvector
#  ahrefs_dr                        :integer
#  ahrefs_lrd                       :integer
#  semrush_traffic                  :integer
#
# Indexes
#
#  index_urls_on_alexa_rank            (alexa_rank)
#  index_urls_on_citation_flow         (citation_flow)
#  index_urls_on_domain_authority      (domain_authority)
#  index_urls_on_price                 (price)
#  index_urls_on_profile_id            (profile_id)
#  index_urls_on_state                 (state)
#  index_urls_on_trust_flow            (trust_flow)
#  urls_search_categories_keyword_idx  (search_categories_keyword_vector)
#  urls_search_description_idx         (search_description_vector)
#  urls_search_idx                     (search_categories_vector)
#

FactoryGirl.define do
  factory :url do
    href             { [Faker::Internet.url, FFaker::Internet.http_url, FFaker::InternetSE.http_url].sample.to_s }
    description      { [FFaker::CheesyLingo.sentence, FFaker::BaconIpsum.sentence, FFaker::DizzleIpsum.sentence].sample }
    price            { Faker::Number.between(10, 4000) }
    domain_age       { Faker::Number.decimal(2) }
    domain_authority { Faker::Number.between(1, 100) }
    page_authority   { Faker::Number.between(1, 100) }
    trust_flow       { Faker::Number.between(1, 100) }
    citation_flow    { Faker::Number.between(1, 100) }
    page_rank        { Faker::Number.between(1, 10_000) }
    alexa_rank       { Faker::Number.between(1, 10_000) }
    back_links       { Faker::Number.between(1, 10_000) }
    profile
    follow_link      { Faker::Boolean.boolean }
    allow_home_page  { Faker::Boolean.boolean }
    allow_blog_post  { Faker::Boolean.boolean }
    required_length  { '750-1000' }

    trait :trust_flow_between_0_and_10 do
      trust_flow { Faker::Number.between(0, 10) }
    end

    trait :citation_flow_between_11_and_20 do
      citation_flow { Faker::Number.between(11, 20) }
    end

    trait :trust_flow_between_11_and_20 do
      trust_flow { Faker::Number.between(11, 20) }
    end

    trait :citation_flow_between_0_and_10 do
      citation_flow { Faker::Number.between(0, 10) }
    end
  end
end
