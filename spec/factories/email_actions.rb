# == Schema Information
#
# Table name: email_actions
#
#  id                :integer          not null, primary key
#  email_trigger_id  :integer
#  email_template_id :integer
#  send_at           :datetime         not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  sent              :boolean          default(FALSE)
#  collate           :boolean          default(FALSE)
#  receiver_id       :integer
#  receiver_type     :string
#
# Indexes
#
#  index_email_actions_on_collate                        (collate)
#  index_email_actions_on_email_template_id              (email_template_id)
#  index_email_actions_on_email_trigger_id               (email_trigger_id)
#  index_email_actions_on_receiver_type_and_receiver_id  (receiver_type,receiver_id)
#

FactoryGirl.define do
  factory :email_action do
    email_trigger_id 1
    email_template_id 1
    send_at { Faker::Date.between(Date.today, 3.days.from_now) }
    sent false
  end

end
