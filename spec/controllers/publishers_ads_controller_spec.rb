require 'rails_helper'

RSpec.describe Api::V1::Publishers::AdsController, type: :controller do
  render_views

  let!(:publisher) { FactoryGirl.create(:publisher_with_profiles) }
  let!(:urls) { FactoryGirl.create_list(:url, 5, profile: publisher.profiles.sample) }
  let!(:ads) { FactoryGirl.create_list(:ad, 5, url: urls.sample, state: 2) }
  let!(:ads_pending) { FactoryGirl.create_list(:ad, 5, url: urls.sample, state: 2) }
  let!(:ads_rejected) { FactoryGirl.create_list(:ad, 5, url: urls.sample, state: 5) }
  let!(:orders) { FactoryGirl.create_list(:order, 5, url: urls.sample, ad: ads.sample) }

  before(:each) do
    login_with_credentials(publisher.user)
  end

  describe 'GET #index' do
    it 'should render all ads' do
      get :index, publisher_id: 'me', format: :json
      expect(response.response_code).to be 200
      expect(response_body(:ads).count).to eq(publisher.ads.count)
    end

    it 'should render ads with :approved state' do
      get :index, publisher_id: 'me', state: 'approved', format: :json
      expect(response.response_code).to be 200
      expect(response_body(:ads).count).to eq(publisher.general_approved_ads.count)
    end

    it 'should render ads with :rejected state' do
      get :index, publisher_id: 'me', state: 'rejected', format: :json
      expect(response.response_code).to be 200
      expect(response_body(:ads).count).to eq(publisher.rejected_ads.count)
    end
  end

  describe 'PUT #approve & reject' do
    it 'should approve the given ad' do
      expect { put :approve, publisher_id: 'me', ad_id: publisher.pending_ads.first.id, format: :json }
        .to change { publisher.approved_by_publisher_ads.count }.by(1)
    end

    context 'when rejecting ad' do
      it 'should reject the given ad' do
        expect { put :reject, publisher_id: 'me', ad_id: publisher.pending_ads.first.id, format: :json }
          .to change { publisher.rejected_ads.count }.by(1)
      end

      context 'when allowing rewrite' do
        let(:email) { FactoryGirl.create(:email, active: true, slug: 'advertiser_content_rewrite') }
        let!(:email_template) { FactoryGirl.create(:email_template, email: email, active: true) }
        let(:ad_params) do
          {
            editable: true,
            notes: 'My notes',
            rejected_reason: 'Too long'
          }
        end
        let(:ad) { publisher.pending_ads.first }

        it 'sends an email to the advertiser' do
          put :reject, publisher_id: 'me', ad_id: ad.id, ad: ad_params, format: :json
          trigger = EmailAction.first.email_trigger
          vars = trigger.vars
          expect(vars[:advertiser_first_name]).to eq(ad.advertiser.try(:first_name))
          expect(vars[:advertiser_last_name]).to eq(ad.advertiser.try(:last_name))
          expect(vars[:publisher_first_name]).to eq(ad.publisher.try(:first_name))
          expect(vars[:publisher_last_name]).to eq(ad.publisher.try(:last_name))
          expect(vars[:campaign_name]).to eq(ad.campaign.try(:name))
          expect(trigger.receiver_id).to eq(ad.advertiser.user.id)
        end
      end
    end
  end
end
