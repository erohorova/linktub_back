require 'rails_helper'

RSpec.describe Api::V1::Publishers::PayoutsController, type: :controller do
  render_views

  let(:publisher) { FactoryGirl.create(:publisher_with_profiles) }
  let(:urls) { Array.new(5) { FactoryGirl.create(:url, profile: publisher.profiles.sample) } }
  let(:ads) { urls.map { |u| FactoryGirl.create(:ad, url: u) } }
  let(:orders) { ads.map { |a| FactoryGirl.create(:order, url: a.url, ad: a) } }

  before(:each) do
    login_with_credentials(publisher.user)
  end

  describe 'GET #show' do
    it 'returns the publishers payment history' do
      orders.each(&:complete)
      get :show, publisher_id: 'me', date_from: 1.month.ago.to_s, date_to: 1.day.from_now.to_s, format: :json
      expect(response.response_code).to be 200
      expect(response_body(:items).sum { |i| i['amount'] }).to eq(response_body(:total_earnings))
    end
  end
end
