require 'rails_helper'

RSpec.describe Api::V1::ContentsController, type: :controller do
  render_views

  before(:each) do
    login_with_credentials(advertiser.user)
  end

  let(:advertiser) do
    FactoryGirl.create(:advertiser)
  end

  let!(:campaign) do
    FactoryGirl.create(:campaign, advertiser: advertiser)
  end

  let(:categories) do
    FactoryGirl.create_list(:category, 4)
  end

  let(:content_hash) do
    { category_ids: categories.map(&:id),
      campaign_id: campaign.id,
      trust_flow_min: '20',
      trust_flow_max: '80',
      domain_authority_min: '10',
      domain_authority_max: '80',
      agency_content: false }
  end

  describe 'GET index' do
    let!(:contents_requested) do
      FactoryGirl.create_list(:content, 3, advertiser: advertiser)
    end

    let!(:contents_received) do
      FactoryGirl.create_list(:content, 2, advertiser: advertiser, state: 2)
    end

    it 'should return all content requested' do
      get :index, advertiser_id: 'me', format: :json
      expect(response.response_code).to be 200
      expect(response_body(:contents).count).to eq(contents_requested.count)
    end

    it 'should return all content received' do
      get :index, advertiser_id: 'me', scope: 'received', format: :json
      expect(response.response_code).to be 200
      expect(response_body(:contents).count).to eq(contents_received.count)
    end
  end

  describe 'POST create' do
    it 'should create a new scalefluence content request for current advertiser' do
      post :create, advertiser_id: 'me', content: content_hash, format: :json
      expect(response.response_code).to be 200
      expect(response_body(:id)).to eq(Content.last.id)
      expect(response_body(:agency_content)).to eq(false)
    end

    it 'should create a new agency content request for current advertiser' do
      content_hash[:agency_content] = true
      post :create, advertiser_id: 'me', content: content_hash, format: :json
      expect(response.response_code).to be 200
      expect(response_body(:id)).to eq(Content.last.id)
      expect(response_body(:agency_content)).to eq(true)
    end
  end

  describe 'PUT update' do
    let(:content) do
      FactoryGirl.create(:content, advertiser: advertiser)
    end

    it 'should update content\'s categories' do
      put :update, advertiser_id: 'me', id: content.id, content: content_hash, format: :json
      expect(response.response_code).to be 200
      expect(response_body(:trust_flow_min)).to eq(20)
      expect(response_body(:trust_flow_max)).to eq(80)
      expect(response_body(:domain_authority_min)).to eq(10)
      expect(response_body(:domain_authority_max)).to eq(80)
      expect(response_body(:categories).count).to eq(categories.count)
    end
  end
end
