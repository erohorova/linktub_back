# encoding: utf-8
require 'rails_helper'

RSpec.describe Api::V1::UsersController, type: :controller do
  render_views

  before(:each) do
    @user = FactoryGirl.create(:user)
    @friend = FactoryGirl.create(:user)
  end

  describe "GET 'check_email'" do
    it 'email does exist' do
      get :check_email, email: @user.email, format: 'json'
      expect(response_body(:email_exists)).to be true
    end

    it 'email does not exist' do
      get :check_email, email: 'newemail@mail.com', format: 'json'
      expect(response_body(:email_exists)).to be false
    end
  end

  describe "PUT 'update/:id'" do
    it 'allows an user to be updated' do
      @attr = { first_name: 'new first_name' }
      request.headers['X-USER-TOKEN'] = @user.authentication_token
      put :update, id: @user.id, user: @attr, format: 'json'
      @user.reload
      expect(response.response_code).to be 200
      expect(@user.first_name).to eq @attr[:first_name]
    end

    it 'should not allow to update an user (bad auth)' do
      @attr = { first_name: 'new first_name' }
      request.headers['X-USER-TOKEN'] = @user.authentication_token + 'wrong'
      put :update, id: @user.id, user: @attr, format: 'json'
      @user.reload
      expect(response.status).to eq 401
      expect(@user.first_name).to_not eq @attr[:first_name]
    end

    it 'should not allow to update an user (bad data)' do
      @attr = { email: 'notanemail' }
      request.headers['X-USER-TOKEN'] = @user.authentication_token
      put :update, id: @user.id, user: @attr, format: 'json'
      @user.reload
      expect(@user.first_name).to_not eq @attr[:first_name]
      expect(response.response_code).to eq 400
    end

    context 'when the user wants to update the password' do
      before :each do
        login_with_credentials(@user)
      end
      context 'when the current password is correct' do
        let(:params) do
          {
            password: 'new_password',
            password_confirmation: 'new_password',
            current_password: @user.password
          }
        end

        it 'updates the password' do
          put :update, id: @user.id, user: params, format: 'json'
          expect(@user.reload.valid_password?(params[:password])).to be(true)
        end

        it 'returns a successful response' do
          put :update, id: @user.id, user: params, format: 'json'
          expect(response.response_code).to eq(200)
        end
      end

      context 'when the current password is incorrect' do
        let(:params) do
          {
            password: 'new_password',
            password_confirmation: 'new_password',
            current_password: 'incorrect password'
          }
        end

        let(:expected_message) { { 'error' => { 'current_password' => ['is invalid'] } } }

        it 'does not change the password' do
          put :update, id: @user.id, user: params, format: 'json'
          expect(@user.reload.valid_password?(params[:password])).to be(false)
        end

        it 'does not return a successful response' do
          put :update, id: @user.id, user: params, format: 'json'

          expect(response.response_code).to eq(400)
        end

        it 'returns an error message' do
          put :update, id: @user.id, user: params, format: 'json'
          expect(response_body).to eq(expected_message)
        end
      end
    end
  end
end
