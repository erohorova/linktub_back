require 'rails_helper'

RSpec.describe Api::V1::LinksController, type: :controller do
  render_views

  before(:each) do |test|
    login_with_credentials(advertiser.user) unless test.metadata[:skip_before]
  end

  let(:advertiser) { FactoryGirl.create(:advertiser) }

  let(:campaign) { FactoryGirl.create(:campaign, advertiser: advertiser) }

  let!(:link) { FactoryGirl.create(:link, campaign: campaign) }

  describe 'GET #index' do
    it 'should return all the links for the advertisers\' campaign' do
      get :index, advertiser_id: 'me', campaign_id: campaign.id, format: :json
      expect(response.response_code).to be 200
      expect(response_body(:links).count).to eq(Link.where(campaign_id: campaign.id).count)
    end
  end

  describe 'POST #create' do
    it 'should create a new link on given campaign' do
      expect do
        post :create, advertiser_id: 'me', campaign_id: campaign.id, link: {
          href: 'http://wwww.avalidurl.com'
        }, format: :json
      end.to change { Link.count }.by(1)
      expect(response.response_code).to be 200
    end
  end

  describe 'GET #orders_built' do
    before(:each) do
      link.ads << FactoryGirl.create_list(:ad, 5)
    end

    it 'returns information about orders built for the given link' do
      get :orders_built, advertiser_id: 'me', link_id: link.id, format: :json
      expect(response.response_code).to be 200
      expect(response_body(:orders_built).count).to eq(link.ads.count)
      expect(response_body(:orders_built)[0].keys).to contain_exactly(
        'href',
        'anchor',
        'domain_authority',
        'trust_flow',
        'full_pub_url',
        'shown_price'
      )
    end
  end

  describe 'DELETE #destroy' do
    it 'should remove selected link' do
      delete :destroy, advertiser_id: 'me', id: link.id, format: :json
      expect(response.response_code).to be 204
    end

    it 'should not remove selected link and must responds with \'unauthorized access\'', skip_before: true do
      delete :destroy, advertiser_id: 'me', id: link.id, format: :json
      expect(response.response_code).to be 401
    end
  end
end
