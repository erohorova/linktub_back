require 'rails_helper'

RSpec.describe Api::V1::Publishers::CountersController, type: :controller do
  render_views

  before(:each) do
    login_with_credentials(publisher.user)
  end

  let(:publisher) { FactoryGirl.create(:publisher) }

  describe 'GET show' do
    it 'should return all counters from publisher\'s content' do
      get :show, publisher_id: 'me', format: :json
      expect(response.response_code).to be 200
      expect(response_body(:active_ads)).to eq(0)
      expect(response_body(:new_orders)).to eq(0)
      expect(response_body(:pricing_and_pausing)).to eq(0)
      expect(response_body(:resubmitted_content)).to eq(0)
      expect(response_body(:active_ads_badge)).to eq(0)
      expect(response_body(:new_orders_badge)).to eq(0)
      expect(response_body(:pricing_and_pausing_badge)).to eq(0)
      expect(response_body(:resubmitted_content_badge)).to eq(0)
    end
  end
end
