require 'rails_helper'

RSpec.describe Admin::CampaignsController, type: :controller do
  render_views

  let(:admin)              { FactoryGirl.create(:admin_user) }
  let!(:pending_campaigns) { FactoryGirl.create_list(:campaign_with_links, 3) }
  let!(:active_campaigns)  { FactoryGirl.create_list(:campaign_with_links, 2, state: Campaign.states[:active]) }

  before(:each) do
    sign_in(admin)
    admin.confirm
  end

  describe 'GET #index' do
    it 'returns success code' do
      get :index
      expect(response.response_code).to be 200
    end

    it 'returns just pending campaigns' do
      get :index
      expect(JSON.parse(assigns(:campaigns)).count).to eq(pending_campaigns.count)
    end
  end

  describe 'PUT #state' do
    it 'changes selected campaign to new state' do
      campaign = pending_campaigns.first
      put :state, id: campaign.id, state: Campaign.states.key(Campaign.states[:active])
      expect(campaign.reload.state).to eq(Campaign.states.key(Campaign.states[:active]))
    end

    it 'changes all its links to \'approved\' after being approved' do
      campaign = pending_campaigns.first
      put :state, id: campaign.id, state: Campaign.states.key(Campaign.states[:active])
      expect(campaign.links.map(&:state).uniq).to eq([Link.states.key(Link.states[:approved])])
    end
  end
end
