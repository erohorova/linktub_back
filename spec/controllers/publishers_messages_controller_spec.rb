require 'rails_helper'

RSpec.describe Api::V1::Publishers::MessagesController, type: :controller do
  render_views

  let(:publisher) { FactoryGirl.create(:publisher) }
  let!(:messages) { FactoryGirl.create_list(:message, 5, user: publisher.user) }

  before(:each) do
    login_with_credentials(publisher.user)
  end

  describe 'GET #index' do
    it 'assigns all messages for given publisher' do
      get :index, publisher_id: 'me', format: :json
      expect(response.response_code).to be 200
      expect(response_body(:messages).count).to eq(publisher.messages.count)
    end
  end

  describe 'POST #create' do
    it 'created a new message' do
      expect do
        post :create, publisher_id: 'me', message: {
          subject: 'This is a subject',
          question_message: 'Here goes the question'
        }, format: :json
      end.to change { Message.count }.by(1)
    end
  end

  describe 'PUT #read' do
    it 'updates the replied message to state: read' do
      @message = messages.first
      @message.update! reply_message: 'This would be the reply'
      @message.reply
      expect do
        put :read, publisher_id: 'me', message_id: @message.id, format: :json
      end.to change { Message.find(@message.id).state }.to(Message.states.key(Message.states[:read]))
      expect(response.response_code).to be 204
    end
  end
end
