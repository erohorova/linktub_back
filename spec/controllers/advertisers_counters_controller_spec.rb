require 'rails_helper'

RSpec.describe Api::V1::Advertisers::CountersController, type: :controller do
  render_views

  before(:each) do
    login_with_credentials(advertiser.user)
  end

  let(:advertiser) do
    FactoryGirl.create(:advertiser)
  end

  describe 'GET show' do
    it 'should return all counters from advertiser\'s content' do
      get :show, advertiser_id: 'me', format: :json
      expect(response.response_code).to be 200
      expect(response_body(:active_content)).to eq(0)
      expect(response_body(:awaiting_pubs)).to eq(0)
      expect(response_body(:urls_approved)).to eq(0)
      expect(response_body(:reuse_content)).to eq(0)
      expect(response_body(:active_content_badge)).to eq(0)
      expect(response_body(:awaiting_pubs_badge)).to eq(0)
      expect(response_body(:urls_approved_badge)).to eq(0)
      expect(response_body(:reuse_content_badge)).to eq(0)
    end
  end
end
