require 'rails_helper'

RSpec.describe Api::V1::Publishers::UrlsController, type: :controller do
  render_views

  let!(:publisher) { FactoryGirl.create(:publisher_with_profiles) }
  let!(:urls) { Array.new(5) { FactoryGirl.create(:url, profile: publisher.profiles.sample) } }

  before(:each) do
    login_with_credentials(publisher.user)
  end

  describe 'GET index' do
    it 'should returns all urls' do
      get :index, publisher_id: 'me', format: :json
      expect(response.response_code).to be 200
      expect(response_body(:urls).count).to eq(publisher.urls.count)
    end

    it 'should returns all urls with state active' do
      3.times { publisher.pending_urls.sample.active }
      get :index, publisher_id: 'me', states: ['active'], format: :json
      expect(response.response_code).to be 200
      expect(response_body(:urls).count).to eq(3)
    end

    it 'should returns all urls with state paused' do
      2.times { publisher.pending_urls.sample.pause }
      get :index, publisher_id: 'me', states: ['paused'], format: :json
      expect(response.response_code).to be 200
      expect(response_body(:urls).count).to eq(2)
    end

    it 'should returns all urls with state active & paused' do
      2.times { publisher.pending_urls.sample.pause }
      1.times { publisher.pending_urls.sample.active }
      get :index, publisher_id: 'me', states: %w(active paused), format: :json
      expect(response.response_code).to be 200
      expect(response_body(:urls).count).to eq(3)
    end
  end
end
