# encoding: utf-8

require 'spec_helper'

describe Api::V1::SessionsController do
  render_views

  before :each do
    @request.env['devise.mapping'] = Devise.mappings[:user]
  end

  let(:advertiser) do
    FactoryGirl.create(:advertiser)
  end

  let(:publisher) do
    FactoryGirl.create(:publisher)
  end

  describe 'create' do
    context 'advertiser with valid login' do
      before do
        advertiser.user.confirm
        @params = {
          email:        advertiser.user.email,
          password:     advertiser.user.password
        }
      end

      it 'should return the user json' do
        post :create, user: @params, format: 'json'
        parsed_response = response_body
        expect(parsed_response['token']).to_not be_nil
      end
    end

    context 'advertiser with invalid login' do
      before do
        advertiser.user.confirm
        @params = {
          email:        advertiser.user.email,
          password:     advertiser.user.password
        }
      end

      it 'should return error with wrong password' do
        @params['password'] = 'baddPassword'
        post :create, user: @params, format: 'json'
        parsed_response = response_body
        expect(parsed_response['error']).to eq 'authentication error'
      end

      it 'should return error with wrong email' do
        @params['email'] = 'bademail@eaea.com'
        post :create, user: @params, format: 'json'
        parsed_response = response_body
        expect(parsed_response['error']).to eq 'authentication error'
      end
    end

    context 'publisher with valid login' do
      before do
        publisher.user.confirm
        @params = {
          email:        publisher.user.email,
          password:     publisher.user.password
        }
      end

      it 'should return the user json' do
        post :create, user: @params, format: 'json'
        parsed_response = response_body
        expect(parsed_response['token']).to_not be_nil
      end
    end

    context 'publisher with invalid login' do
      before do
        publisher.user.confirm
        @params = {
          email:        publisher.user.email,
          password:     publisher.user.password
        }
      end

      it 'should return error with wrong password' do
        @params['password'] = 'baddPassword'
        post :create, user: @params, format: 'json'
        parsed_response = response_body
        expect(parsed_response['error']).to eq 'authentication error'
      end

      it 'should return error with wrong email' do
        @params['email'] = 'bademail@eaea.com'
        post :create, user: @params, format: 'json'
        parsed_response = response_body
        expect(parsed_response['error']).to eq 'authentication error'
      end

      it 'should return error with suspended account' do
        @params = {
          email:        publisher.user.email,
          password:     publisher.user.password
        }
        publisher.terminate
        post :create, user: @params, format: 'json'
        parsed_response = response_body
        expect(parsed_response['errors']).to eq ['Your account has been suspended']
      end
    end
  end
end
