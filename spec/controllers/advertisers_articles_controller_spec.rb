require 'rails_helper'

RSpec.describe Api::V1::Advertisers::ArticlesController, type: :controller do
  render_views

  let(:advertiser) { FactoryGirl.create(:advertiser) }
  let!(:articles) { FactoryGirl.create_list(:article, 5) }
  let!(:url) { FactoryGirl.create(:url) }
  let!(:campaign) { FactoryGirl.create(:campaign, advertiser: advertiser) }
  let!(:link) { FactoryGirl.create(:link, campaign: campaign) }
  let(:content) { FactoryGirl.create(:content, advertiser: advertiser, campaign: campaign) }
  let(:params) do
    [{ content_id: content.id, notes: 'This is a note', url_id: url.id, href: 'http://www.testing.com',
       ad_attributes: { campaign_id: campaign.id, blog_post_title: 'This is a title',
                        link_text: 'This is an anchor', url_id: url.id }}]
  end
  let(:new_articles) { FactoryGirl.create_list(:article, 4, advertiser: advertiser) }

  before(:each) do
    login_with_credentials(advertiser.user)
  end

  describe 'GET #index' do
    it "returns the advertiser's articles" do
      get :index, advertiser_id: 'me', format: :json
      expect(response_body(:articles).count).to eq(advertiser.articles.count)
    end
  end

  describe 'POST #bulk_create' do
    it 'creates new Articles' do
      expect do
        post :bulk_create, articles: params, advertiser_id: 'me', format: :json
      end.to change(Article, :count).by(1)
    end

    it 'removes the urls from the content' do
      content.urls << url
      expect do
        post :bulk_create, articles: params, advertiser_id: 'me', format: :json
      end.to change(content.urls, :count).by(-1)
    end
  end

  describe 'POST #create_orders' do
    it 'creates orders for given articles_ids' do
      expect do
        post :create_orders, articles_ids: new_articles.map(&:id), advertiser_id: 'me', format: :json
      end.to change(Order, :count).by(4)
    end
  end
end
