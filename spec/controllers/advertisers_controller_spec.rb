require 'rails_helper'

RSpec.describe Api::V1::AdvertisersController, type: :controller do
  render_views

  let!(:advertiser) { FactoryGirl.create(:advertiser_with_campaigns, campaigns_count: 3) }
  let(:user_without_advertiser) { FactoryGirl.create(:user) }

  before(:each) do
    login_with_credentials(advertiser.user)
  end

  describe 'GET show' do
    it 'should returns active campaings for advertiser' do
      get :show, id: 'me', format: :json
      expect(response.response_code).to be 200
      expect(advertiser.campaigns.count).to be >= advertiser.active_campaigns.count
    end

    it 'should not returns campaigns (bad auth)' do
      request.headers['X-USER-TOKEN'] = 'wrong_token'
      get :show, id: 'me', format: :json
      expect(response.response_code).to be 401
    end
  end

  describe 'POST #create' do
    it 'must create a new advertiser for the given user' do
      request.headers['X-USER-TOKEN'] = user_without_advertiser.authentication_token
      post :create, format: :json
      expect(response.response_code).to be 200
      expect(user_without_advertiser.advertiser).to_not be_nil
    end
  end
end
