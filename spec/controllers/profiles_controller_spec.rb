require 'rails_helper'

RSpec.describe Api::V1::ProfilesController, type: :controller do
  render_views

  let(:publisher) { FactoryGirl.create(:publisher) }
  let!(:profiles) { FactoryGirl.create_list(:profile, 3, publisher: publisher) }
  let!(:other_profile) { FactoryGirl.create(:profile) }
  let!(:profile_attrs) { FactoryGirl.attributes_for(:profile) }

  before(:each) do
    login_with_credentials(publisher.user)
  end

  describe 'GET index' do
    it 'returns all the profiles for the publisher' do
      get :index, publisher_id: 'me', format: :json
      expect(response.response_code).to be 200
      expect(response_body(:profiles).count).to eq(profiles.count)
    end
  end

  describe 'GET #show' do
    it 'returns the profile given the id' do
      get :show, publisher_id: 'me', id: profiles.first.id, format: :json
      expect(response.response_code).to be 200
      expect(response_body(:name)).to eq(profiles.first.name)
    end

    it 'returns not_found if id doesn\'t belong to a publishers\'s profile' do
      get :show, publisher_id: 'me', id: other_profile.id, format: :json
      expect(response.response_code).to be 404
    end
  end

  describe 'GET #meta_description' do
    it 'returns the meta description for a url', :vcr do
      expected_description = 'The LinkTub Platform Combines the Power of SEO and Social Media.'
      get :meta_description, publisher_id: 'me', url: 'www.linktub.com', format: :json
      expect(response.response_code).to be 200
      expect(response_body(:description)).to eq(expected_description)
    end

    it 'returns error when url not provided', :vcr do
      get :meta_description, publisher_id: 'me', url: '', format: :json
      expect(response.response_code).to be 400
    end

    it 'returns error when url bad formed', :vcr do
      get :meta_description, publisher_id: 'me', url: 'ww.htbadurl.com', format: :json
      expect(response.response_code).to be 400
    end
  end

  describe 'GET #domain_information' do
    it 'returns the trust flow and domain authority from a url' do
      get :domain_information, publisher_id: 'me', url: 'https://www.google.com', format: :json
      expect(response.response_code).to be 200
      expect(response_body(:domain_authority)).to be_between(1, 100).inclusive
      expect(response_body(:trust_flow)).to be_between(1, 100).inclusive
    end
  end

  describe 'GET #ga_muv' do
    it 'returns the monthly unique visitors from a Google Analytics Url' do
      get :ga_muv, publisher_id: 'me', url: 'https://www.google.com', format: :json
      expect(response.response_code).to be 200
      expect(response_body(:visitors)).to be > 0
    end
  end

  describe 'GET #followers' do
    it 'returns the followers from a facebook url' do
      get :facebook_followers, publisher_id: 'me', url: 'https://www.facebook.com/example', format: :json
      expect(response.response_code).to be 200
      expect(response_body(:followers)).to be > 0
    end

    it 'returns the followers from a twitter handle', :vcr  do
      get :twitter_followers, publisher_id: 'me', handle: 'mkbhd', format: :json
      expect(response.response_code).to be 200
      expect(response_body(:count)).to be > 0
    end

    it 'returns the followers from a youtube url' do
      get :youtube_followers, publisher_id: 'me', url: 'https://www.youtube.com/example', format: :json
      expect(response.response_code).to be 200
      expect(response_body(:followers)).to be > 0
    end

    it 'returns the followers from a pinterest url' do
      get :pinterest_followers, publisher_id: 'me', url: 'https://www.pinterest.com/example', format: :json
      expect(response.response_code).to be 200
      expect(response_body(:followers)).to be > 0
    end

    it 'returns the followers from a instagram url' do
      get :instagram_followers, publisher_id: 'me', url: 'https://www.instagram.com/example', format: :json
      expect(response.response_code).to be 200
      expect(response_body(:followers)).to be > 0
    end
  end

  describe 'PUT #update' do
    before(:each) do
      profile_attrs[:facebook_handles_attributes] = [{ price: 150.0, url: 'http://www.facebook.com/test' }]
      profile_attrs[:web_content_attributes] = { href: 'http://www.somepage.com', price: 500 }
    end

    it 'returns the updated profile' do
      put :update, publisher_id: 'me', id: profiles.first.id, profile: profile_attrs, format: :json
      expect(response.response_code).to be 200
      expect(response_body(:name)).to eq(profile_attrs[:name])
    end

    it 'creates the correct product for the profile' do
      post :create, publisher_id: 'me', profile: profile_attrs, format: :json
      expect(response.response_code).to be 200
      expect(response_body(:products).length).to eq(2)
      expect(response_body(:products)).to include('name' => 'social')
      expect(response_body(:products)).to include('name' => 'sponsored')
      expect(Profile.find(response_body(:id)).total_sites).to be(2)
    end
  end

  describe 'POST #create' do
    before(:each) do
      profile_attrs[:facebook_handles_attributes] = [{ price: 150.0, url: 'http://www.facebook.com/test' }]
      profile_attrs[:youtube_handles_attributes] = [{ price: 250.0, url: 'http://www.youtube.com/test' }]
      profile_attrs[:twitter_handles_attributes] = [{ price: 350.0, url: 'http://www.twitter.com/test' }]
    end

    it 'returns the new profile' do
      expect { post :create, publisher_id: 'me', profile: profile_attrs, format: :json }
        .to change { publisher.profiles.count }.by(1)
      expect(response.response_code).to be 200
      expect(response_body(:facebook_handles).count).to eq(1)
      expect(response_body(:youtube_handles).count).to eq(1)
      expect(response_body(:twitter_handles).count).to eq(1)
      expect(Profile.find(response_body(:id)).total_sites).to be(3)
    end

    it 'creates the correct product for the profile' do
      post :create, publisher_id: 'me', profile: profile_attrs, format: :json
      expect(response.response_code).to be 200
      expect(response_body(:products).length).to eq(1)
      expect(response_body(:products)).to include('name' => 'social')
      expect(Profile.find(response_body(:id)).total_sites).to be(3)
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the profile given the id' do
      expect { delete :destroy, publisher_id: 'me', id: profiles.last.id, format: :json }
        .to change { publisher.profiles.count }.by(-1)
      expect(response.response_code).to be 204
    end
  end
end
