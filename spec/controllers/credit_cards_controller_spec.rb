require 'rails_helper'

RSpec.describe Api::V1::CreditCardsController, type: :controller do
  render_views

  before(:each) do
    login_with_credentials(advertiser.user)
  end

  let!(:advertiser)   { FactoryGirl.create(:advertiser) }
  let!(:credit_cards) { FactoryGirl.create_list(:credit_card, 4, user: advertiser.user) }
  let!(:credit_card)  { FactoryGirl.create(:credit_card) }

  describe 'GET #index' do
    it 'return all notifications', :vcr do
      get :index, advertiser_id: 'me', format: :json
      expect(response.response_code).to be 200
      expect(response_body(:credit_cards).count).to eq(advertiser.credit_cards.count)
    end
  end

  describe 'POST #create' do
    it 'should create a new credit card with given information', :vcr do
      expect do
        post :create, advertiser_id: 'me', credit_card: {
          payment_nonce: 'fake-valid-visa-nonce'
        }, format: :json
      end.to change { CreditCard.count }.by(1)
      expect(response.response_code).to be 200
    end
  end

  describe 'PUT update' do
    before(:each) do
      credit_card.update!(braintree_token: '5nn6dy')
    end

    it 'should update credit card\'s information', :vcr do
      old_value = credit_card.cardholder_name
      expect do
        put :update, advertiser_id: 'me', id: credit_card.id, credit_card: {
          payment_nonce: 'fake-valid-visa-nonce',
          payment_token: credit_card.braintree_token
        }, format: :json
      end.to change { CreditCard.find(credit_card.id).cardholder_name }.from(old_value).to(nil)
      expect(response.response_code).to be 200
    end
  end

  describe 'DELETE #destroy' do
    it 'should remove selected credit card' do
      expect do
        delete :destroy, advertiser_id: 'me', id: credit_cards.first.id,
                         format: :json
      end.to change { CreditCard.count }.by(-1)
      expect(response.response_code).to be 204
    end
  end
end
