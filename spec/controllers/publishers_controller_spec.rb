require 'rails_helper'

RSpec.describe Api::V1::PublishersController, type: :controller do
  render_views

  let(:publisher) { FactoryGirl.create(:publisher_with_profiles, profiles_count: 3) }
  let(:user_without_publisher) { FactoryGirl.create(:user) }

  before(:each) do
    request.headers['X-USER-TOKEN'] = publisher.user.authentication_token
  end

  describe 'POST #create' do
    it 'must create a new publisher for the given user' do
      request.headers['X-USER-TOKEN'] = user_without_publisher.authentication_token
      post :create, format: :json
      expect(response.response_code).to be 200
      expect(user_without_publisher.publisher).to_not be_nil
    end
  end
end
