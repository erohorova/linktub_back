require 'rails_helper'

RSpec.describe Api::V1::UrlsController, type: :controller do
  render_views

  let(:advertiser) do
    FactoryGirl.create(:advertiser, auto_approved: true)
  end
  let(:advertiser_without_auto_approve) do
    FactoryGirl.create(:advertiser)
  end
  let!(:urls_1) do
    FactoryGirl.create_list(:url, 3, :trust_flow_between_0_and_10, :citation_flow_between_11_and_20,
                            description: 'Family is good for people')
  end
  let!(:urls_2) do
    FactoryGirl.create_list(:url, 2, :trust_flow_between_11_and_20, :citation_flow_between_0_and_10,
                            description: 'Soccer is the best sport')
  end

  describe 'GET index' do
    before(:each) do
      login_with_credentials(advertiser.user)
    end
    it 'should returns all urls with trust_flow between 0-10' do
      urls_1.map(&:active!)
      get :index,
          domain_authority_min: 0, domain_authority_max: 100,
          trust_flow_min: 0, trust_flow_max: 10,
          citation_flow_min: 0, citation_flow_max: 100,
          shown_price_min: 0, shown_price_max: 4_000,
          words: ['is good'],
          sort_by: 'trust_flow',
          format: :json
      expect(response.response_code).to be 200
      expect(response_body(:urls).count).to eq(urls_1.count)
    end

    it 'returns all urls with trust_flow between 0-10 and sorted by price' do
      urls_1.map(&:active!)
      get :index,
          domain_authority_min: 0, domain_authority_max: 100,
          trust_flow_min: 0, trust_flow_max: 10,
          citation_flow_min: 0, citation_flow_max: 100,
          shown_price_min: 0, shown_price_max: 4_000,
          words: ['people'],
          sort_by: 'price',
          format: :json
      expect(response.response_code).to be 200
      expect(response_body(:urls).map { |u| u['price'].to_i }).to eq(urls_1.map(&:price).sort)
    end

    it 'should returns all urls with trust_flow between 0-20' do
      urls_1.map(&:active!)
      urls_2.map(&:active!)
      get :index,
          domain_authority_min: 0, domain_authority_max: 100,
          trust_flow_min: 0, trust_flow_max: 20,
          citation_flow_min: 0, citation_flow_max: 100,
          shown_price_min: 0, shown_price_max: 4_000,
          words: %w(people soccer),
          sort_by: 'trust_flow',
          format: :json
      expect(response.response_code).to be 200
      expect(response_body(:urls).count).to eq(urls_1.count + urls_2.count)
    end

    it 'should returns all urls with trust_flow between 11-20 & citation_flow between 0-10' do
      urls_2.map(&:active!)
      get :index,
          domain_authority_min: 0, domain_authority_max: 100,
          trust_flow_min: 11, trust_flow_max: 20,
          citation_flow_min: 0, citation_flow_max: 10,
          shown_price_min: 0, shown_price_max: 4_000,
          words: ['sport'],
          sort_by: 'trust_flow',
          format: :json
      expect(response.response_code).to be 200
      expect(response_body(:urls).count).to eq(urls_2.count)
    end

    it 'should returns all urls with trust_flow between 0-20 & citation_flow between 0-10' do
      urls_2.map(&:active!)
      get :index,
          domain_authority_min: 0, domain_authority_max: 100,
          trust_flow_min: 0, trust_flow_max: 20,
          citation_flow_min: 0, citation_flow_max: 10,
          shown_price_min: 0, shown_price_max: 4_000,
          words: ['sport'],
          sort_by: 'trust_flow',
          format: :json
      expect(response.response_code).to be 200
      expect(response_body(:urls).count).to eq(urls_2.count)
    end
  end

  describe 'GET #index' do
    before(:each) do
      login_with_credentials(advertiser_without_auto_approve.user)
    end
    it 'returns an error because the advertiser has not auto_approve' do
      get :index,
          domain_authority_min: 0, domain_authority_max: 100,
          trust_flow_min: 0, trust_flow_max: 20,
          citation_flow_min: 0, citation_flow_max: 10,
          shown_price_min: 0, shown_price_max: 4_000,
          words: ['sport'],
          sort_by: 'trust_flow',
          format: :json
      expect(response.response_code).to be 403
      expect(response_body(:error)).to eq('Cannot search until you have URLs approved')
    end
  end
end
