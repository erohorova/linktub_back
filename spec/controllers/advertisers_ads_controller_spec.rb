require 'rails_helper'

RSpec.describe Api::V1::Advertisers::AdsController, type: :controller do
  render_views

  let(:advertiser) { FactoryGirl.create(:advertiser) }
  let!(:ads) { FactoryGirl.create_list(:ad, 3, advertiser: advertiser, state: Ad.states[:approved]) }
  let!(:cancelled_ads) { FactoryGirl.create_list(:ad, 3, advertiser: advertiser, state: Ad.states[:cancelled]) }
  let!(:rejected_ads) { FactoryGirl.create_list(:ad, 2, advertiser: advertiser, state: Ad.states[:rejected]) }
  let!(:rejected_ad_removed_seen_in_admin) do
    FactoryGirl.create(:ad, advertiser: advertiser, state: Ad.states[:rejected], seen: true)
  end
  let(:one_declined_ad) { FactoryGirl.create(:ad, advertiser: advertiser, state: Ad.states[:rejected]) }
  let(:article) { FactoryGirl.create(:article, advertiser: advertiser) }
  let(:one_ad) { FactoryGirl.create(:ad, advertiser: advertiser, article: article) }

  before(:each) do
    login_with_credentials(advertiser.user)
  end

  describe 'GET #index' do
    it 'should render all advertiser\'s ads' do
      get :index, advertiser_id: 'me', format: :json
      expect(response.response_code).to be 200
      expect(response_body(:ads).count).to eq(Ad.where(advertiser_id: advertiser.id).count)
    end

    it "should render all advertiser's ads with state 'rejected_content'" do
      get :index, state: 'rejected_content', advertiser_id: 'me', format: :json
      expect(response.response_code).to be 200
      expect(response_body(:ads).count).to eq(3)
    end

    it "should render all advertiser's ads with state 'rejected_content'" do
      get :index, state: 'rejected_content', advertiser_id: 'me', format: :json
      expect(response.response_code).to be 200
      expect(response_body(:ads).count).to eq(3)
    end

    it "should render all unseen advertiser's ads with state 'rejected_content' or 'cancelled'" do
      get :index, state: 'rejected_or_cancelled_content', advertiser_id: 'me', format: :json
      expect(response.response_code).to be 200
      expect(response_body(:ads).count).to eq(5)
    end
  end

  describe 'PUT #update' do
    it 'should update given ad' do
      put :update, advertiser_id: 'me', id: one_declined_ad.id, ad: {
        blog_post_body: '<h1>Hello world!</h1>',
        blog_post_title: 'new title'
      }, format: :json
      expect(response.response_code).to be 200
      expect(Notification.count).to eq(1)
      expect(response_body(:blog_post_body)).to eq('<h1>Hello world!</h1>')
      expect(response_body(:blog_post_title)).to eq('new title')
    end

    it 'should update the blog_post_doc and trigger the zip upload', :vcr do
      pending 'there is no connection with S3 to test this feature'
      put :update, advertiser_id: 'me', id: one_ad.id, ad: {
        blog_post_doc: Rack::Test::UploadedFile.new(File.open(File.join(Rails.root, 'spec/fixtures/advertisers/document.docx')))
      }, format: :json
      expect(response.response_code).to be 200
      expect(selected.blog_post_doc.url).not_to be_empty
    end
  end

  describe 'DELETE #destroy' do
    it 'should remove selected campaign' do
      delete :destroy, advertiser_id: 'me', id: ads.first.id, format: :json
      expect(response.response_code).to be 204
    end
  end
end
