# encoding: utf-8

require 'spec_helper'

RSpec.describe Api::V1::NotificationsController, type: :controller do
  render_views

  let(:advertiser) { FactoryGirl.create(:advertiser) }

  let!(:notifications) do
    FactoryGirl.create_list(:notification, 1, user: advertiser.user, notification_type: Notification.types[:active_content])
    FactoryGirl.create_list(:notification, 2, user: advertiser.user, notification_type: Notification.types[:awaiting_publication])
    FactoryGirl.create_list(:notification, 3, user: advertiser.user, notification_type: Notification.types[:urls_approved])
    FactoryGirl.create_list(:notification, 4, user: advertiser.user, notification_type: Notification.types[:rejected_content])
  end

  before(:each) do
    login_with_credentials(advertiser.user)
  end

  describe 'DELETE #destroy' do
    it 'should remove just awaiting_publication_notifications' do
      delete :destroy_type, advertiser_id: 'me', type: 'awaiting_publication', format: :json
      expect(response.response_code).to be 204
      expect(Notification.count).to eq(
        Notification.where.not(notification_type: Notification.types[:awaiting_publication]).count
      )
    end

    it 'should remove just active_content_notifications' do
      delete :destroy_type, advertiser_id: 'me', type: 'active_content', format: :json
      expect(response.response_code).to be 204
      expect(Notification.count).to eq(
        Notification.where.not(notification_type: Notification.types[:active_content]).count
      )
    end

    it 'should remove just rejected_content_notification' do
      delete :destroy_type, advertiser_id: 'me', type: 'rejected_content', format: :json
      expect(response.response_code).to be 204
      expect(Notification.count).to eq(
        Notification.where.not(notification_type: Notification.types[:rejected_content]).count
      )
    end

    it 'should remove just urls_approved_notifications' do
      delete :destroy_type, advertiser_id: 'me', type: 'urls_approved', format: :json
      expect(response.response_code).to be 204
      expect(Notification.count).to eq(
        Notification.where.not(notification_type: Notification.types[:urls_approved]).count
      )
    end

    it 'should return an error because no type was specified' do
      delete :destroy_type, advertiser_id: 'me', format: :json
      expect(response.response_code).to be 404
      expect(response_body(:error)).to eq('Couldn\'t find the type')
    end
  end
end
