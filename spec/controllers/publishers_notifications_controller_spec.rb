# encoding: utf-8

require 'spec_helper'

RSpec.describe Api::V1::NotificationsController, type: :controller do
  render_views

  let(:publisher) { FactoryGirl.create(:publisher) }

  before(:each) do
    login_with_credentials(publisher.user)
    FactoryGirl.create_list(:notification, 1, user: publisher.user, notification_type: Notification.types[:active_ads])
    FactoryGirl.create_list(:notification, 2, user: publisher.user, notification_type: Notification.types[:new_orders])
    FactoryGirl.create_list(:notification, 3, user: publisher.user, notification_type: Notification.types[:pricing_and_pausing])
    FactoryGirl.create_list(:notification, 4, user: publisher.user, notification_type: Notification.types[:resubmitted_content])
  end

  describe 'DELETE #destroy' do
    it 'should remove just active_ads_notifications' do
      delete :destroy_type, publisher_id: 'me', type: 'active_ads', format: :json
      expect(response.response_code).to be 204
      expect(Notification.count).to eq(9)
    end

    it 'should remove just new_orders_notifications' do
      delete :destroy_type, publisher_id: 'me', type: 'new_orders', format: :json
      expect(response.response_code).to be 204
      expect(Notification.count).to eq(8)
    end

    it 'should remove just pricing_and_pausing_notification' do
      delete :destroy_type, publisher_id: 'me', type: 'pricing_and_pausing', format: :json
      expect(response.response_code).to be 204
      expect(Notification.count).to eq(7)
    end

    it 'should remove just resubmitted_content_notifications' do
      delete :destroy_type, publisher_id: 'me', type: 'resubmitted_content', format: :json
      expect(response.response_code).to be 204
      expect(Notification.count).to eq(6)
    end

    it 'should return an error because no type was specified' do
      delete :destroy_type, publisher_id: 'me', format: :json
      expect(response.response_code).to be 404
      expect(response_body(:error)).to eq("Couldn't find the type")
    end
  end
end
