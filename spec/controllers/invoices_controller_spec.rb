require 'rails_helper'

RSpec.describe Api::V1::InvoicesController, type: :controller do
  render_views

  let(:advertiser) { FactoryGirl.create(:advertiser) }
  let!(:invoices) { FactoryGirl.create_list(:invoice, 5, advertiser: advertiser) }
  let!(:other_invoices) { FactoryGirl.create_list(:invoice, 2) }

  before(:each) do
    login_with_credentials(advertiser.user)
  end

  describe 'GET #index' do
    it 'return all invoices' do
      get :index, advertiser_id: 'me', format: :json
      expect(response.response_code).to be 200
      expect(response_body(:invoices).count).to eq(advertiser.invoices.count)
    end
  end
end
