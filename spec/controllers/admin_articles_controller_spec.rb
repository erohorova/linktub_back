require 'rails_helper'

RSpec.describe Admin::ArticlesController, type: :controller do
  render_views

  let(:campaign) { FactoryGirl.create(:campaign) }
  let(:link_repeated) { FactoryGirl.create(:link, campaign: campaign, href: 'http://www.repeatedsite.com') }
  let(:link_other) { FactoryGirl.create(:link, campaign: campaign, href: 'http://www.othersite.com') }
  let(:article_repeated) { FactoryGirl.create(:article, link: link_repeated) }
  let(:other_article) { FactoryGirl.create(:article, link: link_other) }
  let!(:not_bought_articles) { [article_repeated, other_article] }
  let(:admin) { FactoryGirl.create(:admin_user) }
  let(:author) { FactoryGirl.create(:author) }

  before(:each) do
    sign_in(admin)
    admin.confirm
  end

  describe 'GET #index' do
    context 'when there is no article bought' do
      it 'returns the articles' do
        get :index, format: :json
        expect(response.response_code).to be 200
        expect(JSON.parse(response_body(:articles)).count).to eq(not_bought_articles.size)
      end
    end

    context 'when there is an article bought' do
      let(:link_of_bought_article) { FactoryGirl.create(:link, campaign: campaign, href: 'http://www.linkofbought.com') }
      let(:ordered_ad) { FactoryGirl.create(:ad, ordered_date: 1.day.ago) }
      let!(:bougth_article) { FactoryGirl.create(:article, link: link_of_bought_article, ad: ordered_ad) }

      it 'returns all articles but not the bought one' do
        get :index, format: :json
        expect(response.response_code).to be 200
        articles = JSON.parse(response_body(:articles))
        expect(articles.count).to eq(not_bought_articles.size)
        expect(articles.map { |art| art['id'] }).not_to include(bougth_article.id)
      end
    end
  end

  describe 'PUT #update' do
    context 'when the url is not already in use' do
      let(:params) do
        {
          link_attributes: {
            id: other_article.link.id,
            href: 'http://anothersite.com'
          }
        }
      end

      it 'returns a 200 status code' do
        put :update, id: other_article.id, article: params, format: :json
        expect(response.status).to eq(200)
      end
    end

    context 'when changing the author' do
      context 'when the article is in done state' do
        let(:done_article) { FactoryGirl.create(:article, link: link_other, state: Article.states[:done]) }
        let(:params) do
          {
            author_id: author.id
          }
        end

        it 'returns an error' do
          put :update, id: done_article.id, article: params, format: :json
          expect(response.status).to eq(422)
          expect(response_body(:errors)).to eq("Validation failed: #{I18n.t('articles.validations.errors.change_author')}")
        end
      end

      context 'when the article is in admin_approved state' do
        let(:admin_approved_article) { FactoryGirl.create(:article, link: link_other, state: Article.states[:admin_approved]) }
        let(:params) do
          {
            author_id: author.id
          }
        end

        it 'returns an error' do
          put :update, id: admin_approved_article.id, article: params, format: :json
          expect(response.status).to eq(422)
          expect(response_body(:errors)).to eq("Validation failed: #{I18n.t('articles.validations.errors.change_author')}")
        end
      end

      context 'when the article is in assigned state' do
        let(:assigned_article) { FactoryGirl.create(:article, link: link_other, state: Article.states[:assigned]) }
        let(:params) do
          {
            author_id: author.id
          }
        end

        it 'returns a 200 status code' do
          put :update, id: assigned_article.id, article: params, format: :json
          expect(response.status).to eq(200)
        end
      end
    end
  end
end
