require 'rails_helper'

RSpec.describe Api::V1::Publishers::OrdersController, type: :controller do
  render_views

  let!(:publisher) { FactoryGirl.create(:publisher_with_profiles) }
  let!(:urls) { Array.new(5) { FactoryGirl.create(:url, profile: publisher.profiles.sample) } }
  let!(:ads) { Array.new(5) { FactoryGirl.create(:ad, url: urls.sample, state: 4) } }
  let!(:orders) { Array.new(6) { FactoryGirl.create(:order, url: urls.sample, ad: ads.sample) } }

  before(:each) do
    login_with_credentials(publisher.user)
  end

  describe 'GET #index' do
    it 'should return all the completed orders made to the publisher' do
      2.times {
        pending_order = publisher.pending_orders.sample
        pending_order.complete
        pending_order.update!(completed_by: 'admin')
      } # complete 2 orders
      get :index, publisher_id: 'me', format: :json
      expect(response.response_code).to be 200
      expect(response_body(:orders).count).to eq(2)
    end
  end
end
