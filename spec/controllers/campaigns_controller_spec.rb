# encoding: utf-8

require 'spec_helper'

RSpec.describe Api::V1::CampaignsController, type: :controller do
  render_views

  let(:advertiser) do
    FactoryGirl.create(:advertiser)
  end

  let!(:campaigns) do
    FactoryGirl.create_list(:campaign, 3, advertiser: advertiser, state: :active)
  end

  let(:none_active_campaigns) do
    FactoryGirl.create_list(:campaign, 2, advertiser: advertiser, state: :pause)
  end

  let!(:categories) do
    FactoryGirl.create_list(:category, 4)
  end

  let(:campaign_categories) do
    FactoryGirl.create(:campaign, categories: categories, name: 'this is a name')
  end

  describe 'GET #index' do
    before(:each) do
      login_with_credentials(advertiser.user)
    end

    it 'should return all active campaigns for current_advertiser' do
      get :index, advertiser_id: 'me', format: :json
      expect(response.response_code).to be 200
      expect(response_body(:campaigns).count).to eq(campaigns.count)
    end

    it 'should return just 2 active campaigns for current_advertiser' do
      get :index, advertiser_id: 'me', limit: 2, format: :json
      expect(response.response_code).to be 200
      expect(response_body(:campaigns).count).to eq(2)
    end
  end

  describe 'POST #create' do
    before(:each) do
      login_with_credentials(advertiser.user)
    end

    it 'should create a new campaign with given categories' do
      post :create, advertiser_id: 'me', campaign: {
        name: 'this is a name',
        links_attributes: { '0' => { 'href': 'http://rspec.info/' } }
      }, categories_ids: Hash[categories.map.with_index { |cat, inx| [inx, cat.id] }], format: :json
      expect(response.response_code).to be 200
      expect(response_body(:name)).to eq('this is a name')
      expect(response_body(:categories).count).to eq(categories.count)
    end
  end

  describe 'PUT #update' do
    before(:each) do
      login_with_credentials(advertiser.user)
    end

    it 'should update given campaign with given categories ids' do
      put :update, advertiser_id: 'me', id: campaign_categories.id, campaign: {
        name: 'new name'
      }, categories_ids: Hash[[categories.first, categories.last].map.with_index { |cat, inx| [inx, cat.id] }], format: :json
      expect(response.response_code).to be 200
      expect(response_body(:name)).to eq('new name')
      expect(response_body(:categories).count).to eq(2)
    end
  end

  describe 'DELETE #destroy' do
    before(:each) do
      login_with_credentials(advertiser.user)
    end

    it 'should remove selected campaign' do
      delete :destroy, advertiser_id: 'me', id: campaign_categories.id, format: :json
      expect(response.response_code).to be 204
    end
  end

  describe 'GET #show' do
    before(:each) do
      login_with_credentials(advertiser.user)
    end

    let!(:other_ads) do
      FactoryGirl.create_list(:ad, 10, advertiser: advertiser, state: :approved_by_publisher)
    end

    context 'when there is an active content for the selected campaign' do
      let(:ad) do
        FactoryGirl.create(:ad, advertiser: advertiser, state: :approved_by_publisher)
      end

      it 'should render the campaign active link' do
        get :show, advertiser_id: 'me', id: ad.campaign.id, format: :json
        expect(response.response_code).to be 200
        expect(response_body(:links).length).to eq(1)
        expect(response_body(:links)[0]['id']).to eq(ad.link.id)
      end
    end

    context 'when there is not an active content for the selected campaign' do
      let(:ad) do
        FactoryGirl.create(:ad, advertiser: advertiser, state: :pending)
      end

      it 'should render 0 active links' do
        get :show, advertiser_id: 'me', id: ad.campaign.id, format: :json
        expect(response.response_code).to be 200
        expect(response_body(:links).length).to eq(0)
      end
    end
  end
end
