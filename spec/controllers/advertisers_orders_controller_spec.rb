require 'rails_helper'

RSpec.describe Api::V1::Advertisers::OrdersController, type: :controller do
  render_views

  let!(:advertiser) { FactoryGirl.create(:advertiser_with_campaigns) }
  let!(:links) { Array.new(5) { FactoryGirl.create(:link, campaign: advertiser.campaigns.sample) } }
  let!(:ad) { FactoryGirl.create(:ad, url: url) }
  let!(:url) { FactoryGirl.create(:url) }
  let!(:url_not_ordered) { FactoryGirl.create(:url) }
  let!(:orders) { FactoryGirl.create_list(:order, 6, advertiser: advertiser, url: url) }
  let!(:order_with_url) { FactoryGirl.create(:order, advertiser: advertiser, ad: ad, url: url) }

  before(:each) do
    login_with_credentials(advertiser.user)
  end

  describe 'GET #index' do
    it 'return all the pending orders made by the advertiser' do
      get :index, advertiser_id: 'me', format: :json
      expect(response.response_code).to be 200
      # The +1 is because of the order_with_url order
      expect(response_body(:orders).count).to eq(orders.count + 1)
    end
  end

  describe 'POST #create' do
    it 'create a new order for the advertiser' do
      expect do
        post :create, advertiser_id: 'me', order: {
          url_id: url.id,
          ad_attributes: {
            blog_post_title: 'some_title_example',
            url_id: url.id,
            campaign_id: advertiser.campaigns.first.id,
            link_id: advertiser.links.first.id
          }
        }, format: :json
      end.to change { advertiser.orders.count }.by 1
    end
  end

  describe 'PUT #update' do
    it 'update a given order' do
      id = advertiser.orders.first.id
      put :update, advertiser_id: 'me', id: id, order: {
        ad_attributes: {
          url_id: url.id,
          blog_post_title: 'new_title'
        }
      }, format: :json
      expect(response.response_code).to be 200
      expect(response_body['ad']['blog_post_title']).to eq('new_title')
    end
  end

  describe 'DELETE #destroy' do
    it 'destroy the given order' do
      id = advertiser.orders.first.id
      expect do
        delete :destroy, advertiser_id: 'me', id: id, format: :json
      end.to change { advertiser.orders.count }.by (-1)
      expect(response.response_code).to be 204
    end
  end

  describe 'DELETE #destroy_list' do
    it 'destroy all the orders with given ids' do
      advertiser.orders.each { |o| o.ad.create_article(FactoryGirl.attributes_for(:article)) }
      ids = advertiser.orders.map(&:id)
      expect do
        delete :destroy_list, advertiser_id: 'me', orders_ids: ids, format: :json
      end.to change { advertiser.orders.count }.to 0
      expect(response.response_code).to be 204
    end

    it 'changes all the orders\'s articles to removed_from_cart' do
      advertiser.orders.each { |o| o.ad.create_article(FactoryGirl.attributes_for(:article)) }
      ids = advertiser.orders.map(&:id)
      expect do
        delete :destroy_list, advertiser_id: 'me', orders_ids: ids, format: :json
      end.to change { Article.where(ad_id: advertiser.orders.map(&:ad_id), removed_from_cart: false).count }.to 0
      expect(response.response_code).to be 204
    end
  end

  describe 'GET #already_ordered' do
    it 'return that the url has not been ordered' do
      get :already_ordered, advertiser_id: 'me', url_id: url_not_ordered.id, format: :json
      expect(response.response_code).to be 200
      expect(response_body(:already_ordered)).to be false
    end

    it 'return that the url has been ordered' do
      get :already_ordered, advertiser_id: 'me', url_id: order_with_url.url_id, format: :json
      expect(response.response_code).to be 200
      expect(response_body(:already_ordered)).to be true
    end
  end

  # Need a test for 'GET #already_ordered_for_campaign'
end
