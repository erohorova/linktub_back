require 'rails_helper'

RSpec.describe Api::V1::PaymentsController, type: :controller do
  render_views

  let(:advertiser) { FactoryGirl.create(:advertiser) }
  let(:orders) { FactoryGirl.create_list(:order, 3, advertiser: advertiser) }

  before(:each) do
    login_with_credentials(advertiser.user)
  end

  describe 'GET #token' do
    it 'returns a payment token for the transaction', :vcr do
      get :token, advertiser_id: 'me', format: :json
      expect(response.response_code).to be 200
      expect(response_body(:token)).not_to be_empty
    end
  end

  describe 'POST #payment ' do
    it 'creates a payment transaction with a nonce', :vcr do
      expect do
        post :payment, advertiser_id: 'me', payment: {
          payment_nonce: 'fake-valid-nonce',
          last_four: '1111',
          store_payment_method: false,
          orders_ids: orders.map(&:id)
        }, format: :json
      end.to change { Transaction.count }.by(1)
      expect(response.response_code).to be 200
    end
  end
end
