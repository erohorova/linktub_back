require 'spec_helper'

RSpec.describe PaymentService do
  let(:user) { FactoryGirl.create(:user) }
  let!(:payment_service) { PaymentService.new(user) }

  describe 'consuming PaymentService', :vcr do
    it 'returns a valid token for transactions' do
      expect(payment_service.generate_token).not_to be_empty
    end
  end
end
