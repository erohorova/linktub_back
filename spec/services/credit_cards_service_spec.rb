require 'spec_helper'

RSpec.describe CreditCardsService do
  let(:user) { FactoryGirl.create(:user) }
  let!(:credit_cards_service) { CreditCardsService.new(user) }
  let!(:credit_card)  { FactoryGirl.create(:credit_card) }

  describe 'consuming CreditCardsService' do
    it 'create a payment method', :vcr do
      expect(credit_cards_service.create_payment_method('fake-valid-amex-nonce')).to be_success
    end
  end

  describe 'updating payment method' do
    before(:each) do
      credit_card.update!(braintree_token: '5nn6dy')
    end

    it 'updates a payment method given its token', :vcr do
      expect(credit_cards_service.update_payment_method('fake-valid-visa-nonce', credit_card.braintree_token)).to be_success
    end
  end
end
