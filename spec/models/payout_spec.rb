# == Schema Information
#
# Table name: payouts
#
#  id              :integer          not null, primary key
#  ad_id           :integer
#  publisher_id    :integer
#  amount          :string
#  currency        :string           default("USD")
#  paypal_email    :string
#  paypal_batch_id :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_payouts_on_ad_id         (ad_id)
#  index_payouts_on_publisher_id  (publisher_id)
#

require 'rails_helper'

RSpec.describe Payout, type: :model do
  it { is_expected.to belong_to(:ad) }
  it { is_expected.to belong_to(:publisher) }
end
