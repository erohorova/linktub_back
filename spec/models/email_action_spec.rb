# == Schema Information
#
# Table name: email_actions
#
#  id                :integer          not null, primary key
#  email_trigger_id  :integer
#  email_template_id :integer
#  send_at           :datetime         not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  sent              :boolean          default(FALSE)
#  collate           :boolean          default(FALSE)
#  receiver_id       :integer
#  receiver_type     :string
#
# Indexes
#
#  index_email_actions_on_collate                        (collate)
#  index_email_actions_on_email_template_id              (email_template_id)
#  index_email_actions_on_email_trigger_id               (email_trigger_id)
#  index_email_actions_on_receiver_type_and_receiver_id  (receiver_type,receiver_id)
#

require 'rails_helper'

RSpec.describe EmailAction, type: :model do
  it { is_expected.to belong_to(:email_template) }
  it { is_expected.to belong_to(:email_trigger) }
end
