# == Schema Information
#
# Table name: messages
#
#  id               :integer          not null, primary key
#  subject          :string
#  question_message :text
#  reply_message    :text
#  author           :string
#  replied_at       :datetime
#  read_at          :datetime
#  state            :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  user_id          :integer
#
# Indexes
#
#  index_messages_on_state    (state)
#  index_messages_on_user_id  (user_id)
#

require 'rails_helper'

RSpec.describe Message, type: :model do
  # Relationships...
  it { is_expected.to belong_to(:user) }

  # Validations...
  it { is_expected.to validate_presence_of :subject }
  it { is_expected.to validate_presence_of :question_message }

  # State machine
  it { is_expected.to define_enum_for(:state) }
  it { expect(Message.respond_to?(:read)).to be true }
  it { expect(Message.respond_to?(:unread)).to be true }
  it { expect(Message.respond_to?(:sent)).to be true }
end
