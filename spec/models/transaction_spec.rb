# == Schema Information
#
# Table name: transactions
#
#  id                          :integer          not null, primary key
#  braintree_transaction_id    :string
#  payer_id                    :integer
#  payer_type                  :string
#  receiver_id                 :integer
#  receiver_type               :string
#  description                 :text
#  amount                      :string
#  state                       :integer
#  voided_at                   :datetime
#  declined_at                 :datetime
#  rejected_at                 :datetime
#  authorized_at               :datetime
#  submitted_for_settlement_at :datetime
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  is_refund                   :boolean          default(FALSE)
#  amount_submitted            :string
#
# Indexes
#
#  index_transactions_on_payer_type_and_payer_id        (payer_type,payer_id)
#  index_transactions_on_receiver_type_and_receiver_id  (receiver_type,receiver_id)
#

require 'rails_helper'

RSpec.describe Transaction, type: :model do
  let(:advertiser) { FactoryGirl.create(:advertiser) }
  let(:ad_of_first_approved_ad) { FactoryGirl.create(:ad, state: Ad.states[:approved_by_publisher]) }
  let(:order_accepted_by_pub_of_first_approved_ad) do
    FactoryGirl.create(:order, state: Order.states[:completed], ad: ad_of_first_approved_ad)
  end
  let(:price_of_first_approved_ad) do
    content_ids = order_accepted_by_pub_of_first_approved_ad.advertiser.contents.where(
      campaign_id: order_accepted_by_pub_of_first_approved_ad.ad&.campaign_id
    ).pluck(:id)
    content_url = order_accepted_by_pub_of_first_approved_ad.url.content_urls.find_by(content_id: content_ids)
    content_url&.shown_price || order_accepted_by_pub_of_first_approved_ad.url.shown_price
  end

  let(:ad_of_second_approved_ad) { FactoryGirl.create(:ad, state: Ad.states[:approved_by_publisher]) }
  let(:order_accepted_by_pub_of_second_approved_ad) do
    FactoryGirl.create(:order, state: Order.states[:completed], ad: ad_of_second_approved_ad)
  end
  let(:price_of_second_approved_ad) do
    content_ids = order_accepted_by_pub_of_second_approved_ad.advertiser.contents.where(
      campaign_id: order_accepted_by_pub_of_second_approved_ad.ad&.campaign_id
    ).pluck(:id)
    content_url = order_accepted_by_pub_of_second_approved_ad.url.content_urls.find_by(content_id: content_ids)
    content_url&.shown_price || order_accepted_by_pub_of_second_approved_ad.url.shown_price
  end

  let(:ad_not_approved_by_pub) { FactoryGirl.create(:ad, state: Ad.states[:cancelled]) }
  let(:order_of_not_approved_ad) { FactoryGirl.create(:order, ad: ad_not_approved_by_pub) }
  let(:orders) do
    [order_accepted_by_pub_of_first_approved_ad, order_accepted_by_pub_of_second_approved_ad, order_of_not_approved_ad]
  end

  let(:transaction) do
    FactoryGirl.create(
      :transaction,
      orders: orders,
      state: Transaction.states[:authorized],
      payer: advertiser
    )
  end

  # Relationships...
  it { is_expected.to belong_to(:payer) }
  it { is_expected.to belong_to(:receiver) }

  # Respond to methods...
  it 'should have a valid factory' do
    expect(FactoryGirl.build_stubbed(:transaction)).to be_valid
  end

  context 'amount_at_the_moment' do
    it 'returns the amount that would be captured if there was going to be a settlement' do
      expect(transaction.amount_at_the_moment).to eq(price_of_first_approved_ad + price_of_second_approved_ad)
    end
  end

  context 'when transitioning to submitted' do
    before(:each) do
      transaction.submitted
    end

    it 'should save in amount_submitted the sum of the price of the orders with approved_by_pub ad' do
      expect(transaction.amount_submitted.to_i).to eq(price_of_first_approved_ad + price_of_second_approved_ad)
    end
  end
end
