# encoding: utf-8
# == Schema Information
#
# Table name: users
#
#  id                       :integer          not null, primary key
#  email                    :string           default(""), not null
#  encrypted_password       :string           default(""), not null
#  reset_password_token     :string
#  reset_password_sent_at   :datetime
#  sign_in_count            :integer          default(0), not null
#  current_sign_in_at       :datetime
#  last_sign_in_at          :datetime
#  current_sign_in_ip       :inet
#  last_sign_in_ip          :inet
#  authentication_token     :string           default("")
#  braintree_customer_id    :string
#  created_at               :datetime
#  updated_at               :datetime
#  is_active                :boolean          default(TRUE)
#  terminate_reason         :text
#  first_name               :string           not null
#  last_name                :string           not null
#  address_line_1           :string
#  address_line_2           :string
#  company                  :string
#  country                  :string
#  city                     :string
#  zip_code                 :string
#  phone                    :string
#  current_balance          :float            default(0.0)
#  payout_min               :integer
#  subscribe_to_news_letter :boolean          default(TRUE)
#  how_know_about_us        :string           default("")
#  paypal_email             :string
#  state                    :string
#  confirmation_token       :string
#  confirmed_at             :datetime
#  confirmation_sent_at     :datetime
#  payment_method           :string(191)
#  skrill_email             :string
#
# Indexes
#
#  index_users_on_authentication_token  (authentication_token) UNIQUE
#  index_users_on_confirmation_token    (confirmation_token) UNIQUE
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_first_name            (first_name)
#  index_users_on_last_name             (last_name)
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#

require 'spec_helper'

RSpec.describe User do
  # Precence of...
  it { is_expected.to validate_presence_of :first_name }
  it { is_expected.to validate_presence_of :last_name }

  # Relationships...
  it { is_expected.to have_one(:advertiser) }

  # TODO: Future teses for role publisher
  # it { is_expected.to have_one(:publisher) }

  # Respond to methods...
  it { is_expected.to respond_to(:advertiser?) }
  it { is_expected.to respond_to(:publisher?) }

  it 'should have a valid factory' do
    expect(FactoryGirl.build_stubbed(:user)).to be_valid
  end
end
