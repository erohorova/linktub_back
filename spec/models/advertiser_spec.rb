# == Schema Information
#
# Table name: advertisers
#
#  id                                            :integer          not null, primary key
#  billable_date                                 :date
#  suggested_link_email_sent_at                  :datetime
#  any_approved                                  :boolean          default(FALSE)
#  created_at                                    :datetime         not null
#  updated_at                                    :datetime         not null
#  awaiting_publication_spreadsheet_file_name    :string
#  awaiting_publication_spreadsheet_content_type :string
#  awaiting_publication_spreadsheet_file_size    :integer
#  awaiting_publication_spreadsheet_updated_at   :datetime
#  rejected_content_spreadsheet_file_name        :string
#  rejected_content_spreadsheet_content_type     :string
#  rejected_content_spreadsheet_file_size        :integer
#  rejected_content_spreadsheet_updated_at       :datetime
#  active_content_spreadsheet_file_name          :string
#  active_content_spreadsheet_content_type       :string
#  active_content_spreadsheet_file_size          :integer
#  active_content_spreadsheet_updated_at         :datetime
#  user_id                                       :integer
#  email                                         :string
#  content_pipe_zip_file_name                    :string
#  content_pipe_zip_content_type                 :string
#  content_pipe_zip_file_size                    :integer
#  content_pipe_zip_updated_at                   :datetime
#  activated_date                                :datetime
#  terminated_date                               :datetime
#  state                                         :integer
#  auto_approved                                 :boolean          default(FALSE)
#  advertisers_report_spreadsheet_file_name      :string
#  advertisers_report_spreadsheet_content_type   :string
#  advertisers_report_spreadsheet_file_size      :integer
#  advertisers_report_spreadsheet_updated_at     :datetime
#  agent_id                                      :integer
#
# Indexes
#
#  index_advertisers_on_agent_id  (agent_id)
#  index_advertisers_on_state     (state)
#  index_advertisers_on_user_id   (user_id)
#

require 'rails_helper'

RSpec.describe Advertiser, type: :model do
  # Kind of...
  it { is_expected.to be_kind_of(AccountType) }

  # Relationships...
  it { is_expected.to belong_to(:user) }
  it { is_expected.to have_many(:campaigns) }
  it { is_expected.to have_many(:ads) }

  # Respond to methods...
  it { is_expected.to respond_to(:pending_links) }
  it { is_expected.to respond_to(:approved_links) }
  it { is_expected.to respond_to(:active_content_count) }
  it { is_expected.to respond_to(:awaiting_pubs_count) }
  it { is_expected.to respond_to(:rejected_content_count) }
  it { is_expected.to respond_to(:active_content_spreadsheet_doc) }

  # Attachments...
  it { is_expected.to have_attached_file(:active_content_spreadsheet) }
  it do
    is_expected.to validate_attachment_content_type(:active_content_spreadsheet)
      .allowing('application/vnd.ms-excel')
  end

  it 'should have a valid factory' do
    expect(FactoryGirl.build_stubbed(:advertiser)).to be_valid
  end
end
