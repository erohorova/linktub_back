# == Schema Information
#
# Table name: publishers
#
#  id                                         :integer          not null, primary key
#  user_id                                    :integer
#  avatar                                     :string
#  created_at                                 :datetime         not null
#  updated_at                                 :datetime         not null
#  email                                      :string
#  activated_date                             :datetime
#  terminated_date                            :datetime
#  state                                      :integer
#  active_content_spreadsheet_file_name       :string
#  active_content_spreadsheet_content_type    :string
#  active_content_spreadsheet_file_size       :integer
#  active_content_spreadsheet_updated_at      :datetime
#  publishers_report_spreadsheet_file_name    :string
#  publishers_report_spreadsheet_content_type :string
#  publishers_report_spreadsheet_file_size    :integer
#  publishers_report_spreadsheet_updated_at   :datetime
#
# Indexes
#
#  index_publishers_on_state    (state)
#  index_publishers_on_user_id  (user_id)
#

require 'rails_helper'

RSpec.describe Publisher, type: :model do
  # Kind of...
  it { is_expected.to be_kind_of(AccountType) }

  # Relationships...
  it { is_expected.to belong_to(:user) }
  it { is_expected.to have_many(:profiles) }

  # Attachments...
  it { is_expected.to have_attached_file(:active_content_spreadsheet) }
  it do
    is_expected.to validate_attachment_content_type(:active_content_spreadsheet)
      .allowing('application/vnd.ms-excel')
  end

  # Methods...
  it { is_expected.to respond_to(:active_content_spreadsheet_doc) }

  let(:now) { DateTime.now }

  context 'there are publishers that have been reminded of payout change' do
    let!(:email) { FactoryGirl.create(:email, active: true, slug: 'publisher_change_payout_reminder') }
    let!(:email_template) { FactoryGirl.create(:email_template, email: email, active: true) }
    let!(:email_trigger) { FactoryGirl.create(:email_trigger, email: email) }
    let(:publishers_never_reminded) { FactoryGirl.create_list(:publisher, 3) }
    let(:publisher_never_without_active_links) { FactoryGirl.create(:publisher) }
    let(:publishers_reminded_two_months_ago) do
      FactoryGirl.create_list(:publisher, 2)
    end
    let!(:reminds_of_two_months_ago) do
      publishers_reminded_two_months_ago.map do |pub|
        FactoryGirl.create(
          :email_action,
          email_template: email_template,
          receiver: pub.user,
          sent: true,
          send_at: 2.months.ago,
          email_trigger: email_trigger
        )
      end
    end
    let(:publishers_reminded_last_month) do
      FactoryGirl.create_list(:publisher, 5)
    end
    let!(:reminds_of_a_month_ago) do
      publishers_reminded_last_month.map do |pub|
        FactoryGirl.create(
          :email_action,
          email_template: email_template,
          receiver: pub.user,
          sent: true,
          send_at: 7.days.ago,
          email_trigger: email_trigger
        )
      end
    end
    let!(:urls_of_publisher_never_without_active_links) do
      profile = FactoryGirl.create(:profile, publisher: publisher_never_without_active_links)
      FactoryGirl.create(:url, state: Url.states[:pending], profile: profile)
    end
    let!(:profiles_of_publishers_never_reminded) do
      publishers_never_reminded.map do |pub|
        profile = FactoryGirl.create(:profile, publisher: pub)
        url = FactoryGirl.create(:url, profile: profile)
        url.update(state: Url.states[:active])
      end
    end
    let!(:profiles_of_publishers_reminded_two_months_ago) do
      publishers_reminded_two_months_ago.map do |pub|
        profile = FactoryGirl.create(:profile, publisher: pub)
        url = FactoryGirl.create(:url, profile: profile)
        url.update(state: Url.states[:active])
      end
    end
    let!(:profiles_of_publishers_reminded_last_month) do
      publishers_reminded_last_month.map do |pub|
        profile = FactoryGirl.create(:profile, publisher: pub)
        url = FactoryGirl.create(:url, profile: profile)
        url.update(state: Url.states[:active])
      end
    end

    before(:each) { Publisher.send_payout_reminders }

    it 'should have created the pertinent emails' do
      expect(EmailAction.count).to eq(12)
      expect(EmailAction.all.map(&:receiver_id)).to include(*publishers_reminded_two_months_ago.map(&:user_id))
      expect(EmailAction.all.map(&:receiver_id)).to include(*publishers_never_reminded.map(&:user_id))
    end
  end
end
