# == Schema Information
#
# Table name: orders
#
#  id                   :integer          not null, primary key
#  advertiser_id        :integer
#  url_id               :integer
#  ad_id                :integer
#  completed_date       :datetime
#  state                :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  invoice_id           :integer
#  created_from         :string
#  transaction_id       :integer
#  completed_by         :string
#  searched_campaign_id :integer
#
# Indexes
#
#  index_orders_on_ad_id          (ad_id)
#  index_orders_on_advertiser_id  (advertiser_id)
#  index_orders_on_invoice_id     (invoice_id)
#  index_orders_on_state          (state)
#  index_orders_on_url_id         (url_id)
#

require 'rails_helper'

RSpec.describe Order, type: :model do
  # Relationships...
  it { is_expected.to belong_to(:advertiser) }
  it { is_expected.to belong_to(:ad) }
  it { is_expected.to belong_to(:invoice) }
  it { is_expected.to belong_to(:url) }

  # Validations of...
  it { is_expected.to have_db_index(:state) }
  it { is_expected.to define_enum_for(:state) }

  context 'when emailing order changes' do
    let!(:email_pub_order_cancelled) { FactoryGirl.create(:email, active: true, slug: 'publisher_order_cancelled', name: 'poc') }
    let!(:email_template_pub) { FactoryGirl.create(:email_template, email: email_pub_order_cancelled, active: true) }
    let!(:email_adv_order_cancelled) { FactoryGirl.create(:email, active: true, slug: 'advertiser_order_cancelled', name: 'aoc') }
    let!(:email_template_adv) { FactoryGirl.create(:email_template, email: email_adv_order_cancelled, active: true) }
    let(:ad) do
      FactoryGirl.create(:ad,
                         blog_post_title: 'blogPostTitle',
                         full_pub_url: 'www.fullpuburl.com',
                         notes: 'notes')
    end
    let(:order) { FactoryGirl.create(:order, ad: ad) }

    context 'when order is cancelled' do
      before(:each) { order.cancel }

      it 'sends an email to the advertiser' do
        email_trigger = EmailAction.find_by(receiver: order.advertiser.user.id).email_trigger
        vars = email_trigger.vars
        expect(vars[:publisher_first_name]).to eq(order.publisher.first_name)
        expect(vars[:publisher_last_name]).to eq(order.publisher.last_name)
        expect(vars[:advertiser_first_name]).to eq(order.advertiser.first_name)
        expect(vars[:advertiser_last_name]).to eq(order.advertiser.last_name)
        expect(vars[:content_url]).to eq(order.ad.blog_post_doc.url)
        expect(email_trigger.receiver_id).to eq(order.advertiser.user.id)
      end

      it 'sends an email to the publisher' do
        email_trigger = EmailAction.find_by(receiver: order.publisher.user.id).email_trigger
        vars = email_trigger.vars
        expect(vars[:publisher_first_name]).to eq(order.publisher.first_name)
        expect(vars[:publisher_last_name]).to eq(order.publisher.last_name)
        expect(vars[:advertiser_first_name]).to eq(order.advertiser.first_name)
        expect(vars[:advertiser_last_name]).to eq(order.advertiser.last_name)
        expect(vars[:content_url]).to eq(order.ad.blog_post_doc.url)
        expect(vars[:blog_post_url]).to eq(order.ad.blog_post_title)
        expect(vars[:pub_url]).to eq(order.ad.full_pub_url)
        expect(vars[:notes]).to eq(order.ad.notes)
        expect(email_trigger.receiver_id).to eq(order.publisher.user.id)
      end
    end
  end
end
