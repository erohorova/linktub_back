# == Schema Information
#
# Table name: invoices
#
#  id                                              :integer          not null, primary key
#  orders_count                                    :integer          default(0)
#  orders_accepted_count                           :integer          default(0)
#  total_charged                                   :float            default(0.0)
#  advertiser_id                                   :integer
#  created_at                                      :datetime         not null
#  updated_at                                      :datetime         not null
#  accepted_orders_report_spreadsheet_file_name    :string
#  accepted_orders_report_spreadsheet_content_type :string
#  accepted_orders_report_spreadsheet_file_size    :integer
#  accepted_orders_report_spreadsheet_updated_at   :datetime
#
# Indexes
#
#  index_invoices_on_advertiser_id  (advertiser_id)
#

require 'rails_helper'

RSpec.describe Invoice, type: :model do
  # Relationships...
  it { is_expected.to have_many(:orders) }

  # Validations...
  it { is_expected.to validate_presence_of :advertiser }
end
