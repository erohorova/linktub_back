# == Schema Information
#
# Table name: emails
#
#  id                :integer          not null, primary key
#  name              :string
#  receiver_type     :string
#  response_required :boolean          default(FALSE)
#  redirect_url      :string
#  active            :boolean          default(TRUE)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  slug              :string
#  notes             :text
#

require 'rails_helper'

RSpec.describe Email, type: :model do
  it { is_expected.to have_many(:email_templates) }
  it { is_expected.to have_many(:email_triggers) }
end
