# == Schema Information
#
# Table name: credit_cards
#
#  id               :integer          not null, primary key
#  masked_number    :string
#  braintree_token  :string
#  cardholder_name  :string
#  card_type        :string
#  expiration_month :integer
#  expiration_year  :integer
#  image_url        :string
#  street_address   :string
#  extended_address :string
#  city             :string
#  state            :string
#  zip_code         :string
#  country_name     :string
#  user_id          :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
# Indexes
#
#  index_credit_cards_on_braintree_token  (braintree_token)
#  index_credit_cards_on_user_id          (user_id)
#

require 'rails_helper'

RSpec.describe CreditCard, type: :model do
  # Relationships...
  it { is_expected.to belong_to(:user) }

  describe 'validations' do
    it { is_expected.to validate_presence_of :user }
    it { is_expected.to validate_presence_of :masked_number }
    it { is_expected.to validate_presence_of :braintree_token }

    it { is_expected.to validate_uniqueness_of :braintree_token }
    it { is_expected.to validate_uniqueness_of :masked_number }
  end
end
