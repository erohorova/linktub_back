# == Schema Information
#
# Table name: profiles
#
#  id           :integer          not null, primary key
#  name         :string
#  image        :string
#  profile_type :string
#  total_earned :float            default(0.0)
#  total_orders :integer          default(0)
#  total_sites  :integer          default(0)
#  products     :text
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  publisher_id :integer
#  content_type :string           default("collaborate"), not null
#
# Indexes
#
#  index_profiles_on_profile_type  (profile_type)
#  index_profiles_on_publisher_id  (publisher_id)
#

require 'rails_helper'

RSpec.describe Profile, type: :model do
  # Relationships...
  it { is_expected.to have_many(:categories) }
  it { is_expected.to have_many(:facebook_handles) }
  it { is_expected.to have_many(:youtube_handles) }
  it { is_expected.to have_many(:twitter_handles) }
  it { is_expected.to have_many(:instagram_handles) }
  it { is_expected.to have_many(:pinterest_handles) }

  # Validations...
  it { is_expected.to validate_presence_of :name }
  it { is_expected.to validate_presence_of :profile_type }
  it { is_expected.to validate_presence_of :content_type }
  it { is_expected.to validate_uniqueness_of(:name).scoped_to(:publisher_id) }
  it { is_expected.to validate_inclusion_of(:profile_type).in_array(Profile::PROFILE_TYPES) }

  # Respond to methods...
  it 'should have a valid factory' do
    expect(FactoryGirl.build_stubbed(:campaign)).to be_valid
  end
end
