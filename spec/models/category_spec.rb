# == Schema Information
#
# Table name: categories
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  keyword    :string
#
# Indexes
#
#  index_categories_on_name  (name)
#

require 'rails_helper'

RSpec.describe Category, type: :model do
  # Validations to...
  it { is_expected.to validate_presence_of :name }
  it { is_expected.to validate_uniqueness_of :name }

  it 'should have a valid factory' do
    expect(FactoryGirl.build_stubbed(:category)).to be_valid
  end
end
