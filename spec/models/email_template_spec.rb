# == Schema Information
#
# Table name: email_templates
#
#  id           :integer          not null, primary key
#  email_id     :integer
#  template     :text             default("")
#  days_delayed :integer          default(0)
#  active       :boolean          default(TRUE)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  subject      :string
#  collate      :boolean          default(FALSE)
#
# Indexes
#
#  index_email_templates_on_collate   (collate)
#  index_email_templates_on_email_id  (email_id)
#

require 'rails_helper'

RSpec.describe EmailTemplate, type: :model do
  it { is_expected.to belong_to(:email) }
  it { is_expected.to have_many(:email_actions) }
end
