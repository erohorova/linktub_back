# == Schema Information
#
# Table name: admin_users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  type                   :string
#  first_name             :string
#  last_name              :string
#  phone                  :string
#  state                  :integer
#  terminated_date        :date
#  activated_date         :date
#  paypal_email           :string
#  confirmation_token     :string
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  skrill_email           :string
#
# Indexes
#
#  index_admin_users_on_confirmation_token    (confirmation_token) UNIQUE
#  index_admin_users_on_email                 (email) UNIQUE
#  index_admin_users_on_first_name            (first_name)
#  index_admin_users_on_last_name             (last_name)
#  index_admin_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_admin_users_on_type                  (type)
#

require 'rails_helper'

RSpec.describe Author, type: :model do
  let(:author) { FactoryGirl.create(:author) }

  context 'when the author has one article of each state that can have an author' do
    let(:states_for_articles_with_author) do
      %i[
        draft
        assigned
        completed
        admin_rejected
        agency_rejected
        author_rejected
        admin_approved
        cancelled
        done
      ]
    end
    let!(:articles) do
      states_for_articles_with_author.map do |state|
        FactoryGirl.create(:article, state: state, author: author)
      end
    end

    context 'when testing pending_articles_count' do
      it 'returns exactly 4 articles' do
        expect(author.becomes(Author).pending_articles_count).to eq(4)
      end
    end
  end
end
