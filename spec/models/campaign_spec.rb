# == Schema Information
#
# Table name: campaigns
#
#  id                                       :integer          not null, primary key
#  name                                     :string
#  image                                    :string
#  state                                    :integer
#  created_at                               :datetime         not null
#  updated_at                               :datetime         not null
#  advertiser_id                            :integer
#  campaign_report_spreadsheet_file_name    :string
#  campaign_report_spreadsheet_content_type :string
#  campaign_report_spreadsheet_file_size    :integer
#  campaign_report_spreadsheet_updated_at   :datetime
#
# Indexes
#
#  index_campaigns_on_advertiser_id  (advertiser_id)
#  index_campaigns_on_state          (state)
#  index_campaigns_on_updated_at     (updated_at)
#

require 'rails_helper'

RSpec.describe Campaign, type: :model do
  # Relationships...
  it { is_expected.to belong_to(:advertiser) }
  it { is_expected.to have_many(:links) }
  it { is_expected.to have_many(:categories) }

  # Validations...
  it { is_expected.to validate_uniqueness_of(:name).scoped_to(:advertiser_id) }
  it { is_expected.to accept_nested_attributes_for :links }

  # Indexes
  it { is_expected.to have_db_index(:state) }

  # Respond to methods...
  it { is_expected.to respond_to(:pending_links) }
  it { is_expected.to define_enum_for(:state) }

  describe "when calling scope \'active\'" do
    before(:each) do
      FactoryGirl.create_list(:campaign, 2, state: :active)
    end

    it "should respond with just active campaigns for scope \'active\'" do
      expect(Campaign.active.count).to be 2
      expect(Campaign.active.count).to_not be 5
    end
  end

  describe 'a campaign object' do
    let(:campaign) { FactoryGirl.create(:campaign) }
    before(:each) do
      FactoryGirl.create_list(:link, 2, campaign: campaign)
      FactoryGirl.create_list(:link, 3, campaign: campaign, state: :approved)
      FactoryGirl.create_list(:link, 4, campaign: campaign, state: :archived)
    end

    it "should have_many \'pending_links\'" do
      expect(campaign.pending_links.count).to be 2
      expect(campaign.pending_links.count).to_not be 9
    end

    it "should have_many \'approved_links\'" do
      expect(campaign.approved_links.count).to be 3
      expect(campaign.approved_links.count).to_not be 9
    end

    it "should have_many \'not_archived_links\'" do
      expect(campaign.not_archived_links.count).to be 5
      expect(campaign.not_archived_links.count).to_not be 9
    end
  end

  it 'should have a valid factory' do
    expect(FactoryGirl.build_stubbed(:campaign)).to be_valid
  end
end
