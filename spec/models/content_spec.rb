# == Schema Information
#
# Table name: contents
#
#  id                            :integer          not null, primary key
#  advertiser_id                 :integer
#  campaign_id                   :integer
#  state                         :integer          not null
#  domain_authority_min          :integer
#  domain_authority_max          :integer
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#  urls_spreadsheet_file_name    :string
#  urls_spreadsheet_content_type :string
#  urls_spreadsheet_file_size    :integer
#  urls_spreadsheet_updated_at   :datetime
#  trust_flow_min                :integer
#  trust_flow_max                :integer
#  agency_content                :boolean          default(FALSE)
#  created_by                    :string
#  deleted_at                    :datetime
#  ahrefs_dr_min                 :integer
#  ahrefs_dr_max                 :integer
#
# Indexes
#
#  index_contents_on_advertiser_id  (advertiser_id)
#  index_contents_on_campaign_id    (campaign_id)
#  index_contents_on_deleted_at     (deleted_at)
#  index_contents_on_state          (state)
#

require 'rails_helper'

RSpec.describe Content, type: :model do
  # Relationships...
  it { is_expected.to belong_to(:advertiser) }
  it { is_expected.to belong_to(:campaign) }
  it { is_expected.to have_many(:categories) }
  it { is_expected.to have_many(:links) }
end
