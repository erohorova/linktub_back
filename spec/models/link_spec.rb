# == Schema Information
#
# Table name: links
#
#  id                :integer          not null, primary key
#  href              :string
#  approved_date     :date
#  suspended_date    :date
#  flagged_date      :date
#  archived_date     :date
#  reconsidered_date :date
#  declined_date     :date
#  page_rank         :integer          default(0)
#  alexa_rank        :integer          default(0)
#  back_links        :integer          default(0)
#  domain_age        :float            default(0.0)
#  state             :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  campaign_id       :integer
#
# Indexes
#
#  index_links_on_campaign_id  (campaign_id)
#  index_links_on_state        (state)
#  index_links_on_updated_at   (updated_at)
#

require 'rails_helper'

RSpec.describe Link, type: :model do
  # Relationships...
  it { is_expected.to belong_to(:campaign).autosave(true) }
  it { is_expected.to have_many(:ads) }

  # Indexes
  it { is_expected.to have_db_index(:campaign_id) }
  it { is_expected.to have_db_index(:state) }
  it { is_expected.to have_db_index(:updated_at) }

  # Validations...
  it { is_expected.to validate_presence_of :href }
  it { is_expected.to define_enum_for(:state) }

  describe 'a \'Link\' object' do
    before(:each) do
      FactoryGirl.create_list(:link, 27, state: :new_link)
      FactoryGirl.create_list(:link, 14, state: :under_review)
      FactoryGirl.create_list(:link, 7, state: :approved)
      FactoryGirl.create_list(:link, 4, state: :archived)
    end

    it "should have scope \'pending\'" do
      expect(Link.pending.count).to be 41
      expect(Link.pending.count).to_not be 52
    end

    it "should have scope \'approved\'" do
      expect(Link.approved.count).to be 7
      expect(Link.approved.count).to_not be 52
    end

    it "should have scope \'not_archived\'" do
      expect(Link.not_archived.count).to be 48
      expect(Link.not_archived.count).to_not be 52
    end
  end

  it 'should have a valid factory' do
    expect(FactoryGirl.build_stubbed(:link)).to be_valid
  end
end
