# == Schema Information
#
# Table name: articles
#
#  id                  :integer          not null, primary key
#  url_id              :integer
#  link_id             :integer
#  ad_id               :integer
#  advertiser_id       :integer
#  state               :integer
#  pending_date        :datetime
#  approved_date       :datetime
#  rejected_date       :datetime
#  cancelled_date      :datetime
#  eta                 :date
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  author_id           :integer
#  assigned_date       :date
#  rewritten_counter   :integer          default(0)
#  removed_from_cart   :boolean          default(FALSE)
#  author_invoice_id   :integer
#  last_action_applied :string
#
# Indexes
#
#  index_articles_on_ad_id              (ad_id)
#  index_articles_on_advertiser_id      (advertiser_id)
#  index_articles_on_author_id          (author_id)
#  index_articles_on_author_invoice_id  (author_invoice_id)
#  index_articles_on_link_id            (link_id)
#  index_articles_on_state              (state)
#  index_articles_on_url_id             (url_id)
#

require 'rails_helper'

RSpec.describe Article, type: :model do
  # Relationships...
  it { is_expected.to belong_to(:advertiser) }
  it { is_expected.to belong_to(:ad) }
  it { is_expected.to belong_to(:link) }
  it { is_expected.to belong_to(:url) }

  context 'when article is in admin_approved_article state' do
    let(:author) { FactoryGirl.create(:author) }
    let(:admin_approved_article) { FactoryGirl.create(:article, state: Article.states[:admin_approved]) }

    subject { admin_approved_article }

    it 'validates author is not changed' do
      subject.author = author
      expect(subject).not_to be_valid
    end
  end

  context 'when article is in done state' do
    let(:author) { FactoryGirl.create(:author) }
    let(:done_article) { FactoryGirl.create(:article, state: Article.states[:done]) }

    subject { done_article }

    it 'validates author is not changed' do
      subject.author = author
      expect(subject).not_to be_valid
    end
  end

  context 'when article is in assigned state' do
    let(:author) { FactoryGirl.create(:author) }
    let(:assigned_article) { FactoryGirl.create(:article, state: Article.states[:assigned]) }

    subject { assigned_article }

    it 'validates author is not changed' do
      subject.author = author
      expect(subject).to be_valid
    end
  end
end
