# == Schema Information
#
# Table name: ads
#
#  id                   :integer          not null, primary key
#  link_text            :string
#  requested_date       :datetime
#  approved_date        :datetime
#  rejected_date        :datetime
#  reconsidered_date    :datetime
#  cancelled_date       :datetime
#  admin_cancelled      :boolean          default(FALSE)
#  advertiser_cancelled :boolean          default(FALSE)
#  publisher_cancelled  :boolean          default(FALSE)
#  url_price            :decimal(, )      default(0.0)
#  blog_post            :string           default("blog_post")
#  blog_post_title      :string
#  blog_post_body       :string
#  quote_id             :string
#  full_pub_url         :string
#  attach_word_document :boolean          default(TRUE)
#  blog_post_doc        :string
#  blog_post_image_1    :string
#  blog_post_image_2    :string
#  state                :integer
#  rejected_reason      :string
#  notes                :text
#  editable             :boolean
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  advertiser_id        :integer
#  link_id              :integer
#  url_id               :integer
#  campaign_id          :integer
#  rejected_counter     :integer          default(0)
#  ordered_date         :date
#  seen                 :boolean          default(FALSE)
#
# Indexes
#
#  index_ads_on_advertiser_id  (advertiser_id)
#  index_ads_on_campaign_id    (campaign_id)
#  index_ads_on_link_id        (link_id)
#  index_ads_on_state          (state)
#  index_ads_on_url_id         (url_id)
#

require 'rails_helper'

RSpec.describe Ad, type: :model do
  # Relationships...
  it { is_expected.to belong_to(:advertiser) }
  it { is_expected.to belong_to(:link) }
  it { is_expected.to belong_to(:campaign) }
  it { is_expected.to have_one(:order) }
  it { is_expected.to have_one(:payout) }

  describe 'Scopes for each possible state' do
    it { expect(Ad).to respond_to(:cart_item) }
    it { expect(Ad).to respond_to(:pending) }
    it { expect(Ad).to respond_to(:approved_by_publisher) }
    it { expect(Ad).to respond_to(:approved) }
    it { expect(Ad).to respond_to(:rejected) }
    it { expect(Ad).to respond_to(:reconsidered) }
    it { expect(Ad).to respond_to(:resubmitted) }
    it { expect(Ad).to respond_to(:cancelled) }
  end
end
