# == Schema Information
#
# Table name: email_triggers
#
#  id                 :integer          not null, primary key
#  email_id           :integer
#  receiver_id        :integer
#  receiver_type      :string
#  vars               :text
#  response_completed :boolean          default(FALSE)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_email_triggers_on_email_id                       (email_id)
#  index_email_triggers_on_receiver_type_and_receiver_id  (receiver_type,receiver_id)
#

require 'rails_helper'

RSpec.describe EmailTrigger, type: :model do
  it { is_expected.to belong_to(:email) }
  it { is_expected.to have_many(:email_actions) }
end
