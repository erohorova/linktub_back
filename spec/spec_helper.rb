# encoding: utf-8

require 'rubygems'
require 'spork'
require 'paperclip/matchers'

# uncomment the following line to use spork with the debugger
# require 'spork/ext/ruby-debug'

Spork.prefork do
  # Loading more in this block will cause your tests to run faster. However,
  # if you change any configuration or code from libraries loaded here, you'll
  # need to restart spork for it take effect.

  # This file is copied to spec/ when you run 'rails generate rspec:install'
  ENV['RAILS_ENV'] ||= 'test'
  ENV['SKIP_RAILS_ADMIN_INITIALIZER'] = 'false'

  require File.expand_path('../../config/environment', __FILE__)
  require 'rspec/rails'
  require 'factory_girl_rails'

  FactoryGirl.factories.clear
  FactoryGirl.reload

  # Requires supporting ruby files with custom matchers and macros, etc,
  # in spec/support/ and its subdirectories.
  Dir[Rails.root.join('spec/support/**/*.rb')].each { |file| require file }

  # Checks for pending migrations before tests are run.
  # If you are not using ActiveRecord, you can remove this line.
  ActiveRecord::Migration.check_pending! if defined?(ActiveRecord::Migration)

  # Change rails logger level to reduce IO during tests
  Rails.logger.level = 4

  # Remove Braintree logs when running Rspec
  logger = Logger.new('/dev/null')
  logger.level = Logger::INFO
  Braintree::Configuration.logger = logger

  RSpec.configure do |config|
    config.use_transactional_fixtures = false
    config.infer_base_class_for_anonymous_controllers = false
    config.order = 'random'
    config.infer_spec_type_from_file_location!

    config.include FactoryGirl::Syntax::Methods
    config.include RequestHelpers
    config.include Paperclip::Shoulda::Matchers

    # Uncomment if you want to include Devise. Add devise to your gemfile
    # config.include Devise::TestHelpers, type: :controller

    config.before :each do
      DatabaseCleaner.strategy = :truncation
      DatabaseCleaner.start

      allow_any_instance_of(Twitter::REST::Client).to receive(:user).and_return(double({followers_count: 10000}))
      allow_any_instance_of(Linkscape::Client).to receive(:urlMetrics).and_return(double({
        response: double({ code: "200" }),
        data: { domain_authority: 20 }
      }))
      allow_any_instance_of(MajesticService).to receive(:get_data).and_return({ trust_flow: 35, citation_flow: 45 })
    end

    config.after do
      DatabaseCleaner.clean
    end
  end
end

Spork.each_run do
  # This code will be run each time you run your specs.
end
