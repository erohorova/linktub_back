require 'rails_helper'

RSpec.describe Api::V1::CampaignsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/v1/advertisers/me/campaigns').to route_to('api/v1/campaigns#index', advertiser_id: 'me', format: :json)
    end

    it 'routes to #create' do
      expect(post: '/api/v1/advertisers/me/campaigns').to route_to('api/v1/campaigns#create', advertiser_id: 'me', format: :json)
    end

    it 'routes to #names' do
      expect(get: '/api/v1/advertisers/me/campaigns/names').to route_to(
        'api/v1/campaigns#names', advertiser_id: 'me', format: :json
      )
    end

    it 'routes to #update_spreadsheet' do
      expect(put: '/api/v1/advertisers/me/campaigns/1/update_spreadsheet').to route_to(
        'api/v1/campaigns#update_spreadsheet', advertiser_id: 'me', campaign_id: '1', format: :json
      )
    end

    it 'routes to #update' do
      expect(put: '/api/v1/advertisers/me/campaigns/1').to route_to(
        'api/v1/campaigns#update', advertiser_id: 'me', id: '1', format: :json
      )
    end

    it 'routes to #destroy' do
      expect(delete: '/api/v1/advertisers/me/campaigns/1').to route_to(
        'api/v1/campaigns#destroy', advertiser_id: 'me', id: '1', format: :json
      )
    end
  end
end
