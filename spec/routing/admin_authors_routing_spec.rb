require 'rails_helper'

RSpec.describe Admin::AuthorsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/admin/authors').to route_to(
        'admin/authors#index'
      )
    end

    it 'routes to #show' do
      expect(get: '/admin/authors/1').to route_to(
        'admin/authors#show', id: '1'
      )
    end

    it 'routes to #update' do
      expect(put: '/admin/authors/1').to route_to(
        'admin/authors#update', id: '1'
      )
    end

    it 'routes to #create' do
      expect(post: '/admin/authors/').to route_to(
        'admin/authors#create'
      )
    end

    it 'routes to #state' do
      expect(put: '/admin/authors/1/state').to route_to(
        'admin/authors#state', id: '1'
      )
    end
  end
end
