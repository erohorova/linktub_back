require 'rails_helper'

RSpec.describe Admin::WebContentsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/admin/web_contents').to route_to(
        'admin/web_contents#index'
      )
    end

    it 'routes to #update' do
      expect(put: '/admin/web_contents/1').to route_to(
        'admin/web_contents#update', id: '1'
      )
    end

    it 'routes to #state' do
      expect(put: '/admin/web_contents/1/state').to route_to(
        'admin/web_contents#state', id: '1'
      )
    end

    it 'routes to #counter' do
      expect(get: '/admin/web_contents/counter').to route_to(
        'admin/web_contents#counter'
      )
    end
  end
end
