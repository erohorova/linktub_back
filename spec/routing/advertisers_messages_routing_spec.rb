require 'rails_helper'

RSpec.describe Api::V1::Advertisers::MessagesController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/v1/advertisers/me/messages').to route_to(
        'api/v1/advertisers/messages#index', advertiser_id: 'me', format: :json
      )
    end

    it 'routes to #show' do
      expect(get: '/api/v1/advertisers/me/messages/1').to route_to(
        'api/v1/advertisers/messages#show', advertiser_id: 'me', id: '1', format: :json
      )
    end

    it 'routes to #create' do
      expect(post: '/api/v1/advertisers/me/messages').to route_to(
        'api/v1/advertisers/messages#create', advertiser_id: 'me', format: :json
      )
    end

    it 'routes to #update via PUT' do
      expect(put: '/api/v1/advertisers/me/messages/1').to route_to(
        'api/v1/advertisers/messages#update', advertiser_id: 'me', id: '1', format: :json
      )
    end

    it 'routes to #destroy' do
      expect(delete: '/api/v1/advertisers/me/messages/1').to route_to(
        'api/v1/advertisers/messages#destroy', advertiser_id: 'me', id: '1', format: :json
      )
    end

    it 'routes to #read' do
      expect(put: '/api/v1/advertisers/me/messages/1/read').to route_to(
        'api/v1/advertisers/messages#read', advertiser_id: 'me', message_id: '1', format: :json
      )
    end
  end
end
