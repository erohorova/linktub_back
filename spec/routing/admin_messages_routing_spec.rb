require 'rails_helper'

RSpec.describe Admin::MessagesController, type: :routing do
  describe 'routing' do
    it 'routes to #update' do
      expect(put: '/admin/messages/1').to route_to(
        'admin/messages#update', id: '1'
      )
    end
  end
end
