require 'rails_helper'

RSpec.describe Api::V1::Publishers::PayoutsController, type: :routing do
  describe 'routing' do
    it 'routes to #show' do
      expect(get: '/api/v1/publishers/me/payouts').to route_to(
        'api/v1/publishers/payouts#show', publisher_id: 'me', format: :json
      )
    end
  end
end
