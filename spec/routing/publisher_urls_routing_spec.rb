require 'rails_helper'

RSpec.describe Api::V1::Publishers::UrlsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/v1/publishers/me/urls').to route_to(
        'api/v1/publishers/urls#index', publisher_id: 'me', format: :json
      )
    end

    it 'routes to #update' do
      expect(put: '/api/v1/publishers/me/urls/1').to route_to(
        'api/v1/publishers/urls#update', publisher_id: 'me', id: '1', format: :json
      )
    end
  end
end
