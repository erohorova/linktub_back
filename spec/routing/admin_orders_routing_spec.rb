require 'rails_helper'

RSpec.describe Admin::OrdersController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/admin/orders').to route_to(
        'admin/orders#index'
      )
    end

    it 'routes to #state' do
      expect(put: '/admin/orders/1/state').to route_to(
        'admin/orders#state', id: '1'
      )
    end
  end
end
