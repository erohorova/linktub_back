require 'rails_helper'

RSpec.describe Api::V1::Advertisers::OrdersController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/v1/advertisers/me/orders').to route_to(
        'api/v1/advertisers/orders#index', advertiser_id: 'me', format: :json
      )
    end

    it 'routes to #update' do
      expect(put: '/api/v1/advertisers/me/orders/1').to route_to(
        'api/v1/advertisers/orders#update', advertiser_id: 'me', id: '1', format: :json
      )
    end

    it 'routes to #destroy' do
      expect(delete: '/api/v1/advertisers/me/orders/1').to route_to(
        'api/v1/advertisers/orders#destroy', advertiser_id: 'me', id: '1', format: :json
      )
    end

    it 'routes to #destroy_list' do
      expect(delete: '/api/v1/advertisers/me/orders/destroy_list').to route_to(
        'api/v1/advertisers/orders#destroy_list', advertiser_id: 'me', format: :json
      )
    end

    it 'routes to #create' do
      expect(post: '/api/v1/advertisers/me/orders').to route_to(
        'api/v1/advertisers/orders#create', advertiser_id: 'me', format: :json
      )
    end

    it 'routes to #bulk_create' do
      expect(post: '/api/v1/advertisers/me/orders/bulk_create').to route_to(
        'api/v1/advertisers/orders#bulk_create', advertiser_id: 'me', format: :json
      )
    end
  end
end
