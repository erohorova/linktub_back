require 'rails_helper'

RSpec.describe Admin::ArticlesController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/admin/articles').to route_to(
        'admin/articles#index'
      )
    end

    it 'routes to #update' do
      expect(put: '/admin/articles/1').to route_to(
        'admin/articles#update', id: '1'
      )
    end

    it 'routes to #comment' do
      expect(post: '/admin/articles/1/comments').to route_to(
        'admin/articles#comment', id: '1'
      )
    end

    it 'routes to #state' do
      expect(put: '/admin/articles/1/state').to route_to(
        'admin/articles#state', id: '1'
      )
    end
  end
end
