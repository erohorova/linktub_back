require 'rails_helper'

RSpec.describe Api::V1::PaymentsController, type: :routing do
  describe 'routing' do
    it 'routes to #token' do
      expect(get: '/api/v1/advertisers/me/payments/token').to route_to('api/v1/payments#token', advertiser_id: 'me', format: :json)
    end
    it 'routes to #pay' do
      expect(post: '/api/v1/advertisers/me/payments/pay').to route_to('api/v1/payments#payment', advertiser_id: 'me', format: :json)
    end
  end
end
