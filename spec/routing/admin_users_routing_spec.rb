require 'rails_helper'

RSpec.describe Admin::UsersController, type: :routing do
  describe 'routing' do
    it 'routes to #update' do
      expect(put: '/admin/users/1').to route_to(
        'admin/users#update', id: '1'
      )
    end
  end
end
