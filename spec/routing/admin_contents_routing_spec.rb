require 'rails_helper'

RSpec.describe Admin::ContentsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/admin/contents').to route_to(
        'admin/contents#index'
      )
    end

    it 'routes to #new' do
      expect(get: '/admin/campaigns/new').to route_to(
        'admin/campaigns#new'
      )
    end

    it 'routes to #create' do
      expect(post: '/admin/campaigns/').to route_to(
        'admin/campaigns#create'
      )
    end

    it 'routes to #show' do
      expect(get: '/admin/campaigns/1').to route_to(
        'admin/campaigns#show', id: '1'
      )
    end

    it 'routes to #assign' do
      expect(post: '/admin/contents/1/assign').to route_to(
        'admin/contents#assign', id: '1'
      )
    end

    it 'routes to #filter' do
      expect(get: '/admin/contents/1/filter').to route_to(
        'admin/contents#filter', id: '1'
      )
    end

    it 'routes to #search_urls' do
      expect(get: '/admin/contents/search_urls').to route_to(
        'admin/contents#search_urls'
      )
    end

    it 'routes to #counter' do
      expect(get: '/admin/contents/counter').to route_to(
        'admin/contents#counter'
      )
    end

    it 'routes to #order' do
      expect(post: '/admin/contents/1/order').to route_to(
        'admin/contents#order', id: '1'
      )
    end
  end
end
