require 'rails_helper'

RSpec.describe Admin::UrlsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/admin/urls').to route_to(
        'admin/urls#index'
      )
    end

    it 'routes to #update' do
      expect(put: '/admin/urls/1').to route_to(
        'admin/urls#update', id: '1'
      )
    end

    it 'routes to #counter' do
      expect(get: '/admin/urls/counter').to route_to(
        'admin/urls#counter'
      )
    end
  end
end
