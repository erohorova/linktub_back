require 'rails_helper'

RSpec.describe Api::V1::CategoriesController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/v1/categories').to route_to('api/v1/categories#index', format: :json)
    end
  end
end
