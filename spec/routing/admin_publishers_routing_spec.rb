require 'rails_helper'

RSpec.describe Admin::PublishersController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/admin/publishers').to route_to(
        'admin/publishers#index'
      )
    end

    it 'routes to #update' do
      expect(put: '/admin/publishers/1').to route_to(
        'admin/publishers#update', id: '1'
      )
    end

    it 'routes to #show' do
      expect(get: '/admin/publishers/1').to route_to(
        'admin/publishers#show', id: '1'
      )
    end
  end
end
