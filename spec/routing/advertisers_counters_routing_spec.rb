require 'rails_helper'

RSpec.describe Api::V1::Advertisers::CountersController, type: :routing do
  it 'routes to #show' do
    expect(get: '/api/v1/advertisers/me/counters').to route_to(
      'api/v1/advertisers/counters#show', advertiser_id: 'me', format: :json
    )
  end
end
