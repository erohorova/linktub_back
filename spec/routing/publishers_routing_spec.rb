require 'rails_helper'

RSpec.describe Api::V1::PublishersController, type: :routing do
  describe 'routing' do
    it 'routes to #show' do
      expect(get: '/api/v1/publishers/me').to route_to('api/v1/publishers#show', id: 'me', format: :json)
    end

    it 'routes to #create' do
      expect(post: '/api/v1/publishers').to route_to('api/v1/publishers#create', format: :json)
    end
  end
end
