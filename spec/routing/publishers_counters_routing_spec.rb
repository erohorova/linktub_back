require 'rails_helper'

RSpec.describe Api::V1::Publishers::CountersController, type: :routing do
  it 'routes to #show' do
    expect(get: '/api/v1/publishers/me/counters').to route_to(
      'api/v1/publishers/counters#show', publisher_id: 'me', format: :json
    )
  end
end
