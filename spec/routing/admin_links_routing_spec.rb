require 'rails_helper'

RSpec.describe Admin::LinksController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/admin/links').to route_to(
        'admin/links#index'
      )
    end

    it 'routes to #state' do
      expect(put: '/admin/links/1/state').to route_to(
        'admin/links#state', id: '1'
      )
    end
  end
end
