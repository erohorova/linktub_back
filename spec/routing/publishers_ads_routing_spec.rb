require 'rails_helper'

RSpec.describe Api::V1::Publishers::AdsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/v1/publishers/me/ads').to route_to(
        'api/v1/publishers/ads#index', publisher_id: 'me', format: :json
      )
    end

    it 'routes to #approve' do
      expect(put: '/api/v1/publishers/me/ads/1/approve').to route_to(
        'api/v1/publishers/ads#approve', publisher_id: 'me', ad_id: '1', format: :json
      )
    end

    it 'routes to #reject' do
      expect(put: '/api/v1/publishers/me/ads/1/reject').to route_to(
        'api/v1/publishers/ads#reject', publisher_id: 'me', ad_id: '1', format: :json
      )
    end
  end
end
