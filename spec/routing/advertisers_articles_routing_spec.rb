require 'rails_helper'

RSpec.describe Api::V1::Advertisers::ArticlesController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/v1/advertisers/me/articles').to route_to(
        'api/v1/advertisers/articles#index', advertiser_id: 'me', format: :json
      )
    end

    it 'routes to #bulk_create' do
      expect(post: '/api/v1/advertisers/me/articles/bulk_create').to route_to(
        'api/v1/advertisers/articles#bulk_create', advertiser_id: 'me', format: :json
      )
    end

    it 'routes to #create_orders' do
      expect(post: '/api/v1/advertisers/me/articles/create_orders').to route_to(
        'api/v1/advertisers/articles#create_orders', advertiser_id: 'me', format: :json
      )
    end

    it 'routes to #update' do
      expect(put: '/api/v1/advertisers/me/articles/1').to route_to(
        'api/v1/advertisers/articles#update', advertiser_id: 'me', id: '1', format: :json
      )
    end
  end
end
