require 'rails_helper'

RSpec.describe Api::V1::NotificationsController, type: :routing do
  describe 'routing' do
    it 'routes to #destroy_type' do
      expect(delete: '/api/v1/advertisers/me/notifications/destroy_type').to route_to(
        'api/v1/notifications#destroy_type', advertiser_id: 'me', format: :json
      )
      expect(delete: '/api/v1/publishers/me/notifications/destroy_type').to route_to(
        'api/v1/notifications#destroy_type', publisher_id: 'me', format: :json
      )
    end
  end
end
