require 'rails_helper'

RSpec.describe Admin::AuthorInvoicesController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/admin/author_invoices').to route_to(
        'admin/author_invoices#index'
      )
    end

    it 'routes to #create' do
      expect(post: '/admin/author_invoices').to route_to(
        'admin/author_invoices#create'
      )
    end
  end
end
