require 'rails_helper'

RSpec.describe Api::V1::CreditCardsController, type: :routing do
  it 'routes to #index' do
    expect(get: '/api/v1/advertisers/me/credit_cards').to route_to('api/v1/credit_cards#index', advertiser_id: 'me', format: :json)
  end
end
