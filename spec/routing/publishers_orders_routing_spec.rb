require 'rails_helper'

RSpec.describe Api::V1::Publishers::OrdersController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/v1/publishers/me/orders').to route_to(
        'api/v1/publishers/orders#index', publisher_id: 'me', format: :json
      )
    end

    it 'routes to #update' do
      expect(put: '/api/v1/publishers/me/orders/1').to route_to(
        'api/v1/publishers/orders#update', publisher_id: 'me', id: '1', format: :json
      )
    end
  end
end
