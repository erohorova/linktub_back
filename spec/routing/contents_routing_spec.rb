require 'rails_helper'

RSpec.describe Api::V1::ContentsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/v1/advertisers/me/contents').to route_to('api/v1/contents#index', advertiser_id: 'me', format: :json)
    end

    it 'routes to #create' do
      expect(post: '/api/v1/advertisers/me/contents').to route_to('api/v1/contents#create', advertiser_id: 'me', format: :json)
    end

    it 'routes to #update' do
      expect(put: '/api/v1/advertisers/me/contents/1').to route_to('api/v1/contents#update', advertiser_id: 'me', id: '1', format: :json)
    end
  end
end
