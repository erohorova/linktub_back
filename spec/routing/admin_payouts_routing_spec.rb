require 'rails_helper'

RSpec.describe Admin::PayoutsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/admin/payouts').to route_to(
        'admin/payouts#index'
      )
    end

    it 'routes to #pay' do
      expect(put: '/admin/payouts/1/pay').to route_to(
        'admin/payouts#pay', id: '1'
      )
    end

    it 'routes to #cancel' do
      expect(delete: '/admin/payouts/1').to route_to(
        'admin/payouts#cancel', id: '1'
      )
    end
  end
end
