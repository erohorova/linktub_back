require 'rails_helper'

RSpec.describe Api::V1::UrlsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/v1/urls').to route_to('api/v1/urls#index', format: :json)
    end
  end
end
