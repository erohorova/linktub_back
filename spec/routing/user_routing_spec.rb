require 'rails_helper'

RSpec.describe Api::V1::UsersController, type: :routing do
  describe 'routing' do
    it 'routes to #update' do
      expect(put: '/api/v1/users/1').to route_to('api/v1/users#update', id: '1', format: :json)
    end

    it 'routes to #check_email' do
      expect(get: 'api/v1/users/check_email').to route_to('api/v1/users#check_email', format: :json)
    end
  end
end
