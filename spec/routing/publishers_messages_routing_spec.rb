require 'rails_helper'

RSpec.describe Api::V1::Publishers::MessagesController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/v1/publishers/me/messages').to route_to(
        'api/v1/publishers/messages#index', publisher_id: 'me', format: :json
      )
    end

    it 'routes to #show' do
      expect(get: '/api/v1/publishers/me/messages/1').to route_to(
        'api/v1/publishers/messages#show', publisher_id: 'me', id: '1', format: :json
      )
    end

    it 'routes to #create' do
      expect(post: '/api/v1/publishers/me/messages').to route_to(
        'api/v1/publishers/messages#create', publisher_id: 'me', format: :json
      )
    end

    it 'routes to #update via PUT' do
      expect(put: '/api/v1/publishers/me/messages/1').to route_to(
        'api/v1/publishers/messages#update', publisher_id: 'me', id: '1', format: :json
      )
    end

    it 'routes to #destroy' do
      expect(delete: '/api/v1/publishers/me/messages/1').to route_to(
        'api/v1/publishers/messages#destroy', publisher_id: 'me', id: '1', format: :json
      )
    end

    it 'routes to #read' do
      expect(put: '/api/v1/publishers/me/messages/1/read').to route_to(
        'api/v1/publishers/messages#read', publisher_id: 'me', message_id: '1', format: :json
      )
    end
  end
end
