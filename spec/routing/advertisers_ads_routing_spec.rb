require 'rails_helper'

RSpec.describe Api::V1::Advertisers::AdsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/v1/advertisers/me/ads').to route_to(
        'api/v1/advertisers/ads#index', advertiser_id: 'me', format: :json
      )
    end

    it 'routes to #update' do
      expect(put: '/api/v1/advertisers/me/ads/1').to route_to(
        'api/v1/advertisers/ads#update', advertiser_id: 'me', id: '1', format: :json
      )
    end

    it 'routes to #destroy' do
      expect(delete: '/api/v1/advertisers/me/ads/1').to route_to(
        'api/v1/advertisers/ads#destroy', advertiser_id: 'me', id: '1', format: :json
      )
    end
  end
end
