require 'rails_helper'

RSpec.describe Admin::SocialHandlesController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/admin/social_handles').to route_to(
        'admin/social_handles#index'
      )
    end

    it 'routes to #update' do
      expect(put: '/admin/social_handles/1').to route_to(
        'admin/social_handles#update', id: '1'
      )
    end

    it 'routes to #state' do
      expect(put: '/admin/social_handles/1/state').to route_to(
        'admin/social_handles#state', id: '1'
      )
    end

    it 'routes to #counter' do
      expect(get: '/admin/social_handles/counter').to route_to(
        'admin/social_handles#counter'
      )
    end
  end
end
