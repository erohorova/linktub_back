require 'rails_helper'

RSpec.describe Admin::AdsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/admin/ads').to route_to(
        'admin/ads#index'
      )
    end

    it 'routes to #state' do
      expect(put: '/admin/ads/1/state').to route_to(
        'admin/ads#state', id: '1'
      )
    end
  end
end
