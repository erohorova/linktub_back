require 'rails_helper'

RSpec.describe Api::V1::LinksController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/v1/advertisers/me/campaigns/1/links').to route_to(
        'api/v1/links#index', advertiser_id: 'me', campaign_id: '1', format: :json
      )
    end

    it 'routes to #orders_built' do
      expect(get: '/api/v1/advertisers/me/links/1/orders_built').to route_to(
        'api/v1/links#orders_built', advertiser_id: 'me', link_id: '1', format: :json
      )
    end

    it 'routes to #create' do
      expect(post: '/api/v1/advertisers/me/campaigns/1/links').to route_to(
        'api/v1/links#create', advertiser_id: 'me', campaign_id: '1', format: :json
      )
    end
  end
end
