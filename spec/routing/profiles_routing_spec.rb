require 'rails_helper'

RSpec.describe Api::V1::ProfilesController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/api/v1/publishers/me/profiles').to route_to('api/v1/profiles#index', publisher_id: 'me', format: :json)
    end

    it 'routes to #create' do
      expect(post: '/api/v1/publishers/me/profiles').to route_to('api/v1/profiles#create', publisher_id: 'me', format: :json)
    end

    it 'routes to #names' do
      expect(get: '/api/v1/publishers/me/profiles/names').to route_to('api/v1/profiles#names', publisher_id: 'me', format: :json)
    end

    it 'routes to #update' do
      expect(put: '/api/v1/publishers/me/profiles/1').to route_to('api/v1/profiles#update',
                                                                  publisher_id: 'me', id: '1', format: :json)
    end

    it 'routes to #update_spreadsheet' do
      expect(put: '/api/v1/publishers/me/profiles/1/update_spreadsheet').to route_to(
        'api/v1/profiles#update_spreadsheet', publisher_id: 'me', profile_id: '1', format: :json
      )
    end

    it 'routes to #meta_description' do
      expect(get: '/api/v1/publishers/me/profiles/meta_description').to route_to('api/v1/profiles#meta_description',
                                                                                 publisher_id: 'me', format: :json)
    end

    it 'routes to #ga_muv' do
      expect(get: '/api/v1/publishers/me/profiles/ga_muv').to route_to('api/v1/profiles#ga_muv',
                                                                       publisher_id: 'me', format: :json)
    end

    it 'routes to #domain_information' do
      expect(get: '/api/v1/publishers/me/profiles/domain_information').to route_to('api/v1/profiles#domain_information',
                                                                                   publisher_id: 'me', format: :json)
    end

    it 'routes to #facebook_followers' do
      expect(get: '/api/v1/publishers/me/profiles/facebook_followers').to route_to('api/v1/profiles#facebook_followers',
                                                                                   publisher_id: 'me', format: :json)
    end
    it 'routes to #twitter_followers' do
      expect(get: '/api/v1/publishers/me/profiles/twitter_followers').to route_to('api/v1/profiles#twitter_followers',
                                                                                  publisher_id: 'me', format: :json)
    end
    it 'routes to #instagram_followers' do
      expect(get: '/api/v1/publishers/me/profiles/instagram_followers').to route_to('api/v1/profiles#instagram_followers',
                                                                                    publisher_id: 'me', format: :json)
    end
    it 'routes to #pinterest_followers' do
      expect(get: '/api/v1/publishers/me/profiles/pinterest_followers').to route_to('api/v1/profiles#pinterest_followers',
                                                                                    publisher_id: 'me', format: :json)
    end
    it 'routes to #youtube_followers' do
      expect(get: '/api/v1/publishers/me/profiles/youtube_followers').to route_to('api/v1/profiles#youtube_followers',
                                                                                  publisher_id: 'me', format: :json)
    end

    it 'routes to #destroy' do
      expect(delete: '/api/v1/publishers/me/profiles/1').to route_to('api/v1/profiles#destroy',
                                                                     publisher_id: 'me', id: '1', format: :json)
    end
  end
end
