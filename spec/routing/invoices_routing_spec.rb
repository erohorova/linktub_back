require 'rails_helper'

RSpec.describe Api::V1::InvoicesController, type: :routing do
  it 'routes to #index' do
    expect(get: '/api/v1/advertisers/me/invoices').to route_to('api/v1/invoices#index', advertiser_id: 'me', format: :json)
  end
end
