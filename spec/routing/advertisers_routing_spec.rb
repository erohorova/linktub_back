require 'rails_helper'

RSpec.describe Api::V1::AdvertisersController, type: :routing do
  describe 'routing' do
    it 'routes to #show' do
      expect(get: '/api/v1/advertisers/me').to route_to('api/v1/advertisers#show', id: 'me', format: :json)
    end

    it 'routes to #create' do
      expect(post: '/api/v1/advertisers').to route_to('api/v1/advertisers#create', format: :json)
    end
  end
end
