require 'rails_helper'

RSpec.describe Admin::CampaignsController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/admin/campaigns').to route_to(
        'admin/campaigns#index'
      )
    end

    it 'routes to #update' do
      expect(put: '/admin/campaigns/1').to route_to(
        'admin/campaigns#update', id: '1'
      )
    end

    it 'routes to #state' do
      expect(put: '/admin/campaigns/1/state').to route_to(
        'admin/campaigns#state', id: '1'
      )
    end
  end
end
