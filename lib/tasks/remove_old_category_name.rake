namespace :remove_old_category_name do
  desc 'remove old categories from existing profiles'
  task remove_category_names: :environment do
    removing_categories_name = ['BABY', 'BIG DATA', 'BLOG TOPICS ONLY', 'CLOTHING', 'ECONOMY',
                           'ENTREPRENEUR', 'FAMILY', 'POLITICS', 'SELECT', 'TECHNOLOGY',
                           'TELEVISION', 'VIDEO GAMES']
    removing_categories = Category.where(name: removing_categories_name)
    removing_categories.each do |category|
      category.destroy
    end
  end
end