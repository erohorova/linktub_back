namespace :articles do
  desc 'Send emails to authors to check ETA'
  task emails: :environment do
    Article.assigned_past_eta.find_each do |article|
      hours = hours_between(Time.zone.parse(article.eta.to_s), Time.zone.now)
      send_overdue_email(article) if hours > 24 && hours < 48
    end
  end

  def send_overdue_email(article)
    Email.schedule('author_article_past_eta', article.author_id, email_vars(article))
  end

  def hours_between(start_date, end_date)
    return 0 if start_date.nil? || end_date.nil?
    ('%.2f' % ((start_date - end_date).abs / 1.hour)).to_f
  end

  def email_vars(article)
    email_vars = article.attributes.symbolize_keys
    email_vars[:advertiser_first_name] = article.advertiser.try(:first_name)
    email_vars[:advertiser_last_name] = article.advertiser.try(:last_name)
    email_vars[:author_first_name] = article.author.try(:first_name)
    email_vars[:author_last_name] = article.author.try(:last_name)
    email_vars
  end
end
