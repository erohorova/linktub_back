namespace :update_categories do
  desc 'Update category for existing profiles'
  task update_new_categories: :environment do
    computer_categorizings = Categorizing.joins(:category)
      .where('categories.name = ? OR categories.name = ?', 'COMPUTERS', 'WORLD WIDE WEB')
    computer_categorizings.each do |categorizing|
      categorizing.update(category_id: Category.find_by(name: 'TECH').id)
    end

    people_categorizings = Categorizing.joins(:category).where('categories.name = ?', 'PEOPLE')
    people_categorizings.each do |categorizing|
      categorizing.update(category_id: Category.find_by(name: 'ENTERTAINMENT').id)
    end

    tickets_categorizings = Categorizing.joins(:category).where('categories.name = ?', 'TICKETS')
    tickets_categorizings.each do |categorizing|
      categorizing.update(category_id: Category.find_by(name: 'TELEVISION').id)
    end

    writing_categorizings = Categorizing.joins(:category).where('categories.name = ?', 'WRITING')
    writing_categorizings.each do |categorizing|
      categorizing.update(category_id: Category.find_by(name: 'BOOKS').id)
    end
  end
end
