namespace :remove_old_categories do
  desc 'remove old categories from existing profiles'
  task remove_categories: :environment do
    removing_categories = %w[BABY BIG\ DATA BLOG\ TOPICS\ ONLY CLOTHING ECONOMY
                           ENTREPRENEUR FAMILY POLITICS SELECT TECHNOLOGY
                           TELEVISION VIDEO\ GAMES]
    category_ids = Category.where(name: removing_categories).pluck(:id)
    remove_categorizings = Categorizing.where(category_id: category_ids)
    remove_categorizings.each do |categorizing|
      categorizing.destroy
    end
  end
end