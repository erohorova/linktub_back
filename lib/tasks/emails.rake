namespace :emails do
  desc "Lookup and schedule the emails to be sent today."
  task schedule: :environment do
    Email.find_for_delivery
  end

  desc "Lookup and deliver the emails schedule for the past X hours."
  task :schedule_hourly, [:hours] => :environment do |_task, args|
    Email.find_for_delivery_for_past_hours(args[:hours].to_i)
  end

  desc "Lookup and remove all emails already sent."
  task clear_emails: :environment do
    EmailAction.already_sent.destroy_all
  end

  desc "Remind advertisers to submit links"
  task daily_schedule: :environment do
    Advertiser.without_content.find_each do |adv|
      email_vars[:advertiser_first_name] = adv.try(:first_name)
      email_vars[:advertiser_last_name] = adv.try(:last_name)
      Email.schedule('advertiser_campaign_submission_reminder', adv.try(:user_id), email_vars)
    end
  end

  desc 'Remind publishers to change their payout'
  task change_payout: :environment do
    Publisher.send_payout_reminders
  end
end
