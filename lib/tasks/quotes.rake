namespace :quotes do
  desc "Lookup and archive quotes with 60 days or more."
  task archive: :environment do
    Content.active.where('created_at <= ?', 60.days.ago).destroy_all
  end
end
