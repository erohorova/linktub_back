namespace :remove_categories do
  desc 'Update category for existing profiles'
  task remove_old_categories: :environment do
    delete_category = %w[COMPUTERS PEOPLE TICKETS WORLD\ WIDE\ WEB WRITING]
    Category.where(name: delete_category).destroy_all
  end
end