namespace :url_statistics do
  desc "Update Ahrefs DR, LDR and SemRush index"
  task get_url_statistics: :environment do
    urls = Url.where('state > ?', 1)
    AhrefService.new.get_dr_lrd(urls)
  end
end
