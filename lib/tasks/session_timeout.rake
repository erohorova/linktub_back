namespace :session do
  desc "Timeout adv or pub sessions longer than two hours."
  task timeout: :environment do
    User.timedout.find_each(&:invalidate_token)
  end
end
