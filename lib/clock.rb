require File.expand_path('../../config/boot',        __FILE__)
require File.expand_path('../../config/environment', __FILE__)
require 'clockwork'

include Clockwork

module Clockwork
  handler do |job|
    puts "Running #{job}"
  end

  # Constants
  HOURS_BETWEEN_EMAILS = 1.freeze

  # Schedule tasks
  every(HOURS_BETWEEN_EMAILS.hour, 'Send emails for next hour') { `rake emails:schedule_hourly[#{HOURS_BETWEEN_EMAILS}]` }
  every(1.day, 'Send single emails scheduled for the day') { `rake emails:schedule` }
  every(1.day, 'Archive quotes with more than 60 days', at: '00:00') { `rake quotes:archive` }
  every(1.day, 'Send emails for articles (set eta, past eta)') { `rake articles:emails` }
  every(1.hour, 'Timeout sessions longer than 2 hours') { `rake session:timeout` }
  every(1.day, 'Remind publishers to change payout') { `rake emails:change_payout` }
  every(1.month, 'Update statistics for urls') {`rake url_statistics:get_url_statistics`}
end
