module SpreadSheetManager
  module ActsAsSpreadSheetManager
    extend ActiveSupport::Concern

    # This method should return models_attributes[] or nil
    def parse_params_from_file(file, headers)
      return if file.blank?
      spreadsheet = Roo::Spreadsheet.open(file)
      sheet = spreadsheet.sheet(0)
      return if sheet.last_column != headers.length
      sanitize_params((2..spreadsheet.last_row).map { |r| Hash[headers.zip(sheet.row(r))] })
    end

    private

    def sanitize_params(params_array)
      params_array.map { |par| par.map { |k, v| { k => cast_booleans(v) } }.reduce(:merge) }
    end

    def cast_booleans(value)
      value.to_s.is_boolean? ? value.booleanize : value
    end

    module ClassMethods
      def acts_as_spread_sheet_manager(options = {})
      end
    end
  end
end

ActiveRecord::Base.send :include, SpreadSheetManager::ActsAsSpreadSheetManager
