class String
  def valid_url?
    uri = URI.parse self
    uri.is_a? URI::HTTP
  rescue URI::InvalidURIError
    false
  end

  def is_boolean?
    (self.to_s.strip =~ /^(true|yes|y|1)$/i || self.to_s.strip =~ /^(false|no|n|0)$/i) == 0
  end

  def booleanize
    return false unless self.is_boolean?
    return true if self.to_s.strip =~ /^(true|yes|y|1)$/i
    false
  end
end
