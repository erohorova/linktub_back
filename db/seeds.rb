#############################
### GENERATING CATEGORIES ###
#############################

CATEOGORIES = ['SEO', 'BIG DATA', 'TECH', 'ECONOMY', 'TRAVEL', 'EDUCATION',
               'BUSINESS', 'ENTREPRENEUR', 'HEALTH', 'LAW', 'FAMILY', 'SPORTS', 'FASHION',
               'CLOTHING', 'SELECT', 'ART', 'AUTOS', 'BABY', 'BLOG TOPICS ONLY', 'BOOKS',
               'COMPUTERS', 'DATING', 'ELECTRONICS', 'ENTERTAINMENT', 'ENVIRONMENT',
               'FINANCE / LOANS', 'FOOD', 'GOVERNMENT', 'HOLIDAY', 'HOUSE AND HOME',
               'JOBS', 'MONEY', 'MUSIC', 'OUTDOORS', 'PARENTING', 'PEOPLE', 'PETS',
               'POLITICS', 'REAL ESTATE', 'RELIGION', 'SCIENCE', 'SHOPPING', 'TECHNOLOGY',
               'TELEVISION', 'TICKETS', 'VIDEO GAMES', 'WEDDING', 'WORLD WIDE WEB'].freeze

CATEOGORIES.find_all { |name| FactoryGirl.create :category, name: name }

################################
### GENERATING PRIMARY USERS ###
################################
user1 = FactoryGirl.create(:user, email: 'tony@mail.com', first_name: 'Tony', last_name: 'Newton')
user2 = FactoryGirl.create(:user, email: 'adv2@mail.com')
user3 = FactoryGirl.create(:user, email: 'pub2@mail.com')

FactoryGirl.create(:advertiser, user: user1)
FactoryGirl.create(:publisher, user: user1)
FactoryGirl.create(:advertiser, user: user2)
FactoryGirl.create(:publisher, user: user3)

FactoryGirl.create(:admin_user, email: 'admin1@mail.com', password: 'password123')

5.times { |n| FactoryGirl.create(:author, email: "author#{n + 1}@mail.com", password: 'password123') }

##########################################
### GENERATING ADVERTISERS' BASIC INFO ###
##########################################

NUMBER_LINKS = 2 + rand(10)
NUMBER_CAMPAIGNS = 1 + rand(10)
NUMBER_CATEGORIES = 1 + rand(5)

# 1) Creates 20 advertisers
advertisers = FactoryGirl.create_list(:advertiser, 20)

# 2) For each advertiser: create NUMBER_CAMPAIGNS, for each campaign do: add NUMBER_CATEGORIES and NUMBER_LINKS
advertisers.find_all do |adv|
  campaigns = FactoryGirl.create_list(:campaign, NUMBER_CAMPAIGNS, advertiser: adv)

  campaigns.each do |campaign|
    campaign.categories = Category.all.sample(NUMBER_CATEGORIES)
    FactoryGirl.create_list(:link, NUMBER_LINKS, campaign: campaign)
  end
end

#########################################
### GENERATING PUBLISHERS' BASIC INFO ###
#########################################

NUMBER_URLS = 2 + rand(10)
NUMBER_PROFILES = 1 + rand(10)

# 1) Creates 20 publishers
publishers = FactoryGirl.create_list(:publisher, 20)

# 2) For each publisher: create NUMBER_PROFILES, for each profile do: add NUMBER_CATEGORIES and NUMBER_URLS
publishers.find_all do |pub|
  profiles = FactoryGirl.create_list(:profile, NUMBER_PROFILES, publisher: pub)

  profiles.each do |profile|
    profile.categories = Category.all.sample(NUMBER_CATEGORIES)
    NUMBER_URLS.times { FactoryGirl.create(:url, profile: profile) }
  end
end
