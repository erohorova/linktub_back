--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5
-- Dumped by pg_dump version 11.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: social_handles_after_delete_row_tr(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.social_handles_after_delete_row_tr() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    UPDATE profiles SET total_sites = total_sites - 1 WHERE id = OLD.profile_id;
    RETURN NULL;
END;
$$;


--
-- Name: social_handles_after_insert_row_tr(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.social_handles_after_insert_row_tr() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    UPDATE profiles SET total_sites = total_sites + 1 WHERE id = NEW.profile_id;
    RETURN NULL;
END;
$$;


--
-- Name: social_handles_after_update_row_tr(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.social_handles_after_update_row_tr() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF NEW.profile_id <> OLD.profile_id THEN
      UPDATE profiles SET total_sites = total_sites - 1 WHERE id = OLD.profile_id;
      UPDATE profiles SET total_sites = total_sites + 1 WHERE id = NEW.profile_id;
    END IF;
    RETURN NULL;
END;
$$;


--
-- Name: urls_after_delete_row_tr(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.urls_after_delete_row_tr() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    UPDATE profiles SET total_sites = total_sites - 1 WHERE id = OLD.profile_id;
    RETURN NULL;
END;
$$;


--
-- Name: urls_after_insert_row_tr(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.urls_after_insert_row_tr() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    UPDATE profiles SET total_sites = total_sites + 1 WHERE id = NEW.profile_id;
    RETURN NULL;
END;
$$;


--
-- Name: urls_after_update_row_tr(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.urls_after_update_row_tr() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF NEW.profile_id <> OLD.profile_id THEN
      UPDATE profiles SET total_sites = total_sites - 1 WHERE id = OLD.profile_id;
      UPDATE profiles SET total_sites = total_sites + 1 WHERE id = NEW.profile_id;
    END IF;
    RETURN NULL;
END;
$$;


--
-- Name: web_contents_after_delete_row_tr(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.web_contents_after_delete_row_tr() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    UPDATE profiles SET total_sites = total_sites - 1 WHERE id = OLD.profile_id;
    RETURN NULL;
END;
$$;


--
-- Name: web_contents_after_insert_row_tr(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.web_contents_after_insert_row_tr() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    UPDATE profiles SET total_sites = total_sites + 1 WHERE id = NEW.profile_id;
    RETURN NULL;
END;
$$;


--
-- Name: web_contents_after_update_row_tr(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.web_contents_after_update_row_tr() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF NEW.profile_id <> OLD.profile_id THEN
      UPDATE profiles SET total_sites = total_sites - 1 WHERE id = OLD.profile_id;
      UPDATE profiles SET total_sites = total_sites + 1 WHERE id = NEW.profile_id;
    END IF;
    RETURN NULL;
END;
$$;


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: active_admin_comments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.active_admin_comments (
    id integer NOT NULL,
    namespace character varying,
    body text,
    resource_id character varying NOT NULL,
    resource_type character varying NOT NULL,
    author_id integer,
    author_type character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: active_admin_comments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.active_admin_comments_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: active_admin_comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.active_admin_comments_id_seq OWNED BY public.active_admin_comments.id;


--
-- Name: admin_users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.admin_users (
    id integer NOT NULL,
    email character varying DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip inet,
    last_sign_in_ip inet,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    type character varying,
    first_name character varying,
    last_name character varying,
    phone character varying,
    state integer,
    terminated_date date,
    activated_date date,
    paypal_email character varying,
    confirmation_token character varying,
    confirmed_at timestamp without time zone,
    confirmation_sent_at timestamp without time zone,
    skrill_email character varying
);


--
-- Name: admin_users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.admin_users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: admin_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.admin_users_id_seq OWNED BY public.admin_users.id;


--
-- Name: ads; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.ads (
    id integer NOT NULL,
    link_text character varying,
    requested_date timestamp without time zone,
    approved_date timestamp without time zone,
    rejected_date timestamp without time zone,
    reconsidered_date timestamp without time zone,
    cancelled_date timestamp without time zone,
    admin_cancelled boolean DEFAULT false,
    advertiser_cancelled boolean DEFAULT false,
    publisher_cancelled boolean DEFAULT false,
    url_price numeric DEFAULT 0,
    blog_post character varying DEFAULT 'blog_post'::character varying,
    blog_post_title character varying,
    blog_post_body character varying,
    quote_id character varying,
    full_pub_url character varying,
    attach_word_document boolean DEFAULT true,
    blog_post_doc character varying,
    blog_post_image_1 character varying,
    blog_post_image_2 character varying,
    state integer,
    rejected_reason character varying,
    notes text,
    editable boolean,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    advertiser_id integer,
    link_id integer,
    url_id integer,
    campaign_id integer,
    rejected_counter integer DEFAULT 0,
    ordered_date date,
    seen boolean DEFAULT false
);


--
-- Name: ads_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.ads_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ads_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.ads_id_seq OWNED BY public.ads.id;


--
-- Name: advertisers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.advertisers (
    id integer NOT NULL,
    billable_date date,
    suggested_link_email_sent_at timestamp without time zone,
    any_approved boolean DEFAULT false,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    awaiting_publication_spreadsheet_file_name character varying,
    awaiting_publication_spreadsheet_content_type character varying,
    awaiting_publication_spreadsheet_file_size integer,
    awaiting_publication_spreadsheet_updated_at timestamp without time zone,
    rejected_content_spreadsheet_file_name character varying,
    rejected_content_spreadsheet_content_type character varying,
    rejected_content_spreadsheet_file_size integer,
    rejected_content_spreadsheet_updated_at timestamp without time zone,
    active_content_spreadsheet_file_name character varying,
    active_content_spreadsheet_content_type character varying,
    active_content_spreadsheet_file_size integer,
    active_content_spreadsheet_updated_at timestamp without time zone,
    user_id integer,
    email character varying,
    content_pipe_zip_file_name character varying,
    content_pipe_zip_content_type character varying,
    content_pipe_zip_file_size integer,
    content_pipe_zip_updated_at timestamp without time zone,
    activated_date timestamp without time zone,
    terminated_date timestamp without time zone,
    state integer,
    auto_approved boolean DEFAULT false,
    advertisers_report_spreadsheet_file_name character varying,
    advertisers_report_spreadsheet_content_type character varying,
    advertisers_report_spreadsheet_file_size integer,
    advertisers_report_spreadsheet_updated_at timestamp without time zone,
    agent_id integer
);


--
-- Name: advertisers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.advertisers_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: advertisers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.advertisers_id_seq OWNED BY public.advertisers.id;


--
-- Name: articles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.articles (
    id integer NOT NULL,
    url_id integer,
    link_id integer,
    ad_id integer,
    advertiser_id integer,
    state integer,
    pending_date timestamp without time zone,
    approved_date timestamp without time zone,
    rejected_date timestamp without time zone,
    cancelled_date timestamp without time zone,
    eta date,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    author_id integer,
    assigned_date date,
    rewritten_counter integer DEFAULT 0,
    removed_from_cart boolean DEFAULT false,
    author_invoice_id integer,
    last_action_applied character varying
);


--
-- Name: articles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.articles_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: articles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.articles_id_seq OWNED BY public.articles.id;


--
-- Name: author_invoices; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.author_invoices (
    id integer NOT NULL,
    approved_articles_count integer,
    admin_user_id integer,
    paid boolean DEFAULT false,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    invoice_spreadsheet_file_name character varying,
    invoice_spreadsheet_content_type character varying,
    invoice_spreadsheet_file_size integer,
    invoice_spreadsheet_updated_at timestamp without time zone
);


--
-- Name: author_invoices_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.author_invoices_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: author_invoices_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.author_invoices_id_seq OWNED BY public.author_invoices.id;


--
-- Name: campaigns; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.campaigns (
    id integer NOT NULL,
    name character varying,
    image character varying,
    state integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    advertiser_id integer,
    campaign_report_spreadsheet_file_name character varying,
    campaign_report_spreadsheet_content_type character varying,
    campaign_report_spreadsheet_file_size integer,
    campaign_report_spreadsheet_updated_at timestamp without time zone
);


--
-- Name: campaigns_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.campaigns_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: campaigns_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.campaigns_id_seq OWNED BY public.campaigns.id;


--
-- Name: categories; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.categories (
    id integer NOT NULL,
    name character varying NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    keyword character varying
);


--
-- Name: categories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.categories_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.categories_id_seq OWNED BY public.categories.id;


--
-- Name: categorizings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.categorizings (
    id integer NOT NULL,
    category_id integer,
    categorizable_id integer,
    categorizable_type character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: categorizings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.categorizings_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: categorizings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.categorizings_id_seq OWNED BY public.categorizings.id;


--
-- Name: comments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.comments (
    id integer NOT NULL,
    title character varying(50) DEFAULT ''::character varying,
    comment text,
    commentable_id integer,
    commentable_type character varying,
    user_id integer,
    user_type character varying,
    role character varying DEFAULT 'comments'::character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: comments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.comments_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.comments_id_seq OWNED BY public.comments.id;


--
-- Name: configs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.configs (
    id integer NOT NULL,
    category character varying(191),
    name character varying(191),
    data_type character varying,
    value text,
    configable_type character varying(191),
    configable_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: configs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.configs_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: configs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.configs_id_seq OWNED BY public.configs.id;


--
-- Name: content_links; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.content_links (
    id integer NOT NULL,
    content_id integer,
    link_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: content_links_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.content_links_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: content_links_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.content_links_id_seq OWNED BY public.content_links.id;


--
-- Name: content_urls; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.content_urls (
    id integer NOT NULL,
    content_id integer,
    url_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    shown_price double precision DEFAULT 0.0
);


--
-- Name: content_urls_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.content_urls_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: content_urls_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.content_urls_id_seq OWNED BY public.content_urls.id;


--
-- Name: contents; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.contents (
    id integer NOT NULL,
    advertiser_id integer,
    campaign_id integer,
    state integer NOT NULL,
    domain_authority_min integer,
    domain_authority_max integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    urls_spreadsheet_file_name character varying,
    urls_spreadsheet_content_type character varying,
    urls_spreadsheet_file_size integer,
    urls_spreadsheet_updated_at timestamp without time zone,
    trust_flow_min integer,
    trust_flow_max integer,
    agency_content boolean DEFAULT false,
    created_by character varying,
    deleted_at timestamp without time zone,
    ahrefs_dr_min integer,
    ahrefs_dr_max integer
);


--
-- Name: contents_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.contents_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: contents_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.contents_id_seq OWNED BY public.contents.id;


--
-- Name: credit_cards; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.credit_cards (
    id integer NOT NULL,
    masked_number character varying,
    braintree_token character varying,
    cardholder_name character varying,
    card_type character varying,
    expiration_month integer,
    expiration_year integer,
    image_url character varying,
    street_address character varying,
    extended_address character varying,
    city character varying,
    state character varying,
    zip_code character varying,
    country_name character varying,
    user_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: credit_cards_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.credit_cards_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: credit_cards_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.credit_cards_id_seq OWNED BY public.credit_cards.id;


--
-- Name: delayed_jobs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.delayed_jobs (
    id integer NOT NULL,
    priority integer DEFAULT 0 NOT NULL,
    attempts integer DEFAULT 0 NOT NULL,
    handler text NOT NULL,
    last_error text,
    run_at timestamp without time zone,
    locked_at timestamp without time zone,
    failed_at timestamp without time zone,
    locked_by character varying,
    queue character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: delayed_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.delayed_jobs_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: delayed_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.delayed_jobs_id_seq OWNED BY public.delayed_jobs.id;


--
-- Name: email_actions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.email_actions (
    id integer NOT NULL,
    email_trigger_id integer,
    email_template_id integer,
    send_at timestamp without time zone NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    sent boolean DEFAULT false,
    "collate" boolean DEFAULT false,
    receiver_id integer,
    receiver_type character varying
);


--
-- Name: email_actions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.email_actions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: email_actions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.email_actions_id_seq OWNED BY public.email_actions.id;


--
-- Name: email_templates; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.email_templates (
    id integer NOT NULL,
    email_id integer,
    template text DEFAULT ''::text,
    days_delayed integer DEFAULT 0,
    active boolean DEFAULT true,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    subject character varying,
    "collate" boolean DEFAULT false
);


--
-- Name: email_templates_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.email_templates_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: email_templates_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.email_templates_id_seq OWNED BY public.email_templates.id;


--
-- Name: email_triggers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.email_triggers (
    id integer NOT NULL,
    email_id integer,
    receiver_id integer,
    receiver_type character varying,
    vars text,
    response_completed boolean DEFAULT false,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: email_triggers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.email_triggers_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: email_triggers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.email_triggers_id_seq OWNED BY public.email_triggers.id;


--
-- Name: emails; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.emails (
    id integer NOT NULL,
    name character varying,
    receiver_type character varying,
    response_required boolean DEFAULT false,
    redirect_url character varying,
    active boolean DEFAULT true,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    slug character varying,
    notes text
);


--
-- Name: emails_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.emails_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: emails_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.emails_id_seq OWNED BY public.emails.id;


--
-- Name: invoices; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.invoices (
    id integer NOT NULL,
    orders_count integer DEFAULT 0,
    orders_accepted_count integer DEFAULT 0,
    total_charged double precision DEFAULT 0.0,
    advertiser_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    accepted_orders_report_spreadsheet_file_name character varying,
    accepted_orders_report_spreadsheet_content_type character varying,
    accepted_orders_report_spreadsheet_file_size integer,
    accepted_orders_report_spreadsheet_updated_at timestamp without time zone
);


--
-- Name: invoices_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.invoices_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: invoices_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.invoices_id_seq OWNED BY public.invoices.id;


--
-- Name: links; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.links (
    id integer NOT NULL,
    href character varying,
    approved_date date,
    suspended_date date,
    flagged_date date,
    archived_date date,
    reconsidered_date date,
    declined_date date,
    page_rank integer DEFAULT 0,
    alexa_rank integer DEFAULT 0,
    back_links integer DEFAULT 0,
    domain_age double precision DEFAULT 0.0,
    state integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    campaign_id integer
);


--
-- Name: links_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.links_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: links_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.links_id_seq OWNED BY public.links.id;


--
-- Name: messages; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.messages (
    id integer NOT NULL,
    subject character varying,
    question_message text,
    reply_message text,
    author character varying,
    replied_at timestamp without time zone,
    read_at timestamp without time zone,
    state integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    user_id integer
);


--
-- Name: messages_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.messages_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: messages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.messages_id_seq OWNED BY public.messages.id;


--
-- Name: notifications; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.notifications (
    id integer NOT NULL,
    description text,
    user_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    notification_type integer
);


--
-- Name: notifications_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.notifications_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: notifications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.notifications_id_seq OWNED BY public.notifications.id;


--
-- Name: orders; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.orders (
    id integer NOT NULL,
    advertiser_id integer,
    url_id integer,
    ad_id integer,
    completed_date timestamp without time zone,
    state integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    invoice_id integer,
    created_from character varying,
    transaction_id integer,
    completed_by character varying,
    searched_campaign_id integer
);


--
-- Name: orders_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.orders_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: orders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.orders_id_seq OWNED BY public.orders.id;


--
-- Name: payouts; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.payouts (
    id integer NOT NULL,
    ad_id integer,
    publisher_id integer,
    amount character varying,
    currency character varying DEFAULT 'USD'::character varying,
    paypal_email character varying,
    paypal_batch_id character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: payouts_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.payouts_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: payouts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.payouts_id_seq OWNED BY public.payouts.id;


--
-- Name: profiles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.profiles (
    id integer NOT NULL,
    name character varying,
    image character varying,
    profile_type character varying,
    total_earned double precision DEFAULT 0.0,
    total_orders integer DEFAULT 0,
    total_sites integer DEFAULT 0,
    products text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    publisher_id integer,
    content_type character varying DEFAULT 'collaborate'::character varying NOT NULL
);


--
-- Name: profiles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.profiles_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: profiles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.profiles_id_seq OWNED BY public.profiles.id;


--
-- Name: publishers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.publishers (
    id integer NOT NULL,
    user_id integer,
    avatar character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    email character varying,
    activated_date timestamp without time zone,
    terminated_date timestamp without time zone,
    state integer,
    active_content_spreadsheet_file_name character varying,
    active_content_spreadsheet_content_type character varying,
    active_content_spreadsheet_file_size integer,
    active_content_spreadsheet_updated_at timestamp without time zone,
    publishers_report_spreadsheet_file_name character varying,
    publishers_report_spreadsheet_content_type character varying,
    publishers_report_spreadsheet_file_size integer,
    publishers_report_spreadsheet_updated_at timestamp without time zone
);


--
-- Name: publishers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.publishers_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: publishers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.publishers_id_seq OWNED BY public.publishers.id;


--
-- Name: reports; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.reports (
    id integer NOT NULL,
    name character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: reports_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.reports_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: reports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.reports_id_seq OWNED BY public.reports.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    version character varying NOT NULL
);


--
-- Name: social_handles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.social_handles (
    id integer NOT NULL,
    url character varying,
    price double precision,
    network character varying,
    type character varying,
    state integer,
    profile_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    suggested_price double precision DEFAULT 0.0,
    shown_price double precision DEFAULT 0.0
);


--
-- Name: social_handles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.social_handles_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: social_handles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.social_handles_id_seq OWNED BY public.social_handles.id;


--
-- Name: transactions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.transactions (
    id integer NOT NULL,
    braintree_transaction_id character varying,
    payer_id integer,
    payer_type character varying,
    receiver_id integer,
    receiver_type character varying,
    description text,
    amount character varying,
    state integer,
    voided_at timestamp without time zone,
    declined_at timestamp without time zone,
    rejected_at timestamp without time zone,
    authorized_at timestamp without time zone,
    submitted_for_settlement_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    is_refund boolean DEFAULT false,
    amount_submitted character varying
);


--
-- Name: transactions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.transactions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: transactions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.transactions_id_seq OWNED BY public.transactions.id;


--
-- Name: urls; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.urls (
    id integer NOT NULL,
    href character varying,
    description text,
    price double precision DEFAULT 0.0,
    follow_link boolean DEFAULT false,
    author_page character varying,
    state integer,
    approved_date date,
    declined_date date,
    reconsider_date date,
    archived_date date,
    activated_date date,
    paused_date date,
    domain_age double precision DEFAULT 0.0,
    domain_authority integer DEFAULT 0,
    page_authority integer DEFAULT 0,
    page_rank integer DEFAULT 0,
    alexa_rank integer DEFAULT 0,
    back_links integer DEFAULT 0,
    trust_flow integer DEFAULT 0,
    citation_flow integer DEFAULT 0,
    profile_categories_names text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    profile_id integer,
    search_categories_vector tsvector,
    search_description_vector tsvector,
    allow_home_page boolean DEFAULT false,
    allow_blog_post boolean DEFAULT false,
    suggested_price double precision DEFAULT 0.0,
    shown_price double precision DEFAULT 0.0,
    required_length character varying DEFAULT '750-1000'::character varying NOT NULL,
    profile_categories_keyword character varying,
    search_categories_keyword_vector tsvector,
    ahrefs_dr integer,
    ahrefs_lrd integer,
    semrush_traffic integer
);


--
-- Name: urls_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.urls_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: urls_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.urls_id_seq OWNED BY public.urls.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users (
    id integer NOT NULL,
    email character varying DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip inet,
    last_sign_in_ip inet,
    authentication_token character varying DEFAULT ''::character varying,
    braintree_customer_id character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    is_active boolean DEFAULT true,
    terminate_reason text,
    first_name character varying NOT NULL,
    last_name character varying NOT NULL,
    address_line_1 character varying,
    address_line_2 character varying,
    company character varying,
    country character varying,
    city character varying,
    zip_code character varying,
    phone character varying,
    current_balance double precision DEFAULT 0.0,
    payout_min integer,
    subscribe_to_news_letter boolean DEFAULT true,
    how_know_about_us character varying DEFAULT ''::character varying,
    paypal_email character varying,
    state character varying,
    confirmation_token character varying,
    confirmed_at timestamp without time zone,
    confirmation_sent_at timestamp without time zone,
    payment_method character varying(191),
    skrill_email character varying
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: web_contents; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.web_contents (
    id integer NOT NULL,
    href character varying,
    price double precision,
    state integer,
    profile_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    approved_date date,
    declined_date date,
    reconsider_date date,
    archived_date date,
    activated_date date,
    paused_date date,
    suggested_price double precision DEFAULT 0.0,
    shown_price double precision DEFAULT 0.0,
    ga_account_id character varying,
    ga_profile_id character varying,
    website_url character varying,
    muv integer
);


--
-- Name: web_contents_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.web_contents_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: web_contents_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.web_contents_id_seq OWNED BY public.web_contents.id;


--
-- Name: active_admin_comments id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.active_admin_comments ALTER COLUMN id SET DEFAULT nextval('public.active_admin_comments_id_seq'::regclass);


--
-- Name: admin_users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.admin_users ALTER COLUMN id SET DEFAULT nextval('public.admin_users_id_seq'::regclass);


--
-- Name: ads id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ads ALTER COLUMN id SET DEFAULT nextval('public.ads_id_seq'::regclass);


--
-- Name: advertisers id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.advertisers ALTER COLUMN id SET DEFAULT nextval('public.advertisers_id_seq'::regclass);


--
-- Name: articles id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.articles ALTER COLUMN id SET DEFAULT nextval('public.articles_id_seq'::regclass);


--
-- Name: author_invoices id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.author_invoices ALTER COLUMN id SET DEFAULT nextval('public.author_invoices_id_seq'::regclass);


--
-- Name: campaigns id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.campaigns ALTER COLUMN id SET DEFAULT nextval('public.campaigns_id_seq'::regclass);


--
-- Name: categories id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.categories ALTER COLUMN id SET DEFAULT nextval('public.categories_id_seq'::regclass);


--
-- Name: categorizings id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.categorizings ALTER COLUMN id SET DEFAULT nextval('public.categorizings_id_seq'::regclass);


--
-- Name: comments id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.comments ALTER COLUMN id SET DEFAULT nextval('public.comments_id_seq'::regclass);


--
-- Name: configs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.configs ALTER COLUMN id SET DEFAULT nextval('public.configs_id_seq'::regclass);


--
-- Name: content_links id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.content_links ALTER COLUMN id SET DEFAULT nextval('public.content_links_id_seq'::regclass);


--
-- Name: content_urls id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.content_urls ALTER COLUMN id SET DEFAULT nextval('public.content_urls_id_seq'::regclass);


--
-- Name: contents id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contents ALTER COLUMN id SET DEFAULT nextval('public.contents_id_seq'::regclass);


--
-- Name: credit_cards id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.credit_cards ALTER COLUMN id SET DEFAULT nextval('public.credit_cards_id_seq'::regclass);


--
-- Name: delayed_jobs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.delayed_jobs ALTER COLUMN id SET DEFAULT nextval('public.delayed_jobs_id_seq'::regclass);


--
-- Name: email_actions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_actions ALTER COLUMN id SET DEFAULT nextval('public.email_actions_id_seq'::regclass);


--
-- Name: email_templates id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_templates ALTER COLUMN id SET DEFAULT nextval('public.email_templates_id_seq'::regclass);


--
-- Name: email_triggers id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_triggers ALTER COLUMN id SET DEFAULT nextval('public.email_triggers_id_seq'::regclass);


--
-- Name: emails id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.emails ALTER COLUMN id SET DEFAULT nextval('public.emails_id_seq'::regclass);


--
-- Name: invoices id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.invoices ALTER COLUMN id SET DEFAULT nextval('public.invoices_id_seq'::regclass);


--
-- Name: links id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.links ALTER COLUMN id SET DEFAULT nextval('public.links_id_seq'::regclass);


--
-- Name: messages id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.messages ALTER COLUMN id SET DEFAULT nextval('public.messages_id_seq'::regclass);


--
-- Name: notifications id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.notifications ALTER COLUMN id SET DEFAULT nextval('public.notifications_id_seq'::regclass);


--
-- Name: orders id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.orders ALTER COLUMN id SET DEFAULT nextval('public.orders_id_seq'::regclass);


--
-- Name: payouts id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.payouts ALTER COLUMN id SET DEFAULT nextval('public.payouts_id_seq'::regclass);


--
-- Name: profiles id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.profiles ALTER COLUMN id SET DEFAULT nextval('public.profiles_id_seq'::regclass);


--
-- Name: publishers id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.publishers ALTER COLUMN id SET DEFAULT nextval('public.publishers_id_seq'::regclass);


--
-- Name: reports id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.reports ALTER COLUMN id SET DEFAULT nextval('public.reports_id_seq'::regclass);


--
-- Name: social_handles id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.social_handles ALTER COLUMN id SET DEFAULT nextval('public.social_handles_id_seq'::regclass);


--
-- Name: transactions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transactions ALTER COLUMN id SET DEFAULT nextval('public.transactions_id_seq'::regclass);


--
-- Name: urls id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.urls ALTER COLUMN id SET DEFAULT nextval('public.urls_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Name: web_contents id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.web_contents ALTER COLUMN id SET DEFAULT nextval('public.web_contents_id_seq'::regclass);


--
-- Name: active_admin_comments active_admin_comments_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.active_admin_comments
    ADD CONSTRAINT active_admin_comments_pkey PRIMARY KEY (id);


--
-- Name: admin_users admin_users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.admin_users
    ADD CONSTRAINT admin_users_pkey PRIMARY KEY (id);


--
-- Name: ads ads_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ads
    ADD CONSTRAINT ads_pkey PRIMARY KEY (id);


--
-- Name: advertisers advertisers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.advertisers
    ADD CONSTRAINT advertisers_pkey PRIMARY KEY (id);


--
-- Name: articles articles_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.articles
    ADD CONSTRAINT articles_pkey PRIMARY KEY (id);


--
-- Name: author_invoices author_invoices_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.author_invoices
    ADD CONSTRAINT author_invoices_pkey PRIMARY KEY (id);


--
-- Name: campaigns campaigns_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.campaigns
    ADD CONSTRAINT campaigns_pkey PRIMARY KEY (id);


--
-- Name: categories categories_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: categorizings categorizings_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.categorizings
    ADD CONSTRAINT categorizings_pkey PRIMARY KEY (id);


--
-- Name: comments comments_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.comments
    ADD CONSTRAINT comments_pkey PRIMARY KEY (id);


--
-- Name: configs configs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.configs
    ADD CONSTRAINT configs_pkey PRIMARY KEY (id);


--
-- Name: content_links content_links_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.content_links
    ADD CONSTRAINT content_links_pkey PRIMARY KEY (id);


--
-- Name: content_urls content_urls_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.content_urls
    ADD CONSTRAINT content_urls_pkey PRIMARY KEY (id);


--
-- Name: contents contents_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contents
    ADD CONSTRAINT contents_pkey PRIMARY KEY (id);


--
-- Name: credit_cards credit_cards_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.credit_cards
    ADD CONSTRAINT credit_cards_pkey PRIMARY KEY (id);


--
-- Name: delayed_jobs delayed_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.delayed_jobs
    ADD CONSTRAINT delayed_jobs_pkey PRIMARY KEY (id);


--
-- Name: email_actions email_actions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_actions
    ADD CONSTRAINT email_actions_pkey PRIMARY KEY (id);


--
-- Name: email_templates email_templates_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_templates
    ADD CONSTRAINT email_templates_pkey PRIMARY KEY (id);


--
-- Name: email_triggers email_triggers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_triggers
    ADD CONSTRAINT email_triggers_pkey PRIMARY KEY (id);


--
-- Name: emails emails_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.emails
    ADD CONSTRAINT emails_pkey PRIMARY KEY (id);


--
-- Name: invoices invoices_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.invoices
    ADD CONSTRAINT invoices_pkey PRIMARY KEY (id);


--
-- Name: links links_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.links
    ADD CONSTRAINT links_pkey PRIMARY KEY (id);


--
-- Name: messages messages_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.messages
    ADD CONSTRAINT messages_pkey PRIMARY KEY (id);


--
-- Name: notifications notifications_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.notifications
    ADD CONSTRAINT notifications_pkey PRIMARY KEY (id);


--
-- Name: orders orders_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_pkey PRIMARY KEY (id);


--
-- Name: payouts payouts_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.payouts
    ADD CONSTRAINT payouts_pkey PRIMARY KEY (id);


--
-- Name: profiles profiles_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.profiles
    ADD CONSTRAINT profiles_pkey PRIMARY KEY (id);


--
-- Name: publishers publishers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.publishers
    ADD CONSTRAINT publishers_pkey PRIMARY KEY (id);


--
-- Name: reports reports_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.reports
    ADD CONSTRAINT reports_pkey PRIMARY KEY (id);


--
-- Name: social_handles social_handles_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.social_handles
    ADD CONSTRAINT social_handles_pkey PRIMARY KEY (id);


--
-- Name: transactions transactions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.transactions
    ADD CONSTRAINT transactions_pkey PRIMARY KEY (id);


--
-- Name: urls urls_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.urls
    ADD CONSTRAINT urls_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: web_contents web_contents_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.web_contents
    ADD CONSTRAINT web_contents_pkey PRIMARY KEY (id);


--
-- Name: config_index; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX config_index ON public.configs USING btree (configable_type, configable_id, category, name);


--
-- Name: delayed_jobs_priority; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX delayed_jobs_priority ON public.delayed_jobs USING btree (priority, run_at);


--
-- Name: index_active_admin_comments_on_author_type_and_author_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_active_admin_comments_on_author_type_and_author_id ON public.active_admin_comments USING btree (author_type, author_id);


--
-- Name: index_active_admin_comments_on_namespace; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_active_admin_comments_on_namespace ON public.active_admin_comments USING btree (namespace);


--
-- Name: index_active_admin_comments_on_resource_type_and_resource_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_active_admin_comments_on_resource_type_and_resource_id ON public.active_admin_comments USING btree (resource_type, resource_id);


--
-- Name: index_admin_users_on_confirmation_token; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_admin_users_on_confirmation_token ON public.admin_users USING btree (confirmation_token);


--
-- Name: index_admin_users_on_email; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_admin_users_on_email ON public.admin_users USING btree (email);


--
-- Name: index_admin_users_on_first_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_admin_users_on_first_name ON public.admin_users USING btree (first_name);


--
-- Name: index_admin_users_on_last_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_admin_users_on_last_name ON public.admin_users USING btree (last_name);


--
-- Name: index_admin_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_admin_users_on_reset_password_token ON public.admin_users USING btree (reset_password_token);


--
-- Name: index_admin_users_on_type; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_admin_users_on_type ON public.admin_users USING btree (type);


--
-- Name: index_ads_on_advertiser_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_ads_on_advertiser_id ON public.ads USING btree (advertiser_id);


--
-- Name: index_ads_on_campaign_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_ads_on_campaign_id ON public.ads USING btree (campaign_id);


--
-- Name: index_ads_on_link_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_ads_on_link_id ON public.ads USING btree (link_id);


--
-- Name: index_ads_on_state; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_ads_on_state ON public.ads USING btree (state);


--
-- Name: index_ads_on_url_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_ads_on_url_id ON public.ads USING btree (url_id);


--
-- Name: index_advertisers_on_agent_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_advertisers_on_agent_id ON public.advertisers USING btree (agent_id);


--
-- Name: index_advertisers_on_state; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_advertisers_on_state ON public.advertisers USING btree (state);


--
-- Name: index_advertisers_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_advertisers_on_user_id ON public.advertisers USING btree (user_id);


--
-- Name: index_articles_on_ad_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_articles_on_ad_id ON public.articles USING btree (ad_id);


--
-- Name: index_articles_on_advertiser_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_articles_on_advertiser_id ON public.articles USING btree (advertiser_id);


--
-- Name: index_articles_on_author_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_articles_on_author_id ON public.articles USING btree (author_id);


--
-- Name: index_articles_on_author_invoice_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_articles_on_author_invoice_id ON public.articles USING btree (author_invoice_id);


--
-- Name: index_articles_on_link_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_articles_on_link_id ON public.articles USING btree (link_id);


--
-- Name: index_articles_on_state; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_articles_on_state ON public.articles USING btree (state);


--
-- Name: index_articles_on_url_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_articles_on_url_id ON public.articles USING btree (url_id);


--
-- Name: index_author_invoices_on_admin_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_author_invoices_on_admin_user_id ON public.author_invoices USING btree (admin_user_id);


--
-- Name: index_campaigns_on_advertiser_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_campaigns_on_advertiser_id ON public.campaigns USING btree (advertiser_id);


--
-- Name: index_campaigns_on_state; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_campaigns_on_state ON public.campaigns USING btree (state);


--
-- Name: index_campaigns_on_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_campaigns_on_updated_at ON public.campaigns USING btree (updated_at);


--
-- Name: index_categories_on_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_categories_on_name ON public.categories USING btree (name);


--
-- Name: index_categorizings_on_categorizable_type_and_categorizable_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_categorizings_on_categorizable_type_and_categorizable_id ON public.categorizings USING btree (categorizable_type, categorizable_id);


--
-- Name: index_categorizings_on_category_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_categorizings_on_category_id ON public.categorizings USING btree (category_id);


--
-- Name: index_comments_on_commentable_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_comments_on_commentable_id ON public.comments USING btree (commentable_id);


--
-- Name: index_comments_on_commentable_type; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_comments_on_commentable_type ON public.comments USING btree (commentable_type);


--
-- Name: index_comments_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_comments_on_user_id ON public.comments USING btree (user_id);


--
-- Name: index_comments_on_user_type; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_comments_on_user_type ON public.comments USING btree (user_type);


--
-- Name: index_content_links_on_content_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_content_links_on_content_id ON public.content_links USING btree (content_id);


--
-- Name: index_content_links_on_link_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_content_links_on_link_id ON public.content_links USING btree (link_id);


--
-- Name: index_content_urls_on_content_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_content_urls_on_content_id ON public.content_urls USING btree (content_id);


--
-- Name: index_content_urls_on_url_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_content_urls_on_url_id ON public.content_urls USING btree (url_id);


--
-- Name: index_contents_on_advertiser_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_contents_on_advertiser_id ON public.contents USING btree (advertiser_id);


--
-- Name: index_contents_on_campaign_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_contents_on_campaign_id ON public.contents USING btree (campaign_id);


--
-- Name: index_contents_on_deleted_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_contents_on_deleted_at ON public.contents USING btree (deleted_at);


--
-- Name: index_contents_on_state; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_contents_on_state ON public.contents USING btree (state);


--
-- Name: index_credit_cards_on_braintree_token; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_credit_cards_on_braintree_token ON public.credit_cards USING btree (braintree_token);


--
-- Name: index_credit_cards_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_credit_cards_on_user_id ON public.credit_cards USING btree (user_id);


--
-- Name: index_email_actions_on_collate; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_email_actions_on_collate ON public.email_actions USING btree ("collate");


--
-- Name: index_email_actions_on_email_template_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_email_actions_on_email_template_id ON public.email_actions USING btree (email_template_id);


--
-- Name: index_email_actions_on_email_trigger_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_email_actions_on_email_trigger_id ON public.email_actions USING btree (email_trigger_id);


--
-- Name: index_email_actions_on_receiver_type_and_receiver_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_email_actions_on_receiver_type_and_receiver_id ON public.email_actions USING btree (receiver_type, receiver_id);


--
-- Name: index_email_templates_on_collate; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_email_templates_on_collate ON public.email_templates USING btree ("collate");


--
-- Name: index_email_templates_on_email_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_email_templates_on_email_id ON public.email_templates USING btree (email_id);


--
-- Name: index_email_triggers_on_email_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_email_triggers_on_email_id ON public.email_triggers USING btree (email_id);


--
-- Name: index_email_triggers_on_receiver_type_and_receiver_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_email_triggers_on_receiver_type_and_receiver_id ON public.email_triggers USING btree (receiver_type, receiver_id);


--
-- Name: index_invoices_on_advertiser_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_invoices_on_advertiser_id ON public.invoices USING btree (advertiser_id);


--
-- Name: index_links_on_campaign_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_links_on_campaign_id ON public.links USING btree (campaign_id);


--
-- Name: index_links_on_state; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_links_on_state ON public.links USING btree (state);


--
-- Name: index_links_on_updated_at; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_links_on_updated_at ON public.links USING btree (updated_at);


--
-- Name: index_messages_on_state; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_messages_on_state ON public.messages USING btree (state);


--
-- Name: index_messages_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_messages_on_user_id ON public.messages USING btree (user_id);


--
-- Name: index_notifications_on_notification_type; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_notifications_on_notification_type ON public.notifications USING btree (notification_type);


--
-- Name: index_notifications_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_notifications_on_user_id ON public.notifications USING btree (user_id);


--
-- Name: index_orders_on_ad_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_orders_on_ad_id ON public.orders USING btree (ad_id);


--
-- Name: index_orders_on_advertiser_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_orders_on_advertiser_id ON public.orders USING btree (advertiser_id);


--
-- Name: index_orders_on_invoice_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_orders_on_invoice_id ON public.orders USING btree (invoice_id);


--
-- Name: index_orders_on_state; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_orders_on_state ON public.orders USING btree (state);


--
-- Name: index_orders_on_url_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_orders_on_url_id ON public.orders USING btree (url_id);


--
-- Name: index_payouts_on_ad_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_payouts_on_ad_id ON public.payouts USING btree (ad_id);


--
-- Name: index_payouts_on_publisher_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_payouts_on_publisher_id ON public.payouts USING btree (publisher_id);


--
-- Name: index_profiles_on_profile_type; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_profiles_on_profile_type ON public.profiles USING btree (profile_type);


--
-- Name: index_profiles_on_publisher_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_profiles_on_publisher_id ON public.profiles USING btree (publisher_id);


--
-- Name: index_publishers_on_state; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_publishers_on_state ON public.publishers USING btree (state);


--
-- Name: index_publishers_on_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_publishers_on_user_id ON public.publishers USING btree (user_id);


--
-- Name: index_social_handles_on_network; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_social_handles_on_network ON public.social_handles USING btree (network);


--
-- Name: index_social_handles_on_profile_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_social_handles_on_profile_id ON public.social_handles USING btree (profile_id);


--
-- Name: index_social_handles_on_state; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_social_handles_on_state ON public.social_handles USING btree (state);


--
-- Name: index_social_handles_on_type; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_social_handles_on_type ON public.social_handles USING btree (type);


--
-- Name: index_transactions_on_payer_type_and_payer_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_transactions_on_payer_type_and_payer_id ON public.transactions USING btree (payer_type, payer_id);


--
-- Name: index_transactions_on_receiver_type_and_receiver_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_transactions_on_receiver_type_and_receiver_id ON public.transactions USING btree (receiver_type, receiver_id);


--
-- Name: index_urls_on_alexa_rank; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_urls_on_alexa_rank ON public.urls USING btree (alexa_rank);


--
-- Name: index_urls_on_citation_flow; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_urls_on_citation_flow ON public.urls USING btree (citation_flow);


--
-- Name: index_urls_on_domain_authority; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_urls_on_domain_authority ON public.urls USING btree (domain_authority);


--
-- Name: index_urls_on_price; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_urls_on_price ON public.urls USING btree (price);


--
-- Name: index_urls_on_profile_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_urls_on_profile_id ON public.urls USING btree (profile_id);


--
-- Name: index_urls_on_state; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_urls_on_state ON public.urls USING btree (state);


--
-- Name: index_urls_on_trust_flow; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_urls_on_trust_flow ON public.urls USING btree (trust_flow);


--
-- Name: index_users_on_authentication_token; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_authentication_token ON public.users USING btree (authentication_token);


--
-- Name: index_users_on_confirmation_token; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_confirmation_token ON public.users USING btree (confirmation_token);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_email ON public.users USING btree (email);


--
-- Name: index_users_on_first_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_users_on_first_name ON public.users USING btree (first_name);


--
-- Name: index_users_on_last_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_users_on_last_name ON public.users USING btree (last_name);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON public.users USING btree (reset_password_token);


--
-- Name: index_web_contents_on_profile_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_web_contents_on_profile_id ON public.web_contents USING btree (profile_id);


--
-- Name: index_web_contents_on_state; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_web_contents_on_state ON public.web_contents USING btree (state);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX unique_schema_migrations ON public.schema_migrations USING btree (version);


--
-- Name: urls_search_categories_keyword_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX urls_search_categories_keyword_idx ON public.urls USING gin (search_categories_keyword_vector);


--
-- Name: urls_search_description_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX urls_search_description_idx ON public.urls USING gin (search_description_vector);


--
-- Name: urls_search_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX urls_search_idx ON public.urls USING gin (search_categories_vector);


--
-- Name: social_handles social_handles_after_delete_row_tr; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER social_handles_after_delete_row_tr AFTER DELETE ON public.social_handles FOR EACH ROW EXECUTE PROCEDURE public.social_handles_after_delete_row_tr();


--
-- Name: social_handles social_handles_after_insert_row_tr; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER social_handles_after_insert_row_tr AFTER INSERT ON public.social_handles FOR EACH ROW EXECUTE PROCEDURE public.social_handles_after_insert_row_tr();


--
-- Name: social_handles social_handles_after_update_row_tr; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER social_handles_after_update_row_tr AFTER UPDATE ON public.social_handles FOR EACH ROW EXECUTE PROCEDURE public.social_handles_after_update_row_tr();


--
-- Name: urls urls_after_delete_row_tr; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER urls_after_delete_row_tr AFTER DELETE ON public.urls FOR EACH ROW EXECUTE PROCEDURE public.urls_after_delete_row_tr();


--
-- Name: urls urls_after_insert_row_tr; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER urls_after_insert_row_tr AFTER INSERT ON public.urls FOR EACH ROW EXECUTE PROCEDURE public.urls_after_insert_row_tr();


--
-- Name: urls urls_after_update_row_tr; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER urls_after_update_row_tr AFTER UPDATE ON public.urls FOR EACH ROW EXECUTE PROCEDURE public.urls_after_update_row_tr();


--
-- Name: urls urls_search_categories_keyword_vector_update; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER urls_search_categories_keyword_vector_update BEFORE INSERT OR UPDATE ON public.urls FOR EACH ROW EXECUTE PROCEDURE tsvector_update_trigger('search_categories_keyword_vector', 'pg_catalog.english', 'profile_categories_keyword');


--
-- Name: urls urls_search_description_vector_update; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER urls_search_description_vector_update BEFORE INSERT OR UPDATE ON public.urls FOR EACH ROW EXECUTE PROCEDURE tsvector_update_trigger('search_description_vector', 'pg_catalog.english', 'description');


--
-- Name: urls urls_search_vector_update; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER urls_search_vector_update BEFORE INSERT OR UPDATE ON public.urls FOR EACH ROW EXECUTE PROCEDURE tsvector_update_trigger('search_categories_vector', 'pg_catalog.english', 'profile_categories_names');


--
-- Name: web_contents web_contents_after_delete_row_tr; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER web_contents_after_delete_row_tr AFTER DELETE ON public.web_contents FOR EACH ROW EXECUTE PROCEDURE public.web_contents_after_delete_row_tr();


--
-- Name: web_contents web_contents_after_insert_row_tr; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER web_contents_after_insert_row_tr AFTER INSERT ON public.web_contents FOR EACH ROW EXECUTE PROCEDURE public.web_contents_after_insert_row_tr();


--
-- Name: web_contents web_contents_after_update_row_tr; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER web_contents_after_update_row_tr AFTER UPDATE ON public.web_contents FOR EACH ROW EXECUTE PROCEDURE public.web_contents_after_update_row_tr();


--
-- Name: ads fk_rails_01328f71f0; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ads
    ADD CONSTRAINT fk_rails_01328f71f0 FOREIGN KEY (advertiser_id) REFERENCES public.advertisers(id);


--
-- Name: orders fk_rails_04abb31647; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT fk_rails_04abb31647 FOREIGN KEY (advertiser_id) REFERENCES public.advertisers(id);


--
-- Name: credit_cards fk_rails_069bf994f3; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.credit_cards
    ADD CONSTRAINT fk_rails_069bf994f3 FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: contents fk_rails_099aca7789; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contents
    ADD CONSTRAINT fk_rails_099aca7789 FOREIGN KEY (campaign_id) REFERENCES public.campaigns(id);


--
-- Name: categorizings fk_rails_10f590cd01; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.categorizings
    ADD CONSTRAINT fk_rails_10f590cd01 FOREIGN KEY (category_id) REFERENCES public.categories(id);


--
-- Name: articles fk_rails_1334e0a555; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.articles
    ADD CONSTRAINT fk_rails_1334e0a555 FOREIGN KEY (url_id) REFERENCES public.urls(id);


--
-- Name: links fk_rails_157df8a92c; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.links
    ADD CONSTRAINT fk_rails_157df8a92c FOREIGN KEY (campaign_id) REFERENCES public.campaigns(id);


--
-- Name: advertisers fk_rails_159829f07e; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.advertisers
    ADD CONSTRAINT fk_rails_159829f07e FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: content_links fk_rails_1984421ec3; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.content_links
    ADD CONSTRAINT fk_rails_1984421ec3 FOREIGN KEY (link_id) REFERENCES public.links(id);


--
-- Name: email_actions fk_rails_2145fe7bca; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_actions
    ADD CONSTRAINT fk_rails_2145fe7bca FOREIGN KEY (email_template_id) REFERENCES public.email_templates(id);


--
-- Name: messages fk_rails_273a25a7a6; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.messages
    ADD CONSTRAINT fk_rails_273a25a7a6 FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: email_actions fk_rails_295008856a; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_actions
    ADD CONSTRAINT fk_rails_295008856a FOREIGN KEY (email_trigger_id) REFERENCES public.email_triggers(id);


--
-- Name: email_templates fk_rails_308d063836; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_templates
    ADD CONSTRAINT fk_rails_308d063836 FOREIGN KEY (email_id) REFERENCES public.emails(id);


--
-- Name: social_handles fk_rails_3e3e419442; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.social_handles
    ADD CONSTRAINT fk_rails_3e3e419442 FOREIGN KEY (profile_id) REFERENCES public.profiles(id);


--
-- Name: content_links fk_rails_444dcf93c5; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.content_links
    ADD CONSTRAINT fk_rails_444dcf93c5 FOREIGN KEY (content_id) REFERENCES public.contents(id);


--
-- Name: email_triggers fk_rails_4d8cad2217; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_triggers
    ADD CONSTRAINT fk_rails_4d8cad2217 FOREIGN KEY (email_id) REFERENCES public.emails(id);


--
-- Name: articles fk_rails_57c4caebb7; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.articles
    ADD CONSTRAINT fk_rails_57c4caebb7 FOREIGN KEY (author_invoice_id) REFERENCES public.author_invoices(id);


--
-- Name: payouts fk_rails_5a1c500a4c; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.payouts
    ADD CONSTRAINT fk_rails_5a1c500a4c FOREIGN KEY (publisher_id) REFERENCES public.publishers(id);


--
-- Name: content_urls fk_rails_5bc6ec3c4b; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.content_urls
    ADD CONSTRAINT fk_rails_5bc6ec3c4b FOREIGN KEY (content_id) REFERENCES public.contents(id);


--
-- Name: contents fk_rails_66108345e1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.contents
    ADD CONSTRAINT fk_rails_66108345e1 FOREIGN KEY (advertiser_id) REFERENCES public.advertisers(id);


--
-- Name: articles fk_rails_6d214e6e41; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.articles
    ADD CONSTRAINT fk_rails_6d214e6e41 FOREIGN KEY (advertiser_id) REFERENCES public.advertisers(id);


--
-- Name: orders fk_rails_717478821c; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT fk_rails_717478821c FOREIGN KEY (url_id) REFERENCES public.urls(id);


--
-- Name: advertisers fk_rails_71a3fe5d61; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.advertisers
    ADD CONSTRAINT fk_rails_71a3fe5d61 FOREIGN KEY (agent_id) REFERENCES public.admin_users(id);


--
-- Name: payouts fk_rails_9473983927; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.payouts
    ADD CONSTRAINT fk_rails_9473983927 FOREIGN KEY (ad_id) REFERENCES public.ads(id);


--
-- Name: ads fk_rails_96a5447f91; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ads
    ADD CONSTRAINT fk_rails_96a5447f91 FOREIGN KEY (url_id) REFERENCES public.urls(id);


--
-- Name: ads fk_rails_98b1ac1cdb; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ads
    ADD CONSTRAINT fk_rails_98b1ac1cdb FOREIGN KEY (campaign_id) REFERENCES public.campaigns(id);


--
-- Name: campaigns fk_rails_a27ecda997; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.campaigns
    ADD CONSTRAINT fk_rails_a27ecda997 FOREIGN KEY (advertiser_id) REFERENCES public.advertisers(id);


--
-- Name: content_urls fk_rails_af9c165f74; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.content_urls
    ADD CONSTRAINT fk_rails_af9c165f74 FOREIGN KEY (url_id) REFERENCES public.urls(id);


--
-- Name: notifications fk_rails_b080fb4855; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.notifications
    ADD CONSTRAINT fk_rails_b080fb4855 FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: web_contents fk_rails_b23ae2dcc2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.web_contents
    ADD CONSTRAINT fk_rails_b23ae2dcc2 FOREIGN KEY (profile_id) REFERENCES public.profiles(id);


--
-- Name: orders fk_rails_b33ed6c672; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT fk_rails_b33ed6c672 FOREIGN KEY (invoice_id) REFERENCES public.invoices(id);


--
-- Name: publishers fk_rails_bf13308676; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.publishers
    ADD CONSTRAINT fk_rails_bf13308676 FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: ads fk_rails_c0c5ca1db4; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ads
    ADD CONSTRAINT fk_rails_c0c5ca1db4 FOREIGN KEY (link_id) REFERENCES public.links(id);


--
-- Name: articles fk_rails_c19b1b5013; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.articles
    ADD CONSTRAINT fk_rails_c19b1b5013 FOREIGN KEY (ad_id) REFERENCES public.ads(id);


--
-- Name: articles fk_rails_d24cfbb2bd; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.articles
    ADD CONSTRAINT fk_rails_d24cfbb2bd FOREIGN KEY (link_id) REFERENCES public.links(id);


--
-- Name: profiles fk_rails_e6eec6da50; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.profiles
    ADD CONSTRAINT fk_rails_e6eec6da50 FOREIGN KEY (publisher_id) REFERENCES public.publishers(id);


--
-- Name: articles fk_rails_e74ce85cbc; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.articles
    ADD CONSTRAINT fk_rails_e74ce85cbc FOREIGN KEY (author_id) REFERENCES public.admin_users(id);


--
-- Name: author_invoices fk_rails_e9f5766149; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.author_invoices
    ADD CONSTRAINT fk_rails_e9f5766149 FOREIGN KEY (admin_user_id) REFERENCES public.admin_users(id);


--
-- Name: invoices fk_rails_eed57f387a; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.invoices
    ADD CONSTRAINT fk_rails_eed57f387a FOREIGN KEY (advertiser_id) REFERENCES public.advertisers(id);


--
-- Name: orders fk_rails_f25ae31c0b; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT fk_rails_f25ae31c0b FOREIGN KEY (ad_id) REFERENCES public.ads(id);


--
-- Name: urls fk_rails_f3253d131f; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.urls
    ADD CONSTRAINT fk_rails_f3253d131f FOREIGN KEY (profile_id) REFERENCES public.profiles(id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public;

INSERT INTO schema_migrations (version) VALUES ('20141001184617');

INSERT INTO schema_migrations (version) VALUES ('20141009132007');

INSERT INTO schema_migrations (version) VALUES ('20150706174649');

INSERT INTO schema_migrations (version) VALUES ('20150706174705');

INSERT INTO schema_migrations (version) VALUES ('20160711154717');

INSERT INTO schema_migrations (version) VALUES ('20160711183056');

INSERT INTO schema_migrations (version) VALUES ('20160712132447');

INSERT INTO schema_migrations (version) VALUES ('20160712154721');

INSERT INTO schema_migrations (version) VALUES ('20160712164723');

INSERT INTO schema_migrations (version) VALUES ('20160712171328');

INSERT INTO schema_migrations (version) VALUES ('20160713181350');

INSERT INTO schema_migrations (version) VALUES ('20160714184310');

INSERT INTO schema_migrations (version) VALUES ('20160719143200');

INSERT INTO schema_migrations (version) VALUES ('20160719195450');

INSERT INTO schema_migrations (version) VALUES ('20160722135056');

INSERT INTO schema_migrations (version) VALUES ('20160823131618');

INSERT INTO schema_migrations (version) VALUES ('20160909123211');

INSERT INTO schema_migrations (version) VALUES ('20160913173048');

INSERT INTO schema_migrations (version) VALUES ('20160913193658');

INSERT INTO schema_migrations (version) VALUES ('20160926165249');

INSERT INTO schema_migrations (version) VALUES ('20161013123706');

INSERT INTO schema_migrations (version) VALUES ('20161021122523');

INSERT INTO schema_migrations (version) VALUES ('20161021122819');

INSERT INTO schema_migrations (version) VALUES ('20161021130349');

INSERT INTO schema_migrations (version) VALUES ('20161110172137');

INSERT INTO schema_migrations (version) VALUES ('20161201200347');

INSERT INTO schema_migrations (version) VALUES ('20161205134246');

INSERT INTO schema_migrations (version) VALUES ('20161222143838');

INSERT INTO schema_migrations (version) VALUES ('20161222195609');

INSERT INTO schema_migrations (version) VALUES ('20170118134329');

INSERT INTO schema_migrations (version) VALUES ('20170124173144');

INSERT INTO schema_migrations (version) VALUES ('20170126205316');

INSERT INTO schema_migrations (version) VALUES ('20170126205440');

INSERT INTO schema_migrations (version) VALUES ('20170203125618');

INSERT INTO schema_migrations (version) VALUES ('20170206132939');

INSERT INTO schema_migrations (version) VALUES ('20170206133104');

INSERT INTO schema_migrations (version) VALUES ('20170223143236');

INSERT INTO schema_migrations (version) VALUES ('20170313184215');

INSERT INTO schema_migrations (version) VALUES ('20170330120852');

INSERT INTO schema_migrations (version) VALUES ('20170330141949');

INSERT INTO schema_migrations (version) VALUES ('20170427183352');

INSERT INTO schema_migrations (version) VALUES ('20170428173101');

INSERT INTO schema_migrations (version) VALUES ('20170503162205');

INSERT INTO schema_migrations (version) VALUES ('20170510171149');

INSERT INTO schema_migrations (version) VALUES ('20170516180846');

INSERT INTO schema_migrations (version) VALUES ('20170517134036');

INSERT INTO schema_migrations (version) VALUES ('20170518152700');

INSERT INTO schema_migrations (version) VALUES ('20170525153518');

INSERT INTO schema_migrations (version) VALUES ('20170526193218');

INSERT INTO schema_migrations (version) VALUES ('20170612185623');

INSERT INTO schema_migrations (version) VALUES ('20170613131810');

INSERT INTO schema_migrations (version) VALUES ('20170613201507');

INSERT INTO schema_migrations (version) VALUES ('20170627131214');

INSERT INTO schema_migrations (version) VALUES ('20170627172416');

INSERT INTO schema_migrations (version) VALUES ('20170628133454');

INSERT INTO schema_migrations (version) VALUES ('20170629143513');

INSERT INTO schema_migrations (version) VALUES ('20170719180909');

INSERT INTO schema_migrations (version) VALUES ('20170720185641');

INSERT INTO schema_migrations (version) VALUES ('20170726182150');

INSERT INTO schema_migrations (version) VALUES ('20170810155656');

INSERT INTO schema_migrations (version) VALUES ('20170816141944');

INSERT INTO schema_migrations (version) VALUES ('20170816152804');

INSERT INTO schema_migrations (version) VALUES ('20170816191942');

INSERT INTO schema_migrations (version) VALUES ('20170901115455');

INSERT INTO schema_migrations (version) VALUES ('20171005173524');

INSERT INTO schema_migrations (version) VALUES ('20171025182936');

INSERT INTO schema_migrations (version) VALUES ('20171115140811');

INSERT INTO schema_migrations (version) VALUES ('20171116175501');

INSERT INTO schema_migrations (version) VALUES ('20171130123913');

INSERT INTO schema_migrations (version) VALUES ('20171227170647');

INSERT INTO schema_migrations (version) VALUES ('20180105195829');

INSERT INTO schema_migrations (version) VALUES ('20180117143602');

INSERT INTO schema_migrations (version) VALUES ('20180117170051');

INSERT INTO schema_migrations (version) VALUES ('20180117182128');

INSERT INTO schema_migrations (version) VALUES ('20180118165240');

INSERT INTO schema_migrations (version) VALUES ('20180202132538');

INSERT INTO schema_migrations (version) VALUES ('20180202134541');

INSERT INTO schema_migrations (version) VALUES ('20180330195758');

INSERT INTO schema_migrations (version) VALUES ('20180330200002');

INSERT INTO schema_migrations (version) VALUES ('20180330200525');

INSERT INTO schema_migrations (version) VALUES ('20180330200807');

INSERT INTO schema_migrations (version) VALUES ('20180402134024');

INSERT INTO schema_migrations (version) VALUES ('20180403150826');

INSERT INTO schema_migrations (version) VALUES ('20180403181328');

INSERT INTO schema_migrations (version) VALUES ('20180404151136');

INSERT INTO schema_migrations (version) VALUES ('20180404154934');

INSERT INTO schema_migrations (version) VALUES ('20180405154940');

INSERT INTO schema_migrations (version) VALUES ('20180405175345');

INSERT INTO schema_migrations (version) VALUES ('20180418133351');

INSERT INTO schema_migrations (version) VALUES ('20180423015006');

INSERT INTO schema_migrations (version) VALUES ('20180423015546');

INSERT INTO schema_migrations (version) VALUES ('20180423194514');

INSERT INTO schema_migrations (version) VALUES ('20180521002511');

INSERT INTO schema_migrations (version) VALUES ('20180531174334');

INSERT INTO schema_migrations (version) VALUES ('20180606154752');

INSERT INTO schema_migrations (version) VALUES ('20180618134844');

INSERT INTO schema_migrations (version) VALUES ('20180702235612');

INSERT INTO schema_migrations (version) VALUES ('20180815195034');

INSERT INTO schema_migrations (version) VALUES ('20181010142825');

INSERT INTO schema_migrations (version) VALUES ('20181011130505');

INSERT INTO schema_migrations (version) VALUES ('20181108134051');

INSERT INTO schema_migrations (version) VALUES ('20181226170749');

INSERT INTO schema_migrations (version) VALUES ('20190125193312');

INSERT INTO schema_migrations (version) VALUES ('20190429183350');

INSERT INTO schema_migrations (version) VALUES ('20190710144431');

INSERT INTO schema_migrations (version) VALUES ('20190808141452');

INSERT INTO schema_migrations (version) VALUES ('20190809074343');

INSERT INTO schema_migrations (version) VALUES ('20190812081503');

INSERT INTO schema_migrations (version) VALUES ('20190812120348');

INSERT INTO schema_migrations (version) VALUES ('20190816060315');

INSERT INTO schema_migrations (version) VALUES ('20190816063042');

INSERT INTO schema_migrations (version) VALUES ('20190821070431');

INSERT INTO schema_migrations (version) VALUES ('20190823084538');

INSERT INTO schema_migrations (version) VALUES ('20190826132549');

INSERT INTO schema_migrations (version) VALUES ('20190911113046');

INSERT INTO schema_migrations (version) VALUES ('20190917113710');

INSERT INTO schema_migrations (version) VALUES ('20190917120114');

INSERT INTO schema_migrations (version) VALUES ('20190917120359');

INSERT INTO schema_migrations (version) VALUES ('20190919083654');

INSERT INTO schema_migrations (version) VALUES ('20190919110244');

INSERT INTO schema_migrations (version) VALUES ('20190919112734');

INSERT INTO schema_migrations (version) VALUES ('20190919112924');

INSERT INTO schema_migrations (version) VALUES ('20190919125947');

INSERT INTO schema_migrations (version) VALUES ('20190923140550');

