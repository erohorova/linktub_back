class AddAttachmentUrlsReportToContents < ActiveRecord::Migration
  def change
    add_attachment :contents, :urls_spreadsheet
  end
end
