class AddSearchDescrptionVectorToUrls < ActiveRecord::Migration
  def up
    add_column :urls, :search_description_vector, 'tsvector'

    execute <<-SQL
      CREATE INDEX urls_search_description_idx
      ON urls
      USING gin(search_description_vector);
    SQL

    execute <<-SQL
      DROP TRIGGER IF EXISTS urls_search_description_vector_update
      ON urls;

      CREATE TRIGGER urls_search_description_vector_update
      BEFORE INSERT OR UPDATE
      ON urls
      FOR EACH ROW EXECUTE PROCEDURE
      tsvector_update_trigger (search_description_vector, 'pg_catalog.english', description);
    SQL
  end

  def down
    remove_column :urls, :search_description_vector
    execute <<-SQL
      DROP TRIGGER IF EXISTS urls_search_description_vector_update on urls;
    SQL
  end
end
