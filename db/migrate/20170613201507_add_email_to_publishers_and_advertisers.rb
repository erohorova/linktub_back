class AddEmailToPublishersAndAdvertisers < ActiveRecord::Migration
  def change
    add_column :publishers,  :email, :string
    add_column :advertisers, :email, :string
  end
end
