class ChangeColumnNameForAwaitingPubs < ActiveRecord::Migration
  def change
    rename_column :advertisers, :awaiting_publisher_spreadsheet_file_name,    :awaiting_publication_spreadsheet_file_name
    rename_column :advertisers, :awaiting_publisher_spreadsheet_content_type, :awaiting_publication_spreadsheet_content_type
    rename_column :advertisers, :awaiting_publisher_spreadsheet_file_size,    :awaiting_publication_spreadsheet_file_size
    rename_column :advertisers, :awaiting_publisher_spreadsheet_updated_at,   :awaiting_publication_spreadsheet_updated_at
  end
end
