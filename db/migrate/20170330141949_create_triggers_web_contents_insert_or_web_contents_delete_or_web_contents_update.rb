# This migration was auto-generated via `rake db:generate_trigger_migration'.
# While you can edit this file, any changes you make to the definitions here
# will be undone by the next auto-generated trigger migration.

class CreateTriggersWebContentsInsertOrWebContentsDeleteOrWebContentsUpdate < ActiveRecord::Migration
  def up
    create_trigger('web_contents_after_insert_row_tr', generated: true, compatibility: 1)
      .on('web_contents')
      .after(:insert) do
      'UPDATE profiles SET total_sites = total_sites + 1 WHERE id = NEW.profile_id;'
    end

    create_trigger('web_contents_after_delete_row_tr', generated: true, compatibility: 1)
      .on('web_contents')
      .after(:delete) do
      'UPDATE profiles SET total_sites = total_sites - 1 WHERE id = OLD.profile_id;'
    end

    create_trigger('web_contents_after_update_row_tr', generated: true, compatibility: 1)
      .on('web_contents')
      .after(:update) do
      <<-SQL_ACTIONS
        IF NEW.profile_id <> OLD.profile_id THEN
          UPDATE profiles SET total_sites = total_sites - 1 WHERE id = OLD.profile_id;
          UPDATE profiles SET total_sites = total_sites + 1 WHERE id = NEW.profile_id;
        END IF;
      SQL_ACTIONS
    end
  end

  def down
    drop_trigger('web_contents_after_insert_row_tr', 'web_contents', generated: true)
    drop_trigger('web_contents_after_delete_row_tr', 'web_contents', generated: true)
    drop_trigger('web_contents_after_update_row_tr', 'web_contents', generated: true)
  end
end
