class AddIndexOnCampaignsUpdatedAt < ActiveRecord::Migration
  def change
    add_index :campaigns, :updated_at
  end
end
