class AddSearchCategoriesVectorToUrls < ActiveRecord::Migration
  def up
    add_column :urls, :search_categories_vector, 'tsvector'

    execute <<-SQL
      CREATE INDEX urls_search_idx
      ON urls
      USING gin(search_categories_vector);
    SQL

    execute <<-SQL
      DROP TRIGGER IF EXISTS urls_search_vector_update
      ON urls;

      CREATE TRIGGER urls_search_vector_update
      BEFORE INSERT OR UPDATE
      ON urls
      FOR EACH ROW EXECUTE PROCEDURE
      tsvector_update_trigger (search_categories_vector, 'pg_catalog.english', profile_categories_names);
    SQL
  end

  def down
    remove_column :urls, :search_categories_vector
    execute <<-SQL
      DROP TRIGGER IF EXISTS urls_search_vector_update on urls;
    SQL
  end
end
