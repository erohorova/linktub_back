class CreateAds < ActiveRecord::Migration
  def change
    create_table :ads do |t|
      t.string   :link_text

      t.datetime :requested_date
      t.datetime :approved_date
      t.datetime :declined_date
      t.datetime :reconsidered_date
      t.datetime :cancelled_date

      t.boolean  :admin_cancelled,      default: false
      t.boolean  :advertiser_cancelled, default: false
      t.boolean  :publisher_cancelled,  default: false
      t.decimal  :url_price,            default: 0

      t.string   :blog_post,            default: 'blog_post'
      t.string   :blog_post_title
      t.string   :blog_post_body
      t.string   :quote_id
      t.string   :full_pub_url
      t.boolean  :attach_word_document, default: true
      t.string   :blog_post_doc
      t.string   :blog_post_image_1
      t.string   :blog_post_image_2
      t.integer  :state,                index: true

      t.string   :rejected_reason
      t.text     :notes
      t.boolean  :editable,             defualt: true

      t.timestamps null: false
    end
  end
end
