# This migration was auto-generated via `rake db:generate_trigger_migration'.
# While you can edit this file, any changes you make to the definitions here
# will be undone by the next auto-generated trigger migration.

class CreateTriggersMultipleTables < ActiveRecord::Migration
  def up
    create_trigger('social_handles_after_insert_row_tr', generated: true, compatibility: 1)
      .on('social_handles')
      .after(:insert) do
      'UPDATE profiles SET total_sites = total_sites + 1 WHERE id = NEW.profile_id;'
    end

    create_trigger('social_handles_after_delete_row_tr', generated: true, compatibility: 1)
      .on('social_handles')
      .after(:delete) do
      'UPDATE profiles SET total_sites = total_sites - 1 WHERE id = OLD.profile_id;'
    end

    create_trigger('social_handles_after_update_row_tr', generated: true, compatibility: 1)
      .on('social_handles')
      .after(:update) do
      <<-SQL_ACTIONS
        IF NEW.profile_id <> OLD.profile_id THEN
          UPDATE profiles SET total_sites = total_sites - 1 WHERE id = OLD.profile_id;
          UPDATE profiles SET total_sites = total_sites + 1 WHERE id = NEW.profile_id;
        END IF;
      SQL_ACTIONS
    end

    create_trigger('urls_after_insert_row_tr', generated: true, compatibility: 1)
      .on('urls')
      .after(:insert) do
      'UPDATE profiles SET total_sites = total_sites + 1 WHERE id = NEW.profile_id;'
    end

    create_trigger('urls_after_delete_row_tr', generated: true, compatibility: 1)
      .on('urls')
      .after(:delete) do
      'UPDATE profiles SET total_sites = total_sites - 1 WHERE id = OLD.profile_id;'
    end

    create_trigger('urls_after_update_row_tr', generated: true, compatibility: 1)
      .on('urls')
      .after(:update) do
      <<-SQL_ACTIONS
        IF NEW.profile_id <> OLD.profile_id THEN
          UPDATE profiles SET total_sites = total_sites - 1 WHERE id = OLD.profile_id;
          UPDATE profiles SET total_sites = total_sites + 1 WHERE id = NEW.profile_id;
        END IF;
      SQL_ACTIONS
    end
  end

  def down
    drop_trigger('social_handles_after_insert_row_tr', 'social_handles', generated: true)
    drop_trigger('social_handles_after_delete_row_tr', 'social_handles', generated: true)
    drop_trigger('social_handles_after_update_row_tr', 'social_handles', generated: true)
    drop_trigger('urls_after_insert_row_tr', 'urls', generated: true)
    drop_trigger('urls_after_delete_row_tr', 'urls', generated: true)
    drop_trigger('urls_after_update_row_tr', 'urls', generated: true)
  end
end
