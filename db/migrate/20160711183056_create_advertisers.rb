class CreateAdvertisers < ActiveRecord::Migration
  def change
    create_table :advertisers do |t|
      t.date     :billable_date
      t.datetime :suggested_link_email_sent_at
      t.boolean  :any_approved, default: false

      t.timestamps null: false
    end

    add_attachment :advertisers, :awaiting_publisher_spreadsheet
    add_attachment :advertisers, :rejected_content_spreadsheet
    add_attachment :advertisers, :active_content_spreadsheet
  end
end
