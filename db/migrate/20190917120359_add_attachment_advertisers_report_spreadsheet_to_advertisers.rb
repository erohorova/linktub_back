class AddAttachmentAdvertisersReportSpreadsheetToAdvertisers < ActiveRecord::Migration
  def change
    add_attachment :advertisers, :advertisers_report_spreadsheet
  end
end
