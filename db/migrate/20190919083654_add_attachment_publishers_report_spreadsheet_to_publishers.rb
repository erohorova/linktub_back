class AddAttachmentPublishersReportSpreadsheetToPublishers < ActiveRecord::Migration
  def change
    add_attachment :publishers, :publishers_report_spreadsheet
  end
end
