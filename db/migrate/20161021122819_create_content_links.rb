class CreateContentLinks < ActiveRecord::Migration
  def change
    create_table :content_links do |t|
      t.references :content, index: true, foreign_key: true
      t.references :link, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
