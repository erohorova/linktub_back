class AddRequiredLengthToUrls < ActiveRecord::Migration
  def change
    add_column :urls, :required_length, :string, null: false, default: '750-1000'
  end
end
