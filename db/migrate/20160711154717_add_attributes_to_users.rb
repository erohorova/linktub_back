class AddAttributesToUsers < ActiveRecord::Migration
  def up
    change_table :users do |t|
      t.boolean :is_active,                default: true
      t.text    :terminate_reason
      t.string  :first_name,               null: false
      t.string  :last_name,                null: false
      t.string  :address_line_1
      t.string  :address_line_2
      t.string  :company
      t.string  :country
      t.string  :city
      t.integer :state,                    index: true
      t.string  :zip_code
      t.string  :phone
      t.float   :current_balance,          default: 0
      t.integer :payout_min
      t.boolean :subscribe_to_news_letter, default: true
      t.string  :how_know_about_us,        default: ''
    end

    add_index :users, :first_name, unique: false
    add_index :users, :last_name,  unique: false
  end

  def down
    change_table :users do |t|
      drop_table :users
    end
  end
end
