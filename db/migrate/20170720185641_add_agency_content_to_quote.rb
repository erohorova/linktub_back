class AddAgencyContentToQuote < ActiveRecord::Migration
  def change
    add_column :contents,  :agency_content,  :boolean, default: false
  end
end
