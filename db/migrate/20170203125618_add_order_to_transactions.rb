class AddOrderToTransactions < ActiveRecord::Migration
  def change
    add_reference :transactions, :order, index: true, foreign_key: true
  end
end
