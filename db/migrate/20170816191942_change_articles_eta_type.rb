class ChangeArticlesEtaType < ActiveRecord::Migration
  def change
    change_column :articles, :eta, :date
  end
end
