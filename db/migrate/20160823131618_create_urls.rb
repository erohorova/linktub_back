class CreateUrls < ActiveRecord::Migration
  def change
    create_table :urls do |t|
      t.string  :href
      t.text    :description
      t.float   :price,             default: 0

      t.boolean :paused,            default: false
      t.boolean :follow_link,       default: true
      t.string  :author_page

      t.integer :state,             index: true
      t.date    :approved_date
      t.date    :declined_date
      t.date    :reconsider_date
      t.date    :archived_date
      t.date    :activated_date
      t.date    :paused_date

      t.float   :domain_age,        default: 0
      t.integer :domain_authority,  default: 0
      t.integer :page_authority,    default: 0

      t.integer :page_rank,         default: 0
      t.integer :alexa_rank,        default: 0
      t.integer :back_links,        default: 0
      t.integer :trust_flow,        default: 0
      t.integer :citation_flow,     default: 0

      t.text    :profile_categories_names
      t.text    :profile_categories_names

      t.timestamps null: false
    end

    add_index :urls, :domain_authority
    add_index :urls, :trust_flow
    add_index :urls, :citation_flow
    add_index :urls, :alexa_rank
    add_index :urls, :price
  end
end
