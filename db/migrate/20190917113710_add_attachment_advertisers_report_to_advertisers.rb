class AddAttachmentAdvertisersReportToAdvertisers < ActiveRecord::Migration
  def change
    add_attachment :advertisers, :advertisers_report
  end
end
