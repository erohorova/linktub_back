class CreateWebContents < ActiveRecord::Migration
  def change
    create_table :web_contents do |t|
      t.string  :href
      t.float   :price
      t.integer :state, index: true

      t.references :profile, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
