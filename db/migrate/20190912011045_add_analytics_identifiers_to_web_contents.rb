class AddAnalyticsIdentifiersToWebContents < ActiveRecord::Migration
  def change
    add_column  :web_contents, :ga_account_id, :string
    add_column  :web_contents, :ga_profile_id, :string
    add_column  :web_contents, :website_url, :string
    add_column  :web_contents, :muv, :integer
  end
end
