class PriceToTrustFlowOnContents < ActiveRecord::Migration
  def change
    add_column :contents, :trust_flow_min, :integer
    add_column :contents, :trust_flow_max, :integer
    remove_column :contents, :price_min, :integer
    remove_column :contents, :price_max, :integer
  end
end
