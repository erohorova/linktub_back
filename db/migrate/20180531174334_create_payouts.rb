class CreatePayouts < ActiveRecord::Migration
  def change
    create_table :payouts do |t|
      t.references :ad,              index: true, foreign_key: true
      t.references :publisher,       index: true, foreign_key: true
      t.string     :amount
      t.string     :currency,        default: 'USD'
      t.string     :paypal_email
      t.string     :paypal_batch_id

      t.timestamps null: false
    end
  end
end
