class AddRecieverToEmailActions < ActiveRecord::Migration
  def change
    add_reference :email_actions, :receiver, polymorphic: true, index: true
  end
end
