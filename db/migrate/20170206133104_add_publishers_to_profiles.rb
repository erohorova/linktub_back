class AddPublishersToProfiles < ActiveRecord::Migration
  def change
    add_reference :profiles, :publisher, index: true, foreign_key: true
  end
end
