class CreateContentUrls < ActiveRecord::Migration
  def change
    create_table :content_urls do |t|
      t.references :content, index: true, foreign_key: true
      t.references :url, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
