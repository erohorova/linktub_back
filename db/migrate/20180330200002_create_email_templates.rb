class CreateEmailTemplates < ActiveRecord::Migration
  def change
    create_table :email_templates do |t|
      t.references :email, index: true, foreign_key: true
      t.text       :template,     default: ''
      t.integer    :days_delayed, default: 0
      t.boolean    :active,       default: true

      t.timestamps null: false
    end
  end
end
