class AddAttachmentAcceptedOrdersReportSpreadsheetToInvoices < ActiveRecord::Migration
  def change
    add_attachment :invoices, :invoices_report_spreadsheet
  end
end
