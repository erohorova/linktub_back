class AddSearchedCampaignIdToOrders < ActiveRecord::Migration
  def change
    add_column  :orders, :searched_campaign_id, :integer
  end
end
