class AddLastActionAppliedToActions < ActiveRecord::Migration
  def change
    add_column :articles, :last_action_applied, :string, default: nil
  end
end
