class AddAttachmentCampaignReportToCampaigns < ActiveRecord::Migration
  def self.up
    change_table :campaigns do |t|
      t.attachment :campaign_report_spreadsheet
    end
  end

  def self.down
    remove_attachment :campaigns, :campaign_report_spreadsheet
  end
end
