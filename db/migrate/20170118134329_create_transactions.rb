class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.string     :braintree_transaction_id
      t.references :payer, polymorphic: true, index: true
      t.references :receiver, polymorphic: true, index: true
      t.text       :description
      t.string     :amount
      t.integer    :state
      t.datetime   :voided_at
      t.datetime   :declined_at
      t.datetime   :rejected_at
      t.datetime   :authorized_at
      t.datetime   :submitted_for_settlement_at

      t.timestamps null: false
    end
  end
end
