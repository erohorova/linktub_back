class AddShownPriceForProducts < ActiveRecord::Migration
  def change
    add_column :urls,           :shown_price, :float, default: 0
    add_column :social_handles, :shown_price, :float, default: 0
    add_column :web_contents,   :shown_price, :float, default: 0
  end
end
