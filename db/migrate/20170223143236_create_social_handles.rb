class CreateSocialHandles < ActiveRecord::Migration
  def change
    create_table :social_handles do |t|
      t.string     :url
      t.float      :price
      t.string     :network, index: true
      t.string     :type, index: true
      t.integer    :state, index: true
      t.references :profile, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
