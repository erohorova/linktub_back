class AddDefaultValuesToUrls < ActiveRecord::Migration
  def change
    change_column :urls, :allow_home_page, :boolean, default: false
    change_column :urls, :allow_blog_post, :boolean, default: false
    change_column :urls, :follow_link,     :boolean, default: false
  end
end
