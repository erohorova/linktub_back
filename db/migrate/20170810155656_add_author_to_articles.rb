class AddAuthorToArticles < ActiveRecord::Migration
  def change
    add_reference :articles, :author, references: :admin_users, index: true
    add_foreign_key :articles, :admin_users, column: :author_id
  end
end
