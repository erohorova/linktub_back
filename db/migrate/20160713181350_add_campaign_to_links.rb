class AddCampaignToLinks < ActiveRecord::Migration
  def change
    add_reference :links, :campaign, index: true, foreign_key: true
  end
end
