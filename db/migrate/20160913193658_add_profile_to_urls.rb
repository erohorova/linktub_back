class AddProfileToUrls < ActiveRecord::Migration
  def change
    add_reference :urls, :profile, index: true, foreign_key: true
  end
end
