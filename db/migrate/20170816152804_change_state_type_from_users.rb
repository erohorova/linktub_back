class ChangeStateTypeFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :state, :integer
    add_column    :users, :state, :string
  end
end
