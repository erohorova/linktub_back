class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string   :subject
      t.text     :question_message
      t.text     :reply_message
      t.string   :author
      t.datetime :replied_at
      t.datetime :read_at
      t.integer  :state, index: true

      t.timestamps null: false
    end

    add_reference :messages, :user, index: true, foreign_key: true
  end
end
