class AddAttachmentInvoiceSpreadsheetToAuthorInvoices < ActiveRecord::Migration
  def self.up
    change_table :author_invoices do |t|
      t.attachment :invoice_spreadsheet
    end
  end

  def self.down
    remove_attachment :author_invoices, :invoice_spreadsheet
  end
end
