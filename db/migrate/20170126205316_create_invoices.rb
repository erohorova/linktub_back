class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.integer    :orders_count,          default: 0
      t.integer    :orders_accepted_count, default: 0
      t.float      :total_charged,         default: 0
      t.references :advertiser, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
