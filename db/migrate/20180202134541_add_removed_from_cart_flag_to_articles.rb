class AddRemovedFromCartFlagToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :removed_from_cart, :boolean, default: false
  end
end
