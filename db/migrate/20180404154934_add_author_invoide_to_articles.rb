class AddAuthorInvoideToArticles < ActiveRecord::Migration
  def change
    add_reference :articles, :author_invoice, index: true, foreign_key: true
  end
end
