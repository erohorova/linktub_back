class CreateLinks < ActiveRecord::Migration
  def change
    create_table :links do |t|
      t.string  :href

      t.date    :approved_date
      t.date    :suspended_date
      t.date    :flagged_date
      t.date    :archived_date
      t.date    :reconsidered_date
      t.date    :declined_date

      t.integer :page_rank, default: 0
      t.integer :alexa_rank, default: 0
      t.integer :back_links, default: 0
      t.float   :domain_age, default: 0

      t.integer :state, index: true

      t.timestamps null: false
    end
  end
end
