class CreatePublishers < ActiveRecord::Migration
  def change
    create_table :publishers do |t|
      t.references :user, index: true, foreign_key: true
      t.string     :avatar

      t.timestamps null: false
    end
  end
end
