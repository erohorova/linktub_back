class ChangeNotificationTypeToInteger < ActiveRecord::Migration
  def change
    remove_column :notifications, :notification_type, :string
    add_column    :notifications, :notification_type, :integer
    add_index     :notifications, :notification_type
  end
end
