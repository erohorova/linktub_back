class AddColumnsToContent < ActiveRecord::Migration
  def change
    add_column :contents, :ahrefs_dr_min, :integer
    add_column :contents, :ahrefs_dr_max, :integer
  end
end
