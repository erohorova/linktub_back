class CreateAuthorUser < ActiveRecord::Migration
  def change
    add_column :admin_users, :type, :string
    add_column :admin_users, :first_name, :string
    add_column :admin_users, :last_name, :string
    add_column :admin_users, :phone, :string

    add_index :admin_users, :type
    add_index :admin_users, :first_name
    add_index :admin_users, :last_name
  end
end
