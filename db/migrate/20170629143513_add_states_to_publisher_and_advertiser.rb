class AddStatesToPublisherAndAdvertiser < ActiveRecord::Migration
  def change
    add_column :publishers,  :activated_date,  :datetime
    add_column :publishers,  :terminated_date, :datetime
    add_column :publishers,  :state,           :integer
    add_column :advertisers, :activated_date,  :datetime
    add_column :advertisers, :terminated_date, :datetime
    add_column :advertisers, :state,           :integer

    add_index  :publishers, :state
    add_index  :advertisers, :state
  end
end
