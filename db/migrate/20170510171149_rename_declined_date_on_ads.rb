class RenameDeclinedDateOnAds < ActiveRecord::Migration
  def change
    rename_column :ads, :declined_date, :rejected_date
  end
end
