class CreateConfigs < ActiveRecord::Migration
  def change
    create_table :configs do |t|
      t.string :category, limit: 191
      t.string :name, limit: 191
      t.string :data_type
      t.text :value
      t.string :configable_type, limit: 191
      t.integer :configable_id
      t.timestamps null: false
    end
    add_index :configs, [:configable_type, :configable_id, :category, :name], name: :config_index, unique: true
  end
end
