class AddRejectedCounterToAds < ActiveRecord::Migration
  def change
    add_column :ads, :rejected_counter, :integer, default: 0
  end
end
