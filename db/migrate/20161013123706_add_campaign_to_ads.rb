class AddCampaignToAds < ActiveRecord::Migration
  def change
    add_reference :ads, :campaign, index: true, foreign_key: true
  end
end
