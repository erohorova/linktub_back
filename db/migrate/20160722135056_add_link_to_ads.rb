class AddLinkToAds < ActiveRecord::Migration
  def change
    add_reference :ads, :link, index: true, foreign_key: true
  end
end
