class AddStateToAuthors < ActiveRecord::Migration
  def change
    add_column :admin_users, :state,           :integer
    add_column :admin_users, :terminated_date, :date
    add_column :admin_users, :activated_date,  :date
  end
end
