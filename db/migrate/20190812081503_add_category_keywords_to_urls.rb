class AddCategoryKeywordsToUrls < ActiveRecord::Migration
  def change
    add_column :urls, :profile_categories_keyword, :string
  end
end