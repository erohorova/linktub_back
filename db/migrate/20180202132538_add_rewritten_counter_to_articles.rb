class AddRewrittenCounterToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :rewritten_counter, :integer, default: 0
  end
end
