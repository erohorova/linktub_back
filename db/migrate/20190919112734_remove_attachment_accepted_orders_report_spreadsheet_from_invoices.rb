class RemoveAttachmentAcceptedOrdersReportSpreadsheetFromInvoices < ActiveRecord::Migration
  def change
    remove_attachment :invoices, :invoices_report_spreadsheet
  end
end
