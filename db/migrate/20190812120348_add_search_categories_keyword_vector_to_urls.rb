class AddSearchCategoriesKeywordVectorToUrls < ActiveRecord::Migration
  def up
    add_column :urls, :search_categories_keyword_vector, 'tsvector'

    execute <<-SQL
      CREATE INDEX urls_search_categories_keyword_idx
      ON urls
      USING gin(search_categories_keyword_vector);
    SQL

    execute <<-SQL
      DROP TRIGGER IF EXISTS urls_search_categories_keyword_vector_update
      ON urls;

      CREATE TRIGGER urls_search_categories_keyword_vector_update
      BEFORE INSERT OR UPDATE
      ON urls
      FOR EACH ROW EXECUTE PROCEDURE
      tsvector_update_trigger (search_categories_keyword_vector, 'pg_catalog.english', profile_categories_keyword);
    SQL
  end

  def down
    remove_column :urls, :search_categories_keyword_vector
    execute <<-SQL
      DROP TRIGGER IF EXISTS urls_search_categories_keyword_vector_update on urls;
    SQL
  end
end
