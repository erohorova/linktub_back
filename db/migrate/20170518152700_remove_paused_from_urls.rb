class RemovePausedFromUrls < ActiveRecord::Migration
  def change
    remove_column :urls, :paused, :boolean
  end
end
