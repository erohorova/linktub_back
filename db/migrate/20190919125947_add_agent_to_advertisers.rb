class AddAgentToAdvertisers < ActiveRecord::Migration
  def change
    add_reference :advertisers, :agent, references: :admin_users, index: true
    add_foreign_key :advertisers, :admin_users, column: :agent_id
  end
end
