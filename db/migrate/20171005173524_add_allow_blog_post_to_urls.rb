class AddAllowBlogPostToUrls < ActiveRecord::Migration
  def change
    add_column :urls, :allow_blog_post, :boolean
  end
end
