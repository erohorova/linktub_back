class AddInvoiceToOrders < ActiveRecord::Migration
  def change
    add_reference :orders, :invoice, index: true, foreign_key: true
  end
end
