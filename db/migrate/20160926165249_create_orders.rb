class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.references :advertiser, index: true, foreign_key: true
      t.references :url, index: true, foreign_key: true
      t.references :ad, index: true, foreign_key: true
      t.datetime   :completed_date
      t.integer    :state, index: true

      t.timestamps null: false
    end
  end
end
