class AddDeletedAtToContents < ActiveRecord::Migration
  def change
    add_column :contents, :deleted_at, :datetime
    add_index :contents, :deleted_at
  end
end
