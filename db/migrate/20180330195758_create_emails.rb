class CreateEmails < ActiveRecord::Migration
  def change
    create_table :emails do |t|
      t.string  :name
      t.string  :receiver_type
      t.boolean :response_required, default: false
      t.string  :redirect_url
      t.boolean :active,            default: true

      t.timestamps null: false
    end
  end
end
