class CreateEmailActions < ActiveRecord::Migration
  def change
    create_table :email_actions do |t|
      t.references :email_trigger, index: true, foreign_key: true
      t.references :email_template, index: true, foreign_key: true
      t.datetime :send_at, null: false

      t.timestamps null: false
    end
  end
end
