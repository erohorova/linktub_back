class RemoveAttachmentAdvertisersReportFromAdvertisers < ActiveRecord::Migration
  def change
    remove_attachment :advertisers, :advertisers_report
  end
end
