class AddAutoApprovedToAdvertisers < ActiveRecord::Migration
  def change
    add_column :advertisers, :auto_approved, :boolean, default: false
  end
end
