class DeleteOrderIdFromTransaction < ActiveRecord::Migration
  def change
    remove_column :transactions, :order_id, :integer
    add_column    :orders, :transaction_id, :integer
  end
end
