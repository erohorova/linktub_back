class AddSeenToAd < ActiveRecord::Migration
  def change
    add_column :ads, :seen, :boolean, default: false
  end
end
