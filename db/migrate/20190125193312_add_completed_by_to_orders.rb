class AddCompletedByToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :completed_by, :string, default: nil
  end
end
