class AddShownPriceToContentUrl < ActiveRecord::Migration
  def change
    add_column :content_urls, :shown_price, :float, default: 0
  end
end
