class AddPaypalEmailToAuthors < ActiveRecord::Migration
  def change
    add_column :admin_users, :paypal_email, :string
  end
end
