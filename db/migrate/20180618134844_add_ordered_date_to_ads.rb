class AddOrderedDateToAds < ActiveRecord::Migration
  def change
    add_column :ads, :ordered_date, :date
  end
end
