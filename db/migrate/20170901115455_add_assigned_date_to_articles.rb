class AddAssignedDateToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :assigned_date, :date
  end
end
