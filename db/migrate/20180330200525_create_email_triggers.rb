class CreateEmailTriggers < ActiveRecord::Migration
  def change
    create_table :email_triggers do |t|
      t.references :email, index: true, foreign_key: true
      t.references :receiver, polymorphic: true, index: true
      t.text       :vars
      t.boolean    :response_completed, default: false

      t.timestamps null: false
    end
  end
end
