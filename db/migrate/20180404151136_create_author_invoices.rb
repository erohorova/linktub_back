class CreateAuthorInvoices < ActiveRecord::Migration
  def change
    create_table :author_invoices do |t|
      t.integer    :approved_articles_count, defualt: 0
      t.references :admin_user, index: true, foreign_key: true
      t.boolean    :paid, default: false

      t.timestamps null: false
    end
  end
end
