class AddCreatedByToContent < ActiveRecord::Migration
  def change
    add_column :contents, :created_by, :string
  end
end
