class AddAllowHomePageToUrls < ActiveRecord::Migration
  def change
    add_column :urls, :allow_home_page, :boolean
  end
end
