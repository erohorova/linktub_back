class AddSentToEmailActions < ActiveRecord::Migration
  def change
    add_column :email_actions, :sent, :boolean, default: false
  end
end
