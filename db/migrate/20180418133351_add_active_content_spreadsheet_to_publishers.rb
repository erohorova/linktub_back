class AddActiveContentSpreadsheetToPublishers < ActiveRecord::Migration
  def change
    add_attachment :publishers, :active_content_spreadsheet
  end
end
