class RenameColumnAhrefsLdrToUrl < ActiveRecord::Migration
  def change
    rename_column :urls, :ahrefs_ldr, :ahrefs_lrd
  end
end
