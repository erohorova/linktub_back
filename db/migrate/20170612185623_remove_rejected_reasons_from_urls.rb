class RemoveRejectedReasonsFromUrls < ActiveRecord::Migration
  def change
    remove_column :urls, :rejected_reason, :text
  end
end
