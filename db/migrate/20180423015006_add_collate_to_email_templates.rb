class AddCollateToEmailTemplates < ActiveRecord::Migration
  def change
    add_column :email_templates, :collate, :boolean, default: false
    add_index :email_templates, :collate
  end
end
