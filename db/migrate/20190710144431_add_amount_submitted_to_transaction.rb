class AddAmountSubmittedToTransaction < ActiveRecord::Migration
  def change
    add_column :transactions, :amount_submitted, :string
  end
end
