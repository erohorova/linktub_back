class AddCollateToEmailActions < ActiveRecord::Migration
  def change
    add_column :email_actions, :collate, :boolean, default: false
    add_index :email_actions, :collate
  end
end
