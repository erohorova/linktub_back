class AddIsRefundToTransaction < ActiveRecord::Migration
  def change
    add_column :transactions,  :is_refund,  :boolean, default: false
  end
end
