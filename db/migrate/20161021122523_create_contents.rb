class CreateContents < ActiveRecord::Migration
  def change
    create_table :contents do |t|
      t.references :advertiser, index: true, foreign_key: true
      t.references :campaign, index: true, foreign_key: true
      t.integer    :state, null: false, index: true
      t.float      :price_min
      t.float      :price_max
      t.integer    :domain_authority_min
      t.integer    :domain_authority_max

      t.timestamps null: false
    end
  end
end
