class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.references :url,        index: true, foreign_key: true
      t.references :link,       index: true, foreign_key: true
      t.references :ad,         index: true, foreign_key: true
      t.references :advertiser, index: true, foreign_key: true

      t.integer :state, index: true
      t.datetime :pending_date
      t.datetime :approved_date
      t.datetime :rejected_date
      t.datetime :cancelled_date

      t.datetime :eta

      t.timestamps null: false
    end
  end
end
