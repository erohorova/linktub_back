class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :name
      t.string :image
      t.string :profile_type, index: true

      t.float   :total_earned,   default: 0
      t.integer :total_orders, default: 0
      t.integer :total_sites,  default: 0
      t.text    :products

      t.timestamps null: false
    end
  end
end
