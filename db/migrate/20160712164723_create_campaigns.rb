class CreateCampaigns < ActiveRecord::Migration
  def change
    create_table :campaigns do |t|
      t.string  :name
      t.string  :image
      t.integer :state, index: true

      t.timestamps null: false
    end
  end
end
