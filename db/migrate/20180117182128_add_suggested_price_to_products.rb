class AddSuggestedPriceToProducts < ActiveRecord::Migration
  def change
    add_column :urls,           :suggested_price, :float, default: 0
    add_column :social_handles, :suggested_price, :float, default: 0
    add_column :web_contents,   :suggested_price, :float, default: 0
  end
end
