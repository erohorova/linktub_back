class AddIndexToLinksUpdateDate < ActiveRecord::Migration
  def change
    add_index :links, :updated_at
  end
end
