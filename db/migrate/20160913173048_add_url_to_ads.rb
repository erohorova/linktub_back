class AddUrlToAds < ActiveRecord::Migration
  def change
    add_reference :ads, :url, index: true, foreign_key: true
  end
end
