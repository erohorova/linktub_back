class AddAttachmentNewAcceptedOrdersReportSpreadsheetToInvoices < ActiveRecord::Migration
  def change
    add_attachment :invoices, :accepted_orders_report_spreadsheet
  end
end
