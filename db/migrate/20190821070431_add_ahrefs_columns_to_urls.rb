class AddAhrefsColumnsToUrls < ActiveRecord::Migration
  def change
    add_column  :urls,:ahrefs_dr, :integer
    add_column  :urls,:ahrefs_ldr, :integer
    add_column  :urls,:semrush_traffic, :integer
  end
end
