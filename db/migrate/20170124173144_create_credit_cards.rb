class CreateCreditCards < ActiveRecord::Migration
  def change
    create_table :credit_cards do |t|
      t.string     :masked_number
      t.string     :braintree_token, index: true
      t.string     :cardholder_name
      t.string     :card_type
      t.integer    :expiration_month
      t.integer    :expiration_year
      t.string     :image_url

      t.string     :street_address
      t.string     :extended_address
      t.string     :city
      t.string     :state
      t.string     :zip_code
      t.string     :country_name

      t.references :user, index: true, foreign_key: true
      
      t.timestamps null: false
    end
  end
end
