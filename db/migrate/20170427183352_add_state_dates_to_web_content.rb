class AddStateDatesToWebContent < ActiveRecord::Migration
  def change
    add_column :web_contents, :approved_date,   :date
    add_column :web_contents, :declined_date,   :date
    add_column :web_contents, :reconsider_date, :date
    add_column :web_contents, :archived_date,   :date
    add_column :web_contents, :activated_date,  :date
    add_column :web_contents, :paused_date,     :date
  end
end
