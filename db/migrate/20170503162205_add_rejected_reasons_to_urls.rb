class AddRejectedReasonsToUrls < ActiveRecord::Migration
  def change
    add_column :urls, :rejected_reason, :string
  end
end
