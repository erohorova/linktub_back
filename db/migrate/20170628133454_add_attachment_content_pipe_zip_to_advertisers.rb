class AddAttachmentContentPipeZipToAdvertisers < ActiveRecord::Migration
  def self.up
    change_table :advertisers do |t|
      t.attachment :content_pipe_zip
    end
  end

  def self.down
    remove_attachment :advertisers, :content_pipe_zip
  end
end
