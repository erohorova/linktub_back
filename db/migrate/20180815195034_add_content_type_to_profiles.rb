class AddContentTypeToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :content_type, :string, null: false, default: 'collaborate'
  end
end
