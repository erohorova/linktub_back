class AddSlugAndNotesToEmails < ActiveRecord::Migration
  def change
    add_column :emails, :slug, :string
    add_column :emails, :notes, :text
  end
end
